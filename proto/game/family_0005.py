from proto.model import *

class Packet_0005_01(object):
	__slots__ = ['need_dict','map_id']
	def __init__(self, param1 = False, param2 = None):
		self.need_dict = param1
		self.map_id = param2

	def write(self, param1):
		param1.family = 0x05
		param1.subfamily = 0x01
		# self.need_dict used to be boolean, now it is a string for local storage hash,
		# but we don't want to emulate browser localstorage, so we just pretend it is disabled and send ''
		# also keeping param1 boolean compatibility
		if self.need_dict == True:
			param1.writeByte(1)
			param1.writeUTF('')
		elif type(self.need_dict) == type(''):
			param1.writeByte(1)
			param1.writeUTF(self.need_dict)
		else:
			param1.writeByte(0)
		if self.map_id != None:
			param1.writeByte(1)
			param1.writeByte(self.map_id)
		else:
			param1.writeByte(0)

class Packet_0005_02(object):
	__slots__ = ['dict','pos','me','farm','faction']
	def __init__(self, param1):
		if param1.readUnsignedByte() == 1:
			self.dict = PDict()
			self.dict.read(param1)
		else:
			self.dict = None
		if param1.readUnsignedByte() == 1:
			self.pos = Position()
			self.pos.read(param1)
		else:
			self.pos = None
		self.me = PUser()
		self.me.read(param1)
		self.farm = PFarm()
		self.farm.read(param1)
		if param1.readUnsignedByte() == 1:
			self.faction = PFriendsHelp()
			self.faction.read(param1)
		else:
			self.faction = None

	def __str__(self):
		return "{dict,pos,me,farm,faction}"

class PDict(object):
	__slots__ = ['static_dict','sale_events','shop','active_quests','closed_quests','contracts','laboratory','bad_objs_in_warehouse','news','last_readed_news','viral_disabled','is_birthday','current_special_offers','double_gold_events','daily_deal','current_exchange_event_rares','finished_exchange_event_rares','contract_rating_prizes','contract_rare_prize']
	def __init__(self, param1 = None, param2 = None, param3 = None, param4 = None, param5 = None, param6 = None, param7 = False, param8 = None, param9 = None, param10 = '', param11 = False, param12 = False, param13 = None, param14 = None, param15 = None, param16 = None, param17 = None, param18 = None, param19 = None):
		self.static_dict = param1
		self.sale_events = param2
		self.shop = param3
		self.active_quests = param4
		self.closed_quests = param5
		self.contracts = param6
		self.laboratory = param7
		self.bad_objs_in_warehouse = param8
		self.news = param9
		self.last_readed_news = param10
		self.viral_disabled = param11
		self.is_birthday = param12
		self.current_special_offers = param13
		self.double_gold_events = param14
		self.daily_deal = param15
		self.current_exchange_event_rares = param16
		self.finished_exchange_event_rares = param17
		self.contract_rating_prizes = param18
		self.contract_rare_prize = param19

	def read(self, param1):
		if param1.readUnsignedByte() == 1:
			#pylint: disable=unused-variable
			dict_len = param1.readUnsignedInt()
			self.static_dict = PStaticDict()
			self.static_dict.read(param1)
		else:
			self.static_dict = None
		self.sale_events = {}
		for i in xrange(param1.readUnsignedShort()):
			self.sale_events[i] = PSaleEvent()
			self.sale_events[i].read(param1)
		self.shop = {}
		for i in xrange(param1.readUnsignedShort()):
			self.shop[i] = PShopObj()
			self.shop[i].read(param1)
		self.active_quests = {}
		for i in xrange(param1.readUnsignedShort()):
			self.active_quests[i] = PQuest()
			self.active_quests[i].read(param1)
		self.closed_quests = {}
		for i in xrange(param1.readUnsignedShort()):
			self.closed_quests[i] = param1.readUTF()
		self.contracts = {}
		for i in xrange(param1.readUnsignedShort()):
			self.contracts[i] = PContract()
			self.contracts[i].read(param1)
		self.laboratory = {}
		for i in xrange(param1.readUnsignedShort()):
			self.laboratory[i] = PCraft()
			self.laboratory[i].read(param1)
		self.bad_objs_in_warehouse = param1.readBoolean()
		self.news = {}
		for i in xrange(param1.readUnsignedShort()):
			self.news[i] = PNewsInfo()
			self.news[i].read(param1)
		self.last_readed_news = param1.readUTF()
		self.viral_disabled = param1.readBoolean()
		self.is_birthday = param1.readBoolean()
		self.current_special_offers = {}
		for i in xrange(param1.readUnsignedShort()):
			self.current_special_offers[i] = PSpecialOffer()
			self.current_special_offers[i].read(param1)
		self.double_gold_events = {}
		for i in xrange(param1.readUnsignedShort()):
			self.double_gold_events[i] = PDoubleGoldEvent()
			self.double_gold_events[i].read(param1)

		if param1.readUnsignedByte() == 1:
			self.daily_deal = PWhReserveWTime()
			self.daily_deal.read(param1)
		else:
			self.daily_deal = None
		if param1.readUnsignedByte() == 1:
			self.current_exchange_event_rares = PExchangeEventRares()
			self.current_exchange_event_rares.read(param1)
		else:
			self.current_exchange_event_rares = None
		self.finished_exchange_event_rares = {}
		for i in xrange(param1.readUnsignedShort()):
			self.finished_exchange_event_rares[i] = PExchangeEventRares()
			self.finished_exchange_event_rares[i].read(param1)
		self.contract_rating_prizes = {}
		for i in xrange(param1.readUnsignedShort()):
			self.contract_rating_prizes[i] = PContractRatingPrizeInfo()
			self.contract_rating_prizes[i].read(param1)
		if param1.readUnsignedByte() == 1:
			self.contract_rare_prize = PWarehouseReserve()
			self.contract_rare_prize.read(param1)
		else:
			self.contract_rare_prize = None

	def write(self, param1):
		#pylint: disable=unused-argument
		raise Exception("ERROR: WHYYY do we ever want to write PDict???")
