from proto.model import *

class Packet_0040_01(object):
	def __init__(self, param1 = "", param2 = ""):
		self.building_kind = param1
		self.formula_kind = param2

	def write(self, param1):
		param1.family = 0x40
		param1.subfamily = 0x01
		param1.writeUTF(self.building_kind)
		param1.writeUTF(self.formula_kind)

class Packet_0040_02(object):
	def __init__(self, param1):
		self.value = PFarmAction()
		self.value.read(param1)

class Packet_0040_03(object):
	def __init__(self, param1 = "", param2 = "", param3 = False):
		self.building_kind = param1
		self.formula_kind = param2
		self.buy_finish = param3

	def write(self, param1):
		param1.family = 0x40
		param1.subfamily = 0x03
		param1.writeUTF(self.building_kind)
		param1.writeUTF(self.formula_kind)
		param1.writeBoolean(self.buy_finish)

class Packet_0040_04(object):
	COST = 2
	ACTION = 1
	RESERVE = 0

	def __init__(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.RESERVE:
			self.value = PWarehouseReserve()
			self.value.read(param1)
		elif self.variance == self.ACTION:
			self.value = PFarmAction()
			self.value.read(param1)
		elif self.variance == self.COST:
			self.value = PCost()
			self.value.read(param1)
		else:
			raise Exception ("ERROR: incorrect Packet_0040_04 data")

class Packet_0040_05(object):
	def __init__(self, param1 = None, param2 = None):
		self.study = param1
		self.buy_start = param2

	def write(self, param1):
		param1.family = 0x40
		param1.subfamily = 0x05
		self.study.write(param1)
		if self.buy_start != None:
			param1.writeByte(1)
			self.buy_start.write(param1)
		else:
			param1.writeByte(0)

class PCostType(object):
	TICKETS = 1
	GOLD = 0
	def write(self, param1):
		param1.writeByte(self.variance)

	def __init__(self, param1 = 0):
		self.variance = param1

	def read(self, param1):
		self.variance = param1.readUnsignedByte()

class Packet_0040_06(object):
	def __init__(self):
		return

	def write(self, param1):
		param1.family = 0x40
		param1.subfamily = 0x06

class Packet_0040_07(object):
	def __init__(self, param1 = "", param2 = ""):
		self.building_kind = param1
		self.formula_kind = param2

	def write(self, param1):
		param1.family = 0x40
		param1.subfamily = 0x07
		param1.writeUTF(self.building_kind)
		param1.writeUTF(self.formula_kind)
