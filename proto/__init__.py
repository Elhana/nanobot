#!/usr/bin/python
# -*- coding: UTF-8 -*-
import requests
import random
import md5pymod,sha1py
md5py = md5pymod
import proto.model
import proto.game
from pyamf.amf3 import ByteArray
from ast import literal_eval # should be safe for parsing info_object
#import json #use json.loads instead of literal_eval, but need to deal with encoding issues.

comboState = proto.game.family_0020.PGoodyType.COMBO_NEW
comboCount = 0  ## max 375

# Submodules
__all__ = ["game", "model", "tuples"]

class BinaryBuffer(ByteArray):
	def __init__(self, param1 = 0, param2 = 0):
		ByteArray.__init__(self)
		self.family = param1
		self.subfamily = param2
		self.endian = '<' #LITTLE_ENDIAN
		return

sid = ''
PROTO_VERSION = 0xF5
get_friends_url = "http://bl-farm.redspell.ru/get_friends/vk/f.html?sid="
get_raids_url = "http://bl-farm.redspell.ru/get_raids.html?sid="
proto_url = "http://bl-farm.redspell.ru/proto.html?sid="
proto_raid_url = "http://bl-farm.redspell.ru/proto_raid.html?sid="
# Use some windows useragent for public release: 'Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0',\
headers	= { 'User-Agent':'Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0',\
			'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',\
			'Accept-Language': 'ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3',\
			'Accept-Encoding': 'gzip, deflate',\
			'Referer':'https://st-farm.redspell.ru/Farm_d_9_79_2_d56075048d7a014b83beee6a866cf5ae.swf?vmod=1479984171'}
			#'Referer':'http://st-farm.redspell.ru/Farm.swf?vmod=timestamp ? 1386928396 = 2013-12-13T20:13:19+00:00


session = requests.Session()

swf = ""
s1 = None # sessionhash
s2 = None # swfhash
hash_type = None #currently md5 or sha1 randomly
hash_init_const = None #they are changing them now

def SignPacket_md5(packet):
	if s1 == None or s2 == None:
		raise Exception ("ERROR: SignPacket_md5")
	packet_md5=md5py.new(packet,hash_init_const[0],hash_init_const[1],hash_init_const[2],hash_init_const[3])
	packet_md5.update(s1)
	packet_md5.update(s2)
	return packet+packet_md5.hexdigest()

def SignPacket_sha1(packet):
	if s1 == None or s2 == None:
		raise Exception ("ERROR: SignPacket_sha1")
	packet_sha1=sha1py.Sha1Hash(hash_init_const[0],hash_init_const[1],hash_init_const[2],hash_init_const[3],hash_init_const[4]).update(packet)
	_ = packet_sha1.update(s1)
	_ = packet_sha1.update(s2)
	return packet+packet_sha1.hexdigest()

def SignPacket(packet):
	if hash_type == 'md5':
		return SignPacket_md5(packet)
	elif hash_type == 'sha1':
		return SignPacket_sha1(packet)
	else:
		raise Exception ("ERROR: Hash type "+hash_type)

# BinaryBuffer object to ByteArray
def getBinary(param1):
	ba = ByteArray()
	ba.endian = '<'
	ba.writeUnsignedShort(param1.family)
	ba.writeUnsignedByte(param1.subfamily)
	ba.writeUnsignedByte(PROTO_VERSION)
	ba.writeUnsignedInt(len(param1))
	ba.write(param1.getvalue())
	return ba.getvalue()

def parseResponse(respc):
	if len(respc) < 8:
		raise Exception ("ERROR: Response too short")
	else:
		stream = ByteArray()
		stream.endian = '<'
		stream.write(respc[0:4])
		stream.seek(0)
		respc_len = len(respc[8:])
		bbuffer = BinaryBuffer(stream.readUnsignedShort(), stream.readUnsignedByte())
		#pylint: disable=unused-variable
		stream_proto = stream.readUnsignedByte()
		if stream_proto > proto.PROTO_VERSION:
			print "OLD_CLIENT: Get a new version!"
			print "OLD_CLIENT: We implement PROTO_VERSION "+str(proto.PROTO_VERSION)+", but server replied with packet version "+str(stream_proto)+"."
			print "WARNING: We will attempt to match server version on next update and hope it works, but horrible things might happen."
			proto.PROTO_VERSION = stream_proto
			raise Exception ('OLD_CLIENT')
		elif stream_proto < proto.PROTO_VERSION:
			print "We implement PROTO_VERSION "+str(proto.PROTO_VERSION)+", but server replied with packet version "+str(stream_proto)+", this is very strange."
		bbuffer.write(respc[4:])
		bbuffer.seek(0)
		packetLength = bbuffer.readUnsignedInt()

		# It seems if packetLength > respc_len, then there is a second packet at the end, usually 0020_03
		if packetLength < respc_len:
			#print "We have 2nd packet:" + DumpHex(respc[packetLength+8:])
			parseResponse(respc[packetLength+8:])

		if packetLength <= respc_len:
			if bbuffer.family == 0x00 and bbuffer.subfamily == 0x00:
				proto.game.family_0000.Packet_0000_00(bbuffer)
				return None

			elif bbuffer.family == 0x10 and bbuffer.subfamily == 0x02:
				return proto.game.family_0010.Packet_0010_02(bbuffer)

			elif bbuffer.family == 0x05 and bbuffer.subfamily == 0x02:
				return proto.game.family_0005.Packet_0005_02(bbuffer)

			elif bbuffer.family == 0x20 and bbuffer.subfamily == 0x02:
				return proto.game.family_0020.Packet_0020_02(bbuffer)

			elif bbuffer.family == 0x20 and bbuffer.subfamily == 0x03:
				event = proto.game.family_0020.Packet_0020_03(bbuffer)
				return EventHandler(event.value)

			elif bbuffer.family == 0x20 and bbuffer.subfamily == 0x05:
				proto.game.family_0020.Packet_0020_05(bbuffer)
				return None # we don't need it

			elif bbuffer.family == 0x30 and bbuffer.subfamily == 0x02:
				return proto.game.family_0030.Packet_0030_02(bbuffer)

			elif bbuffer.family == 0x40 and bbuffer.subfamily == 0x02:
				return proto.game.family_0040.Packet_0040_02(bbuffer)

			elif bbuffer.family == 0x40 and bbuffer.subfamily == 0x04:
				return proto.game.family_0040.Packet_0040_04(bbuffer)

			elif bbuffer.family == 0x50 and bbuffer.subfamily == 0x02:
				return proto.game.family_0050.Packet_0050_02(bbuffer)

			elif bbuffer.family == 0x50 and bbuffer.subfamily == 0x04:
				return proto.game.family_0050.Packet_0050_04(bbuffer)

			elif bbuffer.family == 0x50 and bbuffer.subfamily == 0x06:
				return proto.game.family_0050.Packet_0050_06(bbuffer)

			elif bbuffer.family == 0x70 and bbuffer.subfamily == 0x02:
				return proto.game.family_0070.Packet_0070_02(bbuffer)

			elif bbuffer.family == 0x70 and bbuffer.subfamily == 0x04:
				return proto.game.family_0070.Packet_0070_04(bbuffer)

			elif bbuffer.family == 0x70 and bbuffer.subfamily == 0x06:
				return # Not handled in original either?

			elif bbuffer.family == 0x70 and bbuffer.subfamily == 0x08:
				return proto.game.family_0070.Packet_0070_08(bbuffer)

			elif bbuffer.family == 0x90 and bbuffer.subfamily == 0x02:
				return proto.game.family_0090.Packet_0090_02(bbuffer)

			elif bbuffer.family == 0x95 and bbuffer.subfamily == 0x08:
				return proto.game.family_0095.Packet_0095_08(bbuffer)

			elif bbuffer.family == 0xA2 and bbuffer.subfamily == 0x10:
				return proto.game.family_00A2.Packet_00A2_10(bbuffer)

			elif bbuffer.family == 0xA2 and bbuffer.subfamily == 0x11:
				return proto.game.family_00A2.Packet_00A2_11(bbuffer)

			else:
				print "FIXME: Packet(%s_%s) not implemented " % (str(hex(bbuffer.family)), str(hex(bbuffer.subfamily)))
				return None

		else:
			raise Exception ("ERROR: Transfer error: packetLength("+str(packetLength)+") < response len("+str(respc_len)+"):")
			#f = open('transfer_error.txt','w')
			#f.write(DumpHex(respc))
			#print DumpHex(respc[0:50])
			#f.close()
			#return None
	return

def DumpHex(string):
	return ":".join("{:02x}".format(ord(c)) for c in string)

#Original Actions
def GetFriendList():
	r = session.get(get_friends_url + sid + "&gzip=off" , headers=headers)

	if r.status_code == 200:
		return parseResponse(r.content)
	else:
		raise Exception ("ERROR: Server response code: "+ str(r.status_code))

def GetRaids(self_or_fr, some_string = ""):
	packet_obj = proto.game.family_0095.Packet_0095_05(self_or_fr,some_string)
	packetBuffer = BinaryBuffer()
	packet_obj.write(packetBuffer)
	packet = getBinary(packetBuffer)
	packet = SignPacket(packet)
	r = session.post(get_raids_url + sid + "&request_id="+str(random.random())+"&proto=95x5&network=vk&gzip=off", data=packet, headers=headers)

	if r.status_code == 200:
		return parseResponse(r.content)
	else:
		raise Exception ("ERROR: Server response code: "+ str(r.status_code))

def GetMyFarm(need_dict = True, map_id = None):
	packet_obj = proto.game.family_0005.Packet_0005_01(need_dict,map_id)
	packetBuffer = BinaryBuffer()
	packet_obj.write(packetBuffer)
	packet = getBinary(packetBuffer)
	packet = SignPacket(packet)
	if len(packet) == 0:
		raise Exception ("ERROR: packet len 0")
	r = session.post(proto_url + sid + "&request_id="+str(random.random())+"&proto=5x1&network=vk&gzip=off", data=packet, headers=headers)

	if r.status_code == 200:
		return parseResponse(r.content)
	else:
		raise Exception ("ERROR: Server response code: "+ str(r.status_code))

def GetSpaceFarm(need_dict = True):
	packet_obj = proto.game.family_00A2.Packet_00A2_0A(need_dict)
	packetBuffer = BinaryBuffer()
	packet_obj.write(packetBuffer)
	packet = getBinary(packetBuffer)
	packet = SignPacket(packet)
	r = session.post(proto_url + sid + "&request_id="+str(random.random())+"&proto=a2xa&network=vk&gzip=off", data=packet, headers=headers)

	if r.status_code == 200:
		return parseResponse(r.content)
	else:
		raise Exception ("ERROR: Server response code: "+ str(r.status_code))

def getFriendFarm(friend,map_id = None):
	packet_obj = proto.game.family_0050.Packet_0050_01(friend.user_id, map_id)
	packetBuffer = BinaryBuffer()
	packet_obj.write(packetBuffer)
	packet = getBinary(packetBuffer)
	packet = SignPacket(packet)
	r = session.post(proto_url + sid + "&request_id="+str(random.random())+"&proto=50x1&network=vk&gzip=off", data=packet, headers=headers)

	if r.status_code == 200:
		return parseResponse(r.content)
	else:
		raise Exception ("ERROR: Server response code: "+ str(r.status_code))

def runFriendTaskAction(friend, map_id, action_variance, obj_id):
	packet_obj = proto.game.family_0050.Packet_0050_03(friend.user_id, map_id, action_variance, obj_id)
	packetBuffer = BinaryBuffer()
	packet_obj.write(packetBuffer)
	packet = getBinary(packetBuffer)
	packet = SignPacket(packet)
	r = session.post(proto_url + sid + "&request_id="+str(random.random())+"&proto=50x3&network=vk&gzip=off", data=packet, headers=headers)

	if r.status_code == 200:
		return parseResponse(r.content)
	else:
		raise Exception ("ERROR: Server response code: "+ str(r.status_code))

def getRaidLog(object_ids, start_from_variance, start_from_value, blong = True):
	packet_obj = proto.game.family_0095.Packet_0095_07(object_ids, start_from_variance, start_from_value, blong)
	packetBuffer = BinaryBuffer()
	packet_obj.write(packetBuffer)
	packet = getBinary(packetBuffer)
	packet = SignPacket(packet)
	r = session.post(proto_raid_url + sid + "&request_id="+str(random.random())+"&proto=95x7", data=packet, headers=headers)

	if r.status_code == 200:
		return parseResponse(r.content)
	else:
		raise Exception ("ERROR: Server response code: "+ str(r.status_code))

# There is some bullshit logic we don't need
def SendCombo(harvest):
	global comboState, comboCount

	if isinstance(harvest.goodies, proto.model.PQuestPrizes):
		goodies = harvest.goodies.value
	else:
		goodies = harvest.goodies

	loot = {}
	for gd in goodies:
		loot[gd] = proto.game.family_0020.PGoodyType_PCost(proto.game.family_0020.PGoodyType(comboState),goodies[gd].value)
		comboCount = comboCount + 1
		if comboState == proto.game.family_0020.PGoodyType.COMBO_NEW:
			comboState = proto.game.family_0020.PGoodyType.COMBO
	packet_obj = proto.game.family_0020.Packet_0020_04(loot)
	packetBuffer = BinaryBuffer()
	packet_obj.write(packetBuffer)
	packet = proto.getBinary(packetBuffer)
	packet = SignPacket(packet)
	r = session.post(proto_url + sid + "&request_id="+str(random.random())+"&proto=20x4&network=vk&gzip=off", data=packet, headers=headers)

	if r.status_code == 200:
		return parseResponse(r.content)
	else:
		raise Exception ("ERROR: Server response code: "+ str(r.status_code))

def SendGifts(gift, buy_gift = False):
	#pylint: disable=unused-argument
	packet_obj = proto.game.family_0070.Packet_0070_01(gift, False) # hardcoding buy_gift it to avoid accidents
	packetBuffer = BinaryBuffer()
	packet_obj.write(packetBuffer)
	packet = getBinary(packetBuffer)
	packet = SignPacket(packet)
	r = session.post(proto_url + sid + "&request_id="+str(random.random())+"&proto=70x1&network=vk&gzip=off", data=packet, headers=headers)

	if r.status_code == 200:
		return parseResponse(r.content)
	else:
		raise Exception ("ERROR: Server response code: "+ str(r.status_code))

def ProcessFriendHelp(friend_id,map_id,is_apply,actions):
	packet_obj = proto.game.family_0050.Packet_0050_05(friend_id, map_id, is_apply, actions)
	packetBuffer = BinaryBuffer()
	packet_obj.write(packetBuffer)
	packet = getBinary(packetBuffer)
	packet = SignPacket(packet)
	r = session.post(proto_url + sid + "&request_id="+str(random.random())+"&proto=50x5&network=vk&gzip=off", data=packet, headers=headers)

	if r.status_code == 200:
		return parseResponse(r.content)
	else:
		raise Exception ("ERROR: Server response code: "+ str(r.status_code))

def ReceiveGifts(gift_ids):
	packet_obj = proto.game.family_0070.Packet_0070_03(gift_ids)
	packetBuffer = BinaryBuffer()
	packet_obj.write(packetBuffer)
	packet = getBinary(packetBuffer)
	packet = SignPacket(packet)
	r = session.post(proto_url + sid + "&request_id="+str(random.random())+"&proto=70x3&network=vk&gzip=off", data=packet, headers=headers)

	if r.status_code == 200:
		return parseResponse(r.content)
	else:
		raise Exception ("ERROR: Server response code: "+ str(r.status_code))

def startLabCraft(building_kind,formula_kind):
	packet_obj = proto.game.family_0040.Packet_0040_01(building_kind,formula_kind)
	packetBuffer = BinaryBuffer()
	packet_obj.write(packetBuffer)
	packet = getBinary(packetBuffer)
	packet = SignPacket(packet)
	r = session.post(proto_url + sid + "&request_id="+str(random.random())+"&proto=40x1&network=vk&gzip=off", data=packet, headers=headers)

	if r.status_code == 200:
		return parseResponse(r.content)
	else:
		raise Exception ("ERROR: Server response code: "+ str(r.status_code))

def getLabCraft(building_kind, formula_kind, buy_finish=False):
	packet_obj = proto.game.family_0040.Packet_0040_03(building_kind, formula_kind, buy_finish)
	packetBuffer = BinaryBuffer()
	packet_obj.write(packetBuffer)
	packet = getBinary(packetBuffer)
	packet = SignPacket(packet)
	r = session.post(proto_url + sid + "&request_id="+str(random.random())+"&proto=40x3&network=vk&gzip=off", data=packet, headers=headers)

	if r.status_code == 200:
		return parseResponse(r.content)
	else:
		raise Exception ("ERROR: Server response code: "+ str(r.status_code))

def MyFarmAction(act_type, action_value, char_pos, map_id):
	packet_obj = proto.game.family_0010.Packet_0010_01(act_type, action_value, char_pos, map_id)
	packetBuffer = BinaryBuffer()
	packet_obj.write(packetBuffer)
	packet = getBinary(packetBuffer)
	packet = SignPacket(packet)
	r = session.post(proto_url + sid + "&request_id="+str(random.random())+"&proto=10x1&network=vk&gzip=off", data=packet, headers=headers)

	if r.status_code == 200:
		return parseResponse(r.content)
	else:
		raise Exception ("ERROR: Server response code: "+ str(r.status_code))

def MyFarmAction20(pact, map_id):
	packet_obj = proto.game.family_0020.Packet_0020_01(pact, map_id)
	packetBuffer = BinaryBuffer()
	packet_obj.write(packetBuffer)
	packet = getBinary(packetBuffer)
	packet = SignPacket(packet)
	r = session.post(proto_url + sid + "&request_id="+str(random.random())+"&proto=20x1&network=vk&gzip=off", data=packet, headers=headers)

	if r.status_code == 200:
		return parseResponse(r.content)
	else:
		raise Exception ("ERROR: Server response code: "+ str(r.status_code))

def MyFarmActionA2(act_type, action_value, char_pos):
	packet_obj = proto.game.family_00A2.Packet_00A2_0F(act_type, action_value, char_pos)
	packetBuffer = BinaryBuffer()
	packet_obj.write(packetBuffer)
	packet = getBinary(packetBuffer)
	packet = SignPacket(packet)
	r = session.post(proto_url + sid + "&request_id="+str(random.random())+"&proto=a2xf&network=vk&gzip=off", data=packet, headers=headers)

	if r.status_code == 200:
		return parseResponse(r.content)
	else:
		raise Exception ("ERROR: Server response code: "+ str(r.status_code))

def get_info_obj():
	obj_url = 'http://st-farm.redspell.ru/library/info_object.json?random='+'{:0>10}'.format(str(int(random.random()*(10**10))))
	r = session.get(obj_url, headers=headers, timeout=15)
	if r.status_code == 200:
		return literal_eval(r.content)
	else:
		raise Exception ("ERROR getting info_object file: Server response code: "+ str(r.status_code))

info = get_info_obj()

def get_lang():
	lang_url = 'http://st-farm.redspell.ru//locale/redspell/LangRU.json?random='+'{:0>10}'.format(str(int(random.random()*(10**10))))
	r = session.get(lang_url, headers=headers, timeout=15)
	if r.status_code == 200:
		langext = literal_eval(r.content)
		langext["st_stone1"] = "Камни"
		langext["st_stone2"] = "Камни"
		langext["st_anc_stone1"] = "Камни"
		langext["st_anc_stone2"] = "Камни"
		langext["magic_box"] = "Нано-сундук"
	else:
		raise Exception ("ERROR getting language file: Server response code: "+ str(r.status_code))
	return langext

lang = get_lang()

def EventHandler(event_array):
	global comboCount, comboState
	PUserEvent = proto.model.PUserEvent
	#print event_array
	for i in event_array:
		if event_array[i].variance == PUserEvent.EDIT_QUEST:
			#AppFacade.questMediator.editQuest(event.value);
			print "Got EDIT_QUEST event"
			#We did something that was a quest objective
		elif event_array[i].variance == PUserEvent.ALARM:
			#sendNotification(NotificationType.SHOW_DIALOG, new Alarm(Lang.getString(_loc_5)));
			raise Exception ("Got ALARM event - this likely means bot got detected")
		elif event_array[i].variance == PUserEvent.FINISH_QUEST:
			#self.finishQuest(event.value);
			print "Got FINISH_QUEST event"
		elif event_array[i].variance == PUserEvent.START_QUEST:
			#AppFacade.questMediator.newQuest(event.value);
			print "Got START_QUEST event"
		elif event_array[i].variance == PUserEvent.FRIENDS_HELP:
			#AppFacade.userProxy.addFriendHelp(event.value);
			#AppFacade.userMediator.updateFriendHelp((event.value as PFriendsHelp).fh_friend_id);
			print "Got FRIENDS_HELP event: "+str(event_array[i].value)
		elif event_array[i].variance == PUserEvent.STORY:
			#AppFacade.questMediator.newStory(event.value);
			print "Got STORY event"
		elif event_array[i].variance == PUserEvent.FINISH_STUDY:
			#AppFacade.runCommand(StudyCompleteCommand, event.value);
			print "Got FINISH_STUDY event"
		elif event_array[i].variance == PUserEvent.LEVEL:
			#self.levelUp(event.value);
			print "Got LEVEL event"
		elif event_array[i].variance == PUserEvent.OBJECT2_BOARD:
			#self.addWorldObject(event.value as PObj);
			print "Got OBJECT2_BOARD event"
		elif event_array[i].variance == PUserEvent.OBJECT_REMOVE_FROM_BOARD:
			#_loc_6 = AppFacade.boardMediator.getWorldObject(event.value);
			#if (_loc_6)
			#	if (_loc_6 is Pet)
			#		AppFacade.boardMediator.freePet(_loc_6.ID);
			#	else
			#		AppFacade.boardMediator.removeObject(_loc_6);
			#		if (_loc_6.kind == "bl_ford" || _loc_6.kind == "bl_web" || _loc_6.kind == "bl_abatis")
			#			FarmCommand.updateAreas();
			print "Got OBJECT_REMOVE_FROM_BOARD event"
		elif event_array[i].variance == PUserEvent.GIFT:
			#AppFacade.userProxy.unshiftGift(event.value as PGift);
			#AppFacade.bottomMediator.refreshGiftNum();
			#print "Got GIFT event:" + repr(event_array[i].value)
			print "We got new gifts."
		elif event_array[i].variance == PUserEvent.FINISH_COMBO:
			#finishCombo(event.value);
			print "EVENT: FINISH_COMBO: We got: [" + lang[event_array[i].value.rare_kind] + "] x " + str(event_array[i].value.rare_cnt)
			#print "Combo in "+str(comboCount)
			################# THIS NEEDS TESTING !!! ############################
			## f = open('drop_stats.txt','a')
			## f.write("{'gs_combo':{0: "+str(event_array[i].value)+"}},\r\n")
			## f.close()
			#####################################################################
			comboState = proto.game.family_0020.PGoodyType.COMBO_NEW
			comboCount = 0
		elif event_array[i].variance == PUserEvent.DROP_FROM_WAREHOUSE:
			#_loc_7 = event.value as PWarehouseReserve;
			#AppFacade.userProxy.changeWarehouseReserve(_loc_7.reserve_type_variance, false, _loc_7.reserve_kind, _loc_7.count);
			print "Got DROP_FROM_WAREHOUSE event"
		elif event_array[i].variance == PUserEvent.ALINK:
			#if (SocialdCommand.status == SocialdCommand.SUCCESS)
			#	self.alink(event.value);
			#else
			#	SocialdCommand.addDependUserEvent(_loc_2);
			print "Got ALINK event"
		elif event_array[i].variance == PUserEvent.ASK_GIFT:
			#if (SocialdCommand.status == SocialdCommand.SUCCESS)
			#	self.askGift(event.value as PGiftAsk);
			#else
			#	SocialdCommand.addDependUserEvent(_loc_2);
			print "Got ASK_GIFT event"
		elif event_array[i].variance == PUserEvent.MESSAGE:
			#self.showMessage(event.value);
			if event_array[i].value.field_1:
				print "EVENT: Message:", event_array[i].value.field_0, event_array[i].value.field_1
			else:
				print "EVENT: Message:", event_array[i].value.field_0
		elif event_array[i].variance == PUserEvent.SHARE_BAKS:
			#_loc_8 = event.value as PSign_uint;
			#BuyMoneyCommand.share(PCost.create(PCost.BAKS, _loc_8.field_1), _loc_8.field_0);
			print "Got SHARE_BAKS event"
		elif event_array[i].variance == PUserEvent.SHARE_GOLD:
			#_loc_8 = event.value as PSign_uint;
			#BuyMoneyCommand.share(PCost.create(PCost.GOLD, _loc_8.field_1), _loc_8.field_0);
			print "Got SHARE_GOLD event"
		elif event_array[i].variance == PUserEvent.START_STIMULATE_PAY:
			#self.startStimulatePay(event.value);
			print "Got START_STIMULATE_PAY event"
		elif event_array[i].variance == PUserEvent.FINISH_STIMULATE_PAY:
			#AppFacade.userMediator.changeDoubleGold();
			print "Got FINISH_STIMULATE_PAY event"
		elif event_array[i].variance == PUserEvent.BAKS:
			print "Got BAKS event"
		elif event_array[i].variance == PUserEvent.GOLD:
			#AppFacade.userProxy.setMoney(event.value, _loc_2.variance == PUserEvent.GOLD);
			print "Got GOLD event"
		elif event_array[i].variance == PUserEvent.ENERGY:
			#AppFacade.userProxy.setEnergy(event.value);
			print "Got ENERGY event"
		elif event_array[i].variance == PUserEvent.EXP:
			#AppFacade.userProxy.setExp(event.value);
			print "Got EXP event"
		elif event_array[i].variance == PUserEvent.FOOD:
			#AppFacade.userProxy.setFood(event.value);
			print "Got FOOD event"
		elif event_array[i].variance == PUserEvent.TIMBER:
			#AppFacade.userProxy.setTimber(event.value);
			print "Got TIMBER event"
		elif event_array[i].variance == PUserEvent.RARE_ITEM:
			#sendNotification(NotificationType.GOODY, PCost.create(PCost.RARE_ITEM, event.value));
			print "Got RARE_ITEM event"
		elif event_array[i].variance == PUserEvent.ADD_PET:
			#self.addPet(event.value);
			print "Got ADD_PET event"
		elif event_array[i].variance == PUserEvent.RARE_RECEIVED:
			#self.rareReceived(event.value);
			print "Got RARE_RECEIVED event"
		elif event_array[i].variance == PUserEvent.ADD_RESERVE:
			#_loc_7 = event.value as PWarehouseReserve;
			#AppFacade.userProxy.changeWarehouseReserve(_loc_7.reserve_type_variance, true, _loc_7.reserve_kind, _loc_7.count);
			print "Got ADD_RESERVE event"
		elif event_array[i].variance == PUserEvent.RELOAD:
			#ErrorCommand.resetFarm(false);
			#Requests.getFarm(true);
			print "Got RELOAD event"
		elif event_array[i].variance == PUserEvent.START_LEVEL_ACCELERATION_ACTION:
			#AppFacade.userMediator.change6levelEvent(event.value, true);
			print "Got START_LEVEL_ACCELERATION_ACTION event"
		elif event_array[i].variance == PUserEvent.FINISHED_LEVEL_ACCELERATION_ACTION:
			#self.finishLevelAction(event.value);
			print "Got FINISHED_LEVEL_ACCELERATION_ACTION event"
		elif event_array[i].variance == PUserEvent.GIFT_FROM_ADMIN:
			#showVipPrizeMsg(event.value as PWarehouseReserve);
			print "Got GIFT_FROM_ADMIN event"
		elif event_array[i].variance == PUserEvent.KICK_FROM_RAID:
			#RaidCommand.removeMember((event.value as str_str).field_0, (event.value as str_str).field_1);
			print "Got KICK_FROM_RAID event"
		elif event_array[i].variance == PUserEvent.RAID_ACCEPT:
			#_loc_9 = 0;
			#_loc_10 = null;
			#_loc_11 = event.value as PRaid;
			#_loc_10 = new VOFriendRaidItem();
			#_loc_10.raid_info = AppFacade.manualProxy.getRaidInfo(_loc_11.kind);
			#_loc_10.start_time = _loc_11.start_date;
			#_loc_10.time_left = _loc_10.raid_info.rd_period + _loc_11.start_date - AppFacade.actionProxy.getServerTime();
			#_loc_9 = _loc_9 + 1;
			#_loc_10.cur_hp = _loc_11.boss_hp;
			#_loc_10.cur_members = _loc_11.members.length;
			#_loc_10.raid_level = _loc_11.level;
			#_loc_10.canAdd = _loc_11.kicked_members.indexOf(AppFacade.userProxy.user_id) == -1;
			#_loc_10.raid = event.value;
			#if (!_loc_10.canAdd)
			#	return;
			#for each (_loc_14 in _loc_11.members)
			#	if (_loc_14.is_leader == true)
			#		_loc_10.message = _loc_14.text;
			#		_loc_10.ub = PUserBase.create(_loc_14.id, "", _loc_14.avatar, "", 1, "m", [], 0, true, PSkin.create(0, null), "", 1, false, PSkin.create(0, null), false, null, _loc_11.id, []);
			#_loc_12 = new RaidStartDialog(_loc_10.raid_info, RaidStartDialog.HELP, _loc_10.raid, _loc_10.time_left);
			#_loc_12.addEventListener(Vevent_array[i].variance, self.addToFriendRaid);
			#RaidMediator.addRares(_loc_10.raid_info, _loc_12);
			#AppFacade.instance.sendNotification(NotificationType.SHOW_DIALOG, _loc_12);
			print "Got RAID_ACCEPT event"
		elif event_array[i].variance == PUserEvent.GOLD_MINE_MIGRATION:
			#_loc_13 = new ProfessorDialog(Lang.getString("gold_mine_migration"), Lang.getString("bt_show", true));
			#_loc_13.addListener(VEvent.CLOSE_DIALOG, self.onProfGoldMineCloseHandler);
			#sendNotification(NotificationType.SHOW_DIALOG, _loc_13);
			print "Got GOLD_MINE_MIGRATION event"
		elif event_array[i].variance == PUserEvent.CLAN_BONUS_CHANGE:
			#print "Got CLAN_BONUS_CHANGE event" # spam
			#event_array[i].value - PClanBonusState
			pass
		else:
			print "Got event id: " + str(event_array[i].variance)
		return

class farm_map(object):
	def __init__(self):
		self.map = set()
	def add_obj(self,obj):
		self.map.update(self.get_obj_map(obj))
	def new_obj(self,obj_x,obj_y,obj_map_id,d=1,sx=3,sy=3):
		self.map.update(self.new_object_map(obj_x,obj_y,obj_map_id,d,sx,sy))				
	def discard(self,obj):
		for p in self.get_obj_map(obj): 
			self.map.discard(p)
	def remove(self,obj):
		for p in self.get_obj_map(obj): 
			self.map.remove(p) # error if point is not in set
	def is_free(self,obj_x,obj_y,obj_map_id,d=1,sx=3,sy=3): #currently used for plants, assuming direction 1 and size 3,3
		this_obj_map = set()
		#This directions are totally fucked up. It seems only 1,3 are used 
		dx,dy = -1,1
		if d == proto.model.PDirection.LEFT_DOWN:
			sy,sx = sx,sy
		for x in xrange(0,sx*dx,dx):
			for y in xrange(0,sy*dy,dy):
				this_obj_map.update([(obj_map_id,obj_x+x,obj_y+y)]) #obj.obj_kind[0:2]
		return (this_obj_map & self.map == set([]))
	@staticmethod
	def get_obj_map(obj):
		this_obj_map = set()
		#This directions are totally fucked up. It seems only 1,3 are used 
		dx,dy = -1,1
		sx,sy = info[obj.obj_kind]['sizex'],info[obj.obj_kind]['sizey']
		if obj.obj_direction.variance == proto.model.PDirection.LEFT_DOWN:
			sy,sx = sx,sy
		for x in xrange(0,sx*dx,dx):
			for y in xrange(0,sy*dy,dy):
				this_obj_map.update([(obj.obj_map_id,obj.obj_pos.x+x,obj.obj_pos.y+y)]) #obj.obj_kind[0:2]
		return this_obj_map
	@staticmethod
	def new_object_map(obj_x,obj_y,obj_map_id,d=1,sx=3,sy=3): #currently used for plants, assuming direction 1 and size 3,3
		this_obj_map = set()
		#This directions are totally fucked up. It seems only 1,3 are used 
		dx,dy = -1,1
		if d == proto.model.PDirection.LEFT_DOWN:
			sy,sx = sx,sy
		for x in xrange(0,sx*dx,dx):
			for y in xrange(0,sy*dy,dy):
				this_obj_map.update([(obj_map_id,obj_x+x,obj_y+y)]) #obj.obj_kind[0:2]
		return this_obj_map
