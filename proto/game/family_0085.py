from proto.model import *

class Packet_0085_05(object):
	GOLD_PACK = 1
	FROM_WAREHOUSE = 0

	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def write(self, param1):
		param1.family = 0x85
		param1.subfamily = 0x05
		param1.writeByte(self.variance)
		if self.variance == self.FROM_WAREHOUSE:
			param1.writeInt(self.value)
		elif self.variance == self.GOLD_PACK:
			param1.writeInt(self.value)
		else:
			raise Exception ("ERROR: incorrect Packet_0085_05 data")

class Packet_0085_06(object):
	def __init__(self, param1):
		if param1.readUnsignedByte() == 1:
			self.value = PShortCompetition()
			self.value.read(param1)
		else:
			self.value = None
