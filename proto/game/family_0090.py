from proto.model import PRaid

class Packet_0090_02(object):
	ALREADY_FINISHED = 2
	TOO_MANY_MEMBERS = 1
	SUCCESS = 0

	def __init__ (self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.SUCCESS:
			self.value = {}
			for i in xrange(param1.readUnsignedShort()):
				self.value[i] = PRaid()
				self.value[i].read(param1)
		elif self.variance == self.TOO_MANY_MEMBERS:
			pass
		elif self.variance == self.ALREADY_FINISHED:
			pass
		else:
			raise Exception ("ERROR: incorrect Packet_0090_02 data")
