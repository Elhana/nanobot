#!/usr/bin/python
# -*- coding: UTF-8 -*-
import requests
import re
import os,sys

def get_sid(login, password):
	java_path = '/usr/lib/jvm/java-8-openjdk-amd64/bin/'
	sid = ''
	s = requests.Session()

	USER_AGENT = "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.152 Safari/537.360"

	headers_original = {
		"Origin": "http://m.vk.com",
		"Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
		"Accept-Charset": "utf-8",
		"User-Agent": USER_AGENT }
	r = s.get('http://m.vk.com/', headers=headers_original, timeout=15)

	url = re.search('action="(.*)"',r.text).groups()[0]
	payload = {
		"email": login,
		"pass": password
	}

	headers_original = {
		"Origin": "https://m.vk.com",
		"User-Agent": USER_AGENT }
	r = s.post(url, data=payload, timeout=15)

	remixsid = s.cookies['remixsid']

	headers_original = {
		"Origin": "http://vk.com",
		"Cookie" : "remixsslsid=1; remixsid=%s" % remixsid,
		"Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
		"Accept-Charset": "utf-8",
		"User-Agent": USER_AGENT }
	r = requests.get('https://vk.com/app2388722', headers=headers_original, timeout=15, allow_redirects=False)
	if r.status_code == 302:
		r = requests.get('https://new.vk.com/app2388722', headers=headers_original, timeout=15)

	params = eval(re.search('params = ({.*})', r.text).groups()[0])
	nano_url = 'http://bl-farm.redspell.ru/play/vk/index.html?api_url=http://api.vk.com/api.php&api_id='+str(params['api_id'])+'&api_settings=7&viewer_id='+str(params['viewer_id'])+'&viewer_type=0&sid='+params['sid']+'&secret='+params['secret']+'&access_token='+params['access_token']+'&user_id=0&group_id=0&is_app_user=1&auth_key='+params['auth_key']+'&language=0&parent_language=0&ad_info='+params['ad_info']+'&is_secure=0&ads_app_id='+params['ads_app_id']+'&api_result='+params['api_result']+'&referrer=unknown&lc_name='+params['lc_name']+'&hash='
	r = requests.get(nano_url)
	sid = re.search('sid:"(.*)"', r.text).groups()[0].encode('ascii')

	#getting swf signature strings
	swf = re.search('\) swf_file = "(.*)";', r.text).groups()[0]
	r = requests.get(swf)
	mypath = ""
	if os.path.dirname(sys.argv[0]) != "":
		mypath = os.path.dirname(sys.argv[0])+os.sep

	with open(mypath+'swf/Farm.swf','wb') as f:
		f.write(r.content)

	os.system(java_path+'java -jar '+mypath+'ffdec/ffdec.jar -selectclass model.ProtoProxy -export script '+mypath+'swf/ '+mypath+'swf/Farm.swf')
	with open(mypath+'swf/scripts/model/ProtoProxy.as','r') as f:
		as3 = f.read()

	proto_ver = int(re.search('PROTO_VERSION:uint = (\d*);', as3).groups()[0])

	md5strings = re.findall('writeUTFBytes\((.*)\);', as3)
	s1 = re.search('private static const '+md5strings[0]+':String = "(.*)";', as3).groups()[0]
	s2 = ''
	for s2_part in xrange(1,len(md5strings)-1): 
		s2 = s2 + md5strings[s2_part][1:-1]
	try:
		hash_ptr = re.search(md5strings[-1]+' = (.*)\(_loc\d+_\);', as3).groups()[0]
	except:
		raise Exception ("They messed up with hash function location again :(")

	i = 0
	init_const = {}
	if re.search('private static function ('+hash_ptr+')\(param1:ByteArray\) : String\r\n\s*{\r\n.*length;', as3) is not None:
		hash_type = 'sha1'
		ic = re.search('var _loc4_:(?:.?int|\*) = (-?[E\.\d]*);\r\n\s*var _loc5_:(?:.?int|\*) = (-?[E\.\d]*);\r\n\s*var _loc6_:(?:.?int|\*) = (-?[E\.\d]*);\r\n\s*var _loc7_:(?:.?int|\*) = (-?[E\.\d]*);\r\n\s*var _loc8_:(?:.?int|\*) = (-?[E\.\d]*);', as3).groups()
		for i in xrange(len(ic)):
			init_const[i]=int(float(ic[i]))
			if init_const[i] < 0:
				init_const[i] = init_const[i] + 2**32
	elif re.search('private static function ('+hash_ptr+')\(param1:ByteArray\) : String\r\n\s*{\r\n.*0;', as3) is not None:
		hash_type = 'md5'
		md5body = re.search('private static function ('+hash_ptr+')\(param1:ByteArray\) : String\r\n\s*{\r\n(.*?)}', as3, flags=re.DOTALL).groups()[1]
		locs = re.findall('_loc(\d+)_ = _loc(\d+)_;', md5body, flags=re.DOTALL)
		for l in locs:
			init_const[i] = int(float(re.search('var _loc'+l[1]+'_:.*? = (\d+);', md5body, flags=re.DOTALL).groups()[0]))
			if init_const[i] < 0:
				init_const[i] = init_const[i] + 2**3
			i = i + 1
	else:
		raise Exception ("ERROR: Couldn't get hash type")

	return sid,proto_ver,swf,s1,s2,hash_type,init_const
