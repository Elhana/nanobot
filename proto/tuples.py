import proto
from pyamf.amf3 import ByteArray

# str_str_str
class str_str_str(object):
	__slots__ = ['field_0', 'field_1', 'field_2']
	def __init__(self, param1 = "", param2 = "", param3 = ""):
		self.field_0 = param1
		self.field_1 = param2
		self.field_2 = param3

	def __repr__(self):
		return '{field_0:"%r", field_1:"%r", field_2:"%r"}' % (self.field_0, self.field_1, self.field_2)

	def write(self, param1):
		param1.writeUTF(self.field_0)
		param1.writeUTF(self.field_1)
		param1.writeUTF(self.field_2)

	def read(self, param1):
		self.field_0 = param1.readUTF()
		self.field_1 = param1.readUTF()
		self.field_2 = param1.readUTF()

# str_str
class str_str(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = "", param2 = ""):
		self.field_0 = param1
		self.field_1 = param2

	def __repr__(self):
		return '{field_0:"%r", field_1:"%r"}' % (self.field_0, self.field_1)

	def write(self, param1):
		param1.writeUTF(self.field_0)
		param1.writeUTF(self.field_1)

	def read(self, param1):
		self.field_0 = param1.readUTF()
		self.field_1 = param1.readUTF()

# str_bool
class str_bool(object):
	def write(self, param1):
		param1.writeUTF(self.field_0)
		param1.writeBoolean(self.field_1)

	def __repr__(self):
		return '{field_0:"%r", field_1:"%r"}' % (self.field_0, self.field_1)

	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = "", param2 = False):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = param1.readUTF()
		self.field_1 = param1.readBoolean()

# str_time
class str_time(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = "", param2 = 0):
		self.field_0 = param1
		self.field_1 = param2

	def __repr__(self):
		return '{field_0:"%r", field_1:"%r"}' % (self.field_0, self.field_1)

	def write(self, param1):
		param1.writeUTF(self.field_0)
		param1.writeDouble(self.field_1)

	def read(self, param1):
		self.field_0 = param1.readUTF()
		self.field_1 = param1.readDouble()

# i_time
class i_time(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = 0, param2 = 0):
		self.field_0 = param1
		self.field_1 = param2

	def __repr__(self):
		return '{field_0:"%r", field_1:"%r"}' % (self.field_0, self.field_1)

	def write(self, param1):
		param1.writeInt(self.field_0)
		param1.writeDouble(self.field_1)

	def read(self, param1):
		self.field_0 = param1.readInt()
		self.field_1 = param1.readDouble()

# str_i
class str_i(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = "", param2 = 0):
		self.field_0 = param1
		self.field_1 = param2

	def __repr__(self):
		return '{field_0:"%r", field_1:%r}' % (self.field_0,self.field_1)

	def write(self, param1):
		param1.writeUTF(self.field_0)
		param1.writeInt(self.field_1)

	def read(self, param1):
		self.field_0 = param1.readUTF()
		self.field_1 = param1.readInt()

# str_oi - str + optional Int
class str_oi(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = "", param2 = None):
		self.field_0 = param1
		self.field_1 = param2

	def __repr__(self):
		return '{field_0:"%r", field_1:%r}' % (self.field_0,self.field_1)

	def write(self, param1):
		param1.writeUTF(self.field_0)
		if self.field_1 != None:
			param1.writeByte(1)
			param1.writeInt(self.field_1)
		else:
			param1.writeByte(0)

	def read(self, param1):
		self.field_0 = param1.readUTF()
		if param1.readUnsignedByte() == 1:
			self.field_1 = param1.readInt()
		else:
			self.field_1 = None

class f_i(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = 0, param2 = 0):
		self.field_0 = param1
		self.field_1 = param2

	def write(self, param1):
		param1.writeDouble(self.field_0)
		param1.writeInt(self.field_1)

	def read(self, param1):
		self.field_0 = param1.readDouble()
		self.field_1 = param1.readInt()

class i_oi(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = 0, param2 = 0):
		self.field_0 = param1
		self.field_1 = param2

	def write(self, param1):
		param1.writeInt(self.field_0)
		if self.field_1 != None:
			param1.writeByte(1)
			param1.writeInt(self.field_1)
		else:
			param1.writeByte(0)

	def read(self, param1):
		self.field_0 = param1.readInt()
		if param1.readUnsignedByte() == 1:
			self.field_1 = param1.readInt()
		else:
			self.field_1 = None

class Position_Position(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = None, param2 = None):
		self.field_0 = param1
		self.field_1 = param2

	def write(self, param1):
		self.field_0.write(param1)
		self.field_1.write(param1)

	def read(self, param1):
		self.field_0 = proto.model.Position()
		self.field_0.read(param1)
		self.field_1 = proto.model.Position()
		self.field_1.read(param1)


class str_PIbuffer(object):
	def write(self, param1):
		param1.writeUTF(self.field_0)
		param1.writeUnsignedInt(len(self.field_1))
		param1.writeBytes(self.field_1)

	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = "", param2 = None):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = param1.readUTF()
		self.field_1 = ByteArray()
		param1.readBytes(self.field_1, 0, param1.readUnsignedInt())

class str_PIbuffer_PIbuffer(object):
	def write(self, param1):
		param1.writeUTF(self.field_0)
		param1.writeUnsignedInt(len(self.field_1))
		param1.writeBytes(self.field_1)
		param1.writeUnsignedInt(len(self.field_2))
		param1.writeBytes(self.field_2)

	__slots__ = ['field_0', 'field_1', 'field_2']
	def __init__(self, param1 = "", param2 = None, param3 = None):
		self.field_0 = param1
		self.field_1 = param2
		self.field_2 = param3

	def read(self, param1):
		self.field_0 = param1.readUTF()
		self.field_1 = ByteArray()
		param1.readBytes(self.field_1, 0, param1.readUnsignedInt())
		self.field_2 = ByteArray()
		param1.readBytes(self.field_2, 0, param1.readUnsignedInt())

class i_i(object):
	def write(self, param1):
		param1.writeInt(self.field_0)
		param1.writeInt(self.field_1)

	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = 0, param2 = 0):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = param1.readInt()
		self.field_1 = param1.readInt()

class i_i_str(object):
	__slots__ = ['field_0', 'field_1', 'field_2']
	def __init__(self, param1 = 0, param2 = 0, param3 = ""):
		self.field_0 = param1
		self.field_1 = param2
		self.field_2 = param3

	def read(self, param1):
		self.field_0 = param1.readInt()
		self.field_1 = param1.readInt()
		self.field_2 = param1.readUTF()

	def write(self, param1):
		param1.writeInt(self.field_0)
		param1.writeInt(self.field_1)
		param1.writeUTF(self.field_2)

class i_str(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = 0, param2 = ""):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = param1.readInt()
		self.field_1 = param1.readUTF()

	def write(self, param1):
		param1.writeInt(self.field_0)
		param1.writeUTF(self.field_1)

class uint_str(object):
	def write(self, param1):
		param1.writeInt(self.field_0)
		param1.writeUTF(self.field_1)

	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = 0, param2 = ""):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = param1.readUnsignedInt()
		self.field_1 = param1.readUTF()

class ub_ub(object):
	def write(self, param1):
		param1.writeByte(self.field_0)
		param1.writeByte(self.field_1)

	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = 0, param2 = 0):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = param1.readUnsignedByte()
		self.field_1 = param1.readUnsignedByte()

class uint_bool(object):
	def write(self, param1):
		param1.writeInt(self.field_0)
		param1.writeBoolean(self.field_1)

	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = 0, param2 = False):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = param1.readUnsignedInt()
		self.field_1 = param1.readBoolean()

class uint_oPosition(object):
	def write(self, param1):
		param1.writeInt(self.field_0)
		if self.field_1 != None:
			param1.writeByte(1)
			self.field_1.write(param1)
		else:
			param1.writeByte(0)

	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = 0, param2 = None):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = param1.readUnsignedInt()
		if param1.readUnsignedByte() == 1:
			self.field_1 = proto.model.Position()
			self.field_1.read(param1)
		else:
			self.field_1 = None

class str_a_str_a(object):
	def write(self, param1):
		param1.writeUTF(self.field_0)
		if self.field_1 == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.field_1))
			for i in xrange(len(self.field_1)):
				param1.writeUTF(self.field_1[i])

	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = "", param2 = None):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = param1.readUTF()
		self.field_1 = {}
		for i in xrange(param1.readUnsignedShort()):
			self.field_1[i] = param1.readUTF()

class i_i_i(object):
	def write(self,param1):
		param1.writeInt(self.field_0)
		param1.writeInt(self.field_1)
		param1.writeInt(self.field_2)

	__slots__ = ['field_0', 'field_1', 'field_2']
	def __init__(self, param1 = 0, param2 = 0, param3 = 0):
		self.field_0 = param1
		self.field_1 = param2
		self.field_2 = param3

	def read(self, param1):
		self.field_0 = param1.readInt()
		self.field_1 = param1.readInt()
		self.field_2 = param1.readInt()

class str_uint(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = "", param2 = 0):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = param1.readUTF()
		self.field_1 = param1.readUnsignedInt()

	def write(self, param1):
		param1.writeUTF(self.field_0)
		param1.writeInt(self.field_1)

class str_us(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = "", param2 = 0):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = param1.readUTF()
		self.field_1 = param1.readUnsignedShort()

	def write(self, param1):
		param1.writeUTF(self.field_0)
		param1.writeShort(self.field_1)

class uint_time(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = 0, param2 = 0):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = param1.readUnsignedInt()
		self.field_1 = param1.readDouble()

	def write(self, param1):
		param1.writeInt(self.field_0)
		param1.writeDouble(self.field_1)

class i_i_i_a_str_a(object):
	__slots__ = ['field_0', 'field_1', 'field_2', 'field_3']
	def __init__(self, param1 = 0, param2 = 0, param3 = 0, param4 = None):
		self.field_0 = param1
		self.field_1 = param2
		self.field_2 = param3
		self.field_3 = param4

	def read(self, param1):
		self.field_0 = param1.readInt()
		self.field_1 = param1.readInt()
		self.field_2 = param1.readInt()
		self.field_3 = {}
		for i in xrange(param1.readUnsignedShort()):
			self.field_3[i] = param1.readUTF()

	def write(self, param1):
		param1.writeInt(self.field_0)
		param1.writeInt(self.field_1)
		param1.writeInt(self.field_2)
		if self.field_3 == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.field_3))
			for i in xrange(len(self.field_3)):
				param1.writeUTF(self.field_3[i])

class i_oi_oi_a_str_a(object):
	__slots__ = ['field_0', 'field_1', 'field_2', 'field_3']
	def __init__(self, param1 = 0, param2 = 0, param3 = 0, param4 = None):
		self.field_0 = param1
		self.field_1 = param2
		self.field_2 = param3
		self.field_3 = param4

	def read(self, param1):
		self.field_0 = param1.readInt()
		if param1.readUnsignedByte() == 1:
			self.field_1 = param1.readInt()
		else:
			self.field_1 = None
		if param1.readUnsignedByte() == 1:
			self.field_2 = param1.readInt()
		else:
			self.field_2 = None
		self.field_3 = {}
		for i in xrange(param1.readUnsignedShort()):
			self.field_3[i] = param1.readUTF()

	def write(self, param1):
		param1.writeInt(self.field_0)
		if self.field_1 != None:
			param1.writeByte(1)
			param1.writeInt(self.field_1)
		else:
			param1.writeByte(0)
		if self.field_2 != None:
			param1.writeByte(1)
			param1.writeInt(self.field_2)
		else:
			param1.writeByte(0)
		if self.field_3 == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.field_3))
			for i in xrange(len(self.field_3)):
				param1.writeUTF(self.field_3[i])

class str_Position(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = "", param2 = None):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = param1.readUTF()
		self.field_1 = proto.model.Position()
		self.field_1.read(param1)

	def write(self, param1):
		param1.writeUTF(self.field_0)
		self.field_1.write(param1)

class str_f(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = "", param2 = 0):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = param1.readUTF()
		self.field_1 = param1.readDouble()

	def write(self, param1):
		param1.writeUTF(self.field_0)
		param1.writeDouble(self.field_1)

# str_time
class str_time_a_str_a(object):
	__slots__ = ['field_0', 'field_1', 'field_2']
	def __init__(self, param1 = "", param2 = 0, param3 = None):
		self.field_0 = param1
		self.field_1 = param2
		self.field_2 = param3

	def __repr__(self):
		return '{field_0:"%r", field_1:"%r", field_2:"%r"}' % (self.field_0, self.field_1, self.field_2)

	def read(self, param1):
		self.field_0 = param1.readUTF()
		self.field_1 = param1.readDouble()
		self.field_2 = {}
		for i in xrange(param1.readUnsignedShort()):
			self.field_2[i] = param1.readUTF()

	def write(self, param1):
		param1.writeUTF(self.field_0)
		param1.writeDouble(self.field_1)
		if self.field_2 == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.field_2))
			for i in xrange(len(self.field_2)):
				param1.writeUTF(self.field_2[i])

class i_otime(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = 0, param2 = 0):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = param1.readInt()
		if param1.readUnsignedByte() == 1:
			self.field_1 = param1.readDouble()
		else:
			self.field_1 = None

	def write(self, param1):
		param1.writeInt(self.field_0)
		if self.field_1 != None:
			param1.writeByte(1)
			param1.writeDouble(self.field_1)
		else:
			param1.writeByte(0)

class f_time(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = 0, param2 = 0):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = param1.readDouble()
		self.field_1 = param1.readDouble()

	def write(self, param1):
		param1.writeDouble(self.field_0)
		param1.writeDouble(self.field_1)

class i_bool(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = 0, param2 = False):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = param1.readInt()
		self.field_1 = param1.readBoolean()

	def write(self, param1):
		param1.writeInt(self.field_0)
		param1.writeBoolean(self.field_1)

class time_time(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = 0, param2 = 0):
		self.field_0 = param1
		self.field_1 = param2

	def __repr__(self):
		return '{field_0:"%r", field_1:"%r"}' % (self.field_0, self.field_1)

	def write(self, param1):
		param1.writeDouble(self.field_0)
		param1.writeDouble(self.field_1)

	def read(self, param1):
		self.field_0 = param1.readDouble()
		self.field_1 = param1.readDouble()

class str_uint_bool(object):
	__slots__ = ['field_0', 'field_1', 'field_2']
	def __init__(self, param1 = "", param2 = 0, param3 = False):
		self.field_0 = param1
		self.field_1 = param2
		self.field_2 = param3

	def read(self, param1):
		self.field_0 = param1.readUTF()
		self.field_1 = param1.readUnsignedInt()
		self.field_2 = param1.readBoolean()

	def write(self, param1):
		param1.writeUTF(self.field_0)
		param1.writeInt(self.field_1)
		param1.writeBoolean(self.field_2)

class time_str(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = 0, param2 = ""):
		self.field_0 = param1
		self.field_1 = param2

	def __repr__(self):
		return '{field_0:"%r", field_1:"%r"}' % (self.field_0, self.field_1)

	def read(self, param1):
		self.field_0 = param1.readDouble()
		self.field_1 = param1.readUTF()

	def write(self, param1):
		param1.writeDouble(self.field_0)
		param1.writeUTF(self.field_1)
