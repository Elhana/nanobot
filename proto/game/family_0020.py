from proto.model import *

class Packet_0020_01(object):
	def __init__(self, param1 = None, param2 = 0):
		self.act = param1
		self.map_id = param2

	def write(self, param1):
		param1.family = 0x20
		param1.subfamily = 0x01
		self.act.write(param1)
		param1.writeInt(self.map_id)

class Packet_0020_02(object):
	RES_GET_EFARM_BONUS = 36
	RES_TAKE_ACHIEVEMENT_PRIZE = 35
	RES_ADD_TO_SCURRENCY_WISH_LIST = 34
	RES_BUY_SUITCASE = 33
	RES_OPEN_SUITCASE = 32
	RES_RESET_MAGIC_ACTION = 31
	RES_MAGIC_ACTION = 30
	RES_PROLONG_OFFER = 29
	RES_BUY_OFFER = 28
	RES_EXCHANGE_EVENT_RARES = 27
	RES_OPEN_GOLD_CASE = 26
	RES_BUY_UNLOCK_RAID_LIST = 25
	RES_NEW_GIFT = 24
	RES_BUY_CASE = 23
	RES_GET_PRIZE_LIKE = 22
	RES_BUY_SEEDS_PACK = 21
	RES_BUY_UNLOCK = 20
	RES_FEED_PET = 19
	RES_BUY_SEGWAY = 18
	RES_BUY_WEAPON = 17
	RES_BUY_FERTILIZER = 16
	RES_BUILDING_BONUS_COLLECTED = 15
	RES_USE_FERTILIZER = 14
	RES_FINISH_BUILDING = 13
	RES_NEW_DECOR = 12
	RES_NEW_TILE = 11
	RES_NEW_ANIMAL = 10
	RES_NEW_BUILDING = 9
	RES_BUY_FAIR_BUILDING = 8
	RES_BUY_RARE_ITEM = 7
	RES_DROP_COLLECTION = 6
	RES_GOLD2_BAKS = 5
	RES_EXPANSION = 4
	RES_WAREHOUSE2_BOARD = 3
	RES_SELL_RESERVES = 2
	RES_BOARD_OBJ_ACTION = 1
	RES_USE_ENERGY = 0

	def __init__(self, param1):
		self.res_variance = param1.readUnsignedByte()
		if self.res_variance == self.RES_USE_ENERGY:
			self.res_value = PUseEnergyResult()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_BOARD_OBJ_ACTION:
			self.res_value = PResBoardObjAction()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_SELL_RESERVES:
			self.res_value = PCosts()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_WAREHOUSE2_BOARD:
			self.res_value = PResWarehouse2board()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_EXPANSION:
			self.res_value = PResExpansion_PSign()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_GOLD2_BAKS:
			self.res_value = param1.readUnsignedInt()
		elif self.res_variance == self.RES_DROP_COLLECTION:
			self.res_value = PQuestPrizes()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_BUY_RARE_ITEM:
			self.res_value = PWarehouseReserve()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_BUY_FAIR_BUILDING:
			self.res_value = param1.uint_PSign()
			self.res_value = param1.read(param1)
		elif self.res_variance == self.RES_NEW_BUILDING:
			self.res_value = PResNewBuilding()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_NEW_ANIMAL:
			self.res_value = PResNewAnimal()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_NEW_TILE:
			self.res_value = param1.readUnsignedInt()
		elif self.res_variance == self.RES_NEW_DECOR:
			self.res_value = PResNewDecor()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_FINISH_BUILDING:
			self.res_value = PResFinishBuilding()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_USE_FERTILIZER:
			self.res_value = PResUseFertilizer()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_BUILDING_BONUS_COLLECTED:
			self.res_value = PCollectBonusResult()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_BUY_FERTILIZER:
			pass
		elif self.res_variance == self.RES_BUY_WEAPON:
			self.res_value = PWarehouseReserve()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_BUY_SEGWAY:
			self.res_value = PResBuySegway()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_FEED_PET:
			self.res_value = PFarmAction()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_BUY_UNLOCK:
			self.res_value = {}
			for i in xrange(param1.readUnsignedByte()):
				self.res_value[i] = param1.readUTF()
		elif self.res_variance == self.RES_BUY_SEEDS_PACK:
			self.res_value = PWarehouseReserve()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_GET_PRIZE_LIKE:
			self.res_value = PGetPrizeLike()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_BUY_CASE:
			self.res_value = PWarehouseReserve()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_NEW_GIFT:
			self.res_value = PWarehouseReserve()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_BUY_UNLOCK_RAID_LIST:
			self.res_value = PCosts_a_str_a()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_OPEN_GOLD_CASE:
			self.res_value = PWarehouseReserve()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_EXCHANGE_EVENT_RARES:
			pass
		elif self.res_variance == self.RES_BUY_OFFER:
			self.res_value = uint_a_PResBuyOffer_a()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_PROLONG_OFFER:
			self.res_value = POffer()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_MAGIC_ACTION:
			self.res_value = {}
			for i in xrange(param1.readUnsignedShort()):
				self.res_value[i] = PFarmAction()
				self.res_value[i].read(param1)
		elif self.res_variance == self.RES_RESET_MAGIC_ACTION:
			self.res_value = {}
			for i in xrange(param1.readUnsignedShort()):
				self.res_value[i] = PFarmAction()
				self.res_value[i].read(param1)
		elif self.res_variance == self.RES_OPEN_SUITCASE:
			self.res_value = PQuestPrizes()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_BUY_SUITCASE:
			self.res_value = PWarehouseReserve()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_ADD_TO_SCURRENCY_WISH_LIST:
			pass
		elif self.res_variance == self.RES_TAKE_ACHIEVEMENT_PRIZE:
			self.res_value = PQuestFinish()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_GET_EFARM_BONUS:
			pass
		else:
			raise Exception ("ERROR: incorrect Packet_0020_02 data")

		self.goodies = {}
		for i in xrange(param1.readUnsignedShort()):
			self.goodies[i] = PQuestPrize()
			self.goodies[i].read(param1)


# It seems only comes as 2nd packet in response?
# Need some kind of EventHandler
class Packet_0020_03(object):
	def __init__(self, param1):
		self.value = {}
		for i in xrange(param1.readUnsignedShort()):
			self.value[i] = PUserEvent()
			self.value[i].read(param1)

# param1 is an array of PGoodyType_Pcost objects
class Packet_0020_04(object):
	def __init__(self, param1 = None):
		self.combo_goodies = param1

	def write(self, param1):
		param1.family = 0x20
		param1.subfamily = 0x04
		if self.combo_goodies == None:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.combo_goodies))
			for i in xrange(len(self.combo_goodies)):
				self.combo_goodies[i].write(param1)

class Packet_0020_05(object):
	def __init__(self, param1 = None):
		self.alwayszero = param1


## Relevant classes ##

# PGoodyType_PCost
# Type(Byte)+CostType(Byte)+[UTF]+Int
class PGoodyType_PCost(object):
	def __init__(self, param1 = None, param2 = None):
		self.field_0 = param1
		self.field_1 = param2

	def __repr__(self):
		return "{f0:%s,f1:%s}" % (self.field_0,self.field_1)

	def write(self, param1):
		self.field_0.write(param1)
		self.field_1.write(param1)

	def read(self, param1):
		self.field_0 = PGoodyType()
		self.field_0.read(param1)
		self.field_1 = PCost()
		self.field_1.read(param1)

class PGoodyType(object):
	COMBO_NEW = 2
	SIMPLE = 1
	COMBO = 0

	def __init__(self, param1 = 0):
		self.variance = param1

	def __repr__(self):
		return "%s" % self.variance

	def write(self, param1):
		param1.writeByte(self.variance)

	def read(self, param1):
		self.variance = param1.readUnsignedByte()

class PCosts_a_str_a(object):
	def write(self, param1):
		self.field_0.write(param1)
		if self.field_1 == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.field_1))
			for i in xrange(len(self.field_1)):
				param1.writeUTF(self.field_1[i])

	def __init__(self, param1 = None, param2 = None):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = PCosts()
		self.field_0.read(param1)
		self.field_1 = {}
		for i in xrange(param1.readUnsignedShort()):
			self.field_1[i] = param1.readUTF()

class PResExpansion_PSign(object):
	def write(self, param1):
		self.field_0.write(param1)
		self.field_1.write(param1)

	def __init__(self, param1 = None, param2 = None):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = PResExpansion()
		self.field_0.read(param1)
		self.field_1 = PSign()
		self.field_1.read(param1)

## 20_01

class PUseEnergyResult(object):
	def __init__(self, param1 = None, param2 = "", param3 = {}, param4 = None):
		self.uer_res = param1
		self.uer_kind = param2
		self.uer_objects = param3
		self.uer_balance_sber_info = param4

	def read(self, param1):
		if param1.readUnsignedByte() == 1:
			self.uer_res = PResUseEnergy()
			self.uer_res.read(param1)
		else:
			self.uer_res = None

		self.uer_kind = param1.readUTF()
		self.uer_objects = {}
		for i in xrange(param1.readUnsignedShort()):
			self.uer_objects[i] = PObj()
			self.uer_objects[i].read(param1)

		if param1.readUnsignedByte() == 1:
			self.uer_balance_sber_info = PBalanceSberInfo()
			self.uer_balance_sber_info.read(param1)
		else:
			self.uer_balance_sber_info = None

	def write(self, param1):
		if self.uer_res != None:
			param1.writeByte(1)
			self.uer_res.write(param1)
		else:
			param1.writeByte(0)

		param1.writeUTF(self.uer_kind)
		if self.uer_objects == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.uer_objects))
			for i in xrange(len(self.uer_objects)):
				self.uer_objects[i].write(param1)

		if self.uer_balance_sber_info != None:
			param1.writeByte(1)
			self.uer_balance_sber_info.write(param1)
		else:
			param1.writeByte(0)

class PResNewDecor(object):
	def write(self, param1):
		if self.rnd_decor_id != None:
			param1.writeByte(1)
			param1.writeInt(self.rnd_decor_id)
		else:
			param1.writeByte(0)

		if self.rnd_balance_sber_info != None:
			param1.writeByte(1)
			self.rnd_balance_sber_info.write(param1)
		else:
			param1.writeByte(0)

	def __init__(self, param1 = 0, param2 = None):
		self.rnd_decor_id = param1
		self.rnd_balance_sber_info = param2


	def read(self, param1):
		if param1.readUnsignedByte() == 1:
			self.rnd_decor_id = param1.readUnsignedInt()
		else:
			self.rnd_decor_id = None

		if param1.readUnsignedByte() == 1:
			self.rnd_balance_sber_info = PBalanceSberInfo()
			self.rnd_balance_sber_info.read(param1)
		else:
			self.rnd_balance_sber_info = None

class PBalanceSberInfo(object):
	OK = 3
	ERROR = 2
	DAILY_LIMIT = 1
	BALANCE = 0

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == 0:
			pass
		elif self.variance == 1:
			pass
		elif self.variance == 2:
			pass
		elif self.variance == 3:
			param1.writeInt(self.value)
		else:
			raise Exception ("ERROR: incorrect PBalanceSberInfo data")

	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == 0:
			pass
		elif self.variance == 1:
			pass
		elif self.variance == 2:
			pass
		elif self.variance == 3:
			self.value = param1.readUnsignedInt()
		else:
			raise Exception ("ERROR: incorrect PBalanceSberInfo data")

class PResBoardObjAction(object):
	SELLED = 2
	STASHED = 1
	MOVED = 0

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.MOVED:
			pass
		elif self.variance == self.STASHED:
			self.value.write(param1)
		elif self.variance == self.SELLED:
			pass
		else:
			raise Exception ("ERROR: incorrect PResBoardObjAction data")

	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.MOVED:
			pass
		elif self.variance == self.STASHED:
			self.value = PWarehouseReserve()
			self.value.read(param1)
		elif self.variance == self.SELLED:
			pass
		else:
			raise Exception ("ERROR: incorrect PResBoardObjAction data")

class PResUseEnergy(object):
	def __init__(self, param1 = 0, param2 = 0, param3 = None):
		self.energy = param1
		self.max_energy = param2
		self.factions = param3

	def read(self, param1):
		self.energy = param1.readUnsignedInt()
		self.max_energy = param1.readUnsignedInt()
		self.factions = {}
		for i in xrange(param1.readUnsignedShort()):
			self.factions[i] = PFarmAction()
			self.factions[i].read(param1)

	def write(self, param1):
		param1.writeInt(self.energy)
		param1.writeInt(self.max_energy)
		if self.factions == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.factions))
			for i in xrange(len(self.factions)):
				self.factions[i].write(param1)


class uint_a_PResBuyOffer_a(object):
	def __init__(self, param1 = 0, param2 = None):
		self.field_0 = param1
		self.field_1 = param2
   
	def read(self, param1):
		self.field_0 = param1.readUnsignedInt()
		self.field_1 = {}
		for i in xrange(param1.readUnsignedByte()):
			self.field_1[i] = PResBuyOffer()
			self.field_1[i].read(param1)
   
	def write(self, param1):
		param1.writeInt(self.field_0)
		if self.field_1 == {}:
			param1.writeByte(0)
		else:
			param1.writeByte(len(self.field_1))
			for i in xrange(len(self.field_1)):
				self.field_1[i].write(param1)

class uint_PSign(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1, param2):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = param1.readUnsignedInt()
		self.field_1 = PSign()
		self.field_1.read(param1)
		
	def write(self, param1):
		param1.writeInt(self.field_0)
		self.field_1.write(param1)

