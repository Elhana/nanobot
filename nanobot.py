#!/usr/bin/python
# -*- coding: UTF-8 -*-

import time
import proto
from lang.RU import lang

## Globals
friendlist = None
raids = None

### Hybrids:
## not yet implemented
sb_lab_list = ["sb_kohlrabi_mod1","sb_brussels_mod1","sb_dill_mod1","sb_lemon_mod1","sb_blackberry_mod1"]
## ancient
sb_anc_list = ["sb_lemon_mod2","sb_broccoli_mod2","sb_pease_mod2","sb_melon_mod3","sb_radish_mod2","sb_wheat_mod3","sb_squash_mod2","sb_cabbage_mod2","sb_eggplant_mod3","sb_pumpkin_mod2","sb_watermelon_mod2","sb_beetroot_mod2","sb_pepper_mod3","sb_tomato_mod2","sb_cucumber_mod3","sb_carrots_mod3","sb_strawberry_mod3"]
## nano
sb_nano_list = ["sb_radish_mod1","sb_grape_mod1","sb_potato_mod1","sb_pease_mod1","sb_melon_mod2","sb_wheat_mod2","sb_strawberry_mod2","sb_pepper_mod2","sb_eggplant_mod2","sb_carrots_mod2","sb_ananas_mod1","sb_cucumber_mod2","sb_wheat_mod1","sb_fungus_mod1","sb_melon_mod1","sb_garlic_mod1","sb_banan_mod1","sb_watermelon_mod1","sb_eggplant_mod1","sb_broccoli_mod1","sb_beetroot_mod1","sb_pepper_mod1","sb_squash_mod1","sb_cabbage_mod1","sb_pumpkin_mod1","sb_carrots_mod1","sb_tomato_mod1","sb_cucumber_mod1","sb_strawberry_mod1"]
## missing
sb_missing_list = ["sb_melon_mod3","sb_broccoli_mod2","sb_garlic_mod2","sb_lemon_mod2","sb_ananas_mod2","sb_pease_mod2","sb_dill_mod2","sb_raspberry_mod2","sb_raspberry_mod1"]

### Nano-animals:
## ancient
an_anc_list = ["an_rabbit_mod2","yn_rabbit_mod2_mid","yn_rabbit_mod2","an_tyrannosaurus","yn_tyrannosaurus_mid","yn_tyrannosaurus","an_cow_mod2","yn_cow_mod2_mid","yn_cow_mod2","an_horse_mod2","yn_horse_mod2_mid","yn_horse_mod2","an_stegosaurus","yn_stegosaurus_mid","yn_stegosaurus","an_turkey_anc","yn_turkey_anc_mid","yn_turkey_anc","an_turkey_mod2","yn_turkey_mod2_mid","yn_turkey_mod2","an_pig_mod2","yn_pig_mod2_mid","yn_pig_mod2","an_triceratops","yn_triceratops_mid","yn_triceratops","an_sheep_mod2","yn_sheep_mod2_mid","yn_sheep_mod2","an_chicken_mod2","yn_chicken_mod2_mid","yn_chicken_mod2","an_brontosaurus","yn_brontosaurus_mid","yn_brontosaurus","an_goat_mod2","yn_goat_mod2_mid","yn_goat_mod2"]
## nano 
an_nano_list = ["an_goose_mod","yn_goose_mod_mid","yn_goose_mod","an_horse_mod","yn_horse_mod_mid","yn_horse_mod","an_rabbit_mod","yn_rabbit_mod_mid","yn_rabbit_mod","an_sheep_mod","yn_sheep_mod_mid","yn_sheep_mod","an_goat_mod","yn_goat_mod_mid","yn_goat_mod","an_pig_mod","yn_pig_mod_mid","yn_pig_mod","an_cow_mod","yn_cow_mod_mid","yn_cow_mod","an_turkey_mod","yn_turkey_mod_mid","yn_turkey_mod","an_chicken_mod","yn_chicken_mod_mid","yn_chicken_mod","an_infra_duckling","yn_infra_duckling_mid","yn_infra_duckling"]
## missing
an_missing_list = ["an_rabbit_mod2","yn_rabbit_mod2_mid","yn_rabbit_mod2","an_bee_mod2","yn_bee_mod2_mid","yn_bee_mod2","an_goose_mod2","yn_goose_mod2_mid","yn_goose_mod2"]

sb_harvest_list = sb_anc_list + sb_nano_list
an_harvest_list = an_anc_list + an_nano_list

watch_list = sb_lab_list + sb_missing_list + an_missing_list


def GetFriendList():
	global friendlist, raids 
	friendlist = proto.GetFriendList()
	raids = proto.GetRaids(1)
	if friendlist != None:
		print "We are: " + friendlist.user.name.encode("utf-8")
	return friendlist

def WalkNeighbours(friendlist):
	for n in friendlist.neighbours:
		this_nb = friendlist.neighbours[n]
		# print str(this_nb.user_base.user_id) +":"+ this_nb.user_base.name.encode("utf-8") +":"+ str(this_nb.user_base.level)+":"+ str(this_nb.fr_actions_exists)
		# Avoid harvesting professor, quests often involve his farm
		if this_nb.fr_actions_exists > 0 and this_nb.user_base.user_id != friendlist.big_friend_id[0]:  ##professor_id = "75981583" = big_friend_id[0]; Kesha bot also has "530630898077", why?
			#LookForMissing(this_nb)
			HarvestNeighbour(this_nb)
	upload_stats()
	print "!! DONE !!"

def HarvestType(nb, farm, farm2, htype):
	for obj in farm.friends_farm.objects:
		this_obj = farm.friends_farm.objects[obj]
		if this_obj.obj_kind[0:3] == htype and FriendCanHarvest(nb, this_obj):
			FriendHarvestCombo(nb, this_obj)
	if farm2 != None:
		for obj in farm2.friends_farm.objects:
			this_obj = farm2.friends_farm.objects[obj]
			if this_obj.obj_kind[0:3] == htype and FriendCanHarvest(nb, this_obj):
				FriendHarvestCombo(nb, this_obj)

def PrintAll(nb, farm, farm2, htype):
	for obj in farm.friends_farm.objects:
		this_obj = farm.friends_farm.objects[obj]
		print this_obj
		print FriendCanHarvest(nb, this_obj)

	if farm2 != None:
		for obj in farm2.friends_farm.objects:
			this_obj = farm2.friends_farm.objects[obj]
			print this_obj
			print FriendCanHarvest(nb, this_obj)


def HarvestKind(nb, farm, farm2, kind):
	for obj in farm.friends_farm.objects:
		this_obj = farm.friends_farm.objects[obj]
		if this_obj.obj_kind == kind and FriendCanHarvest(nb, this_obj):
			FriendHarvestCombo(nb, this_obj)
	if farm2 != None:
		for obj in farm2.friends_farm.objects:
			this_obj = farm2.friends_farm.objects[obj]
			if this_obj.obj_kind == kind and FriendCanHarvest(nb, this_obj):
				FriendHarvestCombo(nb, this_obj)

def HarvestNeighbour(nb):
	farm = proto.getFriendFarm(nb.user_base, 0)
	if farm != None:
		if has_bl_ship(farm):
			farm2 = proto.getFriendFarm(nb.user_base,1)
		else:
			farm2 = None

		#PrintAll(nb, farm, farm2, "")
		#return

		# Hybrid/ancient plants
		for kind in sb_harvest_list:
			HarvestKind(nb, farm, farm2, kind)
		# Stones
		HarvestType(nb, farm, farm2, "st_")
		# Wood
		HarvestType(nb, farm, farm2, "tr_")
		# Hybrid/ancient animals
		for kind in an_harvest_list:
			HarvestKind(nb, farm, farm2, kind)

		# Withered - FIXME	
		# FriendCanHarvest should have an option to look for PSeedbedState.WITHERED, not only PSeedbedState.FULLY_GROWN
		# I can think of two strategies:
		# - Only fix WITHERED, so that friend can accept help
		# - Can we fix and harvest right away?

		# Plants
		HarvestType(nb, farm, farm2, "sb_")
		# Animals
		HarvestType(nb, farm, farm2, "yn_") #young
		HarvestType(nb, farm, farm2, "an_")	#grown

		# Garbage? Haven't seen it in game (spring cleaning?)


def LookForMissing(nb):
	farm = proto.getFriendFarm(nb.user_base, 0)
	if farm != None:
		for kind in sb_harvest_list: ##watch_list:
			for obj in farm.friends_farm.objects:
				this_obj = farm.friends_farm.objects[obj]
				if this_obj.obj_kind == kind:
					print "\t"+str(this_obj.obj_map_id)+":"+str(this_obj.obj_id)+":"+lang[str(this_obj.obj_kind)]

#helpers
def FriendCanHarvest(friend, obj):
	#print obj
	# We have actions?
	has_Actions = friend.fr_actions_exists > 0
	# Obj is FULLY_GROWN?
	if obj.obj_spec_variance == proto.model.PObj.OBJ_SPEC_SEEDBED:
		is_Grown = obj.obj_spec_value.state.variance == proto.model.PSeedbedState.FULLY_GROWN
	else:
		is_Grown = True
	# Obj have fr_ban_neighbor in fertilizers?
	is_Banned = not has_BanFertilizer(obj)

	if has_Actions and is_Grown and is_Banned:
		return True
	else:
		return False

def has_BanFertilizer(obj):
	# Animals?
	if 	obj.obj_spec_variance == proto.model.PObj.OBJ_SPEC_WOOD or \
		obj.obj_spec_variance == proto.model.PObj.OBJ_SPEC_SEEDBED or \
		obj.obj_spec_variance == proto.model.PObj.OBJ_SPEC_ANIMAL or \
		obj.obj_spec_variance == proto.model.PObj.OBJ_SPEC_STONE:
		for i in xrange(len(obj.obj_spec_value.fertilizers)):
			if obj.obj_spec_value.fertilizers[i] == "fr_ban_neighbor":		
				return True
	return False

def has_bl_ship(farm):
	for obj in farm.friends_farm.objects:
		if farm.friends_farm.objects[obj].obj_kind == 'bl_ship' and farm.friends_farm.objects[obj].obj_spec_value.building_state == proto.model.PBuildingState.FINISHED:
			return True
	
def FriendHarvestCombo(friend, obj):
	Packet_0050_03 = proto.game.family_0050.Packet_0050_03
	if obj.obj_kind[0:3] == "tr_":
		action_variance = Packet_0050_03.ACTION_DROP_WOOD
		action_num = obj.obj_spec_value.resource
	elif obj.obj_kind[0:3] == "sb_":
		action_variance = Packet_0050_03.ACTION_HARVEST
		action_num = 1
	elif obj.obj_kind[0:3] == "an_":
		action_variance = Packet_0050_03.ACTION_FEED_ANIMAL
		action_num = 1
	elif obj.obj_kind[0:3] == "yn_":
		action_variance = Packet_0050_03.ACTION_FEED_ANIMAL
		action_num = 1
	elif obj.obj_kind[0:3] == "st_":
		action_variance = Packet_0050_03.ACTION_DROP_STONE
		action_num = obj.obj_spec_value.resource
	elif obj.obj_kind[0:3] == "??_":
		action_variance = Packet_0050_03.ACTION_DROP_GARBAGE
		action_num = 1
	#elif obj.obj_kind[0:3] == "??_":
	#	action_variance = Packet_0050_03.ACTION_MAGIC_BOX_CLICK
	elif obj.obj_kind[0:3] == "bl_":
		print "ERROR: Can't harvest friend's building"
		return None
	elif obj.obj_kind[0:3] == "tl_":
		print "ERROR: Can't harvest friend's tile"
		return None
	elif obj.obj_kind[0:3] == "dc_":
		print "ERROR: Can't harvest friend's decor"
		return None
	elif obj.obj_kind[0:3] == "mp_":
		print "ERROR: Can't harvest friend's decor"
		return None
	elif obj.obj_kind[0:3] == "dp_":
		print "ERROR: Can't harvest friend's decor puzzle"
		return None
	elif obj.obj_kind[0:3] == "mn_":
		print "ERROR: Can't attack friend's monster"
		return None				
	else:
		print "ERROR: Unknown object: %s" % obj.object_kind
		return None

	for i in xrange(action_num):
		print "\t"+str(obj.obj_map_id)+":"+str(obj.obj_id)+":"+lang[str(obj.obj_kind)]
		# Packet_0050_04
		harvest = proto.runFriendTaskAction(friend.user_base, obj.obj_map_id, action_variance, obj.obj_id)

		f = open('drop_stats.txt','a')
		f.write("{'"+obj.obj_kind+"':"+str(harvest.goodies)+"},\r\n")
		f.close()

		proto.SendCombo(harvest)

		friend.fr_actions_exists = friend.fr_actions_exists - 1

		if harvest.magic_box > 0:
			print "\tWe got magic box :)"
			for mb in xrange(min(friend.fr_actions_exists,harvest.magic_box)):
				harvest = proto.runFriendTaskAction(friend.user_base, obj.obj_map_id, Packet_0050_03.ACTION_MAGIC_BOX_CLICK, None)
				f = open('drop_stats.txt','a')
				f.write("{'magic_box':"+str(harvest.goodies)+"},\r\n")
				f.close()
				proto.SendCombo(harvest)
				friend.fr_actions_exists = friend.fr_actions_exists - 1

		if friend.fr_actions_exists == 0:
			return
	return 


def main():
	proto.sid = "e48b5422-bc4c-42eb-9242-839a49a18d51"
	if GetFriendList() != None:
		WalkNeighbours(friendlist)

def upload_stats():
	try:
		with open('drop_stats.txt','r') as statsfile:
			data=statsfile.read() #.replace('\n', '')
	except IOError:
		return

	home = httplib2.Http()
	print "Trying to submit drops stats! Please don't close me!..."
	resp, respc = home.request("http://home.elhana.ru/stats/update.php", "POST", data)
	if respc == 'OK':
		f = open('drop_stats.txt','w')
		f.close()
		print "Success!"
	else:
		print "Something went wrong, we'll try another time."


# If started as a script, do the stuff
if __name__ == "__main__":
	main()


### DEBUG STUFF ### 
# Packet debug
def test_harv():

	# Packet_0050_04
	bbuffer = proto.BinaryBuffer(0x50, 0x04)
	#500004672a00000002019d010000020dc5168c56a4d4410dc5161861a4d4419d010000020201040000000302000000000106
	bbuffer.write('\x2A\x00\x00\x00\x02\x01\x9D\x01\x00\x00\x02\x0D\xC5\x16\x8C\x56\xA4\xD4\x41\x0D\xC5\x16\x18\x61\xA4\xD4\x41\x9D\x01\x00\x00\x02\x02\x01\x04\x00\x00\x00\x03\x02\x00\x00\x00\x00\x01\x06')

	#500004671e0000000603050c00000001180000000709006c5f6d6f6e61726368010000000000
	#bbuffer.write('\x1E\x00\x00\x00\x06\x03\x05\x0C\x00\x00\x00\x01\x18\x00\x00\x00\x07\x09\x00\x6C\x5F\x6D\x6F\x6E\x61\x72\x63\x68\x01\x00\x00\x00\x00\x00')
	bbuffer.seek(0)
	packetLength = bbuffer.readUnsignedInt()
	harvest = proto.game.family_0050.Packet_0050_04(bbuffer)
	print harvest

	print "{"+"test"+":"+str(harvest.goodies)+"},"

	##print proto.SendCombo(harvest) # FAIL

def print_mods():
	# This logic looks right
	print "Hybrids:"
	for i in lang:
		if i in sb_harvest_list:
			print i, lang[i]			
		elif i[0:3] == 'sb_' and i[len(i)-4:len(i)-1] == 'mod':		
			print i, lang[i], "<< missing?"

	# There is exceptions, an_stegosaurus?
	print "Nano-animals:"
	for i in lang:
		if i in an_harvest_list:
			print i, lang[i]			
		elif (i[0:3] == 'an_' or i[0:3] == 'yn_') and (i[len(i)-3:len(i)] == 'anc' or i[len(i)-3:len(i)] == 'mod' or i[len(i)-4:len(i)-1] == 'mod'):		
			print i, lang[i], "<< missing?"

