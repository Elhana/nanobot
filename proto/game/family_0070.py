from proto.model import *

#Packet_0070_01 - Send gift, param1 = {PSendGift,,,}, param2 = gold??
class Packet_0070_01(object):
	def __init__(self, param1 = None, param2 = False):
		self.field_0 = param1
		self.field_1 = param2

	def write(self, param1):
		param1.family = 0x70
		param1.subfamily = 0x01
		if self.field_0 == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.field_0))
			for i in xrange(len(self.field_0)):
				self.field_0[i].write(param1)
		param1.writeBoolean(self.field_1)

#Packet_0070_02 - Send gift response
class Packet_0070_02(object):
	def __init__(self, param1):
		self.value = {}
		for i in xrange(param1.readUnsignedShort()):
			self.value[i] = PSendGiftUser()
			self.value[i].read(param1)

#Packet_0070_03 - get gift
class Packet_0070_03(object):
	def __init__(self, param1 = None):
		self.value = param1

	def write(self, param1):
		param1.family = 0x70
		param1.subfamily = 0x03
		if self.value == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.value))
			for i in xrange(len(self.value)):
				param1.writeInt(self.value[i])

#Packet_0070_03 - get gift response
class Packet_0070_04(object):
	RESULT_ELT_COLLECTION = 1
	RESULT_RESERVE = 0

	def __init__(self, param1):
		self.value = {}
		for i in xrange(param1.readUnsignedShort()):
			self.value[i] = PAcceptGift()
			self.value[i].read(param1)

# Packet_0070_05 - Wishlist add/remove.
class Packet_0070_05(object):
	REMOVE = 1
	ADD = 0

	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def write(self, param1):
		param1.family = 0x70
		param1.subfamily = 0x05
		param1.writeByte(self.variance)
		if self.variance == self.ADD:
			param1.writeUTF(self.value)
		elif self.variance == self.REMOVE:
			param1.writeUTF(self.value)
		else:
			raise Exception ("ERROR: incorrect Packet_0070_05 data")

# Packet_0070_06 - Wishlist ACK? (missing)

# Packet_0070_07 - GiftRefreshCommand
class Packet_0070_07(object):
	def __init__(self):
		pass

	def write(self, param1):
		param1.family = 0x70
		param1.subfamily = 0x07

# Packet_0070_08 - GiftRefresh response
class Packet_0070_08(object):
	def __init__(self, param1):
		self.gifts = {}
		for i in xrange(param1.readUnsignedShort()):
			self.gifts[i] = PGift()
			self.gifts[i].read(param1)

		self.warehouse = {}
		for i in xrange(param1.readUnsignedShort()):
			self.warehouse[i] = PWarehouseReserve()
			self.warehouse[i].read(param1)

		self.collections = {}
		i = param1.readUnsignedShort()
		while i > 0:
			parUTF = param1.readUTF()
			self.collections[parUTF] = PCollectionUser()
			self.collections[parUTF].read(param1)
