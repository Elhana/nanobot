import datetime
from proto.tuples import *
from pyamf.amf3 import ByteArray

### Model classes


# PCosts used by PSimpleGift
class PCosts(object):
	__slots__ = ['value']
	def __init__(self, param1 = None):
		self.value = param1

	def write(self, param1):
		if self.value == {}:
			param1.writeByte(0)
		else:
			param1.writeByte(len(self.value))
			for i in xrange(len(self.value)):
				self.value[i].write(param1)

	def read(self, param1):
		self.value = {}
		for i in xrange(param1.readUnsignedByte()):
			self.value[i] = PCost()
			self.value[i].read(param1)


# PGiftType used by PSimpleGift
class PGiftType(object):
	GIFT_BOXES = 11
	SUITCASES = 10
	COUPONS = 9
	TILES = 8
	Y_ANIMALS = 7
	SEEDS = 6
	DECORS = 5
	WEAPONS = 4
	ELT_COLLECTIONS = 3
	FERTILIZERS = 2
	ENERGIES = 1
	RARE_ITEMS = 0

	def __repr__(self):
		return str(self.variance)

	__slots__ = ['variance']
	def __init__(self, param1=0):
		self.variance = param1

	def write(self, param1):
		param1.writeByte(self.variance)

	def read(self, param1):
		self.variance = param1.readUnsignedByte()

# PSimpleGift used by PUserBase
class PSimpleGift(object):
	__slots__ = ['sg_kind', 'sg_type', 'sg_cnt', 'sg_game_event_id', 'sg_costs', 'sg_order']
	def __init__(self, param1="", param2 = None, param3 = 0, param4 = 0, param5 = None, param6 = 0):
		self.sg_kind = param1
		self.sg_type = param2
		self.sg_cnt = param3
		self.sg_game_event_id = param4
		self.sg_costs = param5
		self.sg_order = param6

	def __repr__(self):
		return	'PSimpleGift('+ \
				'sg_kind: "' + self.sg_kind.encode("utf-8")		+ '",' + \
				'sg_type: ' + str(self.sg_type) 					+ ',' + \
				'sg_cnt: ' + str(self.sg_cnt) 						+ ',' + \
				'sg_game_event_id: ' + str(self.sg_game_event_id) 	+ ',' + \
				'sg_costs: ' + str(self.sg_costs) 					+ ',' + \
				'sg_order: ' + str(self.sg_order) 					+ ',' + \
				')'

	def write(self, param1):
		param1.writeUTF(self.sg_kind)
		self.sg_type.write(param1)
		param1.writeInt(self.sg_cnt)
		if self.sg_game_event_id != None:
			param1.writeByte(1)
			param1.writeByte(self.sg_game_event_id)
		else:
			param1.writeByte(0)
		self.sg_costs.write(param1)
		param1.writeInt(self.sg_order)

	def read(self,param1):
		self.sg_kind = param1.readUTF()
		self.sg_type = PGiftType()
		self.sg_type.read(param1)
		self.sg_cnt = param1.readInt()
		if param1.readUnsignedByte() == 1:
			self.sg_game_event_id = param1.readUnsignedByte()
		else:
			self.sg_game_event_id = None
		self.sg_costs = PCosts()
		self.sg_costs.read(param1)
		self.sg_order = param1.readInt()

# PSkinParams used by PSkin
class PSkinParams(object):

	__slots__ = ['hair', 'shirt', 'pants']
	def __init__(self, param1 = None, param2 = None, param3 = None):
		self.hair = param1
		self.shirt = param2
		self.pants = param3

	def write(self, param1):
		param1.writeInt(self.hair)
		param1.writeInt(self.shirt)
		param1.writeInt(self.pants)

	def read(self, param1):
		self.hair = param1.readUnsignedInt()
		self.shirt = param1.readUnsignedInt()
		self.pants = param1.readUnsignedInt()

# PSkin used by PUserBase
class PSkin(object):
	PL_CLAN_PERS = 8
	PL_MASHA_BODY = 7
	PL_DENIS_BODY = 6
	PL_MASHA = 5
	PL_DENIS = 4
	PL_GIRLFRIEND = 3
	PL_FRIEND = 2
	PL_GIRL = 1
	PL_MAN = 0

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.PL_MAN:
			pass
		elif self.variance == self.PL_GIRL:
			pass
		elif self.variance == self.PL_FRIEND:
			pass
		elif self.variance == self.PL_GIRLFRIEND:
			pass
		elif self.variance == self.PL_DENIS:
			pass
		elif self.variance == self.PL_MASHA:
			pass
		elif self.variance == self.PL_DENIS_BODY:
			self.value.write(param1)
		elif self.variance == self.PL_MASHA_BODY:
			self.value.write(param1)
		elif self.variance == self.PL_CLAN_PERS:
			pass
		else:
			raise Exception ("ERROR: incorrect PSkin data")

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.PL_MAN:
			pass
		elif self.variance == self.PL_GIRL:
			pass
		elif self.variance == self.PL_FRIEND:
			pass
		elif self.variance == self.PL_GIRLFRIEND:
			pass
		elif self.variance == self.PL_DENIS:
			pass
		elif self.variance == self.PL_MASHA:
			pass
		elif self.variance == self.PL_DENIS_BODY:
			self.value = PSkinParams()
			self.value.read(param1)
		elif self.variance == self.PL_MASHA_BODY:
			self.value = PSkinParams()
			self.value.read(param1)
		elif self.variance == self.PL_CLAN_PERS:
			pass
		else:
			raise Exception ("ERROR: incorrect PSkin data")

# PUserBase
class PUserBase(object):
	__slots__ = ['user_id', 'name', 'avatar', 'profile_url', 'level', 'sex', 'wish_list', 'game_day_ago', 'can_post_wall', 'skin', 'segway_current', 'exp', 'viral_disabled', 'skin_spouse', 'vip', 'raid_id', 'raids_finished', 'clan', 'current_trade', 'child','team_build_info']
	def __init__(self, param1 = None, param2 = None, param3 = None, param4 = None, param5 = None, param6 = None, param7 = None, param8 = None, param9 = None, param10 = None, param11 = None, param12 = None, param13 = None, param14 = None, param15 = None, param16 = None, param17 = None, param18 = None, param19 = None, param20 = None):
		self.user_id = param1
		self.name = param2
		self.avatar = param3
		self.profile_url = param4
		self.level = param5
		self.sex = param6
		self.wish_list = param7
		self.game_day_ago = param8
		self.skin = param9
		self.segway_current = param10
		self.exp = param11
		self.viral_disabled = param12
		self.skin_spouse = param13
		self.vip = param14
		self.raid_id = param15
		self.raids_finished = param16
		self.clan = param17
		self.current_trade = param18
		self.child = param19
		self.team_build_info = param20

	def __str__(self):
		return	'PUserBase(user_id:%s,name:%s,level:%s,exp:%s)' % (str(self.user_id), self.name.encode("utf-8"), str(self.level), str(self.exp))

	def read(self, param1):
		self.user_id = param1.readUTF()
		self.name = param1.readUTF()
		self.avatar = param1.readUTF()
		self.profile_url = param1.readUTF()
		self.level = param1.readUnsignedShort()
		self.sex = param1.readUTF()
		self.wish_list = {}
		for i in xrange(param1.readUnsignedByte()):
			self.wish_list[i] = PSimpleGift()
			self.wish_list[i].read(param1)

		self.game_day_ago = param1.readInt()
		self.skin = PSkin()
		self.skin.read(param1)
		self.segway_current = param1.readUTF()
		self.exp = param1.readDouble()
		self.viral_disabled = param1.readBoolean()
		if param1.readUnsignedByte() == 1:
			self.skin_spouse = PSkin()
			self.skin_spouse.read(param1)
		else:
			self.skin_spouse = None

		self.vip = PVip()
		self.vip.read(param1)
		self.raid_id = param1.readUTF()
		self.raids_finished = {}
		for i in xrange (param1.readUnsignedShort()):
			self.raids_finished[i] = param1.readUTF()

		if param1.readUnsignedByte() == 1:
			self.clan = PClanBase()
			self.clan.read(param1)
		else:
			self.clan = None
		if param1.readUnsignedByte() == 1:
			self.current_trade = PTrade()
			self.current_trade.read(param1)
		else:
			self.current_trade = None

		if param1.readUnsignedByte() == 1:
			self.child = PChild()
			self.child.read(param1)
		else:
			self.child = None
		if param1.readUnsignedByte() == 1:
			self.team_build_info = PTeamBuildInfo()
			self.team_build_info.read(param1)
		else:
			self.team_build_info = None

	def write(self, param1):
		param1.writeUTF(self.user_id)
		param1.writeUTF(self.name)
		param1.writeUTF(self.avatar)
		param1.writeUTF(self.profile_url)
		param1.writeShort(self.level)
		param1.writeUTF(self.sex)

		if self.wish_list == {}:
			param1.writeByte(0)
		else:
			param1.writeByte(len(self.wish_list))
			for i in xrange(len(self.wish_list)):
				self.wish_list[i].write(param1)

		param1.writeInt(self.game_day_ago)
		self.skin.write(param1)
		param1.writeUTF(self.segway_current)
		param1.writeDouble(self.exp)
		param1.writeBoolean(self.viral_disabled)
		if self.skin_spouse != None:
			param1.writeByte(1)
			self.skin_spouse.write(param1)
		else:
			param1.writeByte(0)

		self.vip.write(param1)

		param1.writeUTF(self.raid_id)
		if self.raids_finished == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.raids_finished))
			for i in xrange(len(self.raids_finished)):
				param1.writeUTF(self.raids_finished[i])

		if self.clan != None:
			param1.writeByte(1)
			self.clan.write(param1)
		else:
			param1.writeByte(0)
		if self.current_trade != None:
			param1.writeByte(1)
			self.current_trade.write(param1)
		else:
			param1.writeByte(0)
		if self.child != None:
			param1.writeByte(1)
			self.child.write(param1)
		else:
			param1.writeByte(0)
		if self.team_build_info != None:
			param1.writeByte(1)
			self.team_build_info.write(param1)
		else:
			param1.writeByte(0)


# PFarm
class PFarm(object):
	__slots__ = ['init_time', 'objects', 'expansion', 'inactive_objects', 'actions', 'finished_formulas', 'studys', 'pets']
	def __init__ (self, param1=0, param2 = None, param3 = None, param4 = None, param5 = None, param6 = None, param7 = None, param8 = None):
		self.init_time = param1
		self.objects = param2
		self.expansion = param3
		self.inactive_objects = param4
		self.actions = param5
		self.finished_formulas = param6
		self.studys = param7 #science studies
		self.pets = param8

	def __str__(self):
		return "{init_time:%s,objects,expansion,inactive_objects,actions,finished_formulas,studys,pets}" % (self.init_time)
		#return "{\r\ninit_time:%s,\r\nobjects:%s,\r\nexpansion:%s,\r\ninactive_objects:%s,\r\nactions:%s,\r\nfinished_formulas:%s,\r\nstudys:%s,\r\npets:%s}" % (self.init_time, str(self.objects), str(self.expansion), str(self.inactive_objects), str(self.actions), str(self.finished_formulas), str(self.studys), str(self.pets))


	def write(self, param1):
		param1.writeDouble(self.init_time)

		if self.objects == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.objects))
			for i in xrange(len(self.objects)):
				self.objects[i].write(param1)

		if self.expansion == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.expansion))
			for i in xrange(len(self.expansion)):
				self.expansion[i].write(param1)

		if self.inactive_objects == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.inactive_objects))
			for i in xrange(len(self.inactive_objects)):
				self.inactive_objects[i].write(param1)

		if self.actions == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.actions))
			for i in xrange(len(self.actions)):
				self.actions[i].write(param1)

		if self.finished_formulas == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.finished_formulas))
			for i in xrange(len(self.finished_formulas)):
				self.finished_formulas[i].write(param1)

		if self.studys == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.studys))
			for i in xrange(len(self.studys)):
				self.studys[i].write(param1)

		if self.pets == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.pets))
			for i in xrange(len(self.pets)):
				self.pets[i].write(param1)

	def read(self, param1):
		self.init_time = param1.readDouble()
		self.objects = {}
		for i in xrange(param1.readUnsignedShort()):
			self.objects[i] = uint_a_PObj_a()
			self.objects[i].read(param1)

		self.expansion = {}
		for i in xrange(param1.readUnsignedShort()):
			self.expansion[i] = param1.readUnsignedByte()

		self.inactive_objects = {}
		for i in xrange(param1.readUnsignedShort()):
			self.inactive_objects[i] = uint_a_PObj_a()
			self.inactive_objects[i].read(param1)

		self.actions = {}
		for i in xrange(param1.readUnsignedShort()):
			self.actions[i] = PFarmAction()						# PFarmAction class
			self.actions[i].read(param1)

		self.finished_formulas = {}
		for i in xrange(param1.readUnsignedShort()):
			self.finished_formulas[i] = str_str_str()			# str_str_str class
			self.finished_formulas[i].read(param1)

		self.studys = {}
		for i in xrange(param1.readUnsignedShort()):
			self.studys[i] = PMyStudy()							# PMyStudy class
			self.studys[i].read(param1)

		self.pets = {}
		for i in xrange(param1.readUnsignedShort()):
			self.pets[i] = PPet()								# PPet class
			self.pets[i].read(param1)

#PObj used by PFarm
class PObj(object):
	OBJ_SPEC_CLAN_COFFER = 12
	OBJ_SPEC_CLAN_FAIR_BUILDING = 11
	OBJ_SPEC_DECOR_PUZZLE = 10
	OBJ_SPEC_GARBAGE = 9
	OBJ_SPEC_STONE = 8
	OBJ_SPEC_DECOR = 7
	OBJ_SPEC_TILE = 6
	OBJ_SPEC_MONSTER = 5
	OBJ_SPEC_FAIR_BUILDING = 4
	OBJ_SPEC_BUILDING = 3
	OBJ_SPEC_ANIMAL = 2
	OBJ_SPEC_SEEDBED = 1
	OBJ_SPEC_WOOD = 0

	__slots__ = ['obj_id', 'obj_pos', 'obj_kind', 'obj_direction', 'obj_map_id', 'obj_last_update_time', 'obj_spec_variance', 'obj_spec_value']
	def __init__ (self, param1 = 0, param2 = None, param3 = "", param4 = None, param5 = 0, param6 = 0, param7 = 0, param8 = None):
		self.obj_id = param1
		self.obj_pos = param2
		self.obj_kind = param3
		self.obj_direction = param4
		self.obj_map_id = param5
		self.obj_last_update_time = param6
		self.obj_spec_variance = param7
		self.obj_spec_value = param8

	def __repr__(self):
		return "\t{obj_id:%s,obj_pos:%s,obj_kind:%s,obj_direction:%s,obj_map_id:%s}\r\n" % (self.obj_id, str(self.obj_pos), self.obj_kind.encode("utf-8"), str(self.obj_direction), str(self.obj_map_id))
		#return "\r\n{obj_id:%s,obj_pos:%s,obj_kind:%s,obj_direction:%s,obj_map_id:%s,obj_spec_variance:%s,obj_spec_value:%s}" % (self.obj_id, str(self.obj_pos), self.obj_kind.encode("utf-8"), str(self.obj_direction), str(self.obj_map_id), str(self.obj_spec_variance), str(self.obj_spec_value))

	def read(self, param1):
		self.obj_id = param1.readUnsignedInt()
		self.obj_pos = Position()
		self.obj_pos.read(param1)
		self.obj_kind = param1.readUTF()
		self.obj_direction = PDirection()
		self.obj_direction.read(param1)
		if param1.readUnsignedByte() == 1:
			self.obj_map_id = param1.readInt()
		else:
			self.obj_map_id = None

		self.obj_last_update_time = param1.readDouble()
		self.obj_spec_variance = param1.readUnsignedByte()
		if self.obj_spec_variance == self.OBJ_SPEC_WOOD:
			self.obj_spec_value = PWood()
			self.obj_spec_value.read(param1)
		elif self.obj_spec_variance == self.OBJ_SPEC_SEEDBED:
			self.obj_spec_value = PSeedbed()
			self.obj_spec_value.read(param1)
		elif self.obj_spec_variance == self.OBJ_SPEC_ANIMAL:
			self.obj_spec_value = PAnimalObj()
			self.obj_spec_value.read(param1)
		elif self.obj_spec_variance == self.OBJ_SPEC_BUILDING:
			self.obj_spec_value = PBuilding()
			self.obj_spec_value.read(param1)
		elif self.obj_spec_variance == self.OBJ_SPEC_FAIR_BUILDING:
			self.obj_spec_value = PFairBuilding()
			self.obj_spec_value.read(param1)
		elif self.obj_spec_variance == self.OBJ_SPEC_MONSTER:
			self.obj_spec_value = PMonster()
			self.obj_spec_value.read(param1)
		elif self.obj_spec_variance == self.OBJ_SPEC_TILE:
			pass
		elif self.obj_spec_variance == self.OBJ_SPEC_DECOR:
			pass
		elif self.obj_spec_variance == self.OBJ_SPEC_STONE:
			self.obj_spec_value = PStone()
			self.obj_spec_value.read(param1)
		elif self.obj_spec_variance == self.OBJ_SPEC_GARBAGE:
			self.obj_spec_value = PGarbage()
			self.obj_spec_value.read(param1)
		elif self.obj_spec_variance == self.OBJ_SPEC_DECOR_PUZZLE:
			self.obj_spec_value = PDecorPuzzle()
			self.obj_spec_value.read(param1)
		elif self.obj_spec_variance == self.OBJ_SPEC_CLAN_FAIR_BUILDING:
			self.obj_spec_value = PClanFairBuilding()
			self.obj_spec_value.read(param1)
		elif self.obj_spec_variance == self.OBJ_SPEC_CLAN_COFFER:
			pass
		else:
			raise Exception ("ERROR: incorrect PObj data")

	def write(self, param1):
		param1.writeInt(self.obj_id)
		self.obj_pos.write(param1)
		param1.writeUTF(self.obj_kind)
		self.obj_direction.write(param1)
		if self.obj_map_id != None:
			param1.writeByte(1)
			param1.writeInt(self.obj_map_id)
		else:
			param1.writeByte(0)
		param1.writeDouble(self.obj_last_update_time)
		param1.writeByte(self.obj_spec_variance)
		if self.obj_spec_variance == self.OBJ_SPEC_WOOD:
			self.obj_spec_value.write(param1)
		elif self.obj_spec_variance == self.OBJ_SPEC_SEEDBED:
			self.obj_spec_value.write(param1)
		elif self.obj_spec_variance == self.OBJ_SPEC_ANIMAL:
			self.obj_spec_value.write(param1)
		elif self.obj_spec_variance == self.OBJ_SPEC_BUILDING:
			self.obj_spec_value.write(param1)
		elif self.obj_spec_variance == self.OBJ_SPEC_FAIR_BUILDING:
			self.obj_spec_value.write(param1)
		elif self.obj_spec_variance == self.OBJ_SPEC_MONSTER:
			self.obj_spec_value.write(param1)
		elif self.obj_spec_variance == self.OBJ_SPEC_TILE:
			pass
		elif self.obj_spec_variance == self.OBJ_SPEC_DECOR:
			pass
		elif self.obj_spec_variance == self.OBJ_SPEC_STONE:
			self.obj_spec_value.write(param1)
		elif self.obj_spec_variance == self.OBJ_SPEC_GARBAGE:
			self.obj_spec_value.write(param1)
		elif self.obj_spec_variance == self.OBJ_SPEC_DECOR_PUZZLE:
			self.obj_spec_value.write(param1)
		elif self.obj_spec_variance == self.OBJ_SPEC_CLAN_FAIR_BUILDING:
			self.obj_spec_value.write(param1)
		elif self.obj_spec_variance == self.OBJ_SPEC_CLAN_COFFER:
			pass
		else:
			raise Exception ("ERROR: incorrect PObj data")


# Position used by PObj
# Position is a core.Position in original client
class Position(object):
	__slots__ = ['x', 'y', 'raw_x', 'raw_y', 'x', 'y', 'x', 'y', 'x', 'y', 'x', 'y', 'y', 'x', 'x', 'y']
	def __init__(self, param1 = 0, param2 = 0):
		self.x = param1
		self.y = param2
		self.raw_x = param1
		self.raw_y = param2
	def clone(self):
		return Position(self.x, self.y)
	def clone_d(self, param1, param2):
		return Position(self.x + param1, self.y + param2)
	def equal(self, param1, param2):
		return (self.x == param1) and (self.y == param2)
	def equal_p(self, param1):
		return (self.x == param1.x) and (self.y == param1.y)
	def write(self, param1):
		raw_x = (-self.y - 1 << 1) + 1
		raw_y = self.x - ((raw_x + 1) >> 1) + raw_x
		if (raw_y & 1) == 0:
			raw_x = raw_x + 1
		param1.writeShort(raw_y - raw_x >> 1)
		param1.writeShort(raw_y)
	def __str__(self):
		return '{\'x\':%s,\'y\':%s}'% (self.x, self.y)

	def read(self, param1):
		raw_x = param1.readShort()
		raw_y = param1.readShort()
		raw_t = raw_y - (raw_x << 1)
		if (raw_y & 1) == 0:
			raw_t = raw_t - 1

		self.x = ((raw_t + 1) >> 1) + (raw_y - raw_t)
		self.y = -((raw_t >> 1) + 1)

	#not sure if self is ok
	def clientToServer(self,args):
		raw_x = 0
		raw_y = 0
		if len(args) == 1:
			raw_x = args[0].x
			raw_y = args[0].y
		else:
			raw_x = args[0]
			raw_y = args[1]

		args = (-raw_y - 1 << 1) + 1
		raw_t = raw_x - ((args + 1) >> 1) + args
		if (raw_t & 1) == 0:
			args = args + 1
		new_pos = Position(raw_t - args >> 1, raw_t)
		return new_pos

	#not sure if self is ok
	def serverToClient(self, param1, param2):
		raw_t = param2 - (param1 << 1)
		if (param2 & 1) == 0:
			raw_t = raw_t - 1

		new_pos = Position(((raw_t + 1) >> 1) + (param2 - raw_t), -((raw_t >> 1) + 1))
		return new_pos

# PDirection used by PObj
class PDirection(object):
	RIGHT_DOWN = 3
	RIGHT_UP = 2
	LEFT_DOWN = 1 # this directions are fucked up!!
	LEFT_UP = 0

	__slots__ = ['variance']
	def __init__(self, param1 = 0):
		self.variance = param1

	def __repr__(self):
		return str(self.variance)

	def write(self, param1):
		param1.writeByte(self.variance)

	def read(self, param1):
		self.variance = param1.readUnsignedByte()

# PWood used by PObj
class PWood (object):
	__slots__ = ['resource', 'max_resource', 'fertilizers']
	def __init__(self, param1 = 0, param2 = 0, param3 = None):
		self.resource = param1
		self.max_resource = param2
		self.fertilizers = param3

	def __repr__(self):
		return "{resource:%s,max_resource:%s,fertilizers:%s}" % (self.resource, self.max_resource, self.fertilizers)

	def write(self, param1):
		param1.writeInt(self.resource)
		param1.writeInt(self.max_resource)
		if self.fertilizers == None:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.fertilizers))
			for i in xrange(len(self.fertilizers)):
				param1.writeUTF(self.fertilizers[i])

	def read(self, param1):
		self.resource = param1.readUnsignedInt()
		self.max_resource = param1.readUnsignedInt()
		self.fertilizers = {}
		for i in xrange(param1.readUnsignedShort()):
			self.fertilizers[i] = param1.readUTF()

# PSeedbed used by PObj
class PSeedbed(object):
	__slots__ = ['state', 'fertilizers', 'planting_time', 'boost_growth']
	def __init__(self, param1 = None, param2 = None, param3 = 0, param4 = False):
		self.state = param1
		self.fertilizers = param2
		self.planting_time = param3
		self.boost_growth = param4

	def __repr__(self):
		return "{state:%s,fertilizers:%s,planting_time:%s,boost_growth:%s}" % (self.state, self.fertilizers, self.planting_time, self.boost_growth) #datetime.datetime.fromtimestamp() is wrong

	def write(self, param1):
		self.state.write(param1)
		if self.fertilizers == None:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.fertilizers))
			for i in xrange(len(self.fertilizers)):
				self.fertilizers[i].write(param1)
		param1.writeDouble(self.planting_time)
		param1.writeBoolean(self.boost_growth)

	def read(self, param1):
		self.state = PSeedbedState()
		self.state.read(param1)
		self.fertilizers = {}
		for i in xrange(param1.readUnsignedShort()):
			self.fertilizers[i] = str_i()				# str_i
			self.fertilizers[i].read(param1)
		self.planting_time = param1.readDouble()
		self.boost_growth = param1.readBoolean()

# PSeedbedState used by PSeedbed
class PSeedbedState(object):
	WITHERED = 2
	FULLY_GROWN = 1
	GROWN = 0

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def __repr__(self):
		return "{variance:%s,value:%s}" % (self.variance, self.value)

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.GROWN:
			param1.writeInt(self.value)
		elif self.variance == self.FULLY_GROWN:
			return
		elif self.variance == self.WITHERED:
			return
		else:
			raise Exception ("ERROR: incorrect PSeedbedState data")

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.GROWN:
			self.value = param1.readInt()
		elif self.variance == self.FULLY_GROWN:
			return
		elif self.variance == self.WITHERED:
			return
		else:
			raise Exception ("ERROR: incorrect PSeedbedState data")


# PAnimalObj used by PObj
class PAnimalObj(object):
	__slots__ = ['id', 'kind', 'state', 'last_feed_time', 'fertilizers', 'boost_growth']
	def __init__(self, param1 = 0, param2 = "", param3 = None, param4 = 0, param5 = None, param6 = False):
		self.id = param1
		self.kind = param2
		self.state = param3
		self.last_feed_time = param4
		self.fertilizers = param5
		self.boost_growth = param6

	def __repr__(self):
		return "{id:%s,kind:%s,state:%s,last_feed_time:%s,fertilizers:%s,boost_growth:%s}" % (self.id, self.kind, self.state, self.last_feed_time, self.fertilizers, self.boost_growth)

	def write(self, param1):
		param1.writeInt(self.id)
		param1.writeUTF(self.kind)
		self.state.write(param1)
		param1.writeDouble(self.last_feed_time)
		if self.fertilizers == None:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.fertilizers))
			for i in xrange(len(self.fertilizers)):
				param1.writeUTF(self.fertilizers[i])
		param1.writeBoolean(self.boost_growth)

	def read(self, param1):
		self.id = param1.readUnsignedInt()
		self.kind = param1.readUTF()
		self.state = PAnimalState()
		self.state.read(param1)
		self.last_feed_time = param1.readDouble()
		self.fertilizers = {}
		for i in xrange(param1.readUnsignedShort()):
			self.fertilizers[i] = param1.readUTF()
		self.boost_growth = param1.readBoolean()

# PAnimalState used by PAnimalObj
class PAnimalState(object):
	BIG = 2
	MIDDLE = 1
	LITTLE = 0

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def __repr__(self):
		return "{variance:%s,value:%s}" % (self.variance, self.value)

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.LITTLE:
			param1.writeByte(self.value)
		elif self.variance == self.MIDDLE:
			param1.writeByte(self.value)
		elif self.variance == self.BIG:
			return
		else:
			raise Exception ("ERROR: incorrect PAnimalState data")

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.LITTLE:
			self.value = param1.readUnsignedByte()
		elif self.variance == self.MIDDLE:
			self.value = param1.readUnsignedByte()
		elif self.variance == self.BIG:
			return
		else:
			raise Exception ("ERROR: incorrect PAnimalState data")

# PBuilding used by PObj
class PBuilding (object):
	__slots__ = ['building_state', 'can_collect_bonus', 'level', 'facilities']
	def __init__(self, param1 = None , param2 = False, param3 = 0, param4 = None):
		self.building_state = param1
		self.can_collect_bonus = param2
		self.level = param3
		self.facilities = param4

	#def __repr__(self):
	#	return "{building_state:%s,can_collect_bonus:%s,level:%s,facilities:}" % (self.building_state, self.can_collect_bonus, self.level,self.facilities)

	def write(self, param1):
		self.building_state.write(param1)
		param1.writeBoolean(self.can_collect_bonus)
		param1.writeInt(self.level)
		if self.facilities == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.facilities))
			for i in xrange(len(self.facilities)):
				self.facilities[i].write(param1)

	def read(self, param1):
		self.building_state = PBuildingState()
		self.building_state.read(param1)
		self.can_collect_bonus = param1.readBoolean()
		self.level = param1.readInt()
		self.facilities = {}
		for i in xrange(param1.readUnsignedShort()):
			self.facilities[i] = PBuildingFacility()
			self.facilities[i].read(param1)


# PBuildingState used by PBuilding
class PBuildingState(object):
	FINISHED = 1
	STEP = 0

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = -1, param2 = None):
		self.variance = param1
		self.value = param2

	def __repr__(self):
		return "{variance:%s,value:%s}" % (self.variance, self.value)

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.STEP:
			param1.writeByte(self.value)
		elif self.variance == self.FINISHED:
			return
		else:
			raise Exception ("ERROR: incorrect PBuildingState data")

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.STEP:
			self.value = param1.readUnsignedByte()
		elif self.variance == self.FINISHED:
			return
		else:
			raise Exception ("ERROR: incorrect PBuildingState data")

# PMonster class
class PMonster (object):
	__slots__ = ['stamina', 'max_stamina', 'distance']
	def __init__(self, param1 = 0, param2=0, param3=0):
		self.stamina = param1
		self.max_stamina = param2
		self.distance = param3

	def __repr__(self):
		return "{stamina:%s,max_stamina:%s,distance:%s}" % (self.stamina, self.max_stamina, self.distance)

	def write(self, param1):
		param1.writeByte(self.stamina)
		param1.writeByte(self.max_stamina)
		param1.writeByte(self.distance)

	def read(self, param1):
		self.stamina = param1.readUnsignedByte()
		self.max_stamina = param1.readUnsignedByte()
		self.distance = param1.readUnsignedByte()


# PStone class
class PStone(object):
	__slots__ = ['resource', 'max_resource', 'fertilizers']
	def __init__(self, param1 = 0, param2 = 0, param3 = None):
		self.resource = param1
		self.max_resource = param2
		self.fertilizers = param3

	def __repr__(self):
		return "{resource:%s,max_resource:%s,fertilizers:%s}" % (self.resource, self.max_resource, self.fertilizers)

	def write(self, param1):
		param1.writeByte(self.resource)
		param1.writeByte(self.max_resource)
		if self.fertilizers == None:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.fertilizers))
			for i in xrange(len(self.fertilizers)):
				param1.writeUTF(self.fertilizers[i])

	def read(self, param1):
		self.resource = param1.readUnsignedByte()
		self.max_resource = param1.readUnsignedByte()
		self.fertilizers = {}
		for i in xrange(param1.readUnsignedShort()):
			self.fertilizers[i] = param1.readUTF()

# PGarbage class
class PGarbage(object):
	__slots__ = ['resource', 'max_resource']
	def __init__(self, param1 = 0, param2 = 0):
		self.resource = param1
		self.max_resource = param2

	def __repr__(self):
		return "{resource:%s,max_resource:%s}" % (self.resource, self.max_resource)

	def write(self, param1):
		param1.writeByte(self.resource)
		param1.writeByte(self.max_resource)

	def read(self, param1):
		self.resource = param1.readUnsignedByte()
		self.max_resource = param1.readUnsignedByte()

# PDecorPuzzle class
class PDecorPuzzle(object):
	__slots__ = ['decor_puzzle_state']
	def __init__(self, param1 = None):
		self.decor_puzzle_state = param1

	def __repr__(self):
		return "{decor_puzzle_state:%s}" % (self.decor_puzzle_state)

	def write(self, param1):
		self.decor_puzzle_state.write(param1)

	def read(self, param1):
		self.decor_puzzle_state = PBuildingState()
		self.decor_puzzle_state.read(param1)

# PFarmAction used by PFarm
class PFarmAction(object):
	__slots__ = ['time', 'object_id', 'log']
	def __init__(self, param1 = 0, param2 = 0, param3 = None):
		self.time = param1
		self.object_id = param2
		self.log = param3

	def __repr__(self):
		return "{time:%s,object_id:%s,log:%s}" % (datetime.datetime.fromtimestamp(self.time), self.object_id, self.log)

	def write(self, param1):
		param1.writeDouble(self.time)
		param1.writeInt(self.object_id)
		self.log.write(param1)

	def read(self, param1):
		self.time = param1.readDouble()
		self.object_id = param1.readUnsignedInt()
		self.log = PFarmActionLog()
		self.log.read(param1)

# PFarmActionLog used by PFarmAction
class PFarmActionLog(object):
	FERTILIZER = 11
	MAGIC_ACTION_FINISH = 10
	HUNGRY_PET = 9
	P_EXIST_CLAN_STATE = 8
	P_EXIST_USER_STATE = 7
	P_EXIST_USER_SEGWAY = 6
	P_EXEC_STIMULATE_PAY = 5
	P_EXEC_STUDY = 4
	BUILDING_BONUS_READY = 3
	EXEC_FORMULA = 2
	HUNGRY_ANIMAL = 1
	SEEDBED = 0

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def __repr__(self):
		return "{variance:%s,value:%s}" % (self.variance, self.value)

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.SEEDBED:
			self.value.write(param1)
		elif self.variance == self.HUNGRY_ANIMAL:
			pass
		elif self.variance == self.EXEC_FORMULA:
			self.value.write(param1)
		elif self.variance == self.BUILDING_BONUS_READY:
			pass
		elif self.variance == self.P_EXEC_STUDY:
			self.value.write(param1)
		elif self.variance == self.P_EXEC_STIMULATE_PAY:
			self.value.write(param1)
		elif self.variance == self.P_EXIST_USER_SEGWAY:
			param1.writeUTF(self.value)
		elif self.variance == self.P_EXIST_USER_STATE:
			self.value.write(param1)
		elif self.variance == self.P_EXIST_CLAN_STATE:
			self.value.write(param1)
		elif self.variance == self.HUNGRY_PET:
			pass
		elif self.variance == self.MAGIC_ACTION_FINISH:
			self.value.write(param1)
		elif self.variance == self.FERTILIZER:
			param1.writeUTF(self.value)
		else:
			raise Exception ("ERROR: incorrect PFarmActionLog data")

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.SEEDBED:
			self.value = PSeedbedState()
			self.value.read(param1)
		elif self.variance == self.HUNGRY_ANIMAL:
			pass
		elif self.variance == self.EXEC_FORMULA:
			self.value = str_str_str()
			self.value.read(param1)
		elif self.variance == self.BUILDING_BONUS_READY:
			pass
		elif self.variance == self.P_EXEC_STUDY:
			self.value = PMyStudy()
			self.value.read(param1)
		elif self.variance == self.P_EXEC_STIMULATE_PAY:
			self.value = str_oi()
			self.value.read(param1)
		elif self.variance == self.P_EXIST_USER_SEGWAY:
			self.value = param1.readUTF()
		elif self.variance == self.P_EXIST_USER_STATE:
			self.value = PStateKind()
			self.value.read(param1)
		elif self.variance == self.P_EXIST_CLAN_STATE:
			self.value = PStateKind()
			self.value.read(param1)
		elif self.variance == self.HUNGRY_PET:
			pass
		elif self.variance == self.MAGIC_ACTION_FINISH:
			self.value = PMagicActionKind()
			self.value.read(param1)
		elif self.variance == self.FERTILIZER:
			self.value = param1.readUTF()
		else:
			raise Exception ("ERROR: incorrect PFarmActionLog data")

# PMyStudy class
class PMyStudy(object):
	__slots__ = ['kind', 'degree']
	def __init__(self, param1 = "", param2 = 0):
		self.kind = param1
		self.degree = param2

	def __repr__(self):
		return "\r\n{kind:%s,degree:%s}" % (self.kind, self.degree)

	def write(self, param1):
		param1.writeUTF(self.kind)
		param1.writeByte(self.degree)

	def read(self, param1):
		self.kind = param1.readUTF()
		self.degree = param1.readUnsignedByte()


# PPet class
class PPet(object):
	__slots__ = ['pt_id', 'pt_kind']
	def __init__(self, param1 = 0, param2 = ""):
		self.pt_id = param1
		self.pt_kind = param2

	def __repr__(self):
		return "{pt_id:%s,pt_kind:%s}" % (str(self.pt_id), self.pt_kind)

	def write(self, param1):
		param1.writeInt(self.pt_id)
		param1.writeUTF(self.pt_kind)

	def read(self,param1):
		self.pt_id = param1.readUnsignedInt()
		self.pt_kind = param1.readUTF()


# PSign
class PSign(object):
	__slots__ = ['sign_key', 'sign']
	def __init__(self, param1 = "", param2 = ""):
		self.sign_key = param1
		self.sign = param2

	def __repr__(self):
		return "{sign_key:%s,sign:%s}" % (self.sign_key, self.sign)

	def write(self, param1):
		param1.writeUTF(self.sign_key)
		param1.writeUTF(self.sign)

	def read(self, param1):
		self.sign_key = param1.readUTF()
		self.sign = param1.readUTF()

# PResFeedAnimal used by PFresData
class PResFeedAnimal(object):
	__slots__ = ['id', 'state', 'last_feed_time', 'action']
	def __init__(self, param1 = 0, param2 = None, param3 = 0, param4 = None):
		self.id = param1
		self.state = param2
		self.last_feed_time = param3
		self.action = param4

	def __repr__(self):
		return "{id:%s,state:%s,last_feed_time:%s,action:%s}" % (self.id, self.state, datetime.datetime.fromtimestamp(self.last_feed_time), self.action)

	def write(self, param1):
		param1.writeInt(self.id)
		self.state.write(param1)
		param1.writeDouble(self.last_feed_time)
		self.action.write(param1)

	def read(self, param1):
		self.id = param1.readUnsignedInt()
		self.state = PAnimalState()
		self.state.read(param1)
		self.last_feed_time = param1.readDouble()
		self.action = PFarmAction()
		self.action.read(param1)

# PResDropWood
class PResDropWood(object):
	WOOD_DROP = 1
	WOOD_RESOURCE = 0

	__slots__ = ['id', 'wood_variance', 'wood_value', 'monster']
	def __init__(self, param1 = 0, param2 = None, param3 = None, param4 = None):
		self.id = param1
		self.wood_variance = param2
		self.wood_value = param3
		self.monster = param4

	def __repr__(self):
		return "{id:%s,wood_variance:%s,wood_value:%s,monster:%s}" % (self.id, self.wood_variance, self.wood_value, self.monster)

	def write(self, param1):
		param1.writeInt(self.id)
		param1.writeByte(self.wood_variance)
		if self.wood_variance == self.WOOD_RESOURCE:
			param1.writeInt(self.wood_value)
		elif self.wood_variance == self.WOOD_DROP:
			assert self.wood_value == None, 'wood_variance == WOOD_DROP, but wood_value is not None'
		else:
			raise Exception ("ERROR: incorrect wood_variance data")

		if self.monster != None:
			param1.writeByte(1)
			self.monster.write(param1)
		else:
			param1.writeByte(0)

	def read(self,param1):
		self.id = param1.readUnsignedInt()
		self.wood_variance = param1.readUnsignedByte()
		if self.wood_variance == self.WOOD_RESOURCE:
			self.wood_value = param1.readInt()
		elif self.wood_variance == self.WOOD_DROP:
			self.wood_value = None
		else:
			raise Exception ("ERROR: incorrect wood_variance data")
		if param1.readUnsignedByte() == 1:
			self.monster = PObj()
			self.monster.read(param1)
		else:
			self.monster = None

# PResDropStone
class PResDropStone(object):
	STONE_DROP = 1
	STONE_RESOURCE = 0

	__slots__ = ['id', 'stone_variance', 'stone_value', 'monster']
	def __init__(self, param1 = 0, param2 = None, param3 = None, param4 = None):
		self.id = param1
		self.stone_variance = param2
		self.stone_value = param3
		self.monster = param4

	def __repr__(self):
		return "{is:%s,stone_variance:%s,stone_value:%s,monster:%s}" % (self.id, self.stone_variance, self.stone_value, self.monster)

	def write(self, param1):
		param1.writeInt(self.id)
		param1.writeByte(self.stone_variance)
		if self.stone_variance == self.STONE_RESOURCE:
			param1.writeInt(self.stone_value)
		elif self.stone_variance == self.STONE_DROP:
			assert self.stone_value == None, 'stone_variance == STONE_DROP, but stone_value is not None'
		else:
			raise Exception ("ERROR: incorrect stone_variance data")

		if self.monster != None:
			param1.writeByte(1)
			self.monster.write(param1)
		else:
			param1.writeByte(0)

	def read(self,param1):
		self.id = param1.readUnsignedInt()
		self.stone_variance = param1.readUnsignedByte()
		if self.stone_variance == self.STONE_RESOURCE:
			self.stone_value = param1.readInt()
		elif self.stone_variance == self.STONE_DROP:
			self.stone_value = None
		else:
			raise Exception ("ERROR: incorrect stone_variance data")
		if param1.readUnsignedByte() == 1:
			self.monster = PObj()
			self.monster.read(param1)
		else:
			self.monster = None

# PResDropGarbage
class PResDropGarbage(object):
	GARBAGE_DROP = 1
	GARBAGE_RESOURCE = 0

	__slots__ = ['id', 'garbage_variance', 'garbage_value', 'monster']
	def __init__(self, param1 = 0, param2 = None, param3 = None, param4 = None):
		self.id = param1
		self.garbage_variance = param2
		self.garbage_value = param3
		self.monster = param4

	def __repr__(self):
		return "{is:%s,garbage_variance:%s,garbage_value:%s,monster:%s}" % (self.id, self.garbage_variance, self.garbage_value, self.monster)

	def write(self, param1):
		param1.writeInt(self.id)
		param1.writeByte(self.garbage_variance)
		if self.garbage_variance == self.GARBAGE_RESOURCE:
			param1.writeInt(self.garbage_value)
		elif self.garbage_variance == self.GARBAGE_DROP:
			assert self.garbage_value == None, 'garbage_variance == GARBAGE_DROP, but garbage_value is not None'
		else:
			raise Exception ("ERROR: incorrect garbage_variance data")

		if self.monster != None:
			param1.writeByte(1)
			self.monster.write(param1)
		else:
			param1.writeByte(0)

	def read(self,param1):
		self.id = param1.readUnsignedInt()
		self.garbage_variance = param1.readUnsignedByte()
		if self.garbage_variance == self.GARBAGE_RESOURCE:
			self.garbage_value = param1.readInt()
		elif self.garbage_variance == self.GARBAGE_DROP:
			self.garbage_value = None
		else:
			raise Exception ("ERROR: incorrect garbage_variance data")
		if param1.readUnsignedByte() == 1:
			self.monster = PObj()
			self.monster.read(param1)
		else:
			self.monster = None

#PCost used by Packet0020
class PCost(object):
	CLAN_EXP = 11
	CLAN_GOLD = 10
	CLAN_ENERGY = 9
	RAID_RARE = 8
	ELT_COLLECTION = 7
	RARE_ITEM = 6
	EXP = 5
	ENERGY = 4
	FOOD = 3
	GOLD = 2
	BAKS = 1
	TIMBER = 0
	to_str = {8:"RAID_RARE", 7:"ELT_COLLECTION", 6:"RARE_ITEM", 5:"EXP", 4:"ENERGY", 3:"FOOD", 2:"GOLD", 1:"BAKS", 0:"TIMBER"}

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = None, param2 = None):
		self.variance = param1
		self.value = param2

	def __repr__(self):
		return "{'variance':%s,'value':%s}" % (self.variance, self.value)

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.TIMBER:
			param1.writeInt(self.value)
		elif self.variance == self.BAKS:
			param1.writeInt(self.value)
		elif self.variance == self.GOLD:
			param1.writeInt(self.value)
		elif self.variance == self.FOOD:
			param1.writeInt(self.value)
		elif self.variance == self.ENERGY:
			param1.writeInt(self.value)
		elif self.variance == self.EXP:
			param1.writeInt(self.value)
		elif self.variance == self.RARE_ITEM:
			self.value.write(param1)
		elif self.variance == self.ELT_COLLECTION:
			self.value.write(param1)
		elif self.variance == self.RAID_RARE:
			self.value.write(param1)
		elif self.variance == self.CLAN_ENERGY:
			self.value.write(param1)
		elif self.variance == self.CLAN_GOLD:
			self.value.write(param1)
		elif self.variance == self.CLAN_EXP:
			self.value.write(param1)
		else:
			raise Exception ("ERROR: incorrect PCost data")

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.TIMBER:
			self.value = param1.readInt()
		elif self.variance == self.BAKS:
			self.value = param1.readInt()
		elif self.variance == self.GOLD:
			self.value = param1.readInt()
		elif self.variance == self.FOOD:
			self.value = param1.readInt()
		elif self.variance == self.ENERGY:
			self.value = param1.readInt()
		elif self.variance == self.EXP:
			self.value = param1.readInt()
		elif self.variance == self.RARE_ITEM:
			self.value = PRare()
			self.value.read(param1)
		elif self.variance == self.ELT_COLLECTION:
			self.value = PEltCollection()
			self.value.read(param1)
		elif self.variance == self.RAID_RARE:
			self.value = PRaidRare()
			self.value.read(param1)
		elif self.variance == self.CLAN_ENERGY:
			self.value = param1.readInt()
		elif self.variance == self.CLAN_GOLD:
			self.value = param1.readInt()
		elif self.variance == self.CLAN_EXP:
			self.value = param1.readInt()
		else:
			raise Exception ("ERROR: incorrect PCost data")

# PRare used by PCost
class PRare(object):
	__slots__ = ['rare_kind', 'rare_cnt']
	def __init__(self, param1 = "", param2 = 0):
		self.rare_kind = param1
		self.rare_cnt = param2

	def __repr__(self):
		return "{'rare_kind':'%s','rare_cnt':%s}" % (self.rare_kind, self.rare_cnt)

	def read (self, param1):
		self.rare_kind = param1.readUTF()
		self.rare_cnt = param1.readInt()

	def write (self, param1):
		param1.writeUTF(self.rare_kind)
		param1.writeInt(self.rare_cnt)

# PRaidRare
class PRaidRare(object):
	__slots__ = ['r_kind', 'r_cnt']
	def __init__(self, param1 = "", param2 = 0):
		self.r_kind = param1
		self.r_cnt = param2

	def __repr__(self):
		return "{'r_kind':'%s','r_cnt':%s}" % (self.r_kind, self.r_cnt)

	def write(self, param1):
		param1.writeUTF(self.r_kind)
		param1.writeInt(self.r_cnt)

	def read(self, param1):
		self.r_kind = param1.readUTF()
		self.r_cnt = param1.readUnsignedInt()

# PEltCollection
class PEltCollection(object):
	__slots__ = ['ec_kind', 'ec_cnt']
	def __init__(self, param1 = "", param2 = 0):
		self.ec_kind = param1
		self.ec_cnt = param2

	def __repr__(self):
		return "{'ec_kind':'%s','ec_cnt':%s}" % (self.ec_kind, self.ec_cnt)

	def write(self, param1):
		param1.writeUTF(self.ec_kind)
		param1.writeInt(self.ec_cnt)

	def read(self, param1):
		self.ec_kind = param1.readUTF()
		self.ec_cnt = param1.readInt()

# PUserEvent used by 0020_03
class PUserEvent(object):
	BUY_FAIR_BUILDING = 89
	EFARM_BONUS = 88
	TEAM_BUILD_DECLINE = 87
	TEAM_BUILD_APPROVED = 86
	TEAM_BUILD_NEW = 85	
	NEW_TRADE_OFFER = 84
	TRADE_BUY_OFFER = 83
	INIT_TRADE_EVENT = 82
	EXPIRE_QUEST = 81
	BUY_RESERVES = 80
	CLAN_RAID_JOIN = 79
	CLAN_RAID_FINISH = 78
	FINISH_DISTRIBUTION_CONTRACT_PRIZES = 77
	STAGE = 76
	CLAN_CHANGE_LEVEL = 75
	CLAN_COFFER_ENABLE = 74
	OFFER_RESULT = 73
	SHARE_CONTRACT_WIN = 72
	SHARE_CONTRACT_FIRST = 71
	CONTRACT_RATING_PRIZE = 70
	UPDATE_CONTRACTS = 69
	UPDATE_CONTRACT = 68
	CLAN_BONUS_PRIZE = 67
	CLAN_BONUS_CHANGE = 66
	RESTART_GAME = 65
	FINISH_SEND_GIFTS = 64
	BUY_PRODUCT_OFFER = 63
	CLAN_WAREHOUSE_FAIR_BUILDINGS = 62
	CLAN_CHANGE_TARGETS_GAME_EVENT = 61
	CLAN_FAIR_BUILDING_BONUS = 60
	BAD_OBJS_IN_WAREHOUSE = 59
	CLOSE_OFFER = 58
	START_OFFER = 57
	SHARE_CLAN_GOLD = 56
	CLAN_DISSOLUTION = 55
	CLAN_INVITE = 54
	CLAN_CHANGE_TARGETS_LEVEL = 53
	CLAN_SET_ROLE_MEMBER = 52
	CLAN_WAREHOUSE = 51
	REMOVED_FROM_CLAN = 50
	PROGRESS_TARGETS_CLAN = 49
	ACCEPTED_INTO_CLAN = 48
	CLAN_EXP = 47
	CLAN_GOLD = 46
	CLAN_ENERGY = 45
	GOLD_GIFT_CASE_PROGRESS = 44
	ALREADY_GET_PRIZE_FOR_PROMO = 43
	ENTER_PROMO_CODE = 42
	SHARE_BYPASS_IN_FAIR_RATING = 41
	RESET_BUILDING_BONUS = 40
	ALARM = 39
	GOLD_MINE_MIGRATION = 38
	RAID_ACCEPT = 37
	KICK_FROM_RAID = 36
	GIFT_FROM_ADMIN = 35
	FINISHED_LEVEL_ACCELERATION_ACTION = 34
	START_LEVEL_ACCELERATION_ACTION = 33
	ASK_GIFT = 32
	STORY_LINE = 31
	ADD_RESERVE = 30
	RELOAD = 29
	RARE_RECEIVED = 28
	ADD_PET = 27
	FINISH_COMBO = 26
	FINISH_STIMULATE_PAY = 25
	START_STIMULATE_PAY = 24
	FINISH_STUDY = 23
	SHARE_BAKS = 22
	SHARE_GOLD = 21
	GIFT = 20
	ALINK = 19
	DROP_FROM_WAREHOUSE = 18
	OBJECT_REMOVE_FROM_BOARD = 17
	OBJECT2_BOARD = 16
	STORY = 15
	FRIENDS_HELP = 14
	FINISH_QUEST = 13
	EDIT_QUEST = 12
	START_QUEST = 11
	EXP = 10
	MESSAGE = 9
	ELT_COLLECTION = 8
	RAID_RARE = 7
	RARE_ITEM = 6
	LEVEL = 5
	ENERGY = 4
	FOOD = 3
	TIMBER = 2
	GOLD = 1
	BAKS = 0

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = None, param2 = None):
		self.variance = param1
		self.value = param2

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.BAKS:
			self.value = param1.readUnsignedInt()
		elif self.variance == self.GOLD:
			self.value = param1.readUnsignedInt()
		elif self.variance == self.TIMBER:
			self.value = param1.readUnsignedInt()
		elif self.variance == self.FOOD:
			self.value = param1.readUnsignedInt()
		elif self.variance == self.ENERGY:
			self.value = param1.readUnsignedInt()
		elif self.variance == self.LEVEL:
			self.value = PNewLevelInfo()
			self.value.read(param1)
		elif self.variance == self.RARE_ITEM:
			self.value = PRare()
			self.value.read(param1)
		elif self.variance == self.RAID_RARE:
			self.value = PRaidRare()
			self.value.read(param1)
		elif self.variance == self.ELT_COLLECTION:
			self.value = PEltCollection()
			self.value.read(param1)
		elif self.variance == self.MESSAGE:
			self.value = str_obj_PMessageParam_obj()
			self.value.read(param1)
		elif self.variance == self.EXP:
			self.value = param1.readDouble()
		elif self.variance == self.START_QUEST:
			self.value = PQuest()
			self.value.read(param1)
		elif self.variance == self.EDIT_QUEST:
			self.value = PQuest()
			self.value.read(param1)
		elif self.variance == self.FINISH_QUEST:
			self.value = PQuestFinish()
			self.value.read(param1)
		elif self.variance == self.FRIENDS_HELP:
			self.value = PFriendsHelp()
			self.value.read(param1)
		elif self.variance == self.STORY:
			self.value = param1.readUTF()
		elif self.variance == self.OBJECT2_BOARD:
			self.value = PObj()
			self.value.read(param1)
		elif self.variance == self.OBJECT_REMOVE_FROM_BOARD:
			self.value = param1.readUnsignedInt()
		elif self.variance == self.DROP_FROM_WAREHOUSE:
			self.value = PWarehouseReserve()
			self.value.read(param1)
		elif self.variance == self.ALINK:
			self.value = PAlinkEvent()
			self.value.read(param1)
		elif self.variance == self.GIFT:
			self.value = PGift()
			self.value.read(param1)
		elif self.variance == self.SHARE_GOLD:
			self.value = PSign_uint()
			self.value.read(param1)
		elif self.variance == self.SHARE_BAKS:
			self.value = PSign_uint()
			self.value.read(param1)
		elif self.variance == self.FINISH_STUDY:
			self.value = PMyStudy()
			self.value.read(param1)
		elif self.variance == self.START_STIMULATE_PAY:
			self.value = str_time()
			self.value.read(param1)
		elif self.variance == self.FINISH_STIMULATE_PAY:
			self.value = param1.readUTF()
		elif self.variance == self.FINISH_COMBO:
			self.value = PRare()
			self.value.read(param1)
		elif self.variance == self.ADD_PET:
			self.value = PPet()
			self.value.read(param1)
		elif self.variance == self.RARE_RECEIVED:
			self.value = PRareReceived()
			self.value.read(param1)
		elif self.variance == self.RELOAD:
			return
		elif self.variance == self.ADD_RESERVE:
			self.value = PWarehouseReserve()
			self.value.read(param1)
		elif self.variance == self.STORY_LINE:
			self.value = PStoryLine()
			self.value.read(param1)
		elif self.variance == self.ASK_GIFT:
			self.value = PGiftAsk()
			self.value.read(param1)
		elif self.variance == self.START_LEVEL_ACCELERATION_ACTION:
			self.value = param1.readDouble()
		elif self.variance == self.FINISHED_LEVEL_ACCELERATION_ACTION:
			self.value = {}
			for i in xrange(param1.readUnsignedShort()):
				self.value[i] = PCost()
				self.value[i].read(param1)
		elif self.variance == self.GIFT_FROM_ADMIN:
			self.value = PWarehouseReserve()
			self.value.read(param1)
		elif self.variance == self.KICK_FROM_RAID:
			self.value = str_str()
			self.value.read(param1)
		elif self.variance == self.RAID_ACCEPT:
			self.value = PRaid()
			self.value.read(param1)
		elif self.variance == self.GOLD_MINE_MIGRATION:
			return
		elif self.variance == self.ALARM:
			self.value = param1.readUTF()
		elif self.variance == self.RESET_BUILDING_BONUS:
			self.value = param1.readUnsignedInt()
		elif self.variance == self.SHARE_BYPASS_IN_FAIR_RATING:
			self.value = param1.readUTF()
		elif self.variance == self.ENTER_PROMO_CODE:
			pass
		elif self.variance == self.ALREADY_GET_PRIZE_FOR_PROMO:
			pass
		elif self.variance == self.GOLD_GIFT_CASE_PROGRESS:
			self.value = i_i()
			self.value.read(param1)
		elif self.variance == self.CLAN_ENERGY:
			self.value = param1.readUnsignedInt()
		elif self.variance == self.CLAN_GOLD:
			self.value = param1.readUnsignedInt()
		elif self.variance == self.CLAN_EXP:
			self.value = uint_time()
			self.value.read(param1)
		elif self.variance == self.ACCEPTED_INTO_CLAN:
			if param1.readUnsignedByte() == 1:
				self.value = str_PUserClanInfo_PSign()
				self.value.read(param1)
			else:
				self.value = None
		elif self.variance == self.PROGRESS_TARGETS_CLAN:
			self.value = {}
			for i in xrange(param1.readUnsignedShort()):
				self.value[i] = PClanTargetCount()
				self.value[i].read(param1)
		elif self.variance == self.REMOVED_FROM_CLAN:
			pass
		elif self.variance == self.CLAN_WAREHOUSE:
			self.value = PReserves_time()
			self.value.read(param1)
		elif self.variance == self.CLAN_SET_ROLE_MEMBER:
			self.value = PClmRole()
			self.value.read(param1)
		elif self.variance == self.CLAN_CHANGE_TARGETS_LEVEL:
			self.value = i_a_PClanTargetLevelGe_a_time()
			self.value.read(param1)
		elif self.variance == self.CLAN_INVITE:
			self.value = param1.readUTF()
		elif self.variance == self.CLAN_DISSOLUTION:
			pass
		elif self.variance == self.SHARE_CLAN_GOLD:
			self.value = PSign_uint()
			self.value.read(param1)
		elif self.variance == self.START_OFFER:
			self.value = POffer()
			self.value.read(param1)
		elif self.variance == self.CLOSE_OFFER:
			pass
		elif self.variance == self.BAD_OBJS_IN_WAREHOUSE:
			pass
		elif self.variance == self.CLAN_FAIR_BUILDING_BONUS:
			self.value = str_PCost()
			self.read(param1)
		elif self.variance == self.CLAN_CHANGE_TARGETS_GAME_EVENT:
			self.value = a_PClanTargetLevelGe_a_time()
			self.value.read(param1)
		elif self.variance == self.CLAN_WAREHOUSE_FAIR_BUILDINGS:
			self.value = PWarehouseClanFair_time()
			self.value.read(param1)
		elif self.variance == self.BUY_PRODUCT_OFFER:
			self.value = PQuestPrize()
			self.value.read(param1)
		elif self.variance == self.FINISH_SEND_GIFTS:
			pass
		elif self.variance == self.RESTART_GAME:
			self.value = param1.readDouble()
		elif self.variance == self.CLAN_BONUS_CHANGE:
			if param1.readUnsignedByte() == 1:
				self.value = PClanBonusState()
				self.value.read(param1)
			else:
				self.value = None
		elif self.variance == self.CLAN_BONUS_PRIZE:
			self.value = PClanBonusPrize()
			self.value.read(param1)
		elif self.variance == self.UPDATE_CONTRACT:
			self.value = PContract()
			self.value.read(param1)
		elif self.variance == self.UPDATE_CONTRACTS:
			self.value = {}
			for i in xrange(param1.readUnsignedShort()):
				self.value[i] = PContract()
				self.value[i].read(param1)
		elif self.variance == self.CONTRACT_RATING_PRIZE:
			self.value = PContractRprize().read(param1)
			self.value.read(param1)
		elif self.variance == self.SHARE_CONTRACT_FIRST:
			self.value = i_PSign()
			self.value.read(param1)
		elif self.variance == self.SHARE_CONTRACT_WIN:
			self.value = i_PSign()
			self.value.read(param1)
		elif self.variance == self.CLAN_COFFER_ENABLE:
			self.value = str_time_a_str_a()
			self.value.read(param1)
		elif self.variance == self.CLAN_CHANGE_LEVEL:
			self.value = param1.readInt()
		elif self.variance == self.OFFER_RESULT:
			self.value = oPOffer_PCost_ot_uint_a_PResBuyOffer_a_a_PQuestPrize_a_t()
			self.value.read(param1)
		elif self.variance == self.STAGE:
			self.value = PStage()
			self.value.read(param1)
		elif self.variance == self.FINISH_DISTRIBUTION_CONTRACT_PRIZES:
			self.value = param1.readDouble()
		elif self.variance == self.CLAN_RAID_FINISH:
			self.value = PResFinishRaid()
			self.value.read(param1)
		elif self.variance == self.CLAN_RAID_JOIN:
			pass
		elif self.variance == self.BUY_RESERVES:
			self.value = PWarehouseReserve_bool()
			self.value.read(param1)
		elif self.variance == self.EXPIRE_QUEST:
			self.value = param1.readUTF()
		elif self.variance == self.INIT_TRADE_EVENT:
			self.value = PTrade()
			self.value.read(param1)
		elif self.variance == self.TRADE_BUY_OFFER:
			self.value = i_PTradeSaleDescr_a_PCost_a()
			self.value.read(param1)
		elif self.variance == self.NEW_TRADE_OFFER:
			self.value = PTradeSaleDescr()
			self.value.read(param1)
		elif self.variance == self.TEAM_BUILD_NEW:
			self.value = param1.readUTF()
		elif self.variance == self.TEAM_BUILD_APPROVED:
			self.value = param1.readUTF()
		elif self.variance == self.TEAM_BUILD_DECLINE:
			self.value = param1.readUTF()
		elif self.variance == self.EFARM_BONUS:
			self.value = oPQuestPrize_otime()
			self.value.read(param1)
		elif self.variance == self.BUY_FAIR_BUILDING:
			self.value = PBuyFairBuilding_i_oPSign()
			self.value.read(param1)
		else:
			raise Exception ("ERROR: incorrect PUserEvent data")

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.BAKS:
			param1.writeInt(self.value)
		elif self.variance == self.GOLD:
			param1.writeInt(self.value)
		elif self.variance == self.TIMBER:
			param1.writeInt(self.value)
		elif self.variance == self.FOOD:
			param1.writeInt(self.value)
		elif self.variance == self.ENERGY:
			param1.writeInt(self.value)
		elif self.variance == self.LEVEL:
			self.value.write(param1)
		elif self.variance == self.RARE_ITEM:
			self.value.write(param1)
		elif self.variance == self.RAID_RARE:
			self.value.write(param1)
		elif self.variance == self.ELT_COLLECTION:
			self.value.write(param1)
		elif self.variance == self.MESSAGE:
			self.value.write(param1)
		elif self.variance == self.EXP:
			param1.writeDouble(self.value)
		elif self.variance == self.START_QUEST:
			self.value.write(param1)
		elif self.variance == self.EDIT_QUEST:
			self.value.write(param1)
		elif self.variance == self.FINISH_QUEST:
			self.value.write(param1)
		elif self.variance == self.FRIENDS_HELP:
			self.value.write(param1)
		elif self.variance == self.STORY:
			param1.writeUTF(self.value)
		elif self.variance == self.OBJECT2_BOARD:
			self.value.write(param1)
		elif self.variance == self.OBJECT_REMOVE_FROM_BOARD:
			param1.writeInt(self.value)
		elif self.variance == self.DROP_FROM_WAREHOUSE:
			self.value.write(param1)
		elif self.variance == self.ALINK:
			self.value.write(param1)
		elif self.variance == self.GIFT:
			self.value.write(param1)
		elif self.variance == self.SHARE_GOLD:
			self.value.write(param1)
		elif self.variance == self.SHARE_BAKS:
			self.value.write(param1)
		elif self.variance == self.FINISH_STUDY:
			self.value.write(param1)
		elif self.variance == self.START_STIMULATE_PAY:
			self.value.write(param1)
		elif self.variance == self.FINISH_STIMULATE_PAY:
			param1.writeUTF(self.value)
		elif self.variance == self.FINISH_COMBO:
			self.value.write(param1)
		elif self.variance == self.ADD_PET:
			self.value.write(param1)
		elif self.variance == self.RARE_RECEIVED:
			self.value.write(param1)
		elif self.variance == self.RELOAD:
			return
		elif self.variance == self.ADD_RESERVE:
			self.value.write(param1)
		elif self.variance == self.STORY_LINE:
			self.value.write(param1)
		elif self.variance == self.ASK_GIFT:
			self.value.write(param1)
		elif self.variance == self.START_LEVEL_ACCELERATION_ACTION:
			param1.writeDouble(self.value)
		elif self.variance == self.FINISHED_LEVEL_ACCELERATION_ACTION:
			if self.value == {}:
				param1.writeShort(0)
			else:
				param1.writeShort(len(self.value))
				for i in xrange(len(self.value)):
					self.value[i].write(param1)
		elif self.variance == self.GIFT_FROM_ADMIN:
			self.value.write(param1)
		elif self.variance == self.KICK_FROM_RAID:
			self.value.write(param1)
		elif self.variance == self.RAID_ACCEPT:
			self.value.write(param1)
		elif self.variance == self.GOLD_MINE_MIGRATION:
			return
		elif self.variance == self.ALARM:
			param1.writeUTF(self.value)
		elif self.variance == self.RESET_BUILDING_BONUS:
			param1.writeInt(self.value)
		elif self.variance == self.SHARE_BYPASS_IN_FAIR_RATING:
			param1.writeUTF(self.value)
		elif self.variance == self.ENTER_PROMO_CODE:
			pass
		elif self.variance == self.ALREADY_GET_PRIZE_FOR_PROMO:
			pass
		elif self.variance == self.GOLD_GIFT_CASE_PROGRESS:
			self.value.write(param1)
		elif self.variance == self.CLAN_ENERGY:
			param1.writeInt(self.value)
		elif self.variance == self.CLAN_GOLD:
			param1.writeInt(self.value)
		elif self.variance == self.CLAN_EXP:
			self.value.write(param1)
		elif self.variance == self.ACCEPTED_INTO_CLAN:
			if self.value != None:
				param1.writeByte(1)
				self.value.write(param1)
			else:
				param1.writeByte(0)
		elif self.variance == self.PROGRESS_TARGETS_CLAN:
			if self.value == {}:
				param1.writeShort(0)
			else:
				param1.writeShort(len(self.value))
				for i in xrange(len(self.value)):
					self.value[i].write(param1)
		elif self.variance == self.REMOVED_FROM_CLAN:
			pass
		elif self.variance == self.CLAN_WAREHOUSE:
			self.value.write(param1)
		elif self.variance == self.CLAN_SET_ROLE_MEMBER:
			self.value.write(param1)
		elif self.variance == self.CLAN_CHANGE_TARGETS_LEVEL:
			self.value.write(param1)
		elif self.variance == self.CLAN_INVITE:
			param1.writeUTF(self.value)
		elif self.variance == self.CLAN_DISSOLUTION:
			pass
		elif self.variance == self.SHARE_CLAN_GOLD:
			self.value.write(param1)
		elif self.variance == self.START_OFFER:
			self.value.write(param1)
		elif self.variance == self.CLOSE_OFFER:
			pass
		elif self.variance == self.BAD_OBJS_IN_WAREHOUSE:
			pass
		elif self.variance == self.CLAN_FAIR_BUILDING_BONUS:
			self.value.write(param1)
		elif self.variance == self.CLAN_CHANGE_TARGETS_GAME_EVENT:
			self.value.write(param1)
		elif self.variance == self.CLAN_WAREHOUSE_FAIR_BUILDINGS:
			self.value.write(param1)
		elif self.variance == self.BUY_PRODUCT_OFFER:
			self.value.write(param1)
		elif self.variance == self.FINISH_SEND_GIFTS:
			pass
		elif self.variance == self.RESTART_GAME:
			param1.writeDouble(self.value)
		elif self.variance == self.CLAN_BONUS_CHANGE:
			if self.value != None:
				param1.writeByte(1)
				self.value.write(param1)
			else:
				param1.writeByte(0)
		elif self.variance == self.CLAN_BONUS_PRIZE:
			self.value.write(param1)
		elif self.variance == self.UPDATE_CONTRACT:
			self.value.write(param1)
		elif self.variance == self.UPDATE_CONTRACTS:
			if self.value == {}:
				param1.writeShort(0)
			else:
				param1.writeShort(len(self.value))
				for i in xrange(len(self.value)):
					self.value[i].write(param1)
		elif self.variance == self.CONTRACT_RATING_PRIZE:
			self.value.write(param1)
		elif self.variance == self.SHARE_CONTRACT_FIRST:
			self.value.write(param1)
		elif self.variance == self.SHARE_CONTRACT_WIN:
			self.value.write(param1)
		elif self.variance == self.CLAN_COFFER_ENABLE:
			self.value.write(param1)
		elif self.variance == self.CLAN_CHANGE_LEVEL:
			param1.writeInt(self.value)
		elif self.variance == self.OFFER_RESULT:
			self.value.write(param1)
		elif self.variance == self.STAGE:
			self.value.write(param1)
		elif self.variance == self.FINISH_DISTRIBUTION_CONTRACT_PRIZES:
			param1.writeDouble(self.value)
		elif self.variance == self.CLAN_RAID_FINISH:
			self.value.write(param1)
		elif self.variance == self.CLAN_RAID_JOIN:
			pass
		elif self.variance == self.BUY_RESERVES:
			self.value.write(param1)
		elif self.variance == self.EXPIRE_QUEST:
			param1.writeUTF(self.value)
		elif self.variance == self.INIT_TRADE_EVENT:
			self.value.write(param1)
		elif self.variance == self.TRADE_BUY_OFFER:
			self.value.write(param1)
		elif self.variance == self.NEW_TRADE_OFFER:
			self.value.write(param1)
		elif self.variance == self.TEAM_BUILD_NEW:
			param1.writeUTF(self.value)
		elif self.variance == self.TEAM_BUILD_APPROVED:
			param1.writeUTF(self.value)
		elif self.variance == self.TEAM_BUILD_DECLINE:
			param1.writeUTF(self.value)
		elif self.variance == self.EFARM_BONUS:
			self.value.write(param1)
		elif self.variance == self.BUY_FAIR_BUILDING:
			self.value.write(param1)
		else:
			raise Exception ("ERROR: incorrect PUserEvent data")

# str_obj_PMessageParam_obj used by PUserEvent
class str_obj_PMessageParam_obj(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = "", param2 = None):
		self.field_0 = param1
		self.field_1 = param2

	def write(self, param1):
		param1.writeUTF(self.field_0)
		raise Exception ("ERROR: can't write field_1, assoc writing not implemented. FIXME PLEASE!!!") ## self,IS FROM ORIGINAL
		# Looking at read, it is not that hard to implement...

	def read(self, param1):
		self.field_0 = param1.readUTF()
		# self.field_1 = new Object() # ERRR???
		i = param1.readUnsignedShort()
		while param1.readUnsignedShort() > 0:			# FROM ORIGINAL
			parUTF = param1.readUTF()
			self.field_1[parUTF] = PMessageParam()
			self.field_1[parUTF].read(param1)
			i = i - 1

# PFriendsHelp used by PUserEvent
class PFriendsHelp(object):
	__slots__ = ['fh_friend_id', 'fh_actions']
	def __init__(self, param1 = "", param2 = None):
		self.fh_friend_id = param1
		self.fh_actions = param2

	def __repr__(self):
		return "{'fh_friend_id':%s,'fh_actions':%s}" % (self.fh_friend_id, str(self.fh_actions))

	def write(self, param1):
		param1.writeUTF(self.fh_friend_id)
		if self.fh_actions == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.fh_actions))
			for i in xrange(len(self.fh_actions)):
				self.fh_actions[i].write(param1)

	def read(self, param1):
		self.fh_friend_id = param1.readUTF()
		self.fh_actions = {}
		for i in xrange(param1.readUnsignedShort()):
			self.fh_actions[i] = PFaction()
			self.fh_actions[i].read(param1)


# PFaction used by PFriendsHelp
class PFaction(object):
	SEEDBED_REPAIR = 5
	FEED_ANIMAL = 4
	HARVEST = 3
	DROP_GARBAGE = 2
	DROP_STONE = 1
	DROP_WOOD = 0
	rev_var_array = {0:'WOOD',1:'STONE',2:'GARBAGE',3:'HARVEST',4:'FEED',5:'REPAIR'}

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = None , param2 = None):
		self.variance = param1
		self.value = param2

	def __repr__(self):
		#return "{'var':%s,'val':%s}" % (self.rev_var_array[self.variance], self.value)
		#return "{'var':%s}" % (self.rev_var_array[self.variance])
		return "%s" % (self.rev_var_array[self.variance])

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.DROP_WOOD:
			param1.writeInt(self.value)
		elif self.variance == self.DROP_STONE:
			param1.writeInt(self.value)
		elif self.variance == self.DROP_GARBAGE:
			param1.writeInt(self.value)
		elif self.variance == self.HARVEST:
			param1.writeInt(self.value)
		elif self.variance == self.FEED_ANIMAL:
			self.value.write(param1)
		elif self.variance == self.SEEDBED_REPAIR:
			param1.writeInt(self.value)
		else:
			raise Exception ("ERROR: incorrect PFaction data")

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.DROP_WOOD:
			self.value = param1.readInt()
		elif self.variance == self.DROP_STONE:
			self.value = param1.readInt()
		elif self.variance == self.DROP_GARBAGE:
			self.value = param1.readInt()
		elif self.variance == self.HARVEST:
			self.value = param1.readInt()
		elif self.variance == self.FEED_ANIMAL:
			self.value = i_time()
			self.value.read(param1)
		elif self.variance == self.SEEDBED_REPAIR:
			self.value = param1.readInt()
		else:
			raise Exception ("ERROR: incorrect PFaction data")

# PGift
class PGift(object):
	__slots__ = ['gift_id', 'gift_kind', 'gift_count', 'gift_created_at', 'gift_from_user', 'gift_message', 'gift_can_thanks', 'gift_type', 'gift_request_id']
	def __init__(self, param1 = 0, param2 = "", param3 = 0, param4 = 0, param5 = 0, param6 = "", param7 = False, param8 = None, param9 = ""):
		self.gift_id = param1
		self.gift_kind = param2
		self.gift_count = param3
		self.gift_created_at = param4
		self.gift_from_user = param5
		self.gift_message = param6
		self.gift_can_thanks = param7
		self.gift_type = param8
		self.gift_request_id = param9

	def __repr__(self):
		return "{'gift_id':'%s','gift_kind':'%s','gift_count':'%s','gift_created_at':'%s','gift_from_user':'%s','gift_message':'%s','gift_can_thanks':'%s','gift_type':'%s'}" % (str(self.gift_id), self.gift_kind.encode("utf-8"), str(self.gift_count), str(self.gift_created_at), str(self.gift_from_user), self.gift_message.encode("utf-8"), str(self.gift_can_thanks), str(self.gift_type))

	def read(self, param1):
		self.gift_id = param1.readUnsignedInt()
		self.gift_kind = param1.readUTF()
		self.gift_count = param1.readUnsignedShort()
		self.gift_created_at = param1.readDouble()
		self.gift_from_user = param1.readUTF()
		self.gift_message = param1.readUTF()
		self.gift_can_thanks = param1.readBoolean()
		self.gift_type = PGiftType()
		self.gift_type.read(param1)
		if param1.readUnsignedByte() == 1:
			self.gift_request_id = param1.readUTF()
		else:
			self.gift_request_id = None

	def write(self, param1):
		param1.writeInt(self.gift_id)
		param1.writeUTF(self.gift_kind)
		param1.writeShort(self.gift_count)
		param1.writeDouble(self.gift_created_at)
		param1.writeUTF(self.gift_from_user)
		param1.writeUTF(self.gift_message)
		param1.writeBoolean(self.gift_can_thanks)
		self.gift_type.write(param1)
		if self.gift_request_id != None:
			param1.writeByte(1)
			param1.writeUTF(self.gift_request_id)
		else:
			param1.writeByte(0)

# PGiftAsk
class PGiftAsk(object):
	__slots__ = ['g_kind', 'g_type', 'friend_id']
	def __init__(self,param1 = "", param2 = None, param3 = ""):
		self.g_kind = param1
		self.g_type = param2
		self.friend_id = param3

	def write(self,param1):
		param1.writeUTF(self.g_kind)
		self.g_type.write(param1)
		param1.writeUTF(self.friend_id)

	def read(self,param1):
		self.g_kind = param1.readUTF()
		self.g_type = PGiftType()
		self.g_type.read(param1)
		self.friend_id = param1.readUTF()

# PAlinkEvent
class PAlinkEvent(object):
	PRIZE = 2
	MESSAGE = 1
	COST = 0

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = None, param2 = None):
		self.variance = param1
		self.value = param2

	def write(self,param1):
		param1.writeByte(self.variance)
		if self.variance == self.COST:
			if self.value == {}:
				param1.writeShort(0)
			else:
				param1.writeShort(len(self.value))
				for i in xrange(len(self.value)):
					self.value[i].write(param1)
		elif self.variance == self.MESSAGE:
			param1.writeUTF(self.value)
		elif self.variance == self.PRIZE:
			self.value.write(param1)
		else:
			raise Exception ("ERROR: incorrect PAlinkEvent data")

	def read(self,param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.COST:
			self.value = {}
			for i in xrange(param1.readUnsignedShort()):
				self.value[i] = PCost()
				self.value[i].read(param1)
		elif self.variance == self.MESSAGE:
			self.value = param1.readUTF()
		elif self.variance == self.PRIZE:
			self.value = PWarehouseReserve_str()
			self.value.read(param1)
		else:
			raise Exception ("ERROR: incorrect PAlinkEvent data")

# PQuest
class PQuest(object):
	__slots__ = ['qname', 'qtargets']
	def __init__(self,param1 = "", param2 = None):
		self.qname = param1
		self.qtargets = param2

	def write(self,param1):
		param1.writeUTF(self.qname)
		if self.qtargets == {}:
			param1.writeByte(0)
		else:
			param1.writeByte(len(self.qtargets))
			for i in xrange(len(self.qtargets)):
				self.qtargets[i].write(param1)

	def read(self,param1):
		self.qname = param1.readUTF()
		self.qtargets = {}
		for i in xrange(param1.readUnsignedByte()):
			self.qtargets[i] = PQuestTarget()
			self.qtargets[i].read(param1)

# PQuestTarget
class PQuestTarget(object):
	QTDONE = 1
	QTOPEN = 0

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = 0, param2 = 0):
		self.variance = param1
		self.value = param2

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.QTOPEN:
			param1.writeInt(self.value)
		elif self.variance == self.QTDONE:
			return
		else:
			raise Exception ("ERROR: incorrect PQuestTarget data")

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.QTOPEN:
			self.value = param1.readUnsignedInt()
		elif self.variance == self.QTDONE:
			return
		else:
			raise Exception ("ERROR: incorrect PQuestTarget data")


# PQuestFinish
class PQuestFinish(object):
	__slots__ = ['qf_id', 'qf_prize', 'qf_sign']
	def __init__(self,param1 = "", param2 = None, param3 = None):
		self.qf_id = param1
		self.qf_prize = param2
		self.qf_sign = param3

	def write(self,param1):
		param1.writeUTF(self.qf_id)
		self.qf_prize.write(param1)
		self.qf_sign.write(param1)

	def read(self,param1):
		self.qf_id = param1.readUTF()
		self.qf_prize = PQuestPrizes()
		self.qf_prize.read(param1)
		self.qf_sign = PSign()
		self.qf_sign.read(param1)

# PQuestPrize
class PQuestPrize(object):
	RESERVES = 1
	COST = 0

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = None, param2 = None):
		self.variance = param1
		self.value = param2

	def __str__(self):
		return	'{variance:%s,\r\n value:%s}' % (self.variance, self.value)

	def write(self,param1):
		param1.writeByte(self.variance)
		if self.variance == self.COST:
			self.value.write(param1)
		elif self.variance == self.RESERVES:
			self.value.write(param1)
		else:
			raise Exception ("ERROR: incorrect PQuestPrize data")

	def read(self,param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.COST:
			self.value = PCost()
			self.value.read(param1)
		elif self.variance == self.RESERVES:
			self.value = PWarehouseReserve()
			self.value.read(param1)
		else:
			raise Exception ("ERROR: incorrect PQuestPrize data")

# PQuestPrizes
class PQuestPrizes (object):
	__slots__ = ['value']
	def __init__(self, param1 = None):
		self.value = param1

	def __repr__(self):
		return "{'value':'%s'}" % (self.value)

	def write(self,param1):
		if self.value == {}:
			param1.writeByte(0)
		else:
			param1.writeByte(len(self.value))
			for i in xrange(len(self.value)):
				self.value[i].write(param1)

	def read(self,param1):
		self.value = {}
		for i in xrange(param1.readUnsignedByte()):
			self.value[i] = PQuestPrize()
			self.value[i].read(param1)

# PNewLevelInfo
class PNewLevelInfo(object):
	__slots__ = ['new_level', 'new_level_sign']
	def __init__(self, param1 = 0, param2 = None):
		self.new_level = param1
		self.new_level_sign = param2

	def write(self, param1):
		param1.writeShort(self.new_level)
		self.new_level_sign.write(param1)

	def read(self, param1):
		self.new_level = param1.readUnsignedShort()
		self.new_level_sign = PSign()
		self.new_level_sign.read(param1)

# PWarehouseReserve
class PWarehouseReserve(object):
	RESERVE_TYPE_SUITCASES = 16
	RESERVE_TYPE_CLAN_FAIR_BUILDINGS = 15
	RESERVE_TYPE_FAIR_BUILDINGS = 14
	RESERVE_TYPE_RAID_RARES = 13
	RESERVE_TYPE_COUPONS = 12
	RESERVE_TYPE_DECORS_PUZZLE = 11
	RESERVE_TYPE_WEAPONS = 10
	RESERVE_TYPE_Y_ANIMALS = 9
	RESERVE_TYPE_RARE_ITEMS = 8
	RESERVE_TYPE_TILES = 7
	RESERVE_TYPE_DECORS = 6
	RESERVE_TYPE_ENERGIES = 5
	RESERVE_TYPE_ANIMALS = 4
	RESERVE_TYPE_BUILDINGS = 3
	RESERVE_TYPE_FERTILIZERS = 2
	RESERVE_TYPE_FRUITS = 1
	RESERVE_TYPE_SEEDS = 0

	__slots__ = ['reserve_type_variance', 'reserve_kind', 'count']
	def __init__(self, param1 = 0, param2 = "", param3 = 0):
		self.reserve_type_variance = param1
		self.reserve_kind = param2
		self.count = param3

	def write(self, param1):
		param1.writeByte(self.reserve_type_variance)
		param1.writeUTF(self.reserve_kind)
		param1.writeInt(self.count)

	def read(self, param1):
		self.reserve_type_variance = param1.readUnsignedByte()
		self.reserve_kind = param1.readUTF()
		self.count = param1.readUnsignedInt()

# PWarehouseReserve_str
class PWarehouseReserve_str (object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = None, param2 = ""):
		self.field_0 = param1
		self.field_1 = param2

	def write(self, param1):
		self.field_0.write(param1)
		param1.writeUTF(self.field_1)

	def read(self, param1):
		self.field_0 = PWarehouseReserve()
		self.field_0.read(param1)
		self.field_1 = param1.readUTF()

# PSign_uint
class PSign_uint(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = None, param2 = 0):
		self.field_0 = param1
		self.field_1 = param2

	def write(self, param1):
		self.field_0.write(param1)
		param1.writeInt(self.field_1)

	def read(self, param1):
		self.field_0 = PSign()
		self.field_0.read(param1)
		self.field_1 = param1.readUnsignedInt()

# PRareReceived
class PRareReceived (object):
	__slots__ = ['ra_kind', 'friend_id']
	def __init__(self,param1 = "", param2 = ""):
		self.ra_kind = param1
		self.friend_id = param2

	def write(self,param1):
		param1.writeUTF(self.ra_kind)
		param1.writeUTF(self.friend_id)

	def read(self,param1):
		self.ra_kind = param1.readUTF()
		self.friend_id = param1.readUTF()

# PStoryLine
class PStoryLine (object):
	__slots__ = ['sl_kind', 'sl_state']
	def __init__(self,param1 = "", param2 = None):
		self.sl_kind = param1
		self.sl_state = param2

	def write(self,param1):
		param1.writeUTF(self.sl_kind)
		self.sl_state.write(param1)

	def read(self,param1):
		self.sl_kind = param1.readUTF()
		self.sl_state = PSlState()
		self.sl_state.read(param1)

# PSlState
class PSlState (object):
	SLDONE = 1
	SLOPEN = 0

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def write(self,param1):
		param1.writeByte(self.variance)
		if self.variance == self.SLOPEN:
			param1.writeInt(self.value)
		elif self.variance == self.SLDONE:
			if self.value != None:
				param1.writeByte(1)
				self.value.write(param1)
			else:
				param1.writeByte(0)
		else:
			raise Exception ("ERROR: incorrect PSlState data")

	def read(self,param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.SLOPEN:
			self.value = param1.readUnsignedInt()
		elif self.variance == self.SLDONE:
			if param1.readUnsignedByte() == 1:
				self.value = PSign()
				self.value.read(param1)
			else:
				self.value = None
		else:
			raise Exception ("ERROR: incorrect PSlState data")

# PRaid
class PRaid(object):
	__slots__ = ['kind', 'level', 'id', 'members', 'boss_hp', 'ts', 'start_date', 'message', 'my_energy', 'my_energy_proc_time', 'kicked_members']
	def __init__(self, param1 = "", param2 = 0, param3 = "", param4 = None, param5 = 0, param6 = 0, param7 = 0, param8 = None, param9= 0, param10 = 0, param11 = None):
		self.kind = param1
		self.level = param2
		self.id = param3
		self.members = param4
		self.boss_hp = param5
		self.ts = param6
		self.start_date = param7
		self.message = param8
		self.my_energy = param9
		self.my_energy_proc_time = param10
		self.kicked_members = param11

	def __repr__(self):
		return "{'kind':'%s','level':%s,'id':%s,'members':%s,'boss_hp':%s,'ts':%s,'start_date':%s,'my_energy':%s,'my_energy_proc_time':%s,'kicked_members':%s}" % (self.kind.encode("utf-8"), self.level, self.id.encode("utf-8"), self.members, self.boss_hp, self.ts, self.start_date, self.my_energy, self.my_energy_proc_time, self.kicked_members )

	def read(self, param1):
		self.kind = param1.readUTF()
		self.level = param1.readInt()
		self.id = param1.readUTF()
		self.members = {}
		for i in xrange(param1.readUnsignedShort()):
			self.members[i] = PRaidMember()
			self.members[i].read(param1)
		self.boss_hp = param1.readInt()
		self.ts = param1.readDouble()
		self.start_date = param1.readDouble()
		if param1.readUnsignedByte() == 1:
			self.message = PRaidMessage()
			self.message.read(param1)
		else:
			self.message = None
		self.my_energy = param1.readInt()
		if param1.readUnsignedByte() == 1:
			self.my_energy_proc_time = param1.readDouble()
		else:
			self.my_energy_proc_time = None
		self.kicked_members = {}
		for i in xrange(param1.readUnsignedShort()):
			self.kicked_members[i] = param1.readUTF()

	def write(self, param1):
		param1.writeUTF(self.kind)
		param1.writeInt(self.level)
		param1.writeUTF(self.id)
		if self.members == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.members))
			for i in xrange(len(self.members)):
				self.members[i].write(param1)
		param1.writeInt(self.boss_hp)
		param1.writeDouble(self.ts)
		param1.writeDouble(self.start_date)
		if self.message != None:
			param1.writeByte(1)
			self.message.write(param1)
		else:
			param1.writeByte(0)
		param1.writeInt(self.my_energy)
		if self.my_energy_proc_time != None:
			param1.writeByte(1)
			param1.writeDouble(self.my_energy_proc_time)
		else:
			param1.writeByte(0)
		if self.kicked_members == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.kicked_members))
			for i in xrange(len(self.kicked_members)):
				param1.writeUTF(self.kicked_members[i])

# PRaidUser
class PRaidMember(object):
	__slots__ = ['id', 'damage_amount', 'is_leader', 'is_online', 'name', 'avatar', 'prize']
	def __init__(self, param1 = "", param2 = 0, param3 = False, param4 = False, param5 = "", param6 = "", param7 = None):
		self.id = param1
		self.damage_amount = param2
		self.is_leader = param3
		self.is_online = param4
		self.name = param5
		self.avatar = param6
		self.prize = param7

	def __repr__(self):
		return "{'id':'%s','damage_amount':%s,'is_leader':%s,'is_online':%s,'name':%s,'avatar':%s,'prize':%s}" % (self.id.encode("utf-8"), self.damage_amount, self.is_leader, self.is_online, self.name.encode("utf-8"), self.avatar.encode("utf-8"), self.prize)

	def write(self, param1):
		param1.writeUTF(self.id)
		param1.writeInt(self.damage_amount)
		param1.writeBoolean(self.is_leader)
		param1.writeBoolean(self.is_online)
		param1.writeUTF(self.name)
		param1.writeUTF(self.avatar)
		self.prize.write(param1)

	def read(self, param1):
		self.id = param1.readUTF()
		self.damage_amount = param1.readInt()
		self.is_leader = param1.readBoolean()
		self.is_online = param1.readBoolean()
		self.name = param1.readUTF()
		self.avatar = param1.readUTF()
		self.prize = PQuestPrizes()
		self.prize.read(param1)

# PMessageParam
class PMessageParam (object):
	ELEMENT = 1
	STRING = 0

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = "", param2 = ""):
		self.variance = param1
		self.value = param2

	def write(self,param1):
		param1.writeByte(self.variance)
		if self.variance == self.STRING:
			param1.writeUTF(self.value)
		elif self.variance == self.ELEMENT:
			param1.writeUTF(self.value)
		else:
			raise Exception ("ERROR: incorrect PMessageParam data")

	def read(self,param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.STRING:
			self.value = param1.readUTF()
		elif self.variance == self.ELEMENT:
			self.value = param1.readUTF()
		else:
			raise Exception ("ERROR: incorrect PMessageParam data")

class PUser(object):
	__slots__ = ['base', 'stage', 'baks', 'gold', 'clan_gold', 'energy', 'max_energy', 'timber', 'food', 'warehouse', 'collections', 'stories', 'gifts', 'daily_bonus_info', 'weapon_current', 'segways_buy', 'states', 'ra_rf', 'rare_receiveds', 'unlocks', 'story_lines', 'map_id', 'lcomics', 'friends_help', 'was_pay', 'level_acceleration_action', 'gifts_warehouse', 'skittles_bonus', 'coupons_sended', 'last_raid_time', 'raid_finish_data', 'fair_study_stage', 'comp_info', 'comp_id', 'finished_fair_buildings', 'gold_gift_case_progress', 'gold_gift_cases', 'clan_info', 'energy_next_time', 'current_offer', 'clan_bonus', 'current_magic_action', 'random_friends_cnt','achievement_prizes','team_build_req_my', 'team_build_req_me', 'team_build_assistants','contract_expire_quests']
	def __init__(self, param1 = None, param2 = None, param3 = 0, param4 = 0, param5 = 0, param6 = 0, param7 = 0, param8 = 0, param9 = 0, param10 = None, param11 = None, param12 = None, param13 = None, param14 = None, param15 = "", param16 = None, param17 = None, param18 = None, param19 = None, param20 = None, param21 = None, param22 = 0, param23 = False, param24 = None, param25 = False, param26 = 0, param27 = None, param28 = None, param29 = None, param30 = 0, param31 = 0, param32 = 0, param33 = None, param34 = "", param35 = None, param36 = 0, param37 = 0, param38 = None, param39 = 0, param40 = None, param41 = None, param42 = {}, param43 = 0, param44 = 0, param45 = 0, param46 = 0, param47 = 0, param48 = 0):
		self.base = param1
		self.stage = param2
		self.baks = param3
		self.gold = param4
		self.clan_gold = param5
		self.energy = param6
		self.max_energy = param7
		self.timber = param8
		self.food = param9
		self.warehouse = param10
		self.collections = param11
		self.stories = param12
		self.gifts = param13
		self.daily_bonus_info = param14
		self.weapon_current = param15
		self.segways_buy = param16
		self.states = param17
		self.ra_rf = param18
		self.rare_receiveds = param19
		self.unlocks = param20
		self.story_lines = param21
		self.map_id = param22
		self.lcomics = param23
		self.friends_help = param24
		self.was_pay = param25
		self.level_acceleration_action = param26
		self.gifts_warehouse = param27
		self.skittles_bonus = param28
		self.coupons_sended = param29
		self.last_raid_time = param30
		self.raid_finish_data = param31
		self.fair_study_stage = param32
		self.comp_info = param33
		self.comp_id = param34
		self.finished_fair_buildings = param35
		self.gold_gift_case_progress = param36
		self.gold_gift_cases = param37
		self.clan_info = param38
		self.energy_next_time = param39
		self.current_offer = param40
		self.clan_bonus = param41
		self.current_magic_action = param42
		self.random_friends_cnt = param43
		self.achievement_prizes = param44
		self.team_build_req_my = param45
		self.team_build_req_me = param46
		self.team_build_assistants = param47
		self.contract_expire_quests = param48

	def read(self, param1):
		self.base = PUserBase()
		self.base.read(param1)
		self.stage = PStage()
		self.stage.read(param1)
		self.baks = param1.readUnsignedInt()
		self.gold = param1.readUnsignedInt()
		self.clan_gold = param1.readUnsignedInt()
		self.energy = param1.readUnsignedInt()
		self.max_energy = param1.readUnsignedInt()
		self.timber = param1.readUnsignedInt()
		self.food = param1.readUnsignedInt()
		self.warehouse = {}
		for i in xrange(param1.readUnsignedShort()):
			self.warehouse[i] = PWarehouseReserve()
			self.warehouse[i].read(param1)
		self.collections = {}
		i = param1.readUnsignedShort()
		while i > 0:
			parUTF = param1.readUTF()
			self.collections[parUTF] = PCollectionUser()
			self.collections[parUTF].read(param1)
			i = i - 1
		self.stories = {}
		for i in xrange(param1.readUnsignedShort()):
			self.stories[i] = param1.readUTF()
		self.gifts = {}
		for i in xrange(param1.readUnsignedShort()):
			self.gifts[i] = PGift()
			self.gifts[i].read(param1)
		if param1.readUnsignedByte() == 1:
			self.daily_bonus_info = PDailyBonusInfo()
			self.daily_bonus_info.read(param1)
		else:
			self.daily_bonus_info = None
		self.weapon_current = param1.readUTF()
		self.segways_buy = {}
		for i in xrange(param1.readUnsignedShort()):
			self.segways_buy[i] = PSegwayBuy()
			self.segways_buy[i].read(param1)
		self.states = {}
		for i in xrange(param1.readUnsignedShort()):
			self.states[i] = PState()
			self.states[i].read(param1)
		self.ra_rf = {}
		for i in xrange(param1.readUnsignedShort()):
			self.ra_rf[i] = param1.readUTF()
		self.rare_receiveds = {}
		for i in xrange(param1.readUnsignedShort()):
			self.ra_rf[i] = param1.readUTF()
		self.unlocks = {}
		for i in xrange(param1.readUnsignedShort()):
			self.ra_rf[i] = param1.readUTF()
		self.story_lines = {}
		for i in xrange(param1.readUnsignedShort()):
			self.story_lines[i] = PStoryLine()
			self.story_lines[i].read(param1)
		self.map_id = param1.readUnsignedInt()
		self.lcomics = param1.readBoolean()
		self.friends_help = {}
		for i in xrange(param1.readUnsignedShort()):
			self.friends_help[i] = param1.readUTF()
		self.was_pay = param1.readBoolean()
		if param1.readUnsignedByte() == 1:
			self.level_acceleration_action = param1.readDouble()
		else:
			self.level_acceleration_action = None
		self.gifts_warehouse = {}
		for i in xrange(param1.readUnsignedShort()):
			self.gifts_warehouse[i] = PWarehouseReserve()
			self.gifts_warehouse[i].read(param1)
		if param1.readUnsignedByte() == 1:
			self.skittles_bonus = PWarehouseReserve()
			self.skittles_bonus.read(param1)
		else:
			self.skittles_bonus = None
		self.coupons_sended = {}
		for i in xrange(param1.readUnsignedShort()):
			self.coupons_sended[i] = str_a_str_a()
			self.coupons_sended[i].read(param1)
		self.last_raid_time = param1.readDouble()
		if param1.readUnsignedByte() == 1:
			self.raid_finish_data = PRaidResult()
			self.raid_finish_data.read(param1)
		else:
			self.raid_finish_data = None
		self.fair_study_stage = param1.readUnsignedInt()
		if param1.readUnsignedByte() == 1:
			self.comp_info = PCompetitionInfo()
			self.comp_info.read(param1)
		else:
			self.comp_info = None
		if param1.readUnsignedByte() == 1:
			self.comp_id = param1.readUTF()
		else:
			self.comp_id = None
		self.finished_fair_buildings = {}
		for i in xrange(param1.readUnsignedShort()):
			self.finished_fair_buildings[i] = param1.readUTF()
		self.gold_gift_case_progress = param1.readUnsignedInt()
		self.gold_gift_cases = param1.readUnsignedInt()

		if param1.readUnsignedByte() == 1:
			self.clan_info = PUserClanInfo()
			self.clan_info.read(param1)
		else:
			self.clan_info = None
		if param1.readUnsignedByte() == 1:
			self.energy_next_time = param1.readDouble()
		else:
			self.energy_next_time = None

		self.current_offer = {}
		for i in xrange(param1.readUnsignedShort()):
			self.current_offer[i] = POffer()
			self.current_offer[i].read(param1)

		if param1.readUnsignedByte() == 1:
			self.clan_bonus = PClanBonusState()
			self.clan_bonus.read(param1)
		else:
			self.clan_bonus = None
		self.current_magic_action = {}
		for i in xrange(param1.readUnsignedShort()):
			self.current_magic_action[i] = PMagicAction_time()
			self.current_magic_action[i].read(param1)
		self.random_friends_cnt = param1.readInt()

		self.achievement_prizes = {}
		for i in xrange(param1.readUnsignedShort()):
			self.achievement_prizes[i] = param1.readUTF()

		self.team_build_req_my = {}
		for i in xrange(param1.readUnsignedShort()):
			self.team_build_req_my[i] = PTeamBuildReqMy()
			self.team_build_req_my[i].read(param1)

		self.team_build_req_me = {}
		for i in xrange(param1.readUnsignedShort()):
			self.team_build_req_me[i] = param1.readUTF()

		self.team_build_assistants = {}
		for i in xrange(param1.readUnsignedShort()):
			self.team_build_assistants[i] = PTeamBuildAssistants()
			self.team_build_assistants[i].read(param1)
		self.contract_expire_quests = {}
		for i in xrange(param1.readUnsignedShort()):
			self.contract_expire_quests[i] = i_PContractExpireQuest()
			self.contract_expire_quests[i].read(param1)


	def write(self, param1):
		#pylint: disable=unused-argument
		raise Exception("ERROR: can\'t write collections, assoc writing not implemented. FIXME PLEASE!!!")

class PReference(object):
	__slots__ = ['energy_repair_time', 'chop_tree_time', 'drop_stone_time', 'drop_garbage_time', 'sow_seeds_time', 'harvest_time', 'attack_monster', 'feed_animal', 'process_building', 'disable_viral_price', 'dealspot_paying_user', 'dealspot_non_paying_user', 'level_acceleration_time', 'level_acceleration', 'level_acceleration_prize', 'daily_deal_discount', 'raid_energy_leader', 'raid_energy_member', 'raid_timeout', 'raid_timeout_reset_price', 'raid_energy_reg_interval', 'raid_energy_per_rare', 'promo_banner', 'promo_icon', 'promo_date_start', 'promo_date_finish', 'promo_snetwork', 'comp_enter_fruits_cnt', 'comp_enter_price','comp_max_points', 'smart_cloner_price', 'stupid_cloner_price', 'event_split_by_lvl', 'clan_level_members_cnt', 'clan_dissolution_period', 'clan_targets_limit', 'clan_key_creation', 'clan_start_time_expedition', 'clan_create_required_level', 'clan_rename_price', 'cost_send_gifts_all', 'contract_cooldown_refused', 'contract_cooldown_finish', 'contract_cost_reset_cldrefused', 'contract_cost_reset_cldfinish', 'snetwork_currency_conv', 'clan_raid_energy_leader', 'clan_raid_energy_member', 'clan_raid_energy_reg_interval', 'clan_raid_energy_per_rare', 'limit_random_friends_cnt', 'trade_cell_price', 'trade_max_count_rare_in_offer','child_need_first_cooldown','child_improving_price','child_set_gender_price','team_build_max_party','team_build_party_perc','efarm_boost_periods','efarm_max_buy_slot','efarm_slot_cost']
	def __init__(self, param1 = 0, param2 = 0, param3 = 0, param4 = 0, param5 = 0, param6 = 0, param7 = 0, param8 = 0, param9 = 0, param10 = None, param11 = False, param12 = False, param13 = 0, param14 = 0, param15 = None, param16 = 0, param17 = 0, param18 = 0, param19 = 0, param20 = None, param21 = 0, param22 = 0, param23 = "", param24 = "", param25 = 0, param26 = 0, param27 = "", param28 = 0, param29 = None, param301 = None, param30 = None, param31 = None, param32 = 0, param33 = None, param34 = 0, param35 = 0, param36 = None, param37 = 0, param38 = 0, param39 = None, param40 = None, param41 = None, param42 = None, param43 = None, param44 = None, param45 = None, param46 = 0, param47 = 0, param48 = 0, param49 = 0, param50 = 0, param51 = None, param52 = 0, param53 = None, param54 = None, param55 = None, param57 = None, param58 = None, param59 = None, param60 = None, param61 = None):
		self.energy_repair_time = param1
		self.chop_tree_time = param2
		self.drop_stone_time = param3
		self.drop_garbage_time = param4
		self.sow_seeds_time = param5
		self.harvest_time = param6
		self.attack_monster = param7
		self.feed_animal = param8
		self.process_building = param9
		self.disable_viral_price = param10
		self.dealspot_paying_user = param11
		self.dealspot_non_paying_user = param12
		self.level_acceleration_time = param13
		self.level_acceleration = param14
		self.level_acceleration_prize = param15
		self.daily_deal_discount = param16
		self.raid_energy_leader = param17
		self.raid_energy_member = param18
		self.raid_timeout = param19
		self.raid_timeout_reset_price = param20
		self.raid_energy_reg_interval = param21
		self.raid_energy_per_rare = param22
		self.promo_banner = param23
		self.promo_icon = param24
		self.promo_date_start = param25
		self.promo_date_finish = param26
		self.promo_snetwork = param27
		self.comp_enter_fruits_cnt = param28
		self.comp_enter_price = param29
		self.comp_max_points = param301
		self.smart_cloner_price = param30
		self.stupid_cloner_price = param31
		self.event_split_by_lvl = param32
		self.clan_level_members_cnt = param33
		self.clan_dissolution_period = param34
		self.clan_targets_limit = param35
		self.clan_key_creation = param36
		self.clan_start_time_expedition = param37
		self.clan_create_required_level = param38
		self.clan_rename_price = param39
		self.cost_send_gifts_all = param40
		self.contract_cooldown_refused = param41
		self.contract_cooldown_finish = param42
		self.contract_cost_reset_cldrefused = param43
		self.contract_cost_reset_cldfinish = param44
		self.snetwork_currency_conv = param45
		self.clan_raid_energy_leader = param46
		self.clan_raid_energy_member = param47
		self.clan_raid_energy_reg_interval = param48
		self.clan_raid_energy_per_rare = param49
		self.limit_random_friends_cnt = param50
		self.trade_cell_price = param51
		self.trade_max_count_rare_in_offer = param52
		self.child_need_first_cooldown = param53
		self.child_improving_price = param54
		self.child_set_gender_price = param55
		self.team_build_max_party = param57
		self.team_build_party_perc = param58
		self.efarm_boost_periods = param59
		self.efarm_max_buy_slot = param60
		self.efarm_slot_cost = param61

	def read(self, param1):
		self.energy_repair_time = param1.readUnsignedInt()
		self.chop_tree_time = param1.readUnsignedInt()
		self.drop_stone_time = param1.readUnsignedInt()
		self.drop_garbage_time = param1.readUnsignedInt()
		self.sow_seeds_time = param1.readUnsignedInt()
		self.harvest_time = param1.readUnsignedInt()
		self.attack_monster = param1.readUnsignedInt()
		self.feed_animal = param1.readUnsignedInt()
		self.process_building = param1.readUnsignedInt()
		self.disable_viral_price = PCost()
		self.disable_viral_price.read(param1)
		self.dealspot_paying_user = param1.readBoolean()
		self.dealspot_non_paying_user = param1.readBoolean()
		self.level_acceleration_time = param1.readDouble()
		self.level_acceleration = param1.readInt()
		self.level_acceleration_prize = {}
		for i in xrange(param1.readUnsignedShort()):
			self.level_acceleration_prize[i] = PCost()
			self.level_acceleration_prize[i].read(param1)
		self.daily_deal_discount = param1.readInt()
		self.raid_energy_leader = param1.readInt()
		self.raid_energy_member = param1.readInt()
		self.raid_timeout = param1.readDouble()
		self.raid_timeout_reset_price = PCosts()
		self.raid_timeout_reset_price.read(param1)
		self.raid_energy_reg_interval = param1.readDouble()
		self.raid_energy_per_rare = param1.readInt()
		self.promo_banner = param1.readUTF()
		self.promo_icon = param1.readUTF()
		self.promo_date_start = param1.readDouble()
		self.promo_date_finish = param1.readDouble()
		self.promo_snetwork = param1.readUTF()
		self.comp_enter_fruits_cnt = param1.readUnsignedInt()
		self.comp_enter_price = PCost()
		self.comp_enter_price.read(param1)
		self.comp_max_points = param1.readInt()
		self.smart_cloner_price = {}
		for i in xrange(param1.readUnsignedShort()):
			self.smart_cloner_price[i] = PCost()
			self.smart_cloner_price[i].read(param1)
		self.stupid_cloner_price = {}
		for i in xrange(param1.readUnsignedShort()):
			self.stupid_cloner_price[i] = PCost()
			self.stupid_cloner_price[i].read(param1)
		self.event_split_by_lvl = param1.readUnsignedInt()
		self.clan_level_members_cnt = {}
		for i in xrange(param1.readUnsignedShort()):
			self.clan_level_members_cnt[i] = param1.readUnsignedInt()
		self.clan_dissolution_period = param1.readDouble()
		self.clan_targets_limit = param1.readUnsignedByte()
		self.clan_key_creation = PCost()
		self.clan_key_creation.read(param1)
		self.clan_start_time_expedition = param1.readDouble()
		self.clan_create_required_level = param1.readUnsignedByte()
		self.clan_rename_price = PCost()
		self.clan_rename_price.read(param1)
		self.cost_send_gifts_all = PCost()
		self.cost_send_gifts_all.read(param1)
		self.contract_cooldown_refused = {}
		for i in xrange(param1.readUnsignedShort()):
			self.contract_cooldown_refused[i] = param1.readDouble()
		self.contract_cooldown_finish = {}
		for i in xrange(param1.readUnsignedShort()):
			self.contract_cooldown_finish[i] = param1.readDouble()
		self.contract_cost_reset_cldrefused = {}
		for i in xrange(param1.readUnsignedShort()):
			self.contract_cost_reset_cldrefused[i] = PCost()
			self.contract_cost_reset_cldrefused[i].read(param1)
		self.contract_cost_reset_cldfinish = {}
		for i in xrange(param1.readUnsignedShort()):
			self.contract_cost_reset_cldfinish[i] = PCost()
			self.contract_cost_reset_cldfinish[i].read(param1)
		self.snetwork_currency_conv = {}
		for i in xrange(param1.readUnsignedShort()):
			self.snetwork_currency_conv[i] = str_f()
			self.snetwork_currency_conv[i].read(param1)
		self.clan_raid_energy_leader = param1.readInt()
		self.clan_raid_energy_member = param1.readInt()
		self.clan_raid_energy_reg_interval = param1.readDouble()
		self.clan_raid_energy_per_rare = param1.readInt()
		self.limit_random_friends_cnt = param1.readInt()
		self.trade_cell_price = PCost()
		self.trade_cell_price.read(param1)
		self.trade_max_count_rare_in_offer = param1.readInt()
		self.child_need_first_cooldown = param1.readDouble()
		self.child_improving_price = PCost()
		self.child_improving_price.read(param1)
		self.child_set_gender_price = {}
		for i in xrange(param1.readUnsignedShort()):
			self.child_set_gender_price[i] = str_PCost()
			self.child_set_gender_price[i].read(param1)
		self.team_build_max_party = {}
		for i in xrange(param1.readUnsignedShort()):
			self.team_build_max_party[i] = param1.readInt()
		self.team_build_party_perc = param1.readInt()
		self.efarm_boost_periods = {}
		for i in xrange(param1.readUnsignedShort()):
			self.efarm_boost_periods[i] = time_time()
			self.efarm_boost_periods[i].read(param1)
		self.efarm_max_buy_slot = param1.readInt()
		self.efarm_slot_cost = {}
		for i in xrange(param1.readUnsignedShort()):
			self.efarm_slot_cost[i] = PCost()
			self.efarm_slot_cost[i].read(param1)

	def write(self, param1):
		param1.writeInt(self.energy_repair_time)
		param1.writeInt(self.chop_tree_time)
		param1.writeInt(self.drop_stone_time)
		param1.writeInt(self.drop_garbage_time)
		param1.writeInt(self.sow_seeds_time)
		param1.writeInt(self.harvest_time)
		param1.writeInt(self.attack_monster)
		param1.writeInt(self.feed_animal)
		param1.writeInt(self.process_building)
		self.disable_viral_price.write(param1)
		param1.writeBoolean(self.dealspot_paying_user)
		param1.writeBoolean(self.dealspot_non_paying_user)
		param1.writeDouble(self.level_acceleration_time)
		param1.writeInt(self.level_acceleration)
		if self.level_acceleration_prize == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.level_acceleration_prize))
			for i in xrange(len(self.level_acceleration_prize)):
				self.level_acceleration_prize[self].write(param1)
		param1.writeInt(self.daily_deal_discount)
		param1.writeInt(self.raid_energy_leader)
		param1.writeInt(self.raid_energy_member)
		param1.writeDouble(self.raid_timeout)
		self.raid_timeout_reset_price.write(param1)
		param1.writeDouble(self.raid_energy_reg_interval)
		param1.writeInt(self.raid_energy_per_rare)
		param1.writeUTF(self.promo_banner)
		param1.writeUTF(self.promo_icon)
		param1.writeDouble(self.promo_date_start)
		param1.writeDouble(self.promo_date_finish)
		param1.writeUTF(self.promo_snetwork)
		param1.writeInt(self.comp_enter_fruits_cnt)
		self.comp_enter_price.write(param1)
		param1.writeInt(self.comp_max_points)
		if self.smart_cloner_price == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.smart_cloner_price))
			for i in xrange(len(self.smart_cloner_price)):
				self.smart_cloner_price[i].write(param1)
		if self.stupid_cloner_price == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.stupid_cloner_price))
			for i in xrange(len(self.stupid_cloner_price)):
				self.stupid_cloner_price[i].write(param1)
		param1.writeInt(self.event_split_by_lvl)
		if self.clan_level_members_cnt == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.clan_level_members_cnt))
			for i in xrange(len(self.clan_level_members_cnt)):
				param1.writeInt(self.clan_level_members_cnt[i])
		param1.writeDouble(self.clan_dissolution_period)
		param1.writeByte(self.clan_targets_limit)
		self.clan_key_creation.write(param1)
		param1.writeDouble(self.clan_start_time_expedition)
		param1.writeByte(self.clan_create_required_level)
		self.clan_rename_price.write(param1)
		self.cost_send_gifts_all.write(param1)
		if self.contract_cooldown_refused == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.contract_cooldown_refused))
			for i in xrange(len(self.contract_cooldown_refused)):
				param1.writeDouble(self.contract_cooldown_refused[i])
		if self.contract_cooldown_finish == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.contract_cooldown_finish))
			for i in xrange(len(self.contract_cooldown_finish)):
				param1.writeDouble(self.contract_cooldown_finish[i])
		if self.contract_cost_reset_cldrefused == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.contract_cost_reset_cldrefused))
			for i in xrange(len(self.contract_cost_reset_cldrefused)):
				self.contract_cost_reset_cldrefused[i].write(param1)
		if self.contract_cost_reset_cldfinish == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.contract_cost_reset_cldfinish))
			for i in xrange(len(self.contract_cost_reset_cldfinish)):
				self.contract_cost_reset_cldfinish[i].write(param1)
		if self.snetwork_currency_conv == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.snetwork_currency_conv))
			for i in xrange(len(self.snetwork_currency_conv)):
				self.snetwork_currency_conv[i].write(param1)
		param1.writeInt(self.clan_raid_energy_leader)
		param1.writeInt(self.clan_raid_energy_member)
		param1.writeDouble(self.clan_raid_energy_reg_interval)
		param1.writeInt(self.clan_raid_energy_per_rare)
		param1.writeInt(self.limit_random_friends_cnt)
		self.trade_cell_price.write(param1)
		param1.writeInt(self.trade_max_count_rare_in_offer)
		param1.writeDouble(self.child_need_first_cooldown)
		self.child_improving_price.write(param1)
		if self.child_set_gender_price == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.child_set_gender_price))
			for i in xrange(len(self.child_set_gender_price)):
				self.child_set_gender_price[i].write(param1)
		if self.team_build_max_party == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.team_build_max_party))
			for i in xrange(len(self.team_build_max_party)):
				param1.writeInt(self.team_build_max_party[i])
		param1.writeInt(self.team_build_party_perc)
		if self.efarm_boost_periods == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.efarm_boost_periods))
			for i in xrange(len(self.efarm_boost_periods)):
				self.efarm_boost_periods[i].write(param1)
		param1.writeInt(self.efarm_max_buy_slot)
		if self.efarm_slot_cost == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.efarm_slot_cost))
			for i in xrange(len(self.efarm_slot_cost)):
				self.efarm_slot_cost[i].write(param1)

class PWhReserveWTime(object):
	__slots__ = ['reserve', 'time']
	def __init__(self, param1 = None, param2 = 0):
		self.reserve = param1
		self.time = param2

	def write(self, param1):
		self.reserve.write(param1)
		param1.writeDouble(self.time)

	def read(self, param1):
		self.reserve = PWarehouseReserve()
		self.reserve.read(param1)
		self.time = param1.readDouble()

class PLevelInfo(object):
	__slots__ = ['l_id', 'l_require', 'l_bonus', 'l_additional_bonus']
	def __init__(self, param1 = 0, param2 = 0, param3 = None, param4 = None):
		self.l_id = param1
		self.l_require = param2
		self.l_bonus = param3
		self.l_additional_bonus = param4

	def write(self, param1):
		param1.writeInt(self.l_id)
		param1.writeDouble(self.l_require)
		self.l_bonus.write(param1)
		if self.l_additional_bonus != None:
			param1.writeByte(1)
			self.l_additional_bonus.write(param1)
		else:
			param1.writeByte(0)

	def read(self, param1):
		self.l_id = param1.readUnsignedInt()
		self.l_require = param1.readDouble()
		self.l_bonus = PCosts()
		self.l_bonus.read(param1)
		if param1.readUnsignedByte() == 1:
			self.l_additional_bonus = PQuestPrize()
			self.l_additional_bonus.read(param1)
		else:
			self.l_additional_bonus = None

class PStage(object):
	BUILD_WORKSHOP = 89
	BUY_BUSH = 88
	MAKE_HY_STRAWBERRY = 87
	FST_MAKE_HY_STRAWBERRY = 86
	NEW_HY_STRAWBERRY = 85
	BUY_ANYMALS = 84
	MORE_CHICKENS_2 = 83
	GET_MONEY = 82
	GET_MONEY_OPENED = 81
	PLANT_MORE = 80
	FERTILIZER_QUEST = 79
	HAVE_FERTILIZER = 78
	ATTACK_WOLFS = 77
	GET_CHICKEN = 76
	STRAWBERRY_SELL_QUEST = 75
	DROP_STONES = 74
	BUY_ENERGY = 73
	FIND_TIMBER_CREATE_CHICKEN = 72
	FEED_CAT = 71
	STRAWBERRY_QUEST = 70
	DECOR_STUDY = 69
	BUILD_FENCE = 68
	SELL_CUCUMBER_1 = 67
	PLANT_HY_STRWB_CLOSE_LAB = 66
	HYBRID_STUDY_CLOSE_DIALOG = 65
	Q_PLANTS_STUDY_STRWB2 = 64
	Q_PLANTS_STUDY_STRWB1 = 63
	Q_PLANTS_STRAWBERRY_SHOP = 62
	FST_WOOD_CLICKED = 61
	SND_EXPANSION = 60
	HY_CARROT = 59
	BUILD_MILL = 58
	BUILD_BRIDGE = 57
	EXPAND_TERRITORY = 56
	MAKE_PAINT = 55
	PROF_ADVICE = 54
	BUILD_GREENHOUSE = 53
	STUDY_GLUE = 52
	RESEARCH_SND_STRAWBERRY = 51
	FST_STRAWBERRY_PLANT = 50
	FST_STRAWBERRY_GO_WAREHOUSE = 49
	SPEED_UP_CRAFT = 48
	START_FST_CRAFT = 47
	END_RESEARCH_HY_STRAWBERRY = 46
	SPEED_UP_RESEARCH = 45
	START_RESEARCH_HY_STRAWBERRY = 44
	LAB_STUDY = 43
	LAB_PRE_FINISH = 42
	LAB_START_STUDY = 41
	BUY_LAB = 40
	FIND_LAB = 39
	BUY_GOLD = 38
	SELL_EGGPLANTS = 37
	Q_PLANT_STUDY_1 = 36
	Q_BUILD_WAREHOUSE = 35
	WAREHOUSE_PRE_FINISH = 34
	WAREHOUSE_START_STUDY = 33
	PUSH_WAREHOUSE = 32
	TAKE_WAREHOUSE = 31
	FIND_WAREHOUSE = 30
	QEGGPLANTS_STUDY = 29
	SND_EGGPLANT = 28
	FST_EGGPLANT = 27
	HOME_FINISH_STUDY = 26
	HOME_PRE_FINISH = 25
	Q_FIRST_MONSTER = 24
	SND_DAMAGE = 23
	FST_DAMAGE = 22
	DROP_WOOD_STUDY = 21
	FST_DROP_WOOD = 20
	Q_LOOK_AROUND = 19
	TAKE_DROP = 18
	FIRST_LOOK_AROUND = 17
	CLOSE_STORY = 16
	NEW_MAP = 15
	STORM_FINISHED = 14
	INTRO_EGGPLANT = 13
	SND_WOOD_DROPPED = 12
	INTRO_STARTED = 11
	FST_WOOD_DROPPED = 10
	FARM_LOADED = 9
	STATIC_LOADED = 8
	SEX_CHOISE = 7
	CHARACTER_CHOISE = 6
	CHOISE_STARTED = 5
	LANG_FILE = 4
	START_CLIENT = 3
	INIT_CLIENT = 2
	NEW_USER = 1
	STAGE_UNKONWN = 0

	__slots__ = ['variance']
	def __init__(self, param1 = 0):
		self.variance = param1

	def write(self, param1):
		param1.writeByte(self.variance)

	def read(self, param1):
		self.variance = param1.readUnsignedByte()

class PDcComfortLevel(object):
	__slots__ = ['comfort_id', 'comfort_require']
	def __init__(self, param1 = 0, param2 = 0):
		self.comfort_id = param1
		self.comfort_require = param2

	def write(self, param1):
		param1.writeInt(self.comfort_id)
		param1.writeInt(self.comfort_require)

	def read(self, param1):
		self.comfort_id = param1.readUnsignedInt()
		self.comfort_require = param1.readUnsignedInt()

class PComboInfo(object):
	__slots__ = ['combo_level', 'combo_clicks', 'combo_bonus']
	def __init__(self, param1 = 0, param2 = 0, param3 = None):
		self.combo_level = param1
		self.combo_clicks = param2
		self.combo_bonus = param3

	def write(self, param1):
		param1.writeByte(self.combo_level)
		param1.writeShort(self.combo_clicks)
		self.combo_bonus.write(param1)

	def read(self, param1):
		self.combo_level = param1.readUnsignedByte()
		self.combo_clicks = param1.readUnsignedShort()
		self.combo_bonus = PCosts()
		self.combo_bonus.read(param1)


class PShopObj(object):
	SPEC_SUITCASE = 13
	SPEC_FAIR_BUILDING = 12
	SPEC_CASE = 11
	SPEC_SEGWAY = 10
	SPEC_WEAPON = 9
	SPEC_RARE_ITEM = 8
	SPEC_DECOR = 7
	SPEC_TILE = 6
	SPEC_EXPANSION = 5
	SPEC_ENERGY = 4
	SPEC_FERTILIZER = 3
	SPEC_YANIMAL = 2
	SPEC_BUILDING = 1
	SPEC_SEED = 0

	__slots__ = ['requirements', 'order', 'price', 'scurrency_price', 'specials', 'can_buy', 'unlock', 'game_event_id', 'sale_id', 'spec_variance', 'spec_value', 'can_send','map_id']
	def __init__(self, param1 = None, param2 = 0, param3 = None, param4 = None, param5 = None, param6 = False, param7 = None, param8 = 0, param9 = None, param10 = 0, param11 = None, param12 = False, param13 = None):
		self.requirements = param1
		self.order = param2
		self.price = param3
		self.scurrency_price = param4
		self.specials = param5
		self.can_buy = param6
		self.unlock = param7
		self.game_event_id = param8
		self.sale_id = param9
		self.spec_variance = param10
		self.spec_value = param11
		self.can_send = param12
		self.map_id = param12

	def read(self, param1):
		self.requirements = {}
		for i in xrange(param1.readUnsignedShort()):
			self.requirements[i] = PRequirements()
			self.requirements[i].read(param1)
		self.order = param1.readUnsignedShort()
		self.price = PCosts()
		self.price.read(param1)
		if param1.readUnsignedByte() == 1:
			self.scurrency_price = param1.readDouble()
		else:
			self.scurrency_price = None
		self.specials = {}
		for i in xrange(param1.readUnsignedShort()):
			self.specials[i] = PSpecials()
			self.specials[i].read(param1)
		self.can_buy = param1.readBoolean()
		if param1.readUnsignedByte() == 1:
			self.unlock = PShopUnlock()
			self.unlock.read(param1)
		else:
			self.unlock = None
		if param1.readUnsignedByte() == 1:
			self.game_event_id = param1.readUnsignedByte()
		else:
			self.game_event_id = None

		self.sale_id = {}
		for i in xrange(param1.readUnsignedShort()):
			self.sale_id[i] = param1.readUnsignedInt()

		self.spec_variance = param1.readUnsignedByte()
		if self.spec_variance == self.SPEC_SEED:
			self.spec_value = PShopSeed()
			self.spec_value.read(param1)
		elif self.spec_variance == self.SPEC_BUILDING:
			self.spec_value = PShopBuilding()
			self.spec_value.read(param1)
		elif self.spec_variance ==self.SPEC_YANIMAL:
			self.spec_value = PShopAnimal()
			self.spec_value.read(param1)
		elif self.spec_variance == self.SPEC_FERTILIZER:
			self.spec_value = PShopFertilizer()
			self.spec_value.read(param1)
		elif self.spec_variance == self.SPEC_ENERGY:
			self.spec_value = PShopEnergy()
			self.spec_value.read(param1)
		elif self.spec_variance == self.SPEC_EXPANSION:
			self.spec_value = PShopExpansion()
			self.spec_value.read(param1)
		elif self.spec_variance == self.SPEC_TILE:
			self.spec_value = PShopTile()
			self.spec_value.read(param1)
		elif self.spec_variance == self.SPEC_DECOR:
			self.spec_value = PShopDecor()
			self.spec_value.read(param1)
		elif self.spec_variance == self.SPEC_RARE_ITEM:
			self.spec_value = PShopRareItem()
			self.spec_value.read(param1)
		elif self.spec_variance == self.SPEC_WEAPON:
			self.spec_value = PShopWeapon()
			self.spec_value.read(param1)
		elif self.spec_variance == self.SPEC_SEGWAY:
			self.spec_value = PShopSegway()
			self.spec_value.read(param1)
		elif self.spec_variance == self.SPEC_CASE:
			self.spec_value = PShopCase()
			self.spec_value.read(param1)
		elif self.spec_variance == self.SPEC_FAIR_BUILDING:
			self.spec_value = param1.readUTF()
		elif self.spec_variance == self.SPEC_SUITCASE:
			self.spec_value = PShopSuitcase()
			self.spec_value.read(param1)
		else:
			raise Exception ("ERROR: incorrect PShopObj data")
		self.can_send = param1.readBoolean()
		if param1.readUnsignedByte() == 1:
			self.map_id = param1.readInt()
		else:
			self.map_id = None

	def write(self, param1):
		if self.requirements == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.requirements))
			for i in xrange(len(self.requirements)):
				self.requirements[i].write(param1)
		param1.writeShort(self.order)
		self.price.write(param1)
		if self.scurrency_price != None:
			param1.writeByte(1)
			param1.writeDouble(self.scurrency_price)
		else:
			param1.writeByte(0)
		if self.specials == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.specials))
			for i in xrange(len(self.specials)):
				self.specials[i].write(param1)
		param1.writeBoolean(self.can_buy)
		if self.unlock != None:
			param1.writeByte(1)
			self.unlock.write(param1)
		else:
			param1.writeByte(0)
		if self.game_event_id != None:
			param1.writeByte(1)
			param1.writeByte(self.game_event_id)
		else:
			param1.writeByte(0)
		if self.sale_id == None:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.sale_id))
			for i in xrange(len(self.sale_id)):
				param1.writeInt(self.sale_id[i])

		param1.writeByte(self.spec_variance)
		if self.spec_variance == self.SPEC_SEED:
			self.spec_value.write(param1)
		elif self.spec_variance == self.SPEC_BUILDING:
			self.spec_value.write(param1)
		elif self.spec_variance == self.SPEC_YANIMAL:
			self.spec_value.write(param1)
		elif self.spec_variance == self.SPEC_FERTILIZER:
			self.spec_value.write(param1)
		elif self.spec_variance == self.SPEC_ENERGY:
			self.spec_value.write(param1)
		elif self.spec_variance == self.SPEC_EXPANSION:
			self.spec_value.write(param1)
		elif self.spec_variance == self.SPEC_TILE:
			self.spec_value.write(param1)
		elif self.spec_variance == self.SPEC_DECOR:
			self.spec_value.write(param1)
		elif self.spec_variance == self.SPEC_RARE_ITEM:
			self.spec_value.write(param1)
		elif self.spec_variance == self.SPEC_WEAPON:
			self.spec_value.write(param1)
		elif self.spec_variance == self.SPEC_SEGWAY:
			self.spec_value.write(param1)
		elif self.spec_variance == self.SPEC_CASE:
			self.spec_value.write(param1)
		elif self.spec_variance == self.SPEC_FAIR_BUILDING:
			param1.writeUTF(self.spec_value)
		elif self.spec_variance == self.SPEC_SUITCASE:
			self.spec_value.write(param1)
		else:
			raise Exception ("ERROR: incorrect PShopObj data")
		param1.writeBoolean(self.can_send)
		if self.map_id != None:
			param1.writeByte(1)
			param1.writeInt(self.map_id)
		else:
			param1.writeByte(0)

class PShopSeed(object):
	__slots__ = ['sb_kind', 'sb_seed_kind', 'sb_seed_cost', 'sb_grown_time', 'sb_withered_time', 'sb_fruit_kind', 'sb_fruit_cost', 'sb_sell_cost_by_state', 'sb_shop_pack_cnt', 'sb_category', 'sb_reserve_cnt']
	def __init__(self, param1 = "", param2 = "", param3 = None, param4 = 0, param5 = 0, param6 = "", param7 = None, param8 = None, param9 = 0, param10 = "", param11 = 0):
		self.sb_kind = param1
		self.sb_seed_kind = param2
		self.sb_seed_cost = param3
		self.sb_grown_time = param4
		self.sb_withered_time = param5
		self.sb_fruit_kind = param6
		self.sb_fruit_cost = param7
		self.sb_sell_cost_by_state = param8
		self.sb_shop_pack_cnt = param9
		self.sb_category = param10
		self.sb_reserve_cnt = param11

	def write(self, param1):
		param1.writeUTF(self.sb_kind)
		param1.writeUTF(self.sb_seed_kind)
		self.sb_seed_cost.write(param1)
		param1.writeDouble(self.sb_grown_time)
		param1.writeDouble(self.sb_withered_time)
		param1.writeUTF(self.sb_fruit_kind)
		self.sb_fruit_cost.write(param1)
		self.sb_sell_cost_by_state.write(param1)
		if self.sb_shop_pack_cnt == None:
			param1.writeByte(1)
			param1.writeByte(self.sb_shop_pack_cnt)
		else:
			param1.writeByte(0)
		param1.writeUTF(self.sb_category)
		param1.writeInt(self.sb_reserve_cnt)

	def read(self, param1):
		self.sb_kind = param1.readUTF()
		self.sb_seed_kind = param1.readUTF()
		self.sb_seed_cost = PCosts()
		self.sb_seed_cost.read(param1)
		self.sb_grown_time = param1.readDouble()
		self.sb_withered_time = param1.readDouble()
		self.sb_fruit_kind = param1.readUTF()
		self.sb_fruit_cost = PCosts()
		self.sb_fruit_cost.read(param1)
		self.sb_sell_cost_by_state = PSeedbedCostByState()
		self.sb_sell_cost_by_state.read(param1)
		if param1.readUnsignedByte() == 1:
			self.sb_shop_pack_cnt = param1.readUnsignedByte()
		else:
			self.sb_shop_pack_cnt = None
		self.sb_category = param1.readUTF()
		self.sb_reserve_cnt = param1.readInt()

class PSeedbedCostByState(object):
	__slots__ = ['sb_grown_cost', 'sb_fully_grown_cost', 'sb_withered_cost']
	def __init__(self, param1 = None, param2 = None, param3 = None):
		self.sb_grown_cost = param1
		self.sb_fully_grown_cost = param2
		self.sb_withered_cost = param3

	def write(self, param1):
		self.sb_grown_cost.write(param1)
		self.sb_fully_grown_cost.write(param1)
		self.sb_withered_cost.write(param1)

	def read(self, param1):
		self.sb_grown_cost = PCosts()
		self.sb_grown_cost.read(param1)
		self.sb_fully_grown_cost = PCosts()
		self.sb_fully_grown_cost.read(param1)
		self.sb_withered_cost = PCosts()
		self.sb_withered_cost.read(param1)

class PShopBuilding(object):
	__slots__ = ['bl_kind', 'bl_upgrade_level', 'bl_steps_cnt', 'bl_step_cost', 'bl_finish_step_cost', 'bl_sell_cost_by_state', 'bl_uniq', 'bl_can_move', 'bl_mouseover_active', 'bl_capacity', 'bl_k_price', 'bl_k_finish_step_cost', 'bl_index_tab', 'bl_can_enter', 'bl_improvement_kind', 'bl_category', 'bl_reset_bonus_price', 'bl_max_level', 'bl_upgrade_price', 'bl_upgrade_specials', 'bl_facility_price', 'bl_facility_sale_id', 'bl_upgrade_requirements']
	def __init__(self, param1 = "", param2 = 0, param3 = 0, param4 = None, param5 = None, param6 = None, param7 = False, param8 = False, param9 = False, param10 = 0, param11 = None, param12 = None, param13 = 0, param14 = False, param15 = "", param16 = "", param17 = None, param18 = 0, param19 = None, param20 = None, param21 = None, param22 = None, param23 = None):
		self.bl_kind = param1
		self.bl_upgrade_level = param2
		self.bl_steps_cnt = param3
		self.bl_step_cost = param4
		self.bl_finish_step_cost = param5
		self.bl_sell_cost_by_state = param6
		self.bl_uniq = param7
		self.bl_can_move = param8
		self.bl_mouseover_active = param9
		self.bl_capacity = param10
		self.bl_k_price = param11
		self.bl_k_finish_step_cost = param12
		self.bl_index_tab = param13
		self.bl_can_enter = param14
		self.bl_improvement_kind = param15
		self.bl_category = param16
		self.bl_reset_bonus_price = param17
		self.bl_max_level = param18
		self.bl_upgrade_price = param19
		self.bl_upgrade_specials = param20
		self.bl_facility_price = param21
		self.bl_facility_sale_id = param22
		self.bl_upgrade_requirements = param23

	def read(self, param1):
		self.bl_kind = param1.readUTF()
		self.bl_upgrade_level = param1.readInt()
		self.bl_steps_cnt = param1.readUnsignedByte()
		self.bl_step_cost = PCosts()
		self.bl_step_cost.read(param1)
		self.bl_finish_step_cost = PCosts()
		self.bl_finish_step_cost.read(param1)
		if param1.readUnsignedByte() == 1:
			self.bl_sell_cost_by_state = PBuildingCostState()
			self.bl_sell_cost_by_state.read(param1)
		else:
			self.bl_sell_cost_by_state = None
		self.bl_uniq = param1.readBoolean()
		self.bl_can_move = param1.readBoolean()
		self.bl_mouseover_active = param1.readBoolean()
		if param1.readUnsignedByte() == 1:
			self.bl_capacity = param1.readInt()
		else:
			self.bl_capacity = None
		if param1.readUnsignedByte() == 1:
			self.bl_k_price = f_i()
			self.bl_k_price.read(param1)
		else:
			self.bl_k_price = None
		if param1.readUnsignedByte() == 1:
			self.bl_k_finish_step_cost = f_i()
			self.bl_k_finish_step_cost.read(param1)
		else:
			self.bl_k_finish_step_cost = None
		self.bl_index_tab = param1.readUnsignedByte()
		self.bl_can_enter = param1.readBoolean()
		if param1.readUnsignedByte() == 1:
			self.bl_improvement_kind = param1.readUTF()
		else:
			self.bl_improvement_kind = None
		self.bl_category = param1.readUTF()
		self.bl_reset_bonus_price = PCosts()
		self.bl_reset_bonus_price.read(param1)
		self.bl_max_level = param1.readInt()
		self.bl_upgrade_price = {}
		for i in xrange(param1.readUnsignedShort()):
			self.bl_upgrade_price[i] = PCosts()
			self.bl_upgrade_price[i].read(param1)
		self.bl_upgrade_specials = {}
		for i in xrange(param1.readUnsignedShort()):
			self.bl_upgrade_specials[i] = PSpecials()
			self.bl_upgrade_specials[i].read(param1)
		self.bl_facility_price = {}
		for i in xrange(param1.readUnsignedShort()):
			self.bl_facility_price[i] = PBuildingFacility_PCosts()
			self.bl_facility_price[i].read(param1)
		self.bl_facility_sale_id = {}
		for i in xrange(param1.readUnsignedShort()):
			self.bl_facility_sale_id[i] = param1.readUnsignedInt()

		self.bl_upgrade_requirements = {}
		for i in xrange(param1.readUnsignedShort()):
			self.bl_upgrade_requirements[i] = {}
			for j in xrange(param1.readUnsignedShort()):
				self.bl_upgrade_requirements[i][j] = PRequirements()
				self.bl_upgrade_requirements[i][j].read(param1)

	def write(self, param1):
		param1.writeUTF(self.bl_kind)
		param1.writeInt(self.bl_upgrade_level)
		param1.writeByte(self.bl_steps_cnt)
		self.bl_step_cost.write(param1)
		self.bl_finish_step_cost.write(param1)
		if self.bl_sell_cost_by_state != None:
			param1.writeByte(1)
			self.bl_sell_cost_by_state.write(param1)
		else:
			param1.writeByte(0)
		param1.writeBoolean(self.bl_uniq)
		param1.writeBoolean(self.bl_can_move)
		param1.writeBoolean(self.bl_mouseover_active)
		if self.bl_capacity != None:
			param1.writeByte(1)
			param1.writeInt(self.bl_capacity)
		else:
			param1.writeByte(0)
		if self.bl_k_price != None:
			param1.writeByte(1)
			self.bl_k_price.write(param1)
		else:
			param1.writeByte(0)
		if self.bl_k_finish_step_cost != None:
			param1.writeByte(1)
			self.bl_k_finish_step_cost.write(param1)
		else:
			param1.writeByte(0)
		param1.writeByte(self.bl_index_tab)
		param1.writeBoolean(self.bl_can_enter)
		if self.bl_improvement_kind != None:
			param1.writeByte(1)
			param1.writeUTF(self.bl_improvement_kind)
		else:
			param1.writeByte(0)
		param1.writeUTF(self.bl_category)
		self.bl_reset_bonus_price.write(param1)
		param1.writeInt(self.bl_max_level)
		if self.bl_upgrade_price == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.bl_upgrade_price))
			for i in xrange(len(self.bl_upgrade_price)):
				self.bl_upgrade_price[i].write(param1)
		if self.bl_upgrade_specials == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.bl_upgrade_specials))
			for i in xrange(len(self.bl_upgrade_specials)):
				self.bl_upgrade_specials[i].write(param1)
		if self.bl_facility_price == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.bl_facility_price))
			for i in xrange(len(self.bl_facility_price)):
				self.bl_facility_price[i].write(param1)
		if self.bl_facility_sale_id == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.bl_facility_sale_id))
			for i in xrange(len(self.bl_facility_sale_id)):
				param1.writeInt(self.bl_facility_sale_id[i])

		if self.bl_upgrade_requirements == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.bl_upgrade_requirements))
			for i in xrange(len(self.bl_upgrade_requirements)):
				if self.bl_upgrade_requirements[i] == {}:
					param1.writeShort(0)
				else:
					param1.writeShort(len(self.bl_upgrade_requirements[i]))
					for j in xrange(len(self.bl_upgrade_requirements[i])):
						self.bl_upgrade_requirements[i][j].write(param1)


class PBuildingCostState(object):
	__slots__ = ['bl_in_progress_cost', 'bl_finished_cost']
	def __init__(self, param1 = None, param2 = None):
		self.bl_in_progress_cost = param1
		self.bl_finished_cost = param2

	def write(self, param1):
		self.bl_in_progress_cost.write(param1)
		self.bl_finished_cost.write(param1)

	def read(self, param1):
		self.bl_in_progress_cost = PCosts()
		self.bl_in_progress_cost.read(param1)
		self.bl_finished_cost = PCosts()
		self.bl_finished_cost.read(param1)

class PShopAnimal(object):
	__slots__ = ['an_kind', 'an_young_kind', 'an_cost_by_state', 'an_feeds_cnt_by_step', 'an_feeds_period', 'an_buildings', 'an_free_cnt', 'an_category']
	def __init__(self, param1 = "", param2 = "", param3 = None, param4 = 0, param5 = 0, param6 = None, param7 = 0, param8 = ""):
		self.an_kind = param1
		self.an_young_kind = param2
		self.an_cost_by_state = param3
		self.an_feeds_cnt_by_step = param4
		self.an_feeds_period = param5
		self.an_buildings = param6
		self.an_free_cnt = param7
		self.an_category = param8

	def __repr__(self):
		# Just some of it
		return "{'an_kind':%s,'an_young_kind':%s,'an_category':%s}" % (self.an_kind, self.an_young_kind, self.an_category)

	def write(self,param1):
		param1.writeUTF(self.an_kind)
		param1.writeUTF(self.an_young_kind)
		self.an_cost_by_state.write(param1)
		param1.writeByte(self.an_feeds_cnt_by_step)
		param1.writeDouble(self.an_feeds_period)
		if self.an_buildings == {}:
			param1.writeByte(0)
		else:
			param1.writeByte(len(self.an_buildings))
			for i in xrange(len(self.an_buildings)):
				param1.writeUTF(self.an_buildings[i])
		param1.writeShort(self.an_free_cnt)
		param1.writeUTF(self.an_category)

	def read(self, param1):
		self.an_kind = param1.readUTF()
		self.an_young_kind = param1.readUTF()
		self.an_cost_by_state = PAnimalCostByState()
		self.an_cost_by_state.read(param1)
		self.an_feeds_cnt_by_step = param1.readUnsignedByte()
		self.an_feeds_period = param1.readDouble()
		self.an_buildings = {}
		for i in xrange(param1.readUnsignedByte()):
			self.an_buildings[i] = param1.readUTF()
		self.an_free_cnt = param1.readUnsignedShort()
		self.an_category = param1.readUTF()

class PAnimalCostByState(object):
	__slots__ = ['little', 'middle', 'big']
	def __init__(self, param1 = None, param2 = None, param3 = None):
		self.little = param1
		self.middle = param2
		self.big = param3

	def write(self, param1):
		self.little.write(param1)
		self.middle.write(param1)
		self.big.write(param1)

	def read(self, param1):
		self.little = PCosts()
		self.little.read(param1)
		self.middle = PCosts()
		self.middle.read(param1)
		self.big = PCosts()
		self.big.read(param1)

class PShopFertilizer(object):
	__slots__ = ['fert_kind', 'fert_cost', 'fert_action', 'fert_group_cnt', 'fert_kind_for_warehouse']
	def __init__(self, param1 = "", param2 = None, param3 = None, param4 = 0, param5 = ""):
		self.fert_kind = param1
		self.fert_cost = param2
		self.fert_action = param3
		self.fert_group_cnt = param4
		self.fert_kind_for_warehouse = param5

	def write(self, param1):
		param1.writeUTF(self.fert_kind)
		self.fert_cost.write(param1)
		self.fert_action.write(param1)
		param1.writeInt(self.fert_group_cnt)
		if self.fert_kind_for_warehouse != None:
			param1.writeByte(1)
			param1.writeUTF(self.fert_kind_for_warehouse)
		else:
			param1.writeByte(0)

	def read(self, param1):
		self.fert_kind = param1.readUTF()
		self.fert_cost = PCosts()
		self.fert_cost.read(param1)
		self.fert_action = PFertilizerAction()
		self.fert_action.read(param1)
		self.fert_group_cnt = param1.readInt()
		if param1.readUnsignedByte() == 1:
			self.fert_kind_for_warehouse = param1.readUTF()
		else:
			self.fert_kind_for_warehouse = None

class PFertilizerAction(object):
	IMMEDIATE_HUNGER = 18
	CHANGE_SKIN = 17
	ATTACK_MONSTERS = 16
	SUMMON_MONSTER = 15
	CHOP = 14
	MUL = 13
	RND = 12
	CLONE = 11
	CYCLE = 10
	ANTI_FRIEND = 9
	ANIMAL_PROGENY = 8
	ANIMAL_LAST_STEP = 7
	ANIMAL_NEXT_STEP = 6
	ANIMAL_GROWN = 5
	STONES_GROWN = 4
	WOODS_GROWN = 3
	FAST_GROWN_TIME = 2
	FAST_GROWN = 1
	ANTI_WITHERED = 0

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.ANTI_WITHERED:
			pass
		elif self.variance == self.FAST_GROWN:
			self.value = param1.readInt()
		elif self.variance == self.FAST_GROWN_TIME:
			self.value = param1.readDouble()
		elif self.variance == self.WOODS_GROWN:
			self.value = i_i_i_a_str_a()
			self.value.read(param1)
		elif self.variance == self.STONES_GROWN:
			self.value = i_oi_oi_a_str_a()
			self.value.read(param1)
		elif self.variance == self.ANIMAL_GROWN:
			self.value = param1.readInt()
		elif self.variance == self.ANIMAL_NEXT_STEP:
			pass
		elif self.variance == self.ANIMAL_LAST_STEP:
			pass
		elif self.variance == self.ANIMAL_PROGENY:
			self.value = {}
			for i in xrange(param1.readUnsignedShort()):
				self.value[i] = param1.readUTF()
		elif self.variance == self.ANTI_FRIEND:
			pass
		elif self.variance == self.CYCLE:
			self.value = param1.readInt()
		elif self.variance == self.CLONE:
			self.value = param1.readInt()
		elif self.variance == self.RND:
			pass
		elif self.variance == self.MUL:
			self.value = param1.readInt()
		elif self.variance == self.CHOP:
			self.value = i_str()
			self.value.read(param1)
		elif self.variance == self.SUMMON_MONSTER:
			pass
		elif self.variance == self.ATTACK_MONSTERS:
			self.value = param1.readInt()
		elif self.variance == self.CHANGE_SKIN:
			self.value = time_str()
			self.value.read(param1)
		elif self.variance == self.IMMEDIATE_HUNGER:
			pass
		else:
			raise Exception ("ERROR: incorrect PFertilizerAction data")

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.ANTI_WITHERED:
			pass
		elif self.variance == self.FAST_GROWN:
			param1.writeInt(self.value)
		elif self.variance == self.FAST_GROWN_TIME:
			param1.writeDouble(self.value)
		elif self.variance == self.WOODS_GROWN:
			self.value.write(param1)
		elif self.variance == self.STONES_GROWN:
			self.value.write(param1)
		elif self.variance == self.ANIMAL_GROWN:
			param1.writeInt(self.value)
		elif self.variance == self.ANIMAL_NEXT_STEP:
			pass
		elif self.variance == self.ANIMAL_LAST_STEP:
			pass
		elif self.variance == self.ANIMAL_PROGENY:
			if self.value == {}:
				param1.writeShort(0)
			else:
				param1.writeShort(len(self.value))
				for i in xrange(len(self.value)):
					param1.writeUTF(self.value[i])
		elif self.variance == self.ANTI_FRIEND:
			pass
		elif self.variance == self.CYCLE:
			param1.writeInt(self.value)
		elif self.variance == self.CLONE:
			param1.writeInt(self.value)
		elif self.variance == self.RND:
			pass
		elif self.variance == self.MUL:
			param1.writeInt(self.value)
		elif self.variance == self.CHOP:
			self.value.write(param1)
		elif self.variance == self.SUMMON_MONSTER:
			pass
		elif self.variance == self.ATTACK_MONSTERS:
			param1.writeInt(self.value)
		elif self.variance == self.CHANGE_SKIN:
			self.value.write(param1)
		elif self.variance == self.IMMEDIATE_HUNGER:
			pass
		else:
			raise Exception ("ERROR: incorrect PFertilizerAction data")

class PShopEnergy(object):
	EN_ACTION_CHILD_GROWTH = 13
	EN_ACTION_TIME_SCROLLING = 12
	EN_ACTION_INC_CLAN = 11
	EN_ACTION_SPEEDUP_CRAFT = 10
	EN_ACTION_INC_UNLIM = 9
	EN_ACTION_INC_LUCK = 8
	EN_ACTION_INC_ANIMAL_DROP = 7
	EN_ACTION_INC_GROWTH = 6
	EN_ACTION_INC_EXP = 5
	EN_ACTION_INC_FR = 4
	EN_ACTION_SPEEDING_UP = 3
	EN_ACTION_INC_MAX = 2
	EN_ACTION_FULL = 1
	EN_ACTION_INC = 0

	__slots__ = ['en_kind', 'en_action_variance', 'en_action_value', 'en_cost']
	def __init__(self, param1 = "", param2 = 0, param3 = None, param4 = None):
		self.en_kind = param1
		self.en_action_variance = param2
		self.en_action_value = param3
		self.en_cost = param4

	def read(self, param1):
		self.en_kind = param1.readUTF()
		self.en_action_variance = param1.readUnsignedByte()
		if self.en_action_variance == self.EN_ACTION_INC:
			self.en_action_value = param1.readUnsignedShort()
		elif self.en_action_variance == self.EN_ACTION_FULL:
			pass
		elif self.en_action_variance == self.EN_ACTION_INC_MAX:
			self.en_action_value = param1.readUnsignedByte()
		elif self.en_action_variance == self.EN_ACTION_SPEEDING_UP:
			self.en_action_value = param1.readDouble()
		elif self.en_action_variance == self.EN_ACTION_INC_FR:
			self.en_action_value = param1.readUnsignedByte()
		elif self.en_action_variance == self.EN_ACTION_INC_EXP:
			self.en_action_value = f_time()
			self.en_action_value.read(param1)
		elif self.en_action_variance == self.EN_ACTION_INC_GROWTH:
			self.en_action_value = param1.readDouble()
		elif self.en_action_variance == self.EN_ACTION_INC_ANIMAL_DROP:
			self.en_action_value = param1.readDouble()
		elif self.en_action_variance == self.EN_ACTION_INC_LUCK:
			self.en_action_value = param1.readDouble()
		elif self.en_action_variance == self.EN_ACTION_INC_UNLIM:
			self.en_action_value = param1.readDouble()
		elif self.en_action_variance == self.EN_ACTION_SPEEDUP_CRAFT:
			self.en_action_value = param1.readDouble()
		elif self.en_action_variance == self.EN_ACTION_INC_CLAN:
			self.en_action_value = param1.readUnsignedShort()
		elif self.en_action_variance == self.EN_ACTION_TIME_SCROLLING:
			self.en_action_value = param1.readDouble()
		elif self.en_action_variance == self.EN_ACTION_CHILD_GROWTH:
			self.en_action_value = time_time()
			self.en_action_value.read(param1)
		else:
			raise Exception ("ERROR: incorrect PShopEnergy data")
		self.en_cost = PCosts()
		self.en_cost.read(param1)

	def write(self, param1):
		param1.writeUTF(self.en_kind)
		param1.writeByte(self.en_action_variance)
		if self.en_action_variance == self.EN_ACTION_INC:
			param1.writeShort(self.en_action_value)
		elif self.en_action_variance == self.EN_ACTION_FULL:
			pass
		elif self.en_action_variance == self.EN_ACTION_INC_MAX:
			param1.writeByte(self.en_action_value)
		elif self.en_action_variance == self.EN_ACTION_SPEEDING_UP:
			param1.writeDouble(self.en_action_value)
		elif self.en_action_variance == self.EN_ACTION_INC_FR:
			param1.writeByte(self.en_action_value)
		elif self.en_action_variance == self.EN_ACTION_INC_EXP:
			self.en_action_value.write(param1)
		elif self.en_action_variance == self.EN_ACTION_INC_GROWTH:
			param1.writeDouble(self.en_action_value)
		elif self.en_action_variance == self.EN_ACTION_INC_ANIMAL_DROP:
			param1.writeDouble(self.en_action_value)
		elif self.en_action_variance == self.EN_ACTION_INC_LUCK:
			param1.writeDouble(self.en_action_value)
		elif self.en_action_variance == self.EN_ACTION_INC_UNLIM:
			param1.writeDouble(self.en_action_value)
		elif self.en_action_variance == self.EN_ACTION_SPEEDUP_CRAFT:
			param1.writeDouble(self.en_action_value)
		elif self.en_action_variance == self.EN_ACTION_INC_CLAN:
			param1.writeShort(self.en_action_value)
		elif self.en_action_variance == self.EN_ACTION_TIME_SCROLLING:
			param1.writeDouble(self.en_action_value)
		elif self.en_action_variance == self.EN_ACTION_CHILD_GROWTH:
			self.en_action_value.write(param1)
		else:
			raise Exception ("ERROR: incorrect PShopEnergy data")
		self.en_cost.write(param1)

class PShopExpansion(object):
	__slots__ = ['ex_num', 'ex_map_id', 'ex_areas', 'ex_roads', 'ex_unlock_prize']
	def __init__(self, param1 = 0, param2 = 0, param3 = None, param4 = None, param5 = None):
		self.ex_num = param1
		self.ex_map_id = param2
		self.ex_areas = param3
		self.ex_roads = param4
		self.ex_unlock_prize = param5

	def write(self, param1):
		param1.writeByte(self.ex_num)
		param1.writeByte(self.ex_map_id)
		if self.ex_areas == {}:
			param1.writeByte(0)
		else:
			param1.writeByte(len(self.ex_areas))
			for i in xrange(len(self.ex_areas)):
				self.ex_areas[i].write(param1)
		if self.ex_roads == {}:
			param1.writeByte(0)
		else:
			param1.writeByte(len(self.ex_roads))
			for i in xrange(len(self.ex_roads)):
				self.ex_roads[i].write(param1)
		self.ex_unlock_prize.write(param1)

	def read(self, param1):
		self.ex_num = param1.readUnsignedByte()
		self.ex_map_id = param1.readUnsignedByte()
		self.ex_areas = {}
		for i in xrange(param1.readUnsignedByte()):
			self.ex_areas[i] = Position_Position()
			self.ex_areas[i].read(param1)

		self.ex_roads = {}
		for i in xrange(param1.readUnsignedByte()):
			self.ex_roads[i] = Position_Position()
			self.ex_roads[i].read(param1)

		self.ex_unlock_prize = PQuestPrizes()
		self.ex_unlock_prize.read(param1)

class PShopTile(object):
	__slots__ = ['tl_kind', 'tl_sell_cost', 'tl_autoconnect', 'tl_comfort', 'tl_autocorrect_state']
	def __init__(self, param1 = "", param2 = None, param3 = False, param4 = 0, param5 = 0):
		self.tl_kind = param1
		self.tl_sell_cost = param2
		self.tl_autoconnect = param3
		self.tl_comfort = param4
		self.tl_autocorrect_state = param5

	def write(self, param1):
		param1.writeUTF(self.tl_kind)
		self.tl_sell_cost.write(param1)
		param1.writeBoolean(self.tl_autoconnect)
		param1.writeInt(self.tl_comfort)
		if self.tl_autocorrect_state != None:
			param1.writeByte(1)
			param1.writeInt(self.tl_autocorrect_state)
		else:
			param1.writeByte(0)

	def read(self, param1):
		self.tl_kind = param1.readUTF()
		self.tl_sell_cost = PCosts()
		self.tl_sell_cost.read(param1)
		self.tl_autoconnect = param1.readBoolean()
		self.tl_comfort = param1.readUnsignedInt()
		if param1.readUnsignedByte() == 1:
			self.tl_autocorrect_state = param1.readInt()
		else:
			self.tl_autocorrect_state = None

class PShopDecor(object):
	__slots__ = ['dc_kind', 'dc_sell_cost', 'dc_comfort', 'dc_can_rotate', 'dc_category']
	def __init__(self, param1 = "", param2 = None, param3 = 0, param4 = False, param5 = ""):
		self.dc_kind = param1
		self.dc_sell_cost = param2
		self.dc_comfort = param3
		self.dc_can_rotate = param4
		self.dc_category = param5

	def write(self, param1):
		param1.writeUTF(self.dc_kind)
		self.dc_sell_cost.write(param1)
		param1.writeInt(self.dc_comfort)
		param1.writeBoolean(self.dc_can_rotate)
		param1.writeUTF(self.dc_category)

	def read(self, param1):
		self.dc_kind = param1.readUTF()
		self.dc_sell_cost = PCosts()
		self.dc_sell_cost.read(param1)
		self.dc_comfort = param1.readUnsignedInt()
		self.dc_can_rotate = param1.readBoolean()
		self.dc_category = param1.readUTF()

class PShopRareItem(object):
	__slots__ = ['ra_kind', 'ra_group_cnt', 'ra_source']
	def __init__(self, param1 = "", param2 = 0, param3 = None):
		self.ra_kind = param1
		self.ra_group_cnt = param2
		self.ra_source = param3

	def write(self, param1):
		param1.writeUTF(self.ra_kind)
		param1.writeInt(self.ra_group_cnt)
		if self.ra_source == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.ra_source))
			for i in xrange(len(self.ra_source)):
				self.ra_source[i].write(param1)

	def read(self, param1):
		self.ra_kind = param1.readUTF()
		self.ra_group_cnt = param1.readInt()
		self.ra_source = {}
		for i in xrange(param1.readUnsignedShort()):
			self.ra_source[i] = PRareItemSource()
			self.ra_source[i].read(param1)

class PRareItemSource(object):
	TRADE = 7
	E_PLANTS = 6
	BUY = 5
	FRIEND = 4
	GIFT = 3
	CRAFT = 2
	DROP = 1
	ASK = 0

	__slots__ = ['variance']
	def __init__(self, param1 = 0):
		self.variance = param1

	def write(self, param1):
		param1.writeByte(self.variance)

	def read(self, param1):
		self.variance = param1.readUnsignedByte()

class PShopWeapon(object):
	__slots__ = ['wn_kind', 'wn_damage', 'wn_critical_damage', 'wn_chance_critical_damage', 'wn_energy', 'wn_group_cnt']
	def __init__(self, param1 = "", param2 = 0, param3 = 0, param4 = 0, param5 = 0, param6 = 0):
		self.wn_kind = param1
		self.wn_damage = param2
		self.wn_critical_damage = param3
		self.wn_chance_critical_damage = param4
		self.wn_energy = param5
		self.wn_group_cnt = param6

	def write(self, param1):
		param1.writeUTF(self.wn_kind)
		param1.writeByte(self.wn_damage)
		param1.writeByte(self.wn_critical_damage)
		param1.writeByte(self.wn_chance_critical_damage)
		param1.writeByte(self.wn_energy)
		param1.writeInt(self.wn_group_cnt)

	def read(self, param1):
		self.wn_kind = param1.readUTF()
		self.wn_damage = param1.readUnsignedByte()
		self.wn_critical_damage = param1.readUnsignedByte()
		self.wn_chance_critical_damage = param1.readUnsignedByte()
		self.wn_energy = param1.readUnsignedByte()
		self.wn_group_cnt = param1.readInt()

class PShopSegway(object):
	__slots__ = ['sw_kind', 'sw_cost_lease', 'sw_time_lease', 'sw_speed']
	def __init__(self, param1 = "", param2 = None, param3 = 0, param4 = 0):
		self.sw_kind = param1
		self.sw_cost_lease = param2
		self.sw_time_lease = param3
		self.sw_speed = param4

	def write(self,param1):
		param1.writeUTF(self.sw_kind)
		self.sw_cost_lease.write(param1)
		param1.writeDouble(self.sw_time_lease)
		param1.writeDouble(self.sw_speed)

	def read(self,param1):
		self.sw_kind = param1.readUTF()
		self.sw_cost_lease = PCosts()
		self.sw_cost_lease.read(param1)
		self.sw_time_lease = param1.readDouble()
		self.sw_speed = param1.readDouble()

class PShopCase(object):
	__slots__ = ['cs_kind', 'cs_prize', 'cs_coupon_kind', 'cs_coupon_discount', 'cs_coupon_max_count', 'cs_date_finish']
	def __init__(self, param1 = "", param2 = None, param3 = "", param4 = 0, param5 = 0, param6 = 0):
		self.cs_kind = param1
		self.cs_prize = param2
		self.cs_coupon_kind = param3
		self.cs_coupon_discount = param4
		self.cs_coupon_max_count = param5
		self.cs_date_finish = param6

	def write(self, param1):
		param1.writeUTF(self.cs_kind)
		if self.cs_prize == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.cs_prize))
			for i in xrange(len(self.cs_prize)):
				self.cs_prize[i].write(param1)
		param1.writeUTF(self.cs_coupon_kind)
		param1.writeInt(self.cs_coupon_discount)
		param1.writeInt(self.cs_coupon_max_count)
		if self.cs_date_finish != None:
			param1.writeByte(1)
			param1.writeDouble(self.cs_date_finish)
		else:
			param1.writeByte(0)

	def read(self, param1):
		self.cs_kind = param1.readUTF()
		self.cs_prize = {}
		for i in xrange(param1.readUnsignedShort()):
			self.cs_prize[i] = PWarehouseReserve()
			self.cs_prize[i].read(param1)
		self.cs_coupon_kind = param1.readUTF()
		self.cs_coupon_discount = param1.readInt()
		self.cs_coupon_max_count = param1.readInt()
		if param1.readUnsignedByte() == 1:
			self.cs_date_finish = param1.readDouble()
		else:
			self.cs_date_finish = None


class PRequirements(object):
	CHILD_STAGE = 9
	RAID = 8
	BUILDING_LEVEL = 7
	CLAN_EXP = 6
	COMFORT = 5
	STUDY = 4
	BUILDING = 3
	QUEST = 2
	ACTIVE_QUEST = 1
	LEVEL = 0

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.LEVEL:
			self.value = param1.readUnsignedShort()
		elif self.variance == self.ACTIVE_QUEST:
			self.value = param1.readUTF()
		elif self.variance == self.QUEST:
			self.value = param1.readUTF()
		elif self.variance == self.BUILDING:
			self.value = param1.readUTF()
		elif self.variance == self.STUDY:
			self.value = str_uint()
			self.value.read(param1)
		elif self.variance == self.COMFORT:
			self.value = param1.readUnsignedShort()
		elif self.variance == self.CLAN_EXP:
			self.value = param1.readUnsignedInt()
		elif self.variance == self.BUILDING_LEVEL:
			self.value = str_uint()
			self.value.read(param1)
		elif self.variance == self.RAID:
			self.value = param1.readUTF()
		elif self.variance == self.CHILD_STAGE:
			self.value = param1.readUnsignedInt()
		else:
			raise Exception ("ERROR: incorrect PRequirements data")

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.LEVEL:
			param1.writeShort(self.value)
		elif self.variance == self.ACTIVE_QUEST:
			param1.writeUTF(self.value)
		elif self.variance == self.QUEST:
			param1.writeUTF(self.value)
		elif self.variance == self.BUILDING:
			param1.writeUTF(self.value)
		elif self.variance == self.STUDY:
			self.value.write(param1)
		elif self.variance == self.COMFORT:
			param1.writeShort(self.value)
		elif self.variance == self.CLAN_EXP:
			param1.writeInt(self.value)
		elif self.variance == self.BUILDING_LEVEL:
			self.value.write(param1)
		elif self.variance == self.RAID:
			param1.writeUTF(self.value)
		elif self.variance == self.CHILD_STAGE:
			param1.writeInt(self.value)
		else:
			raise Exception ("ERROR: incorrect PRequirements data")

class PShopUnlock(object):
	__slots__ = ['price', 'discount']
	def __init__(self, param1 = 0, param2 = 0):
		self.price = param1
		self.discount = param2

	def write(self, param1):
		param1.writeInt(self.price)
		param1.writeInt(self.discount)

	def read(self, param1):
		self.price = param1.readUnsignedInt()
		self.discount = param1.readInt()

class PSpecials(object):
	NEW_REQ = 7
	BEST_SELLER = 6
	TOP_BEST_SELLER = 5
	DAILY_DEAL = 4
	OFFER = 3
	SALE = 2
	LIMITED = 1
	NEW = 0

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.NEW:
			pass
		elif self.variance == self.LIMITED:
			self.value = param1.readDouble()
		elif self.variance == self.SALE:
			self.value = PCosts()
			self.value.read(param1)
		elif self.variance == self.OFFER:
			self.value = i_oi()
			self.value.read(param1)
		elif self.variance == self.DAILY_DEAL:
			pass
		elif self.variance == self.TOP_BEST_SELLER:
			pass
		elif self.variance == self.BEST_SELLER:
			pass
		elif self.variance == self.NEW_REQ:
			self.value = PRequirements()
			self.value.read(param1)
		else:
			raise Exception ("ERROR: incorrect PRequirements data")

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.NEW:
			pass
		elif self.variance == self.LIMITED:
			param1.writeDouble(self.value)
		elif self.variance == self.SALE:
			self.value.write(param1)
		elif self.variance == self.OFFER:
			self.value.write(param1)
		elif self.variance == self.DAILY_DEAL:
			pass
		elif self.variance == self.TOP_BEST_SELLER:
			pass
		elif self.variance == self.BEST_SELLER:
			pass
		elif self.variance == self.NEW_REQ:
			self.value.write(param1)
		else:
			raise Exception ("ERROR: incorrect PRequirements data")

class PNewsInfo(object):
	__slots__ = ['news_kind', 'news_prize', 'news_button']
	def __init__(self, param1 = "", param2 = None, param3 = None):
		self.news_kind = param1
		self.news_prize = param2
		self.news_button = param3

	def read(self, param1):
		self.news_kind = param1.readUTF()
		self.news_prize = PQuestPrizes()
		self.news_prize.read(param1)
		self.news_button = PNewsButton()
		self.news_button.read(param1)

	def write(self, param1):
		param1.writeUTF(self.news_kind)
		self.news_prize.write(param1)
		self.news_button.write(param1)

class i_i_PQuestPrizes(object):
	def write(self, param1):
		param1.writeInt(self.field_0)
		param1.writeInt(self.field_1)
		self.field_2.write(param1)

	__slots__ = ['field_0', 'field_1', 'field_2']
	def __init__(self, param1 = 0, param2 = 0, param3 = None):
		self.field_0 = param1
		self.field_1 = param2
		self.field_2 = param3

	def read(self, param1):
		self.field_0 = param1.readInt()
		self.field_1 = param1.readInt()
		self.field_2 = PQuestPrizes()
		self.field_2.read(param1)

class PAction(object):
	AC_FALL_VILLAGE_BOX = 147
	AC_FAIR_INVESTMENT = 146
	AC_TEAM_BUILD_REQUEST = 145
	AC_MOVE_TO_MAP = 144	
	AC_FINISH_UPGRADE_BUILDING = 143
	AC_BUY_UPGRADE_BUILDING = 142
	AC_CHILD_STAGE = 141
	AC_CHILD_SKILL = 140
	AC_CHILD_SEX = 139
	AC_CHILD_NEED = 138
	AC_CHILD_NAME = 137
	AC_CHILD_LEVEL = 136
	AC_MAKE_ANY_OF_HY = 135
	AC_THROW_CUBE = 134
	AC_SELL_LOT = 133
	AC_BUY_LOT = 132
	AC_CREATE_LOT = 131
	AC_OPEN_SUITCASE = 130
	AC_DO_CONTRACT = 129
	AC_STAY_CLAN_MEMBER = 128
	AC_CREATE_CLAN = 127
	AC_DONATE_POINTS = 126
	AC_FINISH_FAIR_TRAINING = 125
	AC_DONATE_CONTEST = 124
	AC_WIN_CONTEST = 123
	AC_START_CONTEST = 122
	AC_FINISH_FAIR_BUILDING_HULL = 121
	AC_FINISH_FAIR_BUILDING = 120
	AC_START_FAIR_BUILDING_HULL = 119
	AC_START_FAIR_BUILDING = 118
	AC_BUY_CAT_DECOR = 117
	AC_USE_FERTILIZER_CYCLE = 116
	AC_FINISH_RAID_AS_MEMBER = 115
	AC_FINISH_RAID_TRAINING = 114
	AC_FINISH_RAID = 113
	AC_DROP_SKULL_TREE = 112
	AC_USE_ANTI_WITHERED = 111
	AC_HARVEST_SEEDBED_EVENT = 110
	AC_PR_HARVEST_EVENT = 109
	AC_BUY_TILE_EVENT = 108
	AC_FR_HARVEST_EVENT = 107
	AC_ACCEPT_GIFTS_EVENT = 106
	AC_ACCEPT_GIFTS = 105
	AC_SK_HELP_END = 104
	AC_SK_FEED_ANIMAL = 103
	AC_SK_HARVEST = 102
	AC_ADD_NEIGHBOUR = 101
	AC_GAME_BOOKMARK = 100
	AC_GAME_ENTER_GROUP = 99
	AC_GAME_SHARE = 98
	AC_GAME_LIKE = 97
	AC_EXPERIMENT_COMPLETE = 96
	AC_GET_FOOD_REMOVE = 95
	AC_GET_FOOD = 94
	AC_GO_TO_TERRITORY = 93
	AC_PROCESS_DECOR_PUZZLE = 92
	AC_FINISH_DECOR_PUZZLE = 91
	AC_HAVE_ON_BOARD_EVENT = 90
	AC_GET_RARES_REMOVE = 89
	AC_HAVE_WEAPON_REMOVE = 88
	AC_PR_HELP_END = 87
	AC_HAVE_RARES_REMOVE = 86
	AC_ADD_TIMBER = 85
	AC_GET_WEAPON = 84
	AC_MAKE_HY_REMOVE = 83
	AC_PR_KICK_STONE = 82
	AC_PR_KICK_WOOD = 81
	AC_PR_FEED_ANIMAL = 80
	AC_PR_HARVEST = 79
	AC_PR_DROP_STONE = 78
	AC_PR_DROP_WOOD = 77
	AC_HAVE_FOOD_REMOVE = 76
	AC_HAVE_TIMBER_REMOVE = 75
	AC_HAVE_BAKS_REMOVE = 74
	AC_HAVE_ANIMAL_REMOVE = 73
	AC_HAVE_SEEDS_REMOVE = 72
	AC_HAVE_FRUITS_REMOVE = 71
	AC_MAKE_TILE = 70
	AC_USE_SEGWAY = 69
	AC_USE_EN_ELIXIR = 68
	AC_USE_EN = 67
	AC_ASK_RARE_FRIEND = 66
	AC_WAREHOUSE2BOARD = 65
	AC_STASH = 64
	AC_ADD_BAKS = 63
	AC_GROW_STONE = 62
	AC_HAVE_SEEDS = 61
	AC_APPLY_HELP_FRIEND = 60
	AC_COMBO = 59
	AC_BUY_EN = 58
	AC_BUY_EXPANSIONS = 57
	AC_HAVE_ON_BOARD = 56
	AC_HAVE_FOOD = 55
	AC_HAVE_TIMBER = 54
	AC_GROW_WOOD = 53
	AC_FR_HELP_END = 52
	AC_MOVE_ANIMAL = 51
	AC_HAVE_BAKS = 50
	AC_BUY_SEGWAY = 49
	AC_FEED_PET = 48
	AC_HAVE_RARES = 47
	AC_GET_RARES = 46
	AC_RECIEVE_COLLECTION_REWARD = 45
	AC_GET_COLLECTION_ELEMENTS = 44
	AC_SEND_EL_CL_EVENT = 43
	AC_SEND_GIFTS_EVENT = 42
	AC_SEND_EL_CL = 41
	AC_SEND_GIFTS = 40
	AC_FR_KICK_STONE = 39
	AC_FR_KICK_WOOD = 38
	AC_FR_FEED_ANIMAL = 37
	AC_FR_SEEDBED_REPAIR = 36
	AC_FR_HARVEST = 35
	AC_FR_DROP_GARBAGE = 34
	AC_FR_DROP_STONE = 33
	AC_FR_DROP_WOOD = 32
	AC_AVAILABLE_ANIMAL = 31
	AC_SELL_ANIMAL = 30
	AC_HAVE_ANIMAL = 29
	AC_GROW_ANIMAL = 28
	AC_FEED_ANIMAL = 27
	AC_BUY_ANIMAL = 26
	AC_HAVE_BUILDING = 25
	AC_FINISH_BUILDING = 24
	AC_PROCESS_BUILDING = 23
	AC_GET_DECOR_POINTS = 22
	AC_HAVE_DECOR_LEVEL = 21
	AC_BUY_DECOR = 20
	AC_BUY_TILE = 19
	AC_BUY_BUILDING = 18
	AC_ATTACK_MONSTER_NANOEMITTER = 17
	AC_ATTACK_MONSTER_NEBULIZERGAS = 16
	AC_ATTACK_MONSTER_BEEGUN = 15
	AC_ATTACK_MONSTER = 14
	AC_USE_FERTILIZER = 13
	AC_MAKE_HY = 12
	AC_RESEARCH = 11
	AC_SELL_FRUITS = 10
	AC_HAVE_FRUITS = 9
	AC_HARVEST_SEEDBED_W = 8
	AC_HARVEST_SEEDBED = 7
	AC_PLANT_SEEDBED = 6
	AC_DROP_GARBAGE = 5
	AC_KICK_STONE = 4
	AC_DROP_STONE = 3
	AC_KICK_WOOD = 2
	AC_DROP_WOOD = 1
	AC_ASK_RARE_ITEM = 0

	def write(self, param1):
		param1.writeByte(self.variance)

	__slots__ = ['variance']
	def __init__(self, param1 = 0):
		self.variance = param1

	def read(self, param1):
		self.variance = param1.readUnsignedByte()

class PCell(object):
	def write(self, param1):
		param1.writeByte(self.cell_num)
		param1.writeBoolean(self.cell_opened_today)
		self.cell_goody.write(param1)

	__slots__ = ['cell_num', 'cell_opened_today', 'cell_goody']
	def __init__(self, param1 = 0, param2 = False, param3 = None):
		self.cell_num = param1
		self.cell_opened_today = param2
		self.cell_goody = param3

	def read(self, param1):
		self.cell_num = param1.readUnsignedByte()
		self.cell_opened_today = param1.readBoolean()
		self.cell_goody = PCellGoody()
		self.cell_goody.read(param1)

class PCellGoody(object):
	COST = 1
	WAREHOUSE_RESERVE = 0

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.WAREHOUSE_RESERVE:
			self.value.write(param1)
		elif self.variance == self.COST:
			self.value.write(param1)
		else:
			raise Exception ("ERROR: incorrect PCellGoody data")

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.WAREHOUSE_RESERVE:
			self.value = PWarehouseReserve()
			self.value.read(param1)
		elif self.variance == self.COST:
			self.value = PCost()
			self.value.read(param1)
		else:
			raise Exception ("ERROR: incorrect PCellGoody data")

class PCollection(object):
	def write(self, param1):
		param1.writeUTF(self.cl_kind)
		if self.cl_elements == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.cl_elements))
			for i in xrange(len(self.cl_elements)):
				param1.writeUTF(self.cl_elements[i])
		self.cl_type.write(param1)
		self.cl_bonus.write(param1)

	__slots__ = ['cl_kind', 'cl_elements', 'cl_type', 'cl_bonus']
	def __init__(self, param1 = "", param2 = None, param3 = None, param4 = None):
		self.cl_kind = param1
		self.cl_elements = param2
		self.cl_type = param3
		self.cl_bonus = param4

	def read(self, param1):
		self.cl_kind = param1.readUTF()
		self.cl_elements = {}
		for i in xrange(param1.readUnsignedShort()):
			self.cl_elements[i] = param1.readUTF()
		self.cl_type = PTCollection()
		self.cl_type.read(param1)
		self.cl_bonus = PQuestPrizes()
		self.cl_bonus.read(param1)

class PCollectionUser(object):
	__slots__ = ['cu_elements', 'cu_count_collect']
	def __init__(self, param1 = None, param2 = 0):
		self.cu_elements = param1
		self.cu_count_collect = param2

	def read(self, param1):
		self.cu_elements = {}
		i = param1.readUnsignedShort()
		while i > 0:
			parUTF = param1.readUTF()
			self.cu_elements[parUTF] = param1.readInt()
			i = i - 1
		self.cu_count_collect = param1.readInt()

	def write(self, param1):
		#pylint: disable=unused-argument
		raise Exception("ERROR: can\'t write cu_elements, assoc writing not implemented. FIXME PLEASE!!!")

#str_PCosts
class str_PCosts(object):
	def write(self, param1):
		param1.writeUTF(self.field_0)
		self.field_1.write(param1)

	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = "", param2 = None):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = param1.readUTF()
		self.field_1 = PCosts()
		self.field_1.read(param1)

class PStoryLineInfo(object):
	def write(self, param1):
		param1.writeUTF(self.sl_kind)
		param1.writeByte(self.sl_order)

	__slots__ = ['sl_kind', 'sl_order']
	def __init__(self, param1 = "", param2 = 0):
		self.sl_kind = param1
		self.sl_order = param2

	def read(self, param1):
		self.sl_kind = param1.readUTF()
		self.sl_order = param1.readUnsignedByte()

class PState(object):
	__slots__ = ['state_kind', 'state_time_action', 'state_energy_kind']
	def __init__(self, param1 = None, param2 = 0, param3 = ""):
		self.state_kind = param1
		self.state_time_action = param2
		self.state_energy_kind = param3

	def read(self, param1):
		self.state_kind = PStateKind()
		self.state_kind.read(param1)
		self.state_time_action = param1.readDouble()
		self.state_energy_kind = param1.readUTF()

	def write(self, param1):
		self.state_kind.write(param1)
		param1.writeDouble(self.state_time_action)
		param1.writeUTF(self.state_energy_kind)

class PSpecialOffer(object):
	def write(self, param1):
		param1.writeByte(self.so_id)
		param1.writeDouble(self.so_date_start)
		param1.writeDouble(self.so_date_finish)
		param1.writeUTF(self.so_main_img)
		param1.writeUTF(self.so_icon)

	__slots__ = ['so_id', 'so_date_start', 'so_date_finish', 'so_main_img', 'so_icon']
	def __init__(self, param1 = 0, param2 = 0, param3 = 0, param4 = "", param5 = ""):
		self.so_id = param1
		self.so_date_start = param2
		self.so_date_finish = param3
		self.so_main_img = param4
		self.so_icon = param5

	def read(self, param1):
		self.so_id = param1.readUnsignedByte()
		self.so_date_start = param1.readDouble()
		self.so_date_finish = param1.readDouble()
		self.so_main_img = param1.readUTF()
		self.so_icon = param1.readUTF()

class PCraft(object):
	__slots__ = ['c_base', 'c_chance', 'c_exec_time', 'c_finish_cost', 'c_tabs']
	def __init__(self, param1 = None, param2 = None, param3 = 0, param4 = None, param5 = None):
		self.c_base = param1
		self.c_chance = param2
		self.c_exec_time = param3
		self.c_finish_cost = param4
		self.c_tabs = param5

	def __str__(self):
		return	'{c_kind:%s,\r\n c_chance:%s,\r\n c_exec_time:%s,\r\n c_finish_cost:%s,\r\n c_tabs:%s}' % \
				(self.c_base, self.c_chance, self.c_exec_time, self.c_finish_cost, self.c_tabs)

	def write(self, param1):
		param1.c_base.write(param1)
		if self.c_chance != {}:
			param1.writeByte(1)
			if self.c_chance == {}:
				param1.writeShort(0)
			else:
				param1.writeShort(len(self.c_chance))
				for i in xrange(len(self.c_chance)):
					param1.writeInt(self.c_chance[i])
		else:
			param1.writeByte(0)
		param1.writeDouble(self.c_exec_time)
		self.c_finish_cost.write(param1)

		if self.c_tabs == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.c_tabs))
			for i in xrange(len(self.c_tabs)):
				param1.writeUTF(self.c_tabs[i])

	def read(self, param1):
		self.c_base = PCraftRecipeBase()
		self.c_base.read(param1)
		if param1.readUnsignedByte() == 1:
			self.c_chance = {}
			for i in xrange(param1.readUnsignedShort()):
				self.c_chance[i] = param1.readInt()
		else:
			self.c_chance = {}
		self.c_exec_time = param1.readDouble()
		self.c_finish_cost = PCosts()
		self.c_finish_cost.read(param1)
		self.c_tabs = {}
		for i in xrange(param1.readUnsignedShort()):
			self.c_tabs[i] = param1.readUTF()

class PCraftRecipeBase(object):
	__slots__ = ['cr_kind', 'cr_ingrs', 'cr_result', 'cr_game_event_id', 'cr_requirements']
	def __init__(self, param1 = "", param2 = None, param3 = None, param4 = 0, param5 = None):
		self.cr_kind = param1
		self.cr_ingrs = param2
		self.cr_result = param3
		self.cr_game_event_id = param4
		self.cr_requirements = param5

	def read(self, param1):
		self.cr_kind = param1.readUTF()
		self.cr_ingrs = {}
		for i in xrange(param1.readUnsignedByte()):
			self.cr_ingrs[i] = PFormulaIngr()
			self.cr_ingrs[i].read(param1)
		self.cr_result = {}
		for i in xrange(param1.readUnsignedByte()):
			self.cr_result[i] = PQuestPrize()
			self.cr_result[i].read(param1)
		if param1.readUnsignedByte() == 1:
			self.cr_game_event_id = param1.readInt()
		else:
			self.cr_game_event_id = None
		self.cr_requirements = {}
		for i in xrange(param1.readUnsignedShort()):
			self.cr_requirements[i] = PRequirements()
			self.cr_requirements[i].read(param1)

	def write(self, param1):
		param1.writeUTF(self.cr_kind)
		if self.cr_ingrs == {}:
			param1.writeByte(0)
		else:
			param1.writeByte(len(self.cr_ingrs))
			for i in xrange(len(self.cr_ingrs)):
				self.cr_ingrs[i].write(param1)
		if self.cr_result == {}:
			param1.writeByte(0)
		else:
			param1.writeByte(len(self.cr_result))
			for i in xrange(len(self.cr_result)):
				self.cr_result[i].write(param1)
		if self.cr_game_event_id != None:
			param1.writeByte(1)
			param1.writeInt(self.cr_game_event_id)
		else:
			param1.writeByte(0)
		if self.cr_requirements == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.cr_requirements))
			for i in xrange(len(self.cr_requirements)):
				self.cr_requirements[i].write(param1)

class PDailyBonusInfo(object):
	def write(self, param1):
		param1.writeByte(self.day_num)
		param1.writeByte(self.paid_tries)
		param1.writeByte(self.available_tries)
		if self.cells == {}:
			param1.writeByte(0)
		else:
			param1.writeByte(len(self.cells))
			for i in xrange(len(self.cells)):
				self.cells[i].write(param1)
		param1.writeBoolean(self.gold_taken)

	__slots__ = ['day_num', 'paid_tries', 'available_tries', 'cells', 'gold_taken']
	def __init__(self, param1 = 0, param2 = 0, param3 = 0, param4 = None, param5 = False):
		self.day_num = param1
		self.paid_tries = param2
		self.available_tries = param3
		self.cells = param4
		self.gold_taken = param5

	def read(self, param1):
		self.day_num = param1.readUnsignedByte()
		self.paid_tries = param1.readUnsignedByte()
		self.available_tries = param1.readUnsignedByte()
		self.cells = {}
		for i in xrange(param1.readUnsignedByte()):
			self.cells[i] = PCell()
			self.cells[i].read(param1)
		self.gold_taken = param1.readBoolean()

class PGameEvent(object):
	__slots__ = ['game_event_id', 'game_event_date_start', 'game_event_date_finish', 'game_event_main_img', 'game_event_icon', 'game_event_level', 'game_event_type', 'game_event_prizes']
	def __init__(self, param1 = 0, param2 = 0, param3 = 0, param4 = "", param5 = "", param6 = 0, param7 = None, param8 = None):
		self.game_event_id = param1
		self.game_event_date_start = param2
		self.game_event_date_finish = param3
		self.game_event_main_img = param4
		self.game_event_icon = param5
		self.game_event_level = param6
		self.game_event_type = param7
		self.game_event_prizes = param8

	def __repr__(self):
		return "{'game_event_id':%s,'game_event_date_start':%s,'game_event_date_finish':%s,'game_event_level':%s}" % (self.game_event_id, self.game_event_date_start, self.game_event_date_finish, self.game_event_level)

	def read(self, param1):
		self.game_event_id = param1.readUnsignedByte()
		self.game_event_date_start = param1.readDouble()
		self.game_event_date_finish = param1.readDouble()
		self.game_event_main_img = param1.readUTF()
		self.game_event_icon = param1.readUTF()
		self.game_event_level = param1.readInt()
		self.game_event_type = PGeType()
		self.game_event_type.read(param1)
		self.game_event_prizes = PQuestPrizes()
		self.game_event_prizes.read(param1)

	def write(self, param1):
		param1.writeByte(self.game_event_id)
		param1.writeDouble(self.game_event_date_start)
		param1.writeDouble(self.game_event_date_finish)
		param1.writeUTF(self.game_event_main_img)
		param1.writeUTF(self.game_event_icon)
		param1.writeInt(self.game_event_level)
		self.game_event_type.write(param1)
		self.game_event_prizes.write(param1)


class PGeType(object):
	VILLAGE = 7
	TRADE = 6
	CLAN_RAID = 5
	CONTRACT = 4
	CLAN_FAIR = 3
	ORDINARY = 2
	FAIR = 1
	RAID = 0

	def write(self, param1):
		param1.writeByte(self.variance)

	__slots__ = ['variance']
	def __init__(self, param1 = 0):
		self.variance = param1

	def read(self, param1):
		self.variance = param1.readUnsignedByte()

class PMoney(object):
	CLAN_GOLD = 2
	GOLD = 1
	BAKS = 0

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.BAKS:
			self.value = param1.readInt()
		elif self.variance == self.GOLD:
			self.value = param1.readInt()
		elif self.variance == self.CLAN_GOLD:
			self.value = param1.readInt()
		else:
			raise Exception ("ERROR: incorrect PMoney data")

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.BAKS:
			param1.writeInt(self.value)
		elif self.variance == self.GOLD:
			param1.writeInt(self.value)
		elif self.variance == self.CLAN_GOLD:
			param1.writeInt(self.value)
		else:
			raise Exception ("ERROR: incorrect PMoney data")

class PMoneyPriceInfo(object):
	def write(self, param1):
		param1.writeDouble(self.votes)
		param1.writeInt(self.money)
		param1.writeUTF(self.caption)
		param1.writeUTF(self.icon)
		if self.spec_votes != None:
			param1.writeByte(1)
			param1.writeDouble(self.spec_votes)
		else:
			param1.writeByte(0)
		param1.writeUTF(self.spec_icon)
		param1.writeByte(self.num)
		param1.writeInt(self.bonus)

	__slots__ = ['votes', 'money', 'caption', 'icon', 'spec_votes', 'spec_icon', 'num', 'bonus']
	def __init__(self, param1 = 0, param2 = 0, param3 = "", param4 = "", param5 = 0, param6 = "", param7 = 0, param8 = 0):
		self.votes = param1
		self.money = param2
		self.caption = param3
		self.icon = param4
		self.spec_votes = param5
		self.spec_icon = param6
		self.num = param7
		self.bonus = param8

	def read(self, param1):
		self.votes = param1.readDouble()
		self.money = param1.readInt()
		self.caption = param1.readUTF()
		self.icon = param1.readUTF()
		if param1.readUnsignedByte() == 1:
			self.spec_votes = param1.readDouble()
		else:
			self.spec_votes = None
		self.spec_icon = param1.readUTF()
		self.num = param1.readUnsignedByte()
		self.bonus = param1.readInt()

class PPayHist(object):
	def write(self, param1):
		param1.writeDouble(self.ph_time)
		param1.writeUTF(self.ph_action)
		param1.writeUTF(self.ph_kind)
		self.ph_cost.write(param1)
		param1.writeInt(self.ph_gold)
		if self.ph_details == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.ph_details))
			for i in xrange(len(self.ph_details)):
				self.ph_details[i].write(param1)

	__slots__ = ['ph_time', 'ph_action', 'ph_kind', 'ph_cost', 'ph_gold', 'ph_details']
	def __init__(self, param1 = 0, param2 = "", param3 = "", param4 = None, param5 = 0, param6 = None):
		self.ph_time = param1
		self.ph_action = param2
		self.ph_kind = param3
		self.ph_cost = param4
		self.ph_gold = param5
		self.ph_details = param6

	def read(self, param1):
		self.ph_time = param1.readDouble()
		self.ph_action = param1.readUTF()
		self.ph_kind = param1.readUTF()
		self.ph_cost = PCost()
		self.ph_cost.read(param1)
		self.ph_gold = param1.readInt()
		self.ph_details = {}
		for i in xrange(param1.readUnsignedShort()):
			self.ph_details[i] = PPayHistDetail()
			self.ph_details[i].read(param1)

class PPetInfo(object):
	def write(self, param1):
		param1.writeUTF(self.pt_kind)
		self.pt_food.write(param1)
		param1.writeShort(self.pt_gprice)
		param1.writeDouble(self.pt_feeds_period)
		param1.writeByte(self.pt_max_energy_bonus)

	__slots__ = ['pt_kind', 'pt_food', 'pt_gprice', 'pt_feeds_period', 'pt_max_energy_bonus']
	def __init__(self, param1 = "", param2 = None, param3 = 0, param4 = 0, param5 = 0):
		self.pt_kind = param1
		self.pt_food = param2
		self.pt_gprice = param3
		self.pt_feeds_period = param4
		self.pt_max_energy_bonus = param5

	def read(self, param1):
		self.pt_kind = param1.readUTF()
		self.pt_food = PCosts()
		self.pt_food.read(param1)
		self.pt_gprice = param1.readUnsignedShort()
		self.pt_feeds_period = param1.readDouble()
		self.pt_max_energy_bonus = param1.readUnsignedByte()

class PFormulaIngr(object):
	INGR_SPEC_CLAN_GOLD = 10
	INGR_SPEC_CLAN_ENERGY = 9
	INGR_SPEC_RAID_RARES = 8
	INGR_SPEC_ENERGIES = 7
	INGR_SPEC_TIMBER = 6
	INGR_SPEC_RARE_ITEMS = 5
	INGR_SPEC_Y_ANIMALS = 4
	INGR_SPEC_ANIMALS = 3
	INGR_SPEC_FERTILIZERS = 2
	INGR_SPEC_FRUITS = 1
	INGR_SPEC_SEEDS = 0

	def write(self, param1):
		param1.writeUTF(self.ingr_kind)
		param1.writeShort(self.ingr_cnt)
		param1.writeByte(self.ingr_spec_variance)

	__slots__ = ['ingr_kind', 'ingr_cnt', 'ingr_spec_variance']
	def __init__(self, param1 = "", param2 = 0, param3 = 0):
		self.ingr_kind = param1
		self.ingr_cnt = param2
		self.ingr_spec_variance = param3

	def read(self, param1):
		self.ingr_kind = param1.readUTF()
		self.ingr_cnt = param1.readUnsignedShort()
		self.ingr_spec_variance = param1.readUnsignedByte()

class PDecorPuzzleInfo(object):
	def write(self, param1):
		param1.writeUTF(self.dp_kind)
		param1.writeByte(self.dp_steps_cnt)
		self.dp_step_cost.write(param1)
		if self.dp_comfort == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.dp_comfort))
			for i in xrange(len(self.dp_comfort)):
				param1.writeInt(self.dp_comfort[i])
		param1.writeUTF(self.dp_quest)

	__slots__ = ['dp_kind', 'dp_steps_cnt', 'dp_step_cost', 'dp_comfort', 'dp_quest']
	def __init__(self, param1 = "", param2 = 0, param3 = None, param4 = None, param5 = ""):
		self.dp_kind = param1
		self.dp_steps_cnt = param2
		self.dp_step_cost = param3
		self.dp_comfort = param4
		self.dp_quest = param5

	def read(self, param1):
		self.dp_kind = param1.readUTF()
		self.dp_steps_cnt = param1.readUnsignedByte()
		self.dp_step_cost = PCosts()
		self.dp_step_cost.read(param1)
		self.dp_comfort = {}
		for i in xrange(param1.readUnsignedShort()):
			self.dp_comfort[i] = param1.readUnsignedInt()
		self.dp_quest = param1.readUTF()

class PDoubleGoldEvent(object):
	def write(self, param1):
		param1.writeInt(self.dge_id)
		param1.writeDouble(self.dge_period)
		param1.writeDouble(self.dge_date_start)
		param1.writeShort(self.dge_min_level)
		param1.writeShort(self.dge_max_level)
		if self.dge_gold_nums == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.dge_gold_nums))
			for i in xrange(len(self.dge_gold_nums)):
				param1.writeByte(self.dge_gold_nums[i])

	__slots__ = ['dge_id', 'dge_period', 'dge_date_start', 'dge_min_level', 'dge_max_level', 'dge_gold_nums']
	def __init__(self, param1 = 0, param2 = 0, param3 = 0, param4 = 0, param5 = 0, param6 = None):
		self.dge_id = param1
		self.dge_period = param2
		self.dge_date_start = param3
		self.dge_min_level = param4
		self.dge_max_level = param5
		self.dge_gold_nums = param6

	def read(self, param1):
		self.dge_id = param1.readInt()
		self.dge_period = param1.readDouble()
		self.dge_date_start = param1.readDouble()
		self.dge_min_level = param1.readUnsignedShort()
		self.dge_max_level = param1.readUnsignedShort()
		self.dge_gold_nums = {}
		for i in xrange(param1.readUnsignedShort()):
			self.dge_gold_nums[i] = param1.readUnsignedByte()

class PResNewAnimal(object):
	def write(self, param1):
		param1.writeInt(self.na_id)
		self.na_state.write(param1)
		self.na_faction.write(param1)
		param1.writeBoolean(self.na_boost_growth)

	__slots__ = ['na_id', 'na_state', 'na_faction', 'na_boost_growth']
	def __init__(self, param1 = 0, param2 = None, param3 = None, param4 = False):
		self.na_id = param1
		self.na_state = param2
		self.na_faction = param3
		self.na_boost_growth = param4

	def read(self, param1):
		self.na_id = param1.readUnsignedInt()
		self.na_state = PAnimalState()
		self.na_state.read(param1)
		self.na_faction = PFarmAction()
		self.na_faction.read(param1)
		self.na_boost_growth = param1.readBoolean()

class PResNewBuilding(object):
	def write(self, param1):
		param1.writeInt(self.nb_id)
		self.nb_state.write(param1)

	__slots__ = ['nb_id', 'nb_state']
	def __init__(self, param1 = 0, param2 = None):
		self.nb_id = param1
		self.nb_state = param2

	def read(self, param1):
		self.nb_id = param1.readUnsignedInt()
		self.nb_state = PBuildingState()
		self.nb_state.read(param1)

class PResNewSeedbed(object):
	def write(self, param1):
		param1.writeInt(self.rns_id)
		self.rns_seedbed.write(param1)
		if self.rns_actions == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.rns_actions))
			for i in xrange(len(self.rns_actions)):
				self.rns_actions[i].write(param1)

	__slots__ = ['rns_id', 'rns_seedbed', 'rns_actions']
	def __init__(self, param1 = 0, param2 = None, param3 = None):
		self.rns_id = param1
		self.rns_seedbed = param2
		self.rns_actions = param3

	def read(self, param1):
		self.rns_id = param1.readUnsignedInt()
		self.rns_seedbed = PSeedbed()
		self.rns_seedbed.read(param1)
		self.rns_actions = {}
		for i in xrange(param1.readUnsignedShort()):
			self.rns_actions[i] = PFarmAction()
			self.rns_actions[i].read(param1)

class PRrUseCase(object):
	def write(self, param1):
		param1.writeUTF(self.raid_kind)
		param1.writeUTF(self.kind)
		param1.writeBoolean(self.from_shop)

	__slots__ = ['raid_kind', 'kind', 'from_shop']
	def __init__(self, param1 = "", param2 = "", param3 = False):
		self.raid_kind = param1
		self.kind = param2
		self.from_shop = param3

	def read(self, param1):
		self.raid_kind = param1.readUTF()
		self.kind = param1.readUTF()
		self.from_shop = param1.readBoolean()

class PSaleEvent(object):
	def write(self, param1):
		param1.writeInt(self.se_id)
		param1.writeDouble(self.se_date_start)
		param1.writeDouble(self.se_date_finish)
		param1.writeByte(self.se_price)
		param1.writeByte(self.se_unlock)
		param1.writeByte(self.se_study)

	__slots__ = ['se_id', 'se_date_start', 'se_date_finish', 'se_price', 'se_unlock', 'se_study']
	def __init__(self, param1 = 0, param2 = 0, param3 = 0, param4 = 0, param5 = 0, param6 = 0):
		self.se_id = param1
		self.se_date_start = param2
		self.se_date_finish = param3
		self.se_price = param4
		self.se_unlock = param5
		self.se_study = param6

	def read(self, param1):
		self.se_id = param1.readUnsignedInt()
		self.se_date_start = param1.readDouble()
		self.se_date_finish = param1.readDouble()
		self.se_price = param1.readUnsignedByte()
		self.se_unlock = param1.readUnsignedByte()
		self.se_study = param1.readUnsignedByte()

class PTCollection(object):
	INACTIVE = 2
	EVENT = 1
	CONST = 0

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.CONST:
			pass
		elif self.variance == self.EVENT:
			param1.writeByte(self.value)
		elif self.variance == self.INACTIVE:
			param1.writeDouble(self.value)
		else:
			raise Exception ("ERROR: incorrect PTCollection data")

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.CONST:
			pass
		elif self.variance == self.EVENT:
			self.value = param1.readByte()
		elif self.variance == self.INACTIVE:
			self.value = param1.readDouble()
		else:
			raise Exception ("ERROR: incorrect PTCollection data")

class PStudy(object):
	def write(self, param1):
		param1.writeUTF(self.st_kind)
		if self.st_requirements == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.st_requirements))
			for i in xrange(len(self.st_requirements)):
				self.st_requirements[i].write(param1)
		param1.writeByte(self.st_degree)
		self.st_cost.write(param1)
		param1.writeDouble(self.st_time)
		self.st_cost_complete.write(param1)
		param1.writeUTF(self.st_formula)
		param1.writeBoolean(self.st_usable)
		self.st_cost_instantly.write(param1)
		self.st_old_cost_instantly.write(param1)
		param1.writeInt(self.st_map_id)
		if self.st_sale_id == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.st_sale_id))
			for i in xrange(len(self.st_sale_id)):
				param1.writeInt(self.st_sale_id[i])
		self.st_tickets_cost.write(param1)

	__slots__ = ['st_kind', 'st_requirements', 'st_degree', 'st_cost', 'st_time', 'st_cost_complete', 'st_formula', 'st_usable', 'st_cost_instantly', 'st_old_cost_instantly', 'st_map_id', 'st_sale_id', 'st_tickets_cost']
	def __init__(self, param1 = "", param2 = None, param3 = 0, param4 = None, param5 = 0, param6 = None, param7 = "", param8 = False, param9 = None, param10 = None, param11 = 0, param12 = None, param13 = None):
		self.st_kind = param1
		self.st_requirements = param2
		self.st_degree = param3
		self.st_cost = param4
		self.st_time = param5
		self.st_cost_complete = param6
		self.st_formula = param7
		self.st_usable = param8
		self.st_cost_instantly = param9
		self.st_old_cost_instantly = param10
		self.st_map_id = param11
		self.st_sale_id = param12
		self.st_tickets_cost = param13

	def read(self, param1):
		self.st_kind = param1.readUTF()
		self.st_requirements = {}
		for i in xrange(param1.readUnsignedShort()):
			self.st_requirements[i] = PRequirements()
			self.st_requirements[i].read(param1)
		self.st_degree = param1.readUnsignedByte()
		self.st_cost = PQuestPrizes()
		self.st_cost.read(param1)
		self.st_time = param1.readDouble()
		self.st_cost_complete = PCosts()
		self.st_cost_complete.read(param1)
		self.st_formula = param1.readUTF()
		self.st_usable = param1.readBoolean()
		self.st_cost_instantly = PCosts()
		self.st_cost_instantly.read(param1)
		self.st_old_cost_instantly = PCosts()
		self.st_old_cost_instantly.read(param1)
		self.st_map_id = param1.readInt()
		self.st_sale_id = {}
		for i in xrange(param1.readUnsignedShort()):
			self.st_sale_id[i] = param1.readUnsignedInt()
		self.st_tickets_cost = PCosts()
		self.st_tickets_cost.read(param1)

class PSegwayBuy(object):
	def write(self, param1):
		param1.writeUTF(self.sw_kind)
		if self.sw_time_lease != None:
			param1.writeByte(1)
			param1.writeDouble(self.sw_time_lease)
		else:
			param1.writeByte(0)

	__slots__ = ['sw_kind', 'sw_time_lease']
	def __init__(self, param1 = "", param2 = 0):
		self.sw_kind = param1
		self.sw_time_lease = param2

	def read(self, param1):
		self.sw_kind = param1.readUTF()
		if param1.readUnsignedByte() == 1:
			self.sw_time_lease = param1.readDouble()
		else:
			self.sw_time_lease = None


class PSaveT(object):
	__slots__ = ['st_length', 'st_user', 'st_farm', 'st_friend_actions', 'st_clan', 'st_trade','st_efarm']
	def __init__(self, param1 = 0, param2 = None, param3 = None, param4 = None, param5 = None, param6 = None, param7 = None):
		self.st_length = param1
		self.st_user = param2
		self.st_farm = param3
		self.st_friend_actions = param4
		self.st_clan = param5
		self.st_trade = param6
		self.st_efarm = param7

	def read(self, param1):
		self.st_length = param1.readInt()
		self.st_user = ByteArray()
		param1.readBytes(self.st_user, 0, param1.readUnsignedInt())
		self.st_farm = ByteArray()
		param1.readBytes(self.st_farm, 0, param1.readUnsignedInt())
		self.st_friend_actions = {}
		for i in xrange(param1.readUnsignedShort()):
			self.st_friend_actions[i] = str_PIbuffer()
			self.st_friend_actions[i].read(param1)
		self.st_clan = {}
		if param1.readUnsignedByte() == 1:
			# FIX THIS - original uses readBytes
			for i in xrange(param1.readUnsignedInt()):
				self.st_clan[i] = param1.readByte()
		self.st_trade = {}
		if param1.readUnsignedByte() == 1:
			for i in xrange(param1.readUnsignedInt()):
				self.st_trade[i] = param1.readByte()
		self.st_efarm = {}
		if param1.readUnsignedByte() == 1:
			# FIX THIS - original uses readBytes
			for i in xrange(param1.readUnsignedInt()):
				self.st_efarm[i] = param1.readByte()

	def write(self, param1):
		param1.writeInt(self.st_length)
		param1.writeUnsignedInt(len(self.st_user))
		param1.writeBytes(self.st_user)
		param1.writeUnsignedInt(len(self.st_farm))
		param1.writeBytes(self.st_farm)
		if self.st_friend_actions == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.st_friend_actions))
			for i in xrange(len(self.st_friend_actions)):
				self.st_friend_actions[i].write(param1)

		if self.st_clan != {}:
			param1.writeByte(1)
			param1.writeUnsignedInt(len(self.st_clan))
			for i in xrange(len(self.st_clan)):
				param1.writeByte(self.st_clan[i])
		else:
			param1.writeByte(0)

		if self.st_trade != {}:
			param1.writeByte(1)
			param1.writeUnsignedInt(len(self.st_trade))
			for i in xrange(len(self.st_trade)):
				param1.writeByte(self.st_trade[i])
		else:
			param1.writeByte(0)

		if self.st_efarm != {}:
			param1.writeByte(1)
			param1.writeUnsignedInt(len(self.st_efarm))
			for i in xrange(len(self.st_efarm)):
				param1.writeByte(self.st_efarm[i])
		else:
			param1.writeByte(0)


class PSaveRaidT(object):
	def write(self, param1):
		param1.writeInt(self.str_length)
		param1.writeUnsignedInt(len(self.str_user))
		param1.writeBytes(self.str_user)
		param1.writeUnsignedInt(len(self.str_farm))
		param1.writeBytes(self.str_farm)
		param1.writeUnsignedInt(len(self.str_raid))
		param1.writeBytes(self.str_raid)
		if self.str_friends == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.str_friends))
			for i in xrange(len(self.str_friends)):
				self.str_friends[i].write(param1)

	__slots__ = ['str_length', 'str_user', 'str_farm', 'str_raid', 'str_friends']
	def __init__(self, param1 = 0, param2 = None, param3 = None, param4 = None, param5 = None):
		self.str_length = param1
		self.str_user = param2
		self.str_farm = param3
		self.str_raid = param4
		self.str_friends = param5

	def read(self, param1):
		self.str_length = param1.readInt()
		self.str_user = ByteArray()
		param1.readBytes(self.str_user, 0, param1.readUnsignedInt())
		self.str_farm = ByteArray()
		param1.readBytes(self.str_farm, 0, param1.readUnsignedInt())
		self.str_raid = ByteArray()
		param1.readBytes(self.str_raid, 0, param1.readUnsignedInt())
		self.str_friends = {}
		for i in xrange(param1.readUnsignedShort()):
			self.str_friends[i] = str_PIbuffer_PIbuffer()
			self.str_friends[i].read(param1)

class PQuestInfo(object):
	__slots__ = ['qi_icon', 'qi_disabled', 'qi_targets', 'qi_nesting_level', 'qi_event', 'qi_line', 'qi_refusal', 'qi_story_line', 'qi_story_line_order', 'qi_prize', 'qi_time_expire', 'qi_achievement']
	def __init__(self, param1 = "", param2 = False, param3 = None, param4 = 0, param5 = 0, param6 = 0, param7 = False, param8 = "", param9 = 0, param10 = None, param11 = None, param12 = None):
		self.qi_icon = param1
		self.qi_disabled = param2
		self.qi_targets = param3
		self.qi_nesting_level = param4
		self.qi_event = param5
		self.qi_line = param6
		self.qi_refusal = param7
		self.qi_story_line = param8
		self.qi_story_line_order = param9
		self.qi_prize = param10
		self.qi_time_expire = param11
		self.qi_achievement = param12

	def read(self, param1):
		self.qi_icon = param1.readUTF()
		self.qi_disabled = param1.readBoolean()
		self.qi_targets = {}
		for i in xrange(param1.readUnsignedShort()):
			self.qi_targets[i] = PQuestTargetInfo()
			self.qi_targets[i].read(param1)

		if param1.readUnsignedByte() == 1:
			self.qi_nesting_level = param1.readUnsignedByte()
		else:
			self.qi_nesting_level = None

		if param1.readUnsignedByte() == 1:
			self.qi_event = param1.readUnsignedByte()
		else:
			self.qi_event = None

		if param1.readUnsignedByte() == 1:
			self.qi_line = param1.readUnsignedByte()
		else:
			self.qi_line = None

		self.qi_refusal = param1.readBoolean()
		if param1.readUnsignedByte() == 1:
			self.qi_story_line = param1.readUTF()
		else:
			self.qi_story_line = {}

		if param1.readUnsignedByte() == 1:
			self.qi_story_line_order = param1.readInt()
		else:
			self.qi_story_line_order = None

		self.qi_prize = PQuestPrizes()
		self.qi_prize.read(param1)
		if param1.readUnsignedByte() == 1:
			self.qi_time_expire = param1.readDouble()
		else:
			self.qi_time_expire = None
		if param1.readUnsignedByte() == 1:
			self.qi_achievement = PAchievement()
			self.qi_achievement.read(param1)
		else:
			self.qi_achievement = None

	def write(self, param1):
		param1.writeUTF(self.qi_icon)
		param1.writeBoolean(self.qi_disabled)
		if self.qi_targets == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.qi_targets))
			for i in xrange(len(self.qi_targets)):
				self.qi_targets[i].write(param1)

		if self.qi_nesting_level != None:
			param1.writeByte(1)
			param1.writeByte(self.qi_nesting_level)
		else:
			param1.writeByte(0)

		if self.qi_event != None:
			param1.writeByte(1)
			param1.writeByte(self.qi_event)
		else:
			param1.writeByte(0)

		if self.qi_line != None:
			param1.writeByte(1)
			param1.writeByte(self.qi_line)
		else:
			param1.writeByte(0)

		param1.writeBoolean(self.qi_refusal)
		if self.qi_story_line != {}:
			param1.writeByte(1)
			param1.writeUTF(self.qi_story_line)
		else:
			param1.writeByte(0)

		if self.qi_story_line_order != None:
			param1.writeByte(1)
			param1.writeInt(self.qi_story_line_order)
		else:
			param1.writeByte(0)

		self.qi_prize.write(param1)
		if self.qi_time_expire != None:
			param1.writeByte(1)
			param1.writeDouble(self.qi_time_expire)
		else:
			param1.writeByte(0)
		if self.qi_achievement != None:
			param1.writeByte(1)
			param1.writeDouble(self.qi_achievement)
		else:
			param1.writeByte(0)

class PQuestTargetInfo(object):
	__slots__ = ['qti_name', 'qti_icon', 'qti_count', 'qti_buy_finish', 'qti_action', 'qti_kind','qti_map_id']
	def __init__(self, param1 = "", param2 = "", param3 = 0, param4 = None, param5 = None, param6 = "", param7 = 0):
		self.qti_name = param1
		self.qti_icon = param2
		self.qti_count = param3
		self.qti_buy_finish = param4
		self.qti_action = param5
		self.qti_kind = param6
		self.qti_map_id = param7

	def read(self, param1):
		self.qti_name = param1.readUTF()
		self.qti_icon = param1.readUTF()
		self.qti_count = param1.readUnsignedInt()
		self.qti_buy_finish = PCosts()
		self.qti_buy_finish.read(param1)
		if param1.readUnsignedByte() == 1:
			self.qti_action = PAction()
			self.qti_action.read(param1)
		else:
			self.qti_action = None
		self.qti_kind = param1.readUTF()
		if param1.readUnsignedByte() == 1:
			self.qti_map_id = param1.readInt()
		else:
			self.qti_map_id = None

	def write(self, param1):
		param1.writeUTF(self.qti_name)
		param1.writeUTF(self.qti_icon)
		param1.writeInt(self.qti_count)
		self.qti_buy_finish.write(param1)
		if self.qti_action != None:
			param1.writeByte(1)
			self.qti_action.write(param1)
		else:
			param1.writeByte(0)
		param1.writeUTF(self.qti_kind)
		if self.qti_action != None:
			param1.writeByte(1)
			param1.writeInt(self.qti_map_id)
		else:
			param1.writeByte(0)

class PEventQuestInfo(object):
	def write(self, param1):
		param1.writeUTF(self.eq_quest_prefix)
		param1.writeByte(self.eq_event_id)
		self.eq_prize.write(param1)
		param1.writeByte(self.eq_num_count)
		param1.writeBoolean(self.eq_available)
		if self.eq_requirements == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.eq_requirements))
			for i in xrange(len(self.eq_requirements)):
				self.eq_requirements[i].write(param1)

	__slots__ = ['eq_quest_prefix', 'eq_event_id', 'eq_prize', 'eq_num_count', 'eq_available', 'eq_requirements']
	def __init__(self, param1 = "", param2 = 0, param3 = None, param4 = 0, param5 = False, param6 = None):
		self.eq_quest_prefix = param1
		self.eq_event_id = param2
		self.eq_prize = param3
		self.eq_num_count = param4
		self.eq_available = param5
		self.eq_requirements = param6

	def read(self, param1):
		self.eq_quest_prefix = param1.readUTF()
		self.eq_event_id = param1.readUnsignedByte()
		self.eq_prize = PQuestPrizes()
		self.eq_prize.read(param1)
		self.eq_num_count = param1.readUnsignedByte()
		self.eq_available = param1.readBoolean()
		self.eq_requirements = {}
		for i in xrange(param1.readUnsignedShort()):
			self.eq_requirements[i] = PRequirements()
			self.eq_requirements[i].read(param1)

class PHarvestSeedbedResult(object):
	def write(self, param1):
		if self.field_0 != None:
			param1.writeByte(1)
			self.field_0.write(param1)
		else:
			param1.writeByte(0)

		if self.field_1 == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.field_1))
			for i in xrange(len(self.field_1)):
				self.field_1[i].write(param1)

	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = None, param2 = None):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		if param1.readUnsignedByte() == 1:
			self.field_0 = PResNewSeedbed()
			self.field_0.read(param1)
		else:
			self.field_0 = None

		self.field_1 = {}
		for i in xrange(param1.readUnsignedShort()):
			self.field_1[i] = PWarehouseReserve()
			self.field_1[i].read(param1)

class PResExpansion(object):
	def write(self, param1):
		param1.writeByte(self.expansion_num)
		if self.new_active == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.new_active))
			for i in xrange(len(self.new_active)):
				self.new_active[i].write(param1)

		if self.actions == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.actions))
			for i in xrange(len(self.actions)):
				self.actions[i].write(param1)

		if self.expenses == {}:
			param1.writeByte(0)
		else:
			param1.writeByte(len(self.expenses))
			for i in xrange(len(self.expenses)):
				self.expenses[i].write(param1)

		if self.closed_quests == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.closed_quests))
			for i in xrange(len(self.closed_quests)):
				param1.writeUTF(self.closed_quests[i])

	__slots__ = ['expansion_num', 'new_active', 'actions', 'expenses', 'closed_quests']
	def __init__(self, param1 = 0, param2 = None, param3 = None, param4 = None, param5 = None):
		self.expansion_num = param1
		self.new_active = param2
		self.actions = param3
		self.expenses = param4
		self.closed_quests = param5

	def read(self, param1):
		self.expansion_num = param1.readUnsignedByte()
		self.new_active = {}
		for i in xrange(param1.readUnsignedShort()):
			self.new_active[i] = PObj()
			self.new_active[i].read(param1)

		self.actions = {}
		for i in xrange(param1.readUnsignedShort()):
			self.actions[i] = PFarmAction()
			self.actions[i].read(param1)

		self.expenses = {}
		for i in xrange(param1.readUnsignedShort()):
			self.expenses[i] = PCost()
			self.expenses[i].read(param1)

		self.closed_quests = {}
		for i in xrange(param1.readUnsignedShort()):
			self.closed_quests[i] = param1.readUTF()

class PRaidResult(object):
	R_FAIL = 1
	R_WIN = 0

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.R_WIN:
			self.value = {}
			for i in xrange(param1.readUnsignedShort()):
				self.value[i] = PRaidMember()
				self.value[i].read(param1)
		elif self.variance == self.R_FAIL:
			self.value = i_i_PQuestPrizes()
			self.value.read(param1)
		else:
			raise Exception ("ERROR: incorrect PRaidResult data")

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.R_WIN:
			if self.value == {}:
				param1.writeShort(0)
			else:
				param1.writeShort(len(self.value))
				for i in xrange(len(self.value)):
					self.value[i].write(param1)
		elif self.variance == self.R_FAIL:
			self.value.write(param1)
		else:
			raise Exception ("ERROR: incorrect PRaidResult data")

class PRaidFriendEvent(object):
	__slots__ = ['rf_friend_id', 'rf_event', 'rf_ts']
	def __init__(self, param1 = "", param2 = None, param3 = 0):
		self.rf_friend_id = param1
		self.rf_event = param2
		self.rf_ts = param3

	def read(self, param1):
		self.rf_friend_id = param1.readUTF()
		self.rf_event = {}
		for i in xrange(param1.readUnsignedShort()):
			self.rf_event[i] = PRaidEvent()
			self.rf_event[i].read(param1)
		self.rf_ts = param1.readDouble()

	def write(self, param1):
		param1.writeUTF(self.rf_friend_id)
		if self.rf_event == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.rf_event))
			for i in xrange(len(self.rf_event)):
				self.rf_event[i].write(param1)
		param1.writeDouble(self.rf_ts)

class PRaidEvent(object):
	TEAM_BUILD_REQUEST = 68
	TEAM_BUILD_CANCEL = 67
	TEAM_BUILD_DECLINE = 66
	TEAM_BUILD_APPROVE = 65
	TEAM_BUILD_INFO = 64	
	CLAN_GET_BOOTH_RECIPE_RESULT = 63
	CLAN_CREATE_BOOTH_RECIPE = 62
	CLAN_USE_ENERGY = 61
	CLAN_RAID_FINISH_ECHO = 60
	CLAN_UNLOCK_RAIDS = 59
	CLAN_RAID_BOSS_HP = 58
	CLAN_RAID_ENERGY_CHANGE = 57
	CLAN_RAID_MESSAGE = 56
	CLAN_RAID_MEMBER_ADDED = 55
	CLAN_RAID_JOIN = 54
	CLONER_PROCESS_FINISH = 53
	CLONER_PROCESS = 52
	CLAN_OPEN_COFFER = 51
	CLAN_COFFER_ENABLE = 50
	CLAN_WAREHOUSE_FAIR_BUILDINGS = 49
	CLAN_CHANGE_TARGETS_GAME_EVENT = 48
	CLAN_FAIR_BUILDING_BONUS = 47
	CLAN_PROCESS_FAIR_BUILDING = 46
	GIFT_SENT = 45
	CLAN_CHAT_MESSAGE = 44
	CLAN_NEW_TILE = 43
	CLAN_WOODS_STONES = 42
	CLAN_CHANGE_MESSAGE = 41
	CLAN_CHANGE_TARGETS_LEVEL = 40
	CLAN_ATTACK_MONSTER = 39
	CLAN_ADD_MONSTERS = 38
	CLAN_PROGRESS_TARGETS = 37
	CLAN_CHANGE_EXP = 36
	CLAN_CHANGE_WAREHOUSE = 35
	CLAN_CHANGE_TARGETS = 34
	CLAN_BUY_EXPANSION = 33
	CLAN_FEED_ANIMAL = 32
	CLAN_UPGRADE_BUILDING = 31
	CLAN_CHANGE_TREASURY = 30
	CLAN_BOARD_OBJ_ACTION = 29
	CLAN_MAKE_RECIPE = 28
	CLAN_USE_FERTILIZER = 27
	CLAN_NEW_DECOR = 26
	CLAN_WAREHOUSE2_BOARD = 25
	CLAN_HARVEST = 24
	CLAN_NEW_SEEDBED = 23
	CLAN_FINISH_BUILDING = 22
	CLAN_PROCESS_BUILDING = 21
	CLAN_NEW_BUILDING = 20
	CLAN_SET_ROLE_MEMBER = 19
	CLAN_CHANGE_INFO = 18
	CLAN_REMOVE_REQUEST = 17
	CLAN_NEW_REQUEST = 16
	CLAN_REMOVE_MEMBER = 15
	CLAN_NEW_MEMBER = 14
	CLAN_DROP_GARBAGE = 13
	CLAN_DROP_STONE = 12
	CLAN_DROP_WOOD = 11
	COMPETITION = 10
	COMP_MEMBER_ADDED = 9
	COMP_POINTS = 8
	KICK_FROM_RAID = 7
	ONLINE_MEMBERS = 6
	MEMBER_ADDED = 5
	RAID_FAIL = 4
	RAID_WIN = 3
	BOSS_HP = 2
	MESSAGE = 1
	RAID = 0

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.RAID:
			self.value = PRaid()
			self.value.read(param1)
		elif self.variance == self.MESSAGE:
			self.value = PRaidMessage()
			self.value.read(param1)
		elif self.variance == self.BOSS_HP:
			self.value = i_i_str()
			self.value.read(param1)
		elif self.variance == self.RAID_WIN:
			pass
		elif self.variance == self.RAID_FAIL:
			pass
		elif self.variance == self.MEMBER_ADDED:
			self.value = PRaidMember()
			self.value.read(param1)
		elif self.variance == self.ONLINE_MEMBERS:
			self.value = {}
			for i in xrange(param1.readUnsignedShort()):
				self.value[i] = param1.readUTF()
		elif self.variance == self.KICK_FROM_RAID:
			self.value = str_str()
			self.value.read(param1)
		elif self.variance == self.COMP_POINTS:
			self.value = param1.readInt()
		elif self.variance == self.COMP_MEMBER_ADDED:
			self.value = PCompetitionUser()
			self.value.read(param1)
		elif self.variance == self.COMPETITION:
			self.value = PCompetition()
			self.value.read(param1)
		elif self.variance == self.CLAN_DROP_WOOD:
			self.value = PResDropWood_PQuestPrizes()
			self.value.read(param1)
		elif self.variance == self.CLAN_DROP_STONE:
			self.value = PResDropStone_PQuestPrizes()
			self.value.read(param1)
		elif self.variance == self.CLAN_DROP_GARBAGE:
			self.value = PResDropGarbage_PQuestPrizes()
			self.value.read(param1)
		elif self.variance == self.CLAN_NEW_MEMBER:
			self.value = PClanMember()
			self.value.read(param1)
		elif self.variance == self.CLAN_REMOVE_MEMBER:
			self.value = param1.readUTF()
		elif self.variance == self.CLAN_NEW_REQUEST:
			self.value = PClanRequest()
			self.value.read(param1)
		elif self.variance == self.CLAN_REMOVE_REQUEST:
			self.value = param1.readUTF()
		elif self.variance == self.CLAN_CHANGE_INFO:
			self.value = PClanChangeInfo()
			self.value.read(param1)
		elif self.variance == self.CLAN_SET_ROLE_MEMBER:
			self.value = str_PClmRole()
			self.value.read(param1)
		elif self.variance == self.CLAN_NEW_BUILDING:
			self.value = PObj_a_PQuestPrize_a()
			self.value.read(param1)
		elif self.variance == self.CLAN_PROCESS_BUILDING:
			self.value = PObj_PQuestPrizes()
			self.value.read(param1)
		elif self.variance == self.CLAN_FINISH_BUILDING:
			self.value = PObj_oPFarmAction_a_PQuestPrize_a()
			self.value.read(param1)
		elif self.variance == self.CLAN_NEW_SEEDBED:
			self.value = PObj_a_PFarmAction_a_PQuestPrizes()
			self.value.read(param1)
		elif self.variance == self.CLAN_HARVEST:
			self.value = uint_oPObj_a_PFarmAction_a_PQuestPrizes()
			self.value.read(param1)
		elif self.variance == self.CLAN_WAREHOUSE2_BOARD:
			self.value = PObj_a_PFarmAction_a_a_PQuestPrize_a()
			self.value.read(param1)
		elif self.variance == self.CLAN_NEW_DECOR:
			self.value = PObj_a_PQuestPrize_a()
			self.value.read(param1)
		elif self.variance == self.CLAN_USE_FERTILIZER:
			self.value = PResUseFertilizer_a_PQuestPrize_a()
			self.value.read(param1)
		elif self.variance == self.CLAN_MAKE_RECIPE:
			self.value = PResMakeRecipe()
			self.value.read(param1)
		elif self.variance == self.CLAN_BOARD_OBJ_ACTION:
			self.value = PClanEventBoardObjAction()
			self.value.read(param1)
		elif self.variance == self.CLAN_CHANGE_TREASURY:
			self.value = PClanTreasury()
			self.value.read(param1)
		elif self.variance == self.CLAN_UPGRADE_BUILDING:
			self.value = PObj_a_PQuestPrize_a()
			self.value.read(param1)
		elif self.variance == self.CLAN_FEED_ANIMAL:
			self.value = PObj_PQuestPrizes()
			self.value.read(param1)
		elif self.variance == self.CLAN_BUY_EXPANSION:
			self.value = PResExpansion()
			self.value.read(param1)
		elif self.variance == self.CLAN_CHANGE_TARGETS:
			self.value = {}
			for i in xrange(param1.readUnsignedShort()):
				self.value[i] = PClanTargetCount()
				self.value[i].read(param1)
		elif self.variance == self.CLAN_CHANGE_WAREHOUSE:
			self.value = PReserves()
			self.value.read(param1)
		elif self.variance == self.CLAN_CHANGE_EXP:
			self.value = param1.readUnsignedInt()
		elif self.variance == self.CLAN_PROGRESS_TARGETS:
			self.value = {}
			for i in xrange(param1.readUnsignedShort()):
				self.value[i] = PClanTargetCount()
				self.value[i].read(param1)
		elif self.variance == self.CLAN_ADD_MONSTERS:
			self.value = {}
			for i in xrange(param1.readUnsignedShort()):
				self.value[i] = PObj()
				self.value[i].read(param1)
		elif self.variance == self.CLAN_ATTACK_MONSTER:
			self.value = PObj_PQuestPrizes()
			self.value.read(param1)
		elif self.variance == self.CLAN_CHANGE_TARGETS_LEVEL:
			self.value = i_a_PClanTargetLevelGe_a()
			self.value.read(param1)
		elif self.variance == self.CLAN_CHANGE_MESSAGE:
			self.value = param1.readUTF()
		elif self.variance == self.CLAN_WOODS_STONES:
			self.value = {}
			for i in xrange(param1.readUnsignedShort()):
				self.value[i] = PObj()
				self.value[i].read(param1)
		elif self.variance == self.CLAN_NEW_TILE:
			self.value = PObj_a_PQuestPrize_a()
			self.value.read(param1)
		elif self.variance == self.CLAN_CHAT_MESSAGE:
			self.value = PChatMessage()
			self.value.read(param1)
		elif self.variance == self.GIFT_SENT:
			self.value = {}
			for i in xrange(param1.readUnsignedShort()):
				self.value[i] = PSendGiftUser()
				self.value[i].read(param1)
		elif self.variance == self.CLAN_PROCESS_FAIR_BUILDING:
			self.value = PObj_oPFarmAction()
			self.value.read(param1)
		elif self.variance == self.CLAN_FAIR_BUILDING_BONUS:
			self.value = PUintOfarmAction()
			self.value.read(param1)
		elif self.variance == self.CLAN_CHANGE_TARGETS_GAME_EVENT:
			self.value = {}
			for i in xrange(param1.readUnsignedShort()):
				self.value[i] = PClanTargetLevelGe()
				self.value[i].read(param1)
		elif self.variance == self.CLAN_WAREHOUSE_FAIR_BUILDINGS:
			self.value = PWarehouseClanFair()
			self.value.read(param1)
		elif self.variance == self.CLAN_COFFER_ENABLE:
			self.value = str_time_a_str_a()
			self.value.read(param1)
		elif self.variance == self.CLAN_OPEN_COFFER:
			self.value = str_PQuestPrizes_a_str_a()
			self.value.read(param1)
		elif self.variance == self.CLONER_PROCESS:
			self.value = {}
			for i in xrange(param1.readUnsignedInt()):
				self.value[i] = PQuestPrize()
				self.value[i].read(param1)
		elif self.variance == self.CLONER_PROCESS_FINISH:
			self.value = PClonerFinish()
			self.value.read(param1)
		elif self.variance == self.CLAN_RAID_JOIN:
			self.value = PRaid()
			self.value.read(param1)
		elif self.variance == self.CLAN_RAID_MEMBER_ADDED:
			self.value = PRaidMember()
			self.value.read(param1)
		elif self.variance == self.CLAN_RAID_MESSAGE:
			self.value = PRaidMessage()
			self.value.read(param1)
		elif self.variance == self.CLAN_RAID_ENERGY_CHANGE:
			self.value = i_otime()
			self.value.read(param1)
		elif self.variance == self.CLAN_RAID_BOSS_HP:
			self.value = PClanRaidBossHp()
			self.value.read(param1)
		elif self.variance == self.CLAN_UNLOCK_RAIDS:
			self.value = {}
			for i in xrange(param1.readUnsignedShort()):
				self.value[i] = param1.readUTF()
		elif self.variance == self.CLAN_RAID_FINISH_ECHO:
			pass
		elif self.variance == self.CLAN_USE_ENERGY:
			self.value = PFarmAction()
			self.value.read(param1)
		elif self.variance == self.CLAN_CREATE_BOOTH_RECIPE:
			self.value = PClanBoothRecipe()
			self.value.read(param1)
		elif self.variance == self.CLAN_GET_BOOTH_RECIPE_RESULT:
			self.value = PResGetBoothRecipe()
			self.value.read(param1)
		elif self.variance == self.TEAM_BUILD_INFO:
			self.value = PStrTeamBuildInfo()
			self.value.read(param1)
		elif self.variance == self.TEAM_BUILD_APPROVE:
			self.value = param1.readUTF()
		elif self.variance == self.TEAM_BUILD_DECLINE:
			self.value = param1.readUTF()
		elif self.variance == self.TEAM_BUILD_CANCEL:
			self.value = param1.readUTF()
		elif self.variance == self.TEAM_BUILD_REQUEST:
			self.value = param1.readUTF()
		else:
			raise Exception ("ERROR: incorrect PRaidEvent data")

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.RAID:
			self.value.write(param1)
		elif self.variance == self.MESSAGE:
			self.value.write(param1)
		elif self.variance == self.BOSS_HP:
			self.value.write(param1)
		elif self.variance == self.RAID_WIN:
			pass
		elif self.variance == self.RAID_FAIL:
			pass
		elif self.variance == self.MEMBER_ADDED:
			self.value.write(param1)
		elif self.variance == self.ONLINE_MEMBERS:
			if self.value == {}:
				param1.writeShort(0)
			else:
				param1.writeShort(len(self.value))
				for i in xrange(len(self.value)):
					param1.writeUTF(self.value[i])
		elif self.variance == self.KICK_FROM_RAID:
			self.value.write(param1)
		elif self.variance == self.COMP_POINTS:
			param1.writeInt(self.value)
		elif self.variance == self.COMP_MEMBER_ADDED:
			self.value.write(param1)
		elif self.variance == self.COMPETITION:
			self.value.write(param1)
		elif self.variance == self.CLAN_DROP_WOOD:
			self.value.write(param1)
		elif self.variance == self.CLAN_DROP_STONE:
			self.value.write(param1)
		elif self.variance == self.CLAN_DROP_GARBAGE:
			self.value.write(param1)
		elif self.variance == self.CLAN_NEW_MEMBER:
			self.value.write(param1)
		elif self.variance == self.CLAN_REMOVE_MEMBER:
			param1.writeUTF(self.value)
		elif self.variance == self.CLAN_NEW_REQUEST:
			self.value.write(param1)
		elif self.variance == self.CLAN_REMOVE_REQUEST:
			param1.writeUTF(self.value)
		elif self.variance == self.CLAN_CHANGE_INFO:
			self.value.write(param1)
		elif self.variance == self.CLAN_SET_ROLE_MEMBER:
			self.value.write(param1)
		elif self.variance == self.CLAN_NEW_BUILDING:
			self.value.write(param1)
		elif self.variance == self.CLAN_PROCESS_BUILDING:
			self.value.write(param1)
		elif self.variance == self.CLAN_FINISH_BUILDING:
			self.value.write(param1)
		elif self.variance == self.CLAN_NEW_SEEDBED:
			self.value.write(param1)
		elif self.variance == self.CLAN_HARVEST:
			self.value.write(param1)
		elif self.variance == self.CLAN_WAREHOUSE2_BOARD:
			self.value.write(param1)
		elif self.variance == self.CLAN_NEW_DECOR:
			self.value.write(param1)
		elif self.variance == self.CLAN_USE_FERTILIZER:
			self.value.write(param1)
		elif self.variance == self.CLAN_MAKE_RECIPE:
			self.value.write(param1)
		elif self.variance == self.CLAN_BOARD_OBJ_ACTION:
			self.value.write(param1)
		elif self.variance == self.CLAN_CHANGE_TREASURY:
			self.value.write(param1)
		elif self.variance == self.CLAN_UPGRADE_BUILDING:
			self.value.write(param1)
		elif self.variance == self.CLAN_FEED_ANIMAL:
			self.value.write(param1)
		elif self.variance == self.CLAN_BUY_EXPANSION:
			self.value.write(param1)
		elif self.variance == self.CLAN_CHANGE_TARGETS:
			if self.value == {}:
				param1.writeShort(0)
			else:
				param1.writeShort(len(self.value))
				for i in xrange(len(self.value)):
					self.value[i].write(param1)
		elif self.variance == self.CLAN_CHANGE_WAREHOUSE:
			self.value.write(param1)
		elif self.variance == self.CLAN_CHANGE_EXP:
			param1.writeInt(self.value)
		elif self.variance == self.CLAN_PROGRESS_TARGETS:
			if self.value == {}:
				param1.writeShort(0)
			else:
				param1.writeShort(len(self.value))
				for i in xrange(len(self.value)):
					self.value[i].write(param1)
		elif self.variance == self.CLAN_ADD_MONSTERS:
			if self.value == {}:
				param1.writeShort(0)
			else:
				param1.writeShort(len(self.value))
				for i in xrange(len(self.value)):
					self.value[i].write(param1)
		elif self.variance == self.CLAN_ATTACK_MONSTER:
			self.value.write(param1)
		elif self.variance == self.CLAN_CHANGE_TARGETS_LEVEL:
			self.value.write(param1)
		elif self.variance == self.CLAN_CHANGE_MESSAGE:
			param1.writeUTF(self.value)
		elif self.variance == self.CLAN_WOODS_STONES:
			if self.value == {}:
				param1.writeShort(0)
			else:
				param1.writeShort(len(self.value))
				for i in xrange(len(self.value)):
					self.value[i].write(param1)
		elif self.variance == self.CLAN_NEW_TILE:
			self.value.write(param1)
		elif self.variance == self.CLAN_CHAT_MESSAGE:
			self.value.write(param1)
		elif self.variance == self.GIFT_SENT:
			if self.value == {}:
				param1.writeShort(0)
			else:
				param1.writeShort(len(self.value))
				for i in xrange(len(self.value)):
					self.value[i].write(param1)
		elif self.variance == self.CLAN_PROCESS_FAIR_BUILDING:
			self.value.write(param1)
		elif self.variance == self.CLAN_FAIR_BUILDING_BONUS:
			self.value.write(param1)
		elif self.variance == self.CLAN_CHANGE_TARGETS_GAME_EVENT:
			if self.value == {}:
				param1.writeShort(0)
			else:
				param1.writeShort(len(self.value))
				for i in xrange(len(self.value)):
					self.value[i].write(param1)
		elif self.variance == self.CLAN_WAREHOUSE_FAIR_BUILDINGS:
			self.value.write(param1)
		elif self.variance == self.CLAN_COFFER_ENABLE:
			self.value.write(param1)
		elif self.variance == self.CLAN_OPEN_COFFER:
			self.value.write(param1)
		elif self.variance == self.CLONER_PROCESS:
			if self.value == {}:
				param1.writeInt(0)
			else:
				param1.writeInt(len(self.value))
				for i in xrange(len(self.value)):
					self.value[i].write(param1)
		elif self.variance == self.CLONER_PROCESS_FINISH:
			self.value.write(param1)
		elif self.variance == self.CLAN_RAID_JOIN:
			self.value.write(param1)
		elif self.variance == self.CLAN_RAID_MEMBER_ADDED:
			self.value.write(param1)
		elif self.variance == self.CLAN_RAID_MESSAGE:
			self.value.write(param1)
		elif self.variance == self.CLAN_RAID_ENERGY_CHANGE:
			self.value.write(param1)
		elif self.variance == self.CLAN_RAID_BOSS_HP:
			self.value.write(param1)
		elif self.variance == self.CLAN_UNLOCK_RAIDS:
			if self.value == {}:
				param1.writeShort(0)
			else:
				param1.writeShort(len(self.value))
				for i in xrange(len(self.value)):
					param1.writeUTF(self.value[i])
		elif self.variance == self.CLAN_RAID_FINISH_ECHO:
			pass
		elif self.variance == self.CLAN_USE_ENERGY:
			self.value.write(param1)
		elif self.variance == self.CLAN_CREATE_BOOTH_RECIPE:
			self.value.write(param1)
		elif self.variance == self.CLAN_GET_BOOTH_RECIPE_RESULT:
			self.value.write(param1)
		elif self.variance == self.TEAM_BUILD_INFO:
			self.value.write(param1)
		elif self.variance == self.TEAM_BUILD_APPROVE:
			param1.writeUTF(self.value)
		elif self.variance == self.TEAM_BUILD_DECLINE:
			param1.writeUTF(self.value)
		elif self.variance == self.TEAM_BUILD_CANCEL:
			param1.writeUTF(self.value)
		elif self.variance == self.TEAM_BUILD_REQUEST:
			param1.writeUTF(self.value)
		else:
			raise Exception ("ERROR: incorrect PRaidEvent data")

class PRaidInfo(object):
	__slots__ = ['rd_kind', 'rd_max_members', 'rd_boss_max_hp', 'rd_event_id', 'rd_period', 'rd_requirements', 'rd_price_unlock', 'rd_price', 'rd_prize_to_leader', 'rd_prize_to_all', 'rd_make_stats', 'rd_is_clan', 'rd_rate_boss_hp']
	def __init__(self, param1 = "", param2 = 0, param3 = 0, param4 = 0, param5 = 0, param6 = None, param7 = None, param8 = None, param9 = None, param10 = None, param11 = False, param12 = False, param13 = 0):
		self.rd_kind = param1
		self.rd_max_members = param2
		self.rd_boss_max_hp = param3
		self.rd_event_id = param4
		self.rd_period = param5
		self.rd_requirements = param6
		self.rd_price_unlock = param7
		self.rd_price = param8
		self.rd_prize_to_leader = param9
		self.rd_prize_to_all = param10
		self.rd_make_stats = param11
		self.rd_is_clan = param12
		self.rd_rate_boss_hp = param13

	def read(self, param1):
		self.rd_kind = param1.readUTF()
		self.rd_max_members = param1.readInt()
		self.rd_boss_max_hp = param1.readInt()
		self.rd_event_id = param1.readInt()
		self.rd_period = param1.readDouble()
		self.rd_requirements = {}
		for i in xrange(param1.readUnsignedShort()):
			self.rd_requirements[i] = PRequirements()
			self.rd_requirements[i].read(param1)
		self.rd_price_unlock = PCosts()
		self.rd_price_unlock.read(param1)
		self.rd_price = PCosts()
		self.rd_price.read(param1)
		self.rd_prize_to_leader = PQuestPrizes()
		self.rd_prize_to_leader.read(param1)
		self.rd_prize_to_all = PQuestPrizes()
		self.rd_prize_to_all.read(param1)
		self.rd_make_stats = param1.readBoolean()
		self.rd_is_clan = param1.readBoolean()
		self.rd_rate_boss_hp = param1.readDouble()

	def write(self, param1):
		param1.writeUTF(self.rd_kind)
		param1.writeInt(self.rd_max_members)
		param1.writeInt(self.rd_boss_max_hp)
		param1.writeInt(self.rd_event_id)
		param1.writeDouble(self.rd_period)
		if self.rd_requirements == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.rd_requirements))
			for i in xrange(len(self.rd_requirements)):
				self.rd_requirements[i].write(param1)
		self.rd_price_unlock.write(param1)
		self.rd_price.write(param1)
		self.rd_prize_to_leader.write(param1)
		self.rd_prize_to_all.write(param1)
		param1.writeBoolean(self.rd_make_stats)
		param1.writeBoolean(self.rd_is_clan)
		param1.writeDouble(self.rd_rate_boss_hp)

class PRaidRareInfo(object):
	__slots__ = ['rr_kind', 'rr_group_cnt', 'rr_price_on_level', 'rr_damage', 'rr_heal', 'rr_level', 'rr_source', 'rr_is_clan']
	def __init__(self, param1 = "", param2 = 0, param3 = None, param4 = 0, param5 = 0, param6 = 0, param7 = None, param8 = False):
		self.rr_kind = param1
		self.rr_group_cnt = param2
		self.rr_price_on_level = param3
		self.rr_damage = param4
		self.rr_heal = param5
		self.rr_level = param6
		self.rr_source = param7
		self.rr_is_clan = param8

	def read(self, param1):
		self.rr_kind = param1.readUTF()
		self.rr_group_cnt = param1.readInt()
		self.rr_price_on_level = {}
		for i in xrange(param1.readUnsignedShort()):
			self.rr_price_on_level[i] = str_PCosts()
			self.rr_price_on_level[i].read(param1)
		if param1.readUnsignedByte() == 1:
			self.rr_damage = param1.readInt()
		else:
			self.rr_damage = None
		if param1.readUnsignedByte() == 1:
			self.rr_heal = param1.readInt()
		else:
			self.rr_heal = None
		self.rr_level = param1.readInt()
		self.rr_source = {}
		for i in xrange(param1.readUnsignedShort()):
			self.rr_source[i] = PRareItemSource()
			self.rr_source[i].read(param1)
		self.rr_is_clan = param1.readBoolean()

	def write(self, param1):
		param1.writeUTF(self.rr_kind)
		param1.writeInt(self.rr_group_cnt)
		if self.rr_price_on_level == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.rr_price_on_level))
			for i in xrange(len(self.rr_price_on_level)):
				self.rr_price_on_level[i].write(param1)
		if self.rr_damage != None:
			param1.writeByte(1)
			param1.writeInt(self.rr_damage)
		else:
			param1.writeByte(0)
		if self.rr_heal != None:
			param1.writeByte(1)
			param1.writeInt(self.rr_heal)
		else:
			param1.writeByte(0)
		param1.writeInt(self.rr_level)
		if self.rr_source == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.rr_source))
			for i in xrange(len(self.rr_source)):
				self.rr_source[i].write(param1)
		param1.writeBoolean(self.rr_is_clan)

class PPlant(object):
	def write(self, param1):
		param1.writeUTF(self.kind)
		param1.writeInt(self.cnt)
		param1.writeBoolean(self.is_from_warehouse)

	__slots__ = ['kind', 'cnt', 'is_from_warehouse']
	def __init__(self, param1 = "", param2 = 0, param3 = False):
		self.kind = param1
		self.cnt = param2
		self.is_from_warehouse = param3

	def read(self, param1):
		self.kind = param1.readUTF()
		self.cnt = param1.readInt()
		self.is_from_warehouse = param1.readBoolean()

class PSettingsGame(object):
	__slots__ = ['s_animation', 's_sound', 's_music', 's_friend_actions_enabled', 's_voice', 's_quality', 's_radio', 's_is_music', 's_is_sound', 's_is_voice', 's_is_radio']
	def __init__(self, param1 = False, param2 = 0, param3 = 0, param4 = False, param5 = 0, param6 = "", param7 = 0, param8 = False, param9 = False, param10 = False, param11 = False):
		self.s_animation = param1
		self.s_sound = param2
		self.s_music = param3
		self.s_friend_actions_enabled = param4
		self.s_voice = param5
		self.s_quality = param6
		self.s_radio = param7
		self.s_is_music = param8
		self.s_is_sound = param9
		self.s_is_voice = param10
		self.s_is_radio = param11

	def read(self, param1):
		self.s_animation = param1.readBoolean()
		self.s_sound = param1.readUnsignedByte()
		self.s_music = param1.readUnsignedByte()
		self.s_friend_actions_enabled = param1.readBoolean()
		self.s_voice = param1.readUnsignedByte()
		self.s_quality = param1.readUTF()
		self.s_radio = param1.readUnsignedByte()
		self.s_is_music = param1.readBoolean()
		self.s_is_sound = param1.readBoolean()
		self.s_is_voice = param1.readBoolean()
		self.s_is_radio = param1.readBoolean()

	def write(self, param1):
		param1.writeBoolean(self.s_animation)
		param1.writeByte(self.s_sound)
		param1.writeByte(self.s_music)
		param1.writeBoolean(self.s_friend_actions_enabled)
		param1.writeByte(self.s_voice)
		param1.writeUTF(self.s_quality)
		param1.writeByte(self.s_radio)
		param1.writeBoolean(self.s_is_music)
		param1.writeBoolean(self.s_is_sound)
		param1.writeBoolean(self.s_is_voice)
		param1.writeBoolean(self.s_is_radio)

class i_a_PCost_a(object):
	def write(self, param1):
		param1.writeInt(self.field_0)
		if self.field_1 == {}:
			param1.writeByte(0)
		else:
			param1.writeByte(len(self.field_1))
			for i in xrange(len(self.field_1)):
				self.field_1[i].write(param1)

	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = 0, param2 = None):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = param1.readInt()
		self.field_1 = {}
		for i in xrange(param1.readUnsignedByte()):
			self.field_1[i] = PCost()
			self.field_1[i].read(param1)

class str_PRatingPos(object):
	def write(self, param1):
		param1.writeUTF(self.field_0)
		self.field_1.write(param1)

	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = "", param2 = None):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = param1.readUTF()
		self.field_1 = PRatingPos()
		self.field_1.read(param1)

class PLgKind(object):
	COMPETITION = 1
	RAID = 0

	def write(self, param1):
		param1.writeByte(self.variance)

	__slots__ = ['variance']
	def __init__(self, param1 = 0):
		self.variance = param1

	def read(self, param1):
		self.variance = param1.readUnsignedByte()

class PCompetition(object):
	def write(self, param1):
		param1.writeUTF(self.cp_id)
		param1.writeDouble(self.cp_ts)
		param1.writeDouble(self.cp_finish_ts)
		param1.writeUTF(self.cp_fruit)
		if self.cp_users == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.cp_users))
			for i in xrange(len(self.cp_users)):
				self.cp_users[i].write(param1)
		param1.writeDouble(self.now)

	__slots__ = ['cp_id', 'cp_ts', 'cp_finish_ts', 'cp_fruit', 'cp_users', 'now']
	def __init__(self, param1 = "", param2 = 0, param3 = 0, param4 = "", param5 = None, param6 = 0):
		self.cp_id = param1
		self.cp_ts = param2
		self.cp_finish_ts = param3
		self.cp_fruit = param4
		self.cp_users = param5
		self.now = param6

	def read(self, param1):
		self.cp_id = param1.readUTF()
		self.cp_ts = param1.readDouble()
		self.cp_finish_ts = param1.readDouble()
		self.cp_fruit = param1.readUTF()
		self.cp_users = {}
		for i in xrange(param1.readUnsignedShort()):
			self.cp_users[i] = PCompetitionUser()
			self.cp_users[i].read(param1)
		self.now = param1.readDouble()

class PCompetitionInfo(object):
	def write(self, param1):
		param1.writeDouble(self.ci_period_end_ts)
		param1.writeUTF(self.ci_current_fruit)
		param1.writeUTF(self.ci_next_fruit)
		param1.writeInt(self.ci_fruits_per_gold_pack)
		self.ci_fruit_gold_pack_price.write(param1)
		param1.writeDouble(self.now)

	__slots__ = ['ci_period_end_ts', 'ci_current_fruit', 'ci_next_fruit', 'ci_fruits_per_gold_pack', 'ci_fruit_gold_pack_price', 'now']
	def __init__(self, param1 = 0, param2 = "", param3 = "", param4 = 0, param5 = None, param6 = 0):
		self.ci_period_end_ts = param1
		self.ci_current_fruit = param2
		self.ci_next_fruit = param3
		self.ci_fruits_per_gold_pack = param4
		self.ci_fruit_gold_pack_price = param5
		self.now = param6

	def read(self, param1):
		self.ci_period_end_ts = param1.readDouble()
		self.ci_current_fruit = param1.readUTF()
		self.ci_next_fruit = param1.readUTF()
		self.ci_fruits_per_gold_pack = param1.readInt()
		self.ci_fruit_gold_pack_price = PCost()
		self.ci_fruit_gold_pack_price.read(param1)
		self.now = param1.readDouble()

class PCompetitionUser(object):
	def write(self, param1):
		param1.writeUTF(self.cu_id)
		param1.writeUTF(self.cu_name)
		param1.writeUTF(self.cu_avatar)
		param1.writeInt(self.cu_points)
		param1.writeDouble(self.cu_ts)
		param1.writeInt(self.cu_place)

	__slots__ = ['cu_id', 'cu_name', 'cu_avatar', 'cu_points', 'cu_ts', 'cu_place']
	def __init__(self, param1 = "", param2 = "", param3 = "", param4 = 0, param5 = 0, param6 = 0):
		self.cu_id = param1
		self.cu_name = param2
		self.cu_avatar = param3
		self.cu_points = param4
		self.cu_ts = param5
		self.cu_place = param6

	def read(self, param1):
		self.cu_id = param1.readUTF()
		self.cu_name = param1.readUTF()
		self.cu_avatar = param1.readUTF()
		self.cu_points = param1.readInt()
		self.cu_ts = param1.readDouble()
		self.cu_place = param1.readInt()

class PShortCompetition(object):
	def write(self, param1):
		if self.sc_users == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.sc_users))
			for i in xrange(len(self.sc_users)):
				self.sc_users[i].write(param1)
		param1.writeDouble(self.sc_ts)
		param1.writeDouble(self.sc_finish_ts)

	__slots__ = ['sc_users', 'sc_ts', 'sc_finish_ts']
	def __init__(self, param1 = None, param2 = 0, param3 = 0):
		self.sc_users = param1
		self.sc_ts = param2
		self.sc_finish_ts = param3

	def read(self, param1):
		self.sc_users = {}
		for i in xrange(param1.readUnsignedShort()):
			self.sc_users[i] = PShortCompUser()
			self.sc_users[i].read(param1)
		self.sc_ts = param1.readDouble()
		self.sc_finish_ts = param1.readDouble()


class PShortCompUser(object):
	def write(self, param1):
		param1.writeUTF(self.scu_id)
		param1.writeInt(self.scu_points)
		param1.writeDouble(self.scu_ts)

	__slots__ = ['scu_id', 'scu_points', 'scu_ts']
	def __init__(self, param1 = "", param2 = 0, param3 = 0):
		self.scu_id = param1
		self.scu_points = param2
		self.scu_ts = param3

	def read(self, param1):
		self.scu_id = param1.readUTF()
		self.scu_points = param1.readInt()
		self.scu_ts = param1.readDouble()


class PFairBuilding(object):
	def write(self, param1):
		self.step.write(param1)
		param1.writeInt(self.main_hull)
		if self.hulls_progress == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.hulls_progress))
			for i in xrange(len(self.hulls_progress)):
				self.hulls_progress[i].write(param1)

	__slots__ = ['step', 'main_hull', 'hulls_progress']
	def __init__(self, param1 = None, param2 = 0, param3 = None):
		self.step = param1
		self.main_hull = param2
		self.hulls_progress = param3

	def read(self, param1):
		self.step = PBuildingState()
		self.step.read(param1)
		self.main_hull = param1.readInt()
		self.hulls_progress = {}
		for i in xrange(param1.readUnsignedShort()):
			self.hulls_progress[i] = str_i()
			self.hulls_progress[i].read(param1)

class PFairBuildingHullInfo(object):
	def write(self, param1):
		param1.writeUTF(self.fbl_hull_quest)
		self.fbl_hull_buy_price.write(param1)
		param1.writeUTF(self.fbl_rare_kind)
		param1.writeInt(self.fbl_hull_health)
		param1.writeInt(self.fbl_hull_comfort)

	__slots__ = ['fbl_hull_quest', 'fbl_hull_buy_price', 'fbl_rare_kind', 'fbl_hull_health', 'fbl_hull_comfort']
	def __init__(self, param1 = "", param2 = None, param3 = "", param4 = 0, param5 = 0):
		self.fbl_hull_quest = param1
		self.fbl_hull_buy_price = param2
		self.fbl_rare_kind = param3
		self.fbl_hull_health = param4
		self.fbl_hull_comfort = param5

	def read(self, param1):
		self.fbl_hull_quest = param1.readUTF()
		self.fbl_hull_buy_price = PCosts()
		self.fbl_hull_buy_price.read(param1)
		self.fbl_rare_kind = param1.readUTF()
		self.fbl_hull_health = param1.readInt()
		self.fbl_hull_comfort = param1.readInt()

class PFairBuildingInfo(object):
	def write(self, param1):
		param1.writeUTF(self.fbl_kind)
		self.fbl_buy_price.write(param1)
		param1.writeInt(self.fbl_health)
		param1.writeInt(self.fbl_comfort)
		self.fbl_grow_price.write(param1)
		if self.fbl_hulls == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.fbl_hulls))
			for i in xrange(len(self.fbl_hulls)):
				self.fbl_hulls[i].write(param1)
		param1.writeInt(self.fbl_game_event_id)
		param1.writeDouble(self.fbl_purchase_start)
		param1.writeDouble(self.fbl_purchase_finish)

	__slots__ = ['fbl_kind', 'fbl_buy_price', 'fbl_health', 'fbl_comfort', 'fbl_grow_price', 'fbl_hulls', 'fbl_game_event_id', 'fbl_purchase_start', 'fbl_purchase_finish']
	def __init__(self, param1 = "", param2 = None, param3 = 0, param4 = 0, param5 = None, param6 = None, param7 = 0, param8 = 0, param9 = 0):
		self.fbl_kind = param1
		self.fbl_buy_price = param2
		self.fbl_health = param3
		self.fbl_comfort = param4
		self.fbl_grow_price = param5
		self.fbl_hulls = param6
		self.fbl_game_event_id = param7
		self.fbl_purchase_start = param8
		self.fbl_purchase_finish = param9

	def read(self, param1):
		self.fbl_kind = param1.readUTF()
		self.fbl_buy_price = PCosts()
		self.fbl_buy_price.read(param1)
		self.fbl_health = param1.readInt()
		self.fbl_comfort = param1.readInt()
		self.fbl_grow_price = PCosts()
		self.fbl_grow_price.read(param1)
		self.fbl_hulls = {}
		for i in xrange(param1.readUnsignedShort()):
			self.fbl_hulls[i] = PFairBuildingHullInfo()
			self.fbl_hulls[i].read(param1)
		self.fbl_game_event_id = param1.readInt()
		self.fbl_purchase_start = param1.readDouble()
		self.fbl_purchase_finish = param1.readDouble()


class PFairCompAwardInfo(object):
	def write(self, param1):
		if self.factors == {}:
			param1.writeByte(0)
		else:
			param1.writeByte(len(self.factors))
			for i in xrange(len(self.factors)):
				self.factors[i].write(param1)

		if self.award_per_place == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.award_per_place))
			for i in xrange(len(self.award_per_place)):
				self.award_per_place[i].write(param1)

	__slots__ = ['factors', 'award_per_place']
	def __init__(self, param1 = None, param2 = None):
		self.factors = param1
		self.award_per_place = param2

	def read(self, param1):
		self.factors = {}
		for i in xrange(param1.readUnsignedByte()):
			self.factors[i] = PCost()
			self.factors[i].read(param1)

		self.award_per_place = {}
		for i in xrange(param1.readUnsignedShort()):
			self.award_per_place[i] = i_a_PCost_a()
			self.award_per_place[i].read(param1)

class PResFairBuilding(object):
	def write(self, param1):
		param1.writeInt(self.id)
		param1.writeInt(self.hull_stats)
		if self.farm_actions == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.farm_actions))
			for i in xrange(len(self.farm_actions)):
				self.farm_actions[i].write(param1)

		if self.sign != None:
			param1.writeByte(1)
			self.sign.write(param1)
		else:
			param1.writeByte(0)

	__slots__ = ['id', 'hull_stats', 'farm_actions', 'sign']
	def __init__(self, param1 = 0, param2 = 0, param3 = None, param4 = None):
		self.id = param1
		self.hull_stats = param2
		self.farm_actions = param3
		self.sign = param4

	def read(self, param1):
		self.id = param1.readUnsignedInt()
		self.hull_stats = param1.readInt()
		self.farm_actions = {}
		for i in xrange(param1.readUnsignedShort()):
			self.farm_actions[i] = PFarmAction()
			self.farm_actions[i].read(param1)

		if param1.readUnsignedByte() == 1:
			self.sign = PSign()
			self.sign.read(param1)
		else:
			self.sign = None


class PRating(object):
	def write(self, param1):
		self.tops.write(param1)
		if self.my_pos != None:
			param1.writeByte(1)
			self.my_pos.write(param1)
		else:
			param1.writeByte(0)

	__slots__ = ['tops', 'my_pos']
	def __init__(self, param1 = None, param2 = None):
		self.tops = param1
		self.my_pos = param2

	def read(self, param1):
		self.tops = PRatingTops()
		self.tops.read(param1)
		if param1.readUnsignedByte() == 1:
			self.my_pos = PRatingPos()
			self.my_pos.read(param1)
		else:
			self.my_pos = None

class PRatingPos(object):
	def write(self, param1):
		param1.writeInt(self.rpos)
		param1.writeInt(self.rval)

	__slots__ = ['rpos', 'rval']
	def __init__(self, param1 = 0, param2 = 0):
		self.rpos = param1
		self.rval = param2

	def read(self, param1):
		self.rpos = param1.readInt()
		self.rval = param1.readInt()

class PRatingTops(object):
	def write(self, param1):
		if self.value == {}:
			param1.writeByte(0)
		else:
			param1.writeByte(len(self.value))
			for i in xrange(len(self.value)):
				self.value[i].write(param1)

	__slots__ = ['value']
	def __init__(self, param1 = None):
		self.value = param1

	def read(self, param1):
		self.value = {}
		for i in xrange(param1.readUnsignedByte()):
			self.value[i] = str_PRatingPos()
			self.value[i].read(param1)

class PBuildingFacility(object):
	FERTILIZE = 1
	PLANT = 0

	def write(self, param1):
		param1.writeByte(self.variance)

	__slots__ = ['variance']
	def __init__(self, param1 = 0):
		self.variance = param1

	def read(self, param1):
		self.variance = param1.readUnsignedByte()

class PBuildingFacility_PCosts(object):
	def write(self, param1):
		self.field_0.write(param1)
		self.field_1.write(param1)

	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = None, param2 = None):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = PBuildingFacility()
		self.field_0.read(param1)
		self.field_1 = PCosts()
		self.field_1.read(param1)

class PExchangeEventRares(object):
	def write(self, param1):
		if self.eer_expenses == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.eer_expenses))
			for i in xrange(len(self.eer_expenses)):
				self.eer_expenses[i].write(param1)
		if self.eer_prize == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.eer_prize))
			for i in xrange(len(self.eer_prize)):
				self.eer_prize[i].write(param1)
		param1.writeDouble(self.err_date_finish)

	__slots__ = ['eer_expenses', 'eer_prize', 'err_date_finish']
	def __init__(self, param1 = None, param2 = None, param3 = 0):
		self.eer_expenses = param1
		self.eer_prize = param2
		self.err_date_finish = param3

	def read(self, param1):
		self.eer_expenses = {}
		for i in xrange(param1.readUnsignedShort()):
			self.eer_expenses[i] = PCost()
			self.eer_expenses[i].read(param1)
		self.eer_prize = {}
		for i in xrange(param1.readUnsignedShort()):
			self.eer_prize[i] = PQuestPrize()
			self.eer_prize[i].read(param1)
		self.err_date_finish = param1.readDouble()

class a_PWarehouseReserve_a_time(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = None, param2 = 0):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = {}
		for i in xrange(param1.readUnsignedShort()):
			self.field_0[i] = PWarehouseReserve()
			self.field_0[i].read(param1)
		self.field_1 = param1.readDouble()

	def write(self, param1):
		if self.field_0 == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.field_0))
			for i in xrange(len(self.field_0)):
				self.field_0[i].write(param1)
		param1.writeDouble(self.field_1)


class i_a_PClanTargetLevelGe_a(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = 0, param2 = None):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = param1.readInt()
		self.field_1 = {}
		for i in xrange(param1.readUnsignedShort()):
			self.field_1[i] = PClanTargetLevelGe()
			self.field_1[i].read(param1)

	def write(self, param1):
		param1.writeInt(self.field_0)
		if self.field_1 == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.field_1))
			for i in xrange(len(self.field_1)):
				self.field_1[i].write(param1)

class i_a_PClanTargetLevelGe_a_time(object):
	__slots__ = ['field_0', 'field_1', 'field_2']
	def __init__(self, param1 = 0, param2 = None, param3 = 0):
		self.field_0 = param1
		self.field_1 = param2
		self.field_2 = param3

	def read(self, param1):
		self.field_0 = param1.readInt()
		self.field_1 = {}
		for i in xrange(param1.readUnsignedShort()):
			self.field_1[i] = PClanTargetLevelGe()
			self.field_1[i].read(param1)
		self.field_2 = param1.readDouble()

	def write(self, param1):
		param1.writeInt(self.field_0)
		if self.field_1 == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.field_1))
			for i in xrange(len(self.field_1)):
				self.field_1[i].write(param1)
		param1.writeDouble(self.field_2)

class a_PClanTargetLevelGe_a_time(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = None, param2 = 0):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = {}
		for i in xrange(param1.readUnsignedShort()):
			self.field_0[i] = PClanTargetLevelGe()
			self.field_0[i].read(param1)
		self.field_1 = param1.readDouble()

	def write(self, param1):
		if self.field_0 == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.field_0))
			for i in xrange(len(self.field_0)):
				self.field_0[i].write(param1)
		param1.writeDouble(self.field_1)

class PAct(object):
	GET_EFARM_BONUS = 49
	TAKE_ACHIEVEMENT_PRIZE = 48
	ADD_TO_SCURRENCY_WISH_LIST = 47
	BUY_SUITCASE = 46
	OPEN_SUITCASE = 45
	RESET_MAGIC_ACTION = 44
	MAGIC_ACTION = 43
	DROP_FROM_WAREHOUSE = 42
	PURCHASE_CLAN_FAIR_BUILDING = 41
	COLLECT_BONUS_CLAN_FAIR_BUILDING = 40
	PROCESS_CLAN_FAIR_BUILDING = 39
	PROLONG_OFFER = 38
	BUY_OFFER = 37
	BUY_INGREDIENTS = 36
	EXCHANGE_EVENT_RARES = 35
	OPEN_GOLD_CASE = 34
	BUY_BUILDING_FACILITY = 33
	START_EVENT_QUEST = 32
	BUY_UNLOCK_RAID_LIST = 31
	UPGRADE_BUILDING = 30
	RESET_BONUS_TIME = 29
	NEW_GIFT = 28
	SKITTLES_GIFT = 27
	BUY_CASE = 26
	GET_PRIZE_LIKE = 25
	USER_COMPLITE_QUEST = 24
	BUY_SEEDS_PACK = 23
	REFUSAL_QUEST = 22
	BUY_UNLOCK = 21
	FEED_PET = 20
	BUY_SEGWAY = 19
	BUY_WEAPON = 18
	BUY_FERTILIZER = 17
	EXECUTE_QUEST_TARGET = 16
	COLLECT_BUILDING_BONUS = 15
	USE_FERTILIZER = 14
	FINISH_BUILDING = 13
	NEW_DECOR = 12
	NEW_TILE = 11
	NEW_ANIMAL = 10
	NEW_BUILDING = 9
	BUY_FAIR_BUILDING = 8
	BUY_RARE_ITEM = 7
	DROP_COLLECTION = 6
	GOLD2_BAKS = 5
	BUY_EXPANSION = 4
	WAREHOUSE2_BOARD = 3
	SELL_RESERVES = 2
	BOARD_OBJ_ACTION = 1
	USE_ENERGY = 0

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.USE_ENERGY:
			self.value = PUseEnergy()
			self.value.read(param1)
		elif self.variance == self.BOARD_OBJ_ACTION:
			self.value = PBoardObjAction()
			self.value.read(param1)
		elif self.variance == self.SELL_RESERVES:
			self.value = PWarehouseReserve()
			self.value.read(param1)
		elif self.variance == self.WAREHOUSE2_BOARD:
			self.value = PWarehouse2board()
			self.value.read(param1)
		elif self.variance == self.BUY_EXPANSION:
			self.value = param1.readUnsignedByte()
		elif self.variance == self.GOLD2_BAKS:
			self.value = param1.readUnsignedInt()
		elif self.variance == self.DROP_COLLECTION:
			self.value = param1.readUTF()
		elif self.variance == self.BUY_RARE_ITEM:
			self.value = PRareItem()
			self.value.read(param1)
		elif self.variance == self.BUY_FAIR_BUILDING:
			self.value = PBuyFairBuilding()
			self.value.read(param1)
		elif self.variance == self.NEW_BUILDING:
			self.value = PNewBuilding()
			self.value.read(param1)
		elif self.variance == self.NEW_ANIMAL:
			self.value = PNewAnimal()
			self.value.read(param1)
		elif self.variance == self.NEW_TILE:
			self.value = PNewTile()
			self.value.read(param1)
		elif self.variance == self.NEW_DECOR:
			self.value = PNewDecor()
			self.value.read(param1)
		elif self.variance == self.FINISH_BUILDING:
			self.value = PFinishBuilding()
			self.value.read(param1)
		elif self.variance == self.USE_FERTILIZER:
			self.value = PUseFeritilizer()
			self.value.read(param1)
		elif self.variance == self.COLLECT_BUILDING_BONUS:
			self.value = uint_PCollectBonusType()
			self.value.read(param1)
		elif self.variance == self.EXECUTE_QUEST_TARGET:
			self.value = PExecQtarget()
			self.value.read(param1)
		elif self.variance == self.BUY_FERTILIZER:
			self.value = param1.readUTF()
		elif self.variance == self.BUY_WEAPON:
			self.value = param1.readUTF()
		elif self.variance == self.BUY_SEGWAY:
			self.value = PNewSegway()
			self.value.read(param1)
		elif self.variance == self.FEED_PET:
			self.value = PFeedPet()
			self.value.read(param1)
		elif self.variance == self.BUY_UNLOCK:
			self.value = param1.readUTF()
		elif self.variance == self.REFUSAL_QUEST:
			self.value = param1.readUTF()
		elif self.variance == self.BUY_SEEDS_PACK:
			self.value = param1.readUTF()
		elif self.variance == self.USER_COMPLITE_QUEST:
			self.value = param1.readUTF()
		elif self.variance == self.GET_PRIZE_LIKE:
			self.value = PGetPrizeLike()
			self.value.read(param1)
		elif self.variance == self.BUY_CASE:
			self.value = str_a_PCost_a()
			self.value.read(param1)
		elif self.variance == self.SKITTLES_GIFT:
			pass
		elif self.variance == self.NEW_GIFT:
			self.value = PWarehouseReserve()
			self.value.read(param1)
		elif self.variance == self.RESET_BONUS_TIME:
			self.value = param1.readUnsignedInt()
		elif self.variance == self.UPGRADE_BUILDING:
			self.value = param1.readUnsignedInt()
		elif self.variance == self.BUY_UNLOCK_RAID_LIST:
			self.value = {}
			for i in xrange(param1.readUnsignedShort()):
				self.value[i] = param1.readUTF()
		elif self.variance == self.START_EVENT_QUEST:
			self.value = param1.readUnsignedByte()
		elif self.variance == self.BUY_BUILDING_FACILITY:
			self.value = PBuyBuildingFacility()
			self.value.read(param1)
		elif self.variance == self.OPEN_GOLD_CASE:
			pass
		elif self.variance == self.EXCHANGE_EVENT_RARES:
			self.value = PQuestPrize()
			self.value.read(param1)
		elif self.variance == self.BUY_INGREDIENTS:
			self.value = PFormulaIngr()
			self.value.read(param1)
		elif self.variance == self.BUY_OFFER:
			pass
		elif self.variance == self.PROLONG_OFFER:
			self.value = param1.readUTF()
		elif self.variance == self.PROCESS_CLAN_FAIR_BUILDING:
			self.value = PProcessClanFairBuilding()
			self.value.read(param1)
		elif self.variance == self.COLLECT_BONUS_CLAN_FAIR_BUILDING:
			self.value = param1.readUnsignedInt()
		elif self.variance == self.PURCHASE_CLAN_FAIR_BUILDING:
			self.value = param1.readUnsignedInt()
		elif self.variance == self.DROP_FROM_WAREHOUSE:
			self.value = PWarehouseReserve()
			self.value.read(param1)
		elif self.variance == self.MAGIC_ACTION:
			self.value = PMagicAction()
			self.value.read(param1)
		elif self.variance == self.RESET_MAGIC_ACTION:
			self.value = PMagicAction()
			self.value.read(param1)
		elif self.variance == self.OPEN_SUITCASE:
			self.value = param1.readUTF()
		elif self.variance == self.BUY_SUITCASE:
			self.value = PScurrencyWishList()
			self.value.read(param1)
		elif self.variance == self.ADD_TO_SCURRENCY_WISH_LIST:
			self.value = str_uint()
			self.value.read(param1)
		elif self.variance == self.TAKE_ACHIEVEMENT_PRIZE:
			self.value = param1.readUTF()
		elif self.variance == self.GET_EFARM_BONUS:
			pass
		else:
			raise Exception ("ERROR: incorrect PAct data")

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.USE_ENERGY:
			self.value.write(param1)
		elif self.variance == self.BOARD_OBJ_ACTION:
			self.value.write(param1)
		elif self.variance == self.SELL_RESERVES:
			self.value.write(param1)
		elif self.variance == self.WAREHOUSE2_BOARD:
			self.value.write(param1)
		elif self.variance == self.BUY_EXPANSION:
			param1.writeByte(self.value)
		elif self.variance == self.GOLD2_BAKS:
			param1.writeInt(self.value)
		elif self.variance == self.DROP_COLLECTION:
			param1.writeUTF(self.value)
		elif self.variance == self.BUY_RARE_ITEM:
			self.value.write(param1)
		elif self.variance == self.BUY_FAIR_BUILDING:
			self.value.write(param1)
		elif self.variance == self.NEW_BUILDING:
			self.value.write(param1)
		elif self.variance == self.NEW_ANIMAL:
			self.value.write(param1)
		elif self.variance == self.NEW_TILE:
			self.value.write(param1)
		elif self.variance == self.NEW_DECOR:
			self.value.write(param1)
		elif self.variance == self.FINISH_BUILDING:
			self.value.write(param1)
		elif self.variance == self.USE_FERTILIZER:
			self.value.write(param1)
		elif self.variance == self.COLLECT_BUILDING_BONUS:
			self.value.write(param1)
		elif self.variance == self.EXECUTE_QUEST_TARGET:
			self.value.write(param1)
		elif self.variance == self.BUY_FERTILIZER:
			param1.writeUTF(self.value)
		elif self.variance == self.BUY_WEAPON:
			param1.writeUTF(self.value)
		elif self.variance == self.BUY_SEGWAY:
			self.value.write(param1)
		elif self.variance == self.FEED_PET:
			self.value.write(param1)
		elif self.variance == self.BUY_UNLOCK:
			param1.writeUTF(self.value)
		elif self.variance == self.REFUSAL_QUEST:
			param1.writeUTF(self.value)
		elif self.variance == self.BUY_SEEDS_PACK:
			param1.writeUTF(self.value)
		elif self.variance == self.USER_COMPLITE_QUEST:
			param1.writeUTF(self.value)
		elif self.variance == self.GET_PRIZE_LIKE:
			self.value.write(param1)
		elif self.variance == self.BUY_CASE:
			self.value.write(param1)
		elif self.variance == self.SKITTLES_GIFT:
			pass
		elif self.variance == self.NEW_GIFT:
			self.value.write(param1)
		elif self.variance == self.RESET_BONUS_TIME:
			param1.writeInt(self.value)
		elif self.variance == self.UPGRADE_BUILDING:
			param1.writeInt(self.value)
		elif self.variance == self.BUY_UNLOCK_RAID_LIST:
			if self.value == {}:
				param1.writeShort(0)
			else:
				param1.writeShort(len(self.value))
				for i in xrange(len(self.value)):
					param1.writeUTF(self.value[i])
		elif self.variance == self.START_EVENT_QUEST:
			param1.writeByte(self.value)
		elif self.variance == self.BUY_BUILDING_FACILITY:
			self.value.write(param1)
		elif self.variance == self.OPEN_GOLD_CASE:
			pass
		elif self.variance == self.EXCHANGE_EVENT_RARES:
			self.value.write(param1)
		elif self.variance == self.BUY_INGREDIENTS:
			self.value.write(param1)
		elif self.variance == self.BUY_OFFER:
			pass
		elif self.variance == self.PROLONG_OFFER:
			param1.writeUTF(self.value)
		elif self.variance == self.PROCESS_CLAN_FAIR_BUILDING:
			self.value.write(param1)
		elif self.variance == self.COLLECT_BONUS_CLAN_FAIR_BUILDING:
			param1.writeInt(self.value)
		elif self.variance == self.PURCHASE_CLAN_FAIR_BUILDING:
			param1.writeInt(self.value)
		elif self.variance == self.DROP_FROM_WAREHOUSE:
			self.value.write(param1)
		elif self.variance == self.MAGIC_ACTION:
			self.value.write(param1)
		elif self.variance == self.RESET_MAGIC_ACTION:
			self.value.write(param1)
		elif self.variance == self.OPEN_SUITCASE:
			param1.writeUTF(self.value)
		elif self.variance == self.BUY_SUITCASE:
			self.value.write(param1)
		elif self.variance == self.ADD_TO_SCURRENCY_WISH_LIST:
			self.value.write(param1)
		elif self.variance == self.TAKE_ACHIEVEMENT_PRIZE:
			param1.writeUTF(self.value)
		elif self.variance == self.GET_EFARM_BONUS:
			pass
		else:
			raise Exception ("ERROR: incorrect PAct data")

class PNewSeedbed(object):
	def write(self, param1):
		self.pos.write(param1)
		param1.writeUTF(self.seed)
		param1.writeBoolean(self.from_shop)

	__slots__ = ['pos', 'seed', 'from_shop']
	def __init__(self, param1 = None, param2 = "", param3 = False):
		self.pos = param1
		self.seed = param2
		self.from_shop = param3

	def read(self, param1):
		self.pos = Position()
		self.pos.read(param1)
		self.seed = param1.readUTF()
		self.from_shop = param1.readBoolean()

class PResAttackMonster(object):
	def write(self, param1):
		param1.writeInt(self.stamina)
		self.pos.write(param1)
		param1.writeBoolean(self.is_crit)
		if self.sign_opt != None:
			param1.writeByte(1)
			self.sign_opt.write(param1)
		else:
			param1.writeByte(0)

	__slots__ = ['stamina', 'pos', 'is_crit', 'sign_opt']
	def __init__(self, param1 = 0, param2 = None, param3 = False, param4 = None):
		self.stamina = param1
		self.pos = param2
		self.is_crit = param3
		self.sign_opt = param4

	def read(self, param1):
		self.stamina = param1.readUnsignedInt()
		self.pos = Position()
		self.pos.read(param1)
		self.is_crit = param1.readBoolean()
		if param1.readUnsignedByte() == 1:
			self.sign_opt = PSign()
			self.sign_opt.read(param1)
		else:
			self.sign_opt = None

class PClanRoleInfo(object):
	__slots__ = ['cr_kind', 'cr_max_energy', 'cr_req_level']
	def __init__(self, param1 = None, param2 = 0, param3 = 0):
		self.cr_kind = param1
		self.cr_max_energy = param2
		self.cr_req_level = param3

	def read(self, param1):
		self.cr_kind = PClmRole()
		self.cr_kind.read(param1)
		self.cr_max_energy = param1.readUnsignedShort()
		self.cr_req_level = param1.readUnsignedShort()

	def write(self, param1):
		self.cr_kind.write(param1)
		param1.writeShort(self.cr_max_energy)
		param1.writeShort(self.cr_req_level)

class PClmRole(object):
	FARMER = 3
	COOL_FARMER = 2
	SUB_LEADER = 1
	LEADER = 0

	__slots__ = ['variance']
	def __init__(self, param1 = 0):
		self.variance = param1

	def read(self, param1):
		self.variance = param1.readUnsignedByte()

	def write(self, param1):
		param1.writeByte(self.variance)

class PFertilizerType(object):
	ATTACK_MONSTERS = 7
	WAREHOUSE_RESERVE = 6
	STONE = 5
	WOOD = 4
	ANIMAL = 3
	NOT_ENOUGHT_PLACE = 2
	OBJECTS = 1
	SEEDBED = 0

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.SEEDBED:
			self.value = PResUseFertSeedbed()
			self.value.read(param1)
		elif self.variance == self.OBJECTS:
			self.value = {}
			for i in xrange(param1.readUnsignedShort()):
				self.value[i] = PObj()
				self.value[i].read(param1)
		elif self.variance == self.NOT_ENOUGHT_PLACE:
			self.value = param1.readUTF()
		elif self.variance == self.ANIMAL:
			self.value = PResUseFertAnimal()
			self.value.read(param1)
		elif self.variance == self.WOOD:
			self.value = PWood()
			self.value.read(param1)
		elif self.variance == self.STONE:
			self.value = PStone()
			self.value.read(param1)
		elif self.variance == self.WAREHOUSE_RESERVE:
			self.value = PWarehouseReserve()
			self.value.read(param1)
		elif self.variance == self.ATTACK_MONSTERS:
			self.value = {}
			for i in xrange(param1.readUnsignedShort()):
				self.value[i] = uint_PResAttackMonster()
				self.value[i].read(param1)
		else:
			raise Exception ("ERROR: incorrect PFertilizerType data")

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.SEEDBED:
			self.value.write(param1)
		elif self.variance == self.OBJECTS:
			if self.value == {}:
				param1.writeShort(0)
			else:
				param1.writeShort(len(self.value))
				for i in xrange(len(self.value)):
					self.value[i].write(param1)
		elif self.variance == self.NOT_ENOUGHT_PLACE:
			param1.writeUTF(self.value)
		elif self.variance == self.ANIMAL:
			self.value.write(param1)
		elif self.variance == self.WOOD:
			self.value.write(param1)
		elif self.variance == self.STONE:
			self.value.write(param1)
		elif self.variance == self.WAREHOUSE_RESERVE:
			self.value.write(param1)
		elif self.variance == self.ATTACK_MONSTERS:
			if self.value == {}:
				param1.writeShort(0)
			else:
				param1.writeShort(len(self.value))
				for i in xrange(len(self.value)):
					self.value[i].write(param1)
		else:
			raise Exception ("ERROR: incorrect PFertilizerType data")

class PResFinishBuilding(object):
	__slots__ = ['building_id', 'building_state', 'building_event']
	def __init__(self, param1 = 0, param2 = None, param3 = None, param4 = None):
		self.building_id = param1
		self.building_state = param2
		self.building_event = param3
		self.sign = param4

	def read(self, param1):
		self.building_id = param1.readUnsignedInt()
		self.building_state = PBuildingState()
		self.building_state.read(param1)
		if param1.readUnsignedByte() == 1:
			self.building_event = PFarmAction()
			self.building_event.read(param1)
		else:
			self.building_event = None
		if param1.readUnsignedByte() == 1:
			self.sign = PSign()
			self.sign.read(param1)
		else:
			self.sign = None


	def write(self, param1):
		param1.writeInt(self.building_id)
		self.building_state.write(param1)
		if self.building_event != None:
			param1.writeByte(1)
			self.building_event.write(param1)
		else:
			param1.writeByte(0)
		if self.sign != None:
			param1.writeByte(1)
			self.sign.write(param1)
		else:
			param1.writeByte(0)

class PResUseFertAnimal(object):
	def write(self, param1):
		self.animal.write(param1)
		if self.animal_events == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.animal_events))
			for i in xrange(len(self.animal_events)):
				self.animal_events[i].write(param1)

	__slots__ = ['animal', 'animal_events']
	def __init__(self, param1 = None, param2 = None):
		self.animal = param1
		self.animal_events = param2

	def read(self, param1):
		self.animal = PObj()
		self.animal.read(param1)
		self.animal_events = {}
		for i in xrange(param1.readUnsignedShort()):
			self.animal_events[i] = PFarmAction()
			self.animal_events[i].read(param1)

class PResUseFertilizer(object):
	def write(self, param1):
		self.fert_type.write(param1)
		param1.writeUTF(self.fert_kind)

	__slots__ = ['fert_type', 'fert_kind']
	def __init__(self, param1 = None, param2 = ""):
		self.fert_type = param1
		self.fert_kind = param2

	def read(self, param1):
		self.fert_type = PFertilizerType()
		self.fert_type.read(param1)
		self.fert_kind = param1.readUTF()

class PResUseFertSeedbed(object):
	def write(self, param1):
		self.seedbed.write(param1)
		if self.seedbed_events == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.seedbed_events))
			for i in xrange(len(self.seedbed_events)):
				self.seedbed_events[i].write(param1)

	__slots__ = ['seedbed', 'seedbed_events']
	def __init__(self, param1 = None, param2 = None):
		self.seedbed = param1
		self.seedbed_events = param2

	def read(self, param1):
		self.seedbed = PObj()
		self.seedbed.read(param1)
		self.seedbed_events = {}
		for i in xrange(param1.readUnsignedShort()):
			self.seedbed_events[i] = PFarmAction()
			self.seedbed_events[i].read(param1)

class PResWarehouse2board(object):
	CLAN_FAIR_BUILDING = 6
	FAIR_BUILDING = 5
	DECOR_PUZZLE = 4
	DECOR = 3
	TILE = 2
	BUILDING = 1
	ANIMAL = 0

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.ANIMAL:
			self.value = PResNewAnimal()
			self.value.read(param1)
		elif self.variance == self.BUILDING:
			self.value = PResNewBuilding()
			self.value.read(param1)
		elif self.variance == self.TILE:
			self.value = param1.readUnsignedInt()
		elif self.variance == self.DECOR:
			self.value = param1.readUnsignedInt()
		elif self.variance == self.DECOR_PUZZLE:
			self.value = param1.readUnsignedInt()
		elif self.variance == self.FAIR_BUILDING:
			self.value = PResFairBuilding()
			self.value.read(param1)
		elif self.variance == self.CLAN_FAIR_BUILDING:
			self.value = param1.readUnsignedInt()
		else:
			raise Exception ("ERROR: incorrect PResWarehouse2board data")

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.ANIMAL:
			self.value.write(param1)
		elif self.variance == self.BUILDING:
			self.value.write(param1)
		elif self.variance == self.TILE:
			param1.writeInt(self.value)
		elif self.variance == self.DECOR:
			param1.writeInt(self.value)
		elif self.variance == self.DECOR_PUZZLE:
			param1.writeInt(self.value)
		elif self.variance == self.FAIR_BUILDING:
			self.value.write(param1)
		elif self.variance == self.CLAN_FAIR_BUILDING:
			param1.writeInt(self.value)
		else:
			raise Exception ("ERROR: incorrect PResWarehouse2board data")

class PBoardObjAction(object):
	ACTION_SELL = 2
	ACTION_STASH = 1
	ACTION_MOVE = 0

	def write(self, param1):
		self.obj.write(param1)
		param1.writeByte(self.action_variance)
		if self.action_variance == self.ACTION_MOVE:
			self.action_value.write(param1)
		elif self.action_variance == self.ACTION_STASH:
			pass
		elif self.action_variance == self.ACTION_SELL:
			pass
		else:
			raise Exception ("ERROR: incorrect PBoardObjAction data")

	__slots__ = ['obj', 'action_variance', 'action_value']
	def __init__(self, param1 = 0, param2 = 0, param3 = None):
		self.obj = param1
		self.action_variance = param2
		self.action_value = param3

	def read(self, param1):
		self.obj = PObjId()
		self.obj.read(param1)
		self.action_variance = param1.readUnsignedByte()
		if self.action_variance == self.ACTION_MOVE:
			self.action_value = PMove()
			self.action_value.read(param1)
		elif self.action_variance == self.ACTION_STASH:
			pass
		elif self.action_variance == self.ACTION_SELL:
			pass
		else:
			raise Exception ("ERROR: incorrect PBoardObjAction data")


class PObjId(object):
	CLAN_FAIR_BUILDING = 7
	OBJ_FAIR_BUILDING = 6
	OBJ_DECOR_PUZZLE = 5
	OBJ_DECOR = 4
	OBJ_TILE = 3
	OBJ_SEEDBED = 2
	OBJ_BUILDING = 1
	OBJ_ANIMAL = 0

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.OBJ_ANIMAL:
			self.value = param1.readUnsignedInt()
		elif self.variance == self.OBJ_BUILDING:
			self.value = param1.readUnsignedInt()
		elif self.variance == self.OBJ_SEEDBED:
			self.value = param1.readUnsignedInt()
		elif self.variance == self.OBJ_TILE:
			self.value = param1.readUnsignedInt()
		elif self.variance == self.OBJ_DECOR:
			self.value = param1.readUnsignedInt()
		elif self.variance == self.OBJ_DECOR_PUZZLE:
			self.value = param1.readUnsignedInt()
		elif self.variance == self.OBJ_FAIR_BUILDING:
			self.value = param1.readUnsignedInt()
		elif self.variance == self.CLAN_FAIR_BUILDING:
			self.value = param1.readUnsignedInt()
		else:
			raise Exception ("ERROR: incorrect PBoardObjAction data")

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.OBJ_ANIMAL:
			param1.writeInt(self.value)
		elif self.variance == self.OBJ_BUILDING:
			param1.writeInt(self.value)
		elif self.variance == self.OBJ_SEEDBED:
			param1.writeInt(self.value)
		elif self.variance == self.OBJ_TILE:
			param1.writeInt(self.value)
		elif self.variance == self.OBJ_DECOR:
			param1.writeInt(self.value)
		elif self.variance == self.OBJ_DECOR_PUZZLE:
			param1.writeInt(self.value)
		elif self.variance == self.OBJ_FAIR_BUILDING:
			param1.writeInt(self.value)
		elif self.variance == self.CLAN_FAIR_BUILDING:
			param1.writeInt(self.value)
		else:
			raise Exception ("ERROR: incorrect PBoardObjAction data")

class PBuyBuildingFacility(object):
	__slots__ = ['id', 'facility']
	def __init__(self, param1 = 0, param2 = None):
		self.id = param1
		self.facility = param2

	def read(self, param1):
		self.id = param1.readUnsignedInt()
		self.facility = PBuildingFacility()
		self.facility.read(param1)

	def write(self, param1):
		param1.writeInt(self.id)
		self.facility.write(param1)

class PBuyFairBuilding(object):
	BY_OBJ = 1
	BY_KIND = 0

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.BY_KIND:
			self.value = str_Position()
			self.value.read(param1)
		elif self.variance == self.BY_OBJ:
			self.value = param1.readInt()
		else:
			raise Exception ("ERROR: incorrect PBuyFairBuilding data")

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.BY_KIND:
			self.value.write(param1)
		elif self.variance == self.BY_OBJ:
			param1.writeInt(self.value)
		else:
			raise Exception ("ERROR: incorrect PBuyFairBuilding data")

class PCollectBonusType(object):
	RESTORE_BEDS = 5
	FERTILIZE = 4
	PLANT_SEEDS = 3
	FEED_ANIMALS = 2
	GATHER_PLANTS = 1
	NORMAL = 0

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.NORMAL:
			pass
		elif self.variance == self.GATHER_PLANTS:
			pass
		elif self.variance == self.FEED_ANIMALS:
			pass
		elif self.variance == self.PLANT_SEEDS:
			self.value.write(param1)
		elif self.variance == self.FERTILIZE:
			pass
		elif self.variance == self.RESTORE_BEDS:
			pass
		else:
			raise Exception ("ERROR: incorrect PCollectBonusType data")

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.NORMAL:
			pass
		elif self.variance == self.GATHER_PLANTS:
			pass
		elif self.variance == self.FEED_ANIMALS:
			pass
		elif self.variance == self.PLANT_SEEDS:
			self.value = PPlantSeeds()
			self.value.read(param1)
		elif self.variance == self.FERTILIZE:
			pass
		elif self.variance == self.RESTORE_BEDS:
			pass
		else:
			raise Exception ("ERROR: incorrect PCollectBonusType data")

class PPlantSeeds(object):
	__slots__ = ['plants', 'positions']
	def __init__(self, param1 = None, param2 = None):
		self.plants = param1
		self.positions = param2

	def read(self, param1):
		self.plants = {}
		for i in xrange(param1.readUnsignedShort()):
			self.plants[i] = PPlant()
			self.plants[i].read(param1)
		self.positions = {}
		for i in xrange(param1.readUnsignedShort()):
			self.positions[i] = Position()
			self.positions[i].read(param1)

	def write(self, param1):
		if self.plants == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.plants))
			for i in xrange(len(self.plants)):
				self.plants[i].write(param1)
		if self.positions == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.positions))
			for i in xrange(len(self.positions)):
				self.positions[i].write(param1)

class PUseFeritilizer(object):
	def write(self, param1):
		param1.writeUTF(self.fert_kind)
		self.uf_spec.write(param1)

	__slots__ = ['fert_kind', 'uf_spec']
	def __init__(self, param1 = "", param2 = None):
		self.fert_kind = param1
		self.uf_spec = param2

	def read(self, param1):
		self.fert_kind = param1.readUTF()
		self.uf_spec = PUfSpec()
		self.uf_spec.read(param1)

class PUseEnergy(object):
	def write(self, param1):
		param1.writeUTF(self.kind)
		param1.writeBoolean(self.from_shop)
		param1.writeInt(self.count)

	__slots__ = ['kind', 'from_shop', 'count']
	def __init__(self, param1 = "", param2 = False, param3 = 0):
		self.kind = param1
		self.from_shop = param2
		self.count = param3

	def read(self, param1):
		self.kind = param1.readUTF()
		self.from_shop = param1.readBoolean()
		self.count = param1.readInt()

class PUfSpec(object):
	MONSTER = 5
	WOOD = 4
	STONE = 3
	ANIMAL = 2
	POS = 1
	SEEDBED = 0

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.SEEDBED:
			self.value = uint_oPosition()
			self.value.read(param1)
		elif self.variance == self.POS:
			self.value = Position()
			self.value.read(param1)
		elif self.variance == self.ANIMAL:
			self.value = uint_oPosition()
			self.value.read(param1)
		elif self.variance == self.STONE:
			self.value = param1.readUnsignedInt()
		elif self.variance == self.WOOD:
			self.value = param1.readUnsignedInt()
		elif self.variance == self.MONSTER:
			self.value = param1.readUnsignedInt()
		else:
			raise Exception ("ERROR: incorrect PUfSpec data")

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.SEEDBED:
			self.value.write(param1)
		elif self.variance == self.POS:
			self.value.write(param1)
		elif self.variance == self.ANIMAL:
			self.value.write(param1)
		elif self.variance == self.STONE:
			param1.writeInt(self.value)
		elif self.variance == self.WOOD:
			param1.writeInt(self.value)
		elif self.variance == self.WOOD:
			param1.writeInt(self.value)
		elif self.variance == self.MONSTER:
			param1.writeInt(self.value)
		else:
			raise Exception ("ERROR: incorrect PUfSpec data")


class PNewAnimal(object):
	def write(self, param1):
		param1.writeUTF(self.animal_kind)
		self.pos.write(param1)

	__slots__ = ['animal_kind', 'pos']
	def __init__(self, param1 = "", param2 = None):
		self.animal_kind = param1
		self.pos = param2

	def read(self, param1):
		self.animal_kind = param1.readUTF()
		self.pos = Position()
		self.pos.read(param1)

class PMove(object):
	def write(self, param1):
		self.m_pos.write(param1)
		self.m_direction.write(param1)

	__slots__ = ['m_pos', 'm_direction']
	def __init__(self, param1 = None, param2 = None):
		self.m_pos = param1
		self.m_direction = param2

	def read(self, param1):
		self.m_pos = Position()
		self.m_pos.read(param1)
		self.m_direction = PDirection()
		self.m_direction.read(param1)

class PNewBuilding(object):
	def write(self, param1):
		param1.writeUTF(self.build_kind)
		self.pos.write(param1)

	__slots__ = ['build_kind', 'pos']
	def __init__(self, param1 = "", param2 = None):
		self.build_kind = param1
		self.pos = param2

	def read(self, param1):
		self.build_kind = param1.readUTF()
		self.pos = Position()
		self.pos.read(param1)


class PNewDecor(object):
	def write(self, param1):
		param1.writeUTF(self.decor_kind)
		self.pos.write(param1)

	__slots__ = ['decor_kind', 'pos']
	def __init__(self, param1 = "", param2 = None):
		self.decor_kind = param1
		self.pos = param2

	def read(self, param1):
		self.decor_kind = param1.readUTF()
		self.pos = Position()
		self.pos.read(param1)

class PNewTile(object):
	def write(self, param1):
		param1.writeUTF(self.tile_kind)
		self.pos.write(param1)

	__slots__ = ['tile_kind', 'pos']
	def __init__(self, param1 = "", param2 = None):
		self.tile_kind = param1
		self.pos = param2

	def read(self, param1):
		self.tile_kind = param1.readUTF()
		self.pos = Position()
		self.pos.read(param1)

class PWarehouse2board(object):
	def write(self, param1):
		param1.writeUTF(self.obj_kind)
		self.pos.write(param1)

	__slots__ = ['obj_kind', 'pos']
	def __init__(self, param1 = "", param2 = None):
		self.obj_kind = param1
		self.pos = param2

	def read(self, param1):
		self.obj_kind = param1.readUTF()
		self.pos = Position()
		self.pos.read(param1)

class PNewSegway(object):
	def write(self, param1):
		param1.writeUTF(self.kind)
		param1.writeBoolean(self.is_lease)

	__slots__ = ['kind', 'is_lease']
	def __init__(self, param1 = "", param2 = False):
		self.kind = param1
		self.is_lease = param2

	def read(self, param1):
		self.kind = param1.readUTF()
		self.is_lease = param1.readBoolean()

class PFeedPet(object):
	def write(self, param1):
		param1.writeInt(self.pet_id)
		param1.writeBoolean(self.use_gold)

	__slots__ = ['pet_id', 'use_gold']
	def __init__(self, param1 = 0, param2 = False):
		self.pet_id = param1
		self.use_gold = param2

	def read(self, param1):
		self.pet_id = param1.readUnsignedInt()
		self.use_gold = param1.readBoolean()

class PExecQtarget(object):
	def write(self, param1):
		param1.writeUTF(self.qname)
		param1.writeUTF(self.tname)

	__slots__ = ['qname', 'tname']
	def __init__(self, param1 = "", param2 = ""):
		self.qname = param1
		self.tname = param2

	def read(self, param1):
		self.qname = param1.readUTF()
		self.tname = param1.readUTF()

class PGetPrizeLike(object):
	ALREADYT_LIKED = 2
	NOT_LIKED = 1
	LIKED = 0

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.LIKED:
			self.value.write(param1)
		elif self.variance == self.NOT_LIKED:
			pass
		elif self.variance == self.ALREADYT_LIKED:
			pass
		else:
			raise Exception ("ERROR: incorrect PGetPrizeLike data")

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.LIKED:
			self.value = PQuestPrizes()
			self.value.read(param1)
		elif self.variance == self.NOT_LIKED:
			pass
		elif self.variance == self.ALREADYT_LIKED:
			pass
		else:
			raise Exception ("ERROR: incorrect PGetPrizeLike data")

class PFinishBuilding(object):
	def write(self, param1):
		param1.writeInt(self.building_id)
		param1.writeBoolean(self.use_gold)

	__slots__ = ['building_id', 'use_gold']
	def __init__(self, param1 = 0, param2 = False):
		self.building_id = param1
		self.use_gold = param2

	def read(self, param1):
		self.building_id = param1.readUnsignedInt()
		self.use_gold = param1.readBoolean()

class PRareItem(object):
	BUY_TYPE_GROUP = 1
	BUY_TYPE_CNT = 0

	def write(self, param1):
		param1.writeUTF(self.ra_kind)
		param1.writeByte(self.buy_type_variance)
		if self.buy_type_variance == self.BUY_TYPE_CNT:
			param1.writeInt(self.buy_type_value)
		elif self.buy_type_variance == self.BUY_TYPE_GROUP:
			pass
		else:
			raise Exception ("ERROR: incorrect PRareItem data")

	__slots__ = ['ra_kind', 'buy_type_variance', 'buy_type_value']
	def __init__(self, param1 = "", param2 = 0, param3 = None):
		self.ra_kind = param1
		self.buy_type_variance = param2
		self.buy_type_value = param3

	def read(self, param1):
		self.ra_kind = param1.readUTF()
		self.buy_type_variance = param1.readUnsignedByte()
		if self.buy_type_variance == self.BUY_TYPE_CNT:
			self.buy_type_value = param1.readUnsignedInt()
		elif self.buy_type_variance == self.BUY_TYPE_GROUP:
			pass
		else:
			raise Exception ("ERROR: incorrect PRareItem data")

class uint_PCollectBonusType(object):
	def write(self, param1):
		param1.writeInt(self.field_0)
		self.field_1.write(param1)

	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = 0, param2 = None):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = param1.readUnsignedInt()
		self.field_1 = PCollectBonusType()
		self.field_1.read(param1)

class str_a_PCost_a(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = "", param2 = None):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = param1.readUTF()
		self.field_1 = {}
		for i in xrange(param1.readUnsignedShort()):
			self.field_1[i] = PCost()
			self.field_1[i].read(param1)

	def write(self, param1):
		param1.writeUTF(self.field_0)
		if self.field_1 == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.field_1))
			for i in xrange(len(self.field_1)):
				self.field_1[i].write(param1)

class PRecipeInfo(object):
	__slots__ = ['rc_base', 'rc_building', 'rc_available_amount']
	def __init__(self, param1 = None, param2 = "", param3 = None):
		self.rc_base = param1
		self.rc_building = param2
		self.rc_available_amount = param3

	def read(self, param1):
		self.rc_base = PCraftRecipeBase()
		self.rc_base.read(param1)
		self.rc_building = param1.readUTF()
		self.rc_available_amount = {}
		for i in xrange(param1.readUnsignedShort()):
			self.rc_available_amount[i] = param1.readUnsignedInt()

	def write(self, param1):
		self.rc_base.write(param1)
		param1.writeUTF(self.rc_building)
		if self.rc_available_amount == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.rc_available_amount))
			for i in xrange(len(self.rc_available_amount)):
				param1.writeInt(self.rc_available_amount[i])

class PUserClanInfo(object):
	__slots__ = ['uci_energy', 'uci_energy_next_time', 'uci_role', 'uci_read_chat_time']
	def __init__(self, param1 = 0, param2 = 0, param3 = None, param4 = 0):
		self.uci_energy = param1
		self.uci_energy_next_time = param2
		self.uci_role = param3
		self.uci_read_chat_time = param4

	def read(self, param1):
		self.uci_energy = param1.readUnsignedInt()
		if param1.readUnsignedByte() == 1:
			self.uci_energy_next_time = param1.readDouble()
		else:
			self.uci_energy_next_time = None
		self.uci_role = PClmRole()
		self.uci_role.read(param1)
		self.uci_read_chat_time = param1.readDouble()

	def write(self, param1):
		param1.writeInt(self.uci_energy)
		if self.uci_energy_next_time != None:
			param1.writeByte(1)
			param1.writeDouble(self.uci_energy_next_time)
		else:
			param1.writeByte(0)
		self.uci_role.write(param1)
		param1.writeDouble(self.uci_read_chat_time)

class PClanBase(object):
	__slots__ = ['cl_id', 'cl_name', 'cl_emblem', 'cl_level', 'cl_max_members', 'cl_members_count', 'cl_description', 'cl_rating', 'cl_requests_limit', 'cl_requests_count', 'cl_dissolution_time', 'cl_place', 'cl_requests_min_level']
	def __init__(self, param1 = "", param2 = "", param3 = "", param4 = 0, param5 = 0, param6 = 0, param7 = "", param8 = 0, param9 = 0, param10 = 0, param11 = 0, param12 = 0, param13 = 0):
		self.cl_id = param1
		self.cl_name = param2
		self.cl_emblem = param3
		self.cl_level = param4
		self.cl_max_members = param5
		self.cl_members_count = param6
		self.cl_description = param7
		self.cl_rating = param8
		self.cl_requests_limit = param9
		self.cl_requests_count = param10
		self.cl_dissolution_time = param11
		self.cl_place = param12
		self.cl_requests_min_level = param13

	def __repr__(self):
		return "{cl_id:%s,cl_name:%s,cl_emblem,cl_level:%s,cl_max_members:%s,cl_members_count:%s,cl_description:%s,cl_rating:%s,cl_place:%s,cl_requests_limit,cl_requests_count,cl_dissolution_time}" % (self.cl_id.encode("utf-8"), self.cl_name.encode("utf-8"), self.cl_level, self.cl_max_members, self.cl_members_count, self.cl_description.encode("utf-8"), self.cl_rating, self.cl_place)


	def read(self, param1):
		self.cl_id = param1.readUTF()
		self.cl_name = param1.readUTF()
		self.cl_emblem = param1.readUTF()
		self.cl_level = param1.readUnsignedShort()
		self.cl_max_members = param1.readUnsignedShort()
		self.cl_members_count = param1.readUnsignedShort()
		self.cl_description = param1.readUTF()
		self.cl_rating = param1.readUnsignedInt()
		self.cl_requests_limit = param1.readUnsignedShort()
		self.cl_requests_count = param1.readUnsignedShort()
		if param1.readUnsignedByte() == 1:
			self.cl_dissolution_time = param1.readDouble()
		else:
			self.cl_dissolution_time = None
		self.cl_place = param1.readUnsignedInt()
		if param1.readUnsignedByte() == 1:
			self.cl_requests_min_level = param1.readUnsignedInt()
		else:
			self.cl_requests_min_level = None

	def write(self, param1):
		param1.writeUTF(self.cl_id)
		param1.writeUTF(self.cl_name)
		param1.writeUTF(self.cl_emblem)
		param1.writeShort(self.cl_level)
		param1.writeShort(self.cl_max_members)
		param1.writeShort(self.cl_members_count)
		param1.writeUTF(self.cl_description)
		param1.writeInt(self.cl_rating)
		param1.writeShort(self.cl_requests_limit)
		param1.writeShort(self.cl_requests_count)
		if self.cl_dissolution_time != None:
			param1.writeByte(1)
			param1.writeDouble(self.cl_dissolution_time)
		else:
			param1.writeByte(0)
		param1.writeInt(self.cl_place)
		if self.cl_requests_min_level != None:
			param1.writeByte(1)
			param1.writeInt(self.cl_requests_min_level)
		else:
			param1.writeByte(0)

class PClan(object):
	__slots__ = ['base', 'main']
	def __init__(self, param1 = None, param2 = None):
		self.base = param1
		self.main = param2

	def __repr__(self):
		return "{base:%s,main:%s}" % (self.base,self.main)

	def read(self, param1):
		self.base = PClanBase()
		self.base.read(param1)
		self.main = PClanMain()
		self.main.read(param1)

	def write(self, param1):
		self.base.write(param1)
		self.main.write(param1)

class PClanMain(object):
	__slots__ = ['cl_members', 'cl_requests', 'cl_treasury', 'cl_recipe_amount', 'cl_rename_amount', 'cl_coffer_state', 'cl_raid_id','cl_booth_recipe_state','cl_booth_coffer']
	def __init__(self, param1 = None, param2 = None, param3 = None, param4 = None, param5 = 0, param6 = None, param7 = None, param8 = None, param9 = None):
		self.cl_members = param1
		self.cl_requests = param2
		self.cl_treasury = param3
		self.cl_recipe_amount = param4
		self.cl_rename_amount = param5
		self.cl_coffer_state = param6
		self.cl_raid_id = param7
		self.cl_booth_recipe_state = param8
		self.cl_booth_coffer = param9

	def __repr__(self):
		return "{cl_members:%s,cl_requests:%s,cl_treasury:%s,cl_recipe_amount:%s,cl_rename_amount:%s}" % (self.cl_members,self.cl_requests,self.cl_treasury,self.cl_recipe_amount,self.cl_rename_amount)

	def read(self, param1):
		self.cl_members = {}
		for i in xrange(param1.readUnsignedShort()):
			self.cl_members[i] = PClanMember()
			self.cl_members[i].read(param1)
		self.cl_requests = {}
		for i in xrange(param1.readUnsignedShort()):
			self.cl_requests[i] = PClanRequest()
			self.cl_requests[i].read(param1)
		self.cl_treasury = PClanTreasury()
		self.cl_treasury.read(param1)
		self.cl_recipe_amount = {}
		for i in xrange(param1.readUnsignedShort()):
			self.cl_recipe_amount[i] = str_uint()
			self.cl_recipe_amount[i].read(param1)
		self.cl_rename_amount = param1.readUnsignedByte()
		if param1.readUnsignedByte() == 1:
			self.cl_coffer_state = PClanCofferState()
			self.cl_coffer_state.read(param1)
		else:
			self.cl_coffer_state = None
		if param1.readUnsignedByte() == 1:
			self.cl_raid_id = str_time()
			self.cl_raid_id.read(param1)
		else:
			self.cl_raid_id = None
		self.cl_booth_recipe_state = PClanBoothRecipeState()
		self.cl_booth_recipe_state.read(param1)
		if param1.readUnsignedByte() == 1:
			self.cl_booth_coffer = PClanCoffer()
			self.cl_booth_coffer.read(param1)
		else:
			self.cl_booth_coffer = None

	def write(self, param1):
		if self.cl_members == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.cl_members))
			for i in xrange(len(self.cl_members)):
				self.cl_members[i].write(param1)
		if self.cl_requests == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.cl_requests))
			for i in xrange(len(self.cl_requests)):
				self.cl_requests[i].write(param1)
		self.cl_treasury.write(param1)
		if self.cl_recipe_amount == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.cl_recipe_amount))
			for i in xrange(len(self.cl_recipe_amount)):
				self.cl_recipe_amount[i].write(param1)
		param1.writeByte(self.cl_rename_amount)
		if self.cl_coffer_state != None:
			param1.writeByte(1)
			self.cl_coffer_state.write(param1)
		else:
			param1.writeByte(0)
		if self.cl_raid_id != None:
			param1.writeByte(1)
			self.cl_raid_id.write(param1)
		else:
			param1.writeByte(0)
		self.cl_booth_recipe_state.write(param1)
		if self.cl_booth_coffer != None:
			param1.writeByte(1)
			self.cl_booth_coffer.write(param1)
		else:
			param1.writeByte(0)

class PClanMember(object):
	__slots__ = ['clm_id', 'clm_name', 'clm_avatar', 'clm_level', 'clm_role', 'clm_factor_utility_0', 'clm_factor_utility_1', 'clm_is_online', 'clm_vip']
	def __init__(self, param1 = "" , param2 = "", param3 = "", param4 = 0, param5 = None, param6 = 0, param7 = 0, param8 = False, param9 = False):
		self.clm_id = param1
		self.clm_name = param2
		self.clm_avatar = param3
		self.clm_level = param4
		self.clm_role = param5
		self.clm_factor_utility_0 = param6
		self.clm_factor_utility_1 = param7
		self.clm_is_online = param8
		self.clm_vip = param9

	def read(self, param1):
		self.clm_id = param1.readUTF()
		self.clm_name = param1.readUTF()
		self.clm_avatar = param1.readUTF()
		self.clm_level = param1.readUnsignedInt()
		self.clm_role = PClmRole()
		self.clm_role.read(param1)
		self.clm_factor_utility_0 = param1.readUnsignedInt()
		self.clm_factor_utility_1 = param1.readUnsignedInt()
		self.clm_is_online = param1.readBoolean()
		self.clm_vip = PVip()
		self.clm_vip.read(param1)

	def write(self, param1):
		param1.writeUTF(self.clm_id)
		param1.writeUTF(self.clm_name)
		param1.writeUTF(self.clm_avatar)
		param1.writeInt(self.clm_level)
		self.clm_role.write(param1)
		param1.writeInt(self.clm_factor_utility_0)
		param1.writeInt(self.clm_factor_utility_1)
		param1.writeBoolean(self.clm_is_online)
		self.clm_vip.write(param1)

class PClanRequest(object):
	__slots__ = ['clr_id', 'clr_name', 'clr_avatar', 'clr_level', 'clr_time']
	def __init__(self, param1 = "", param2 = "", param3 = "", param4 = 0, param5 = ""):
		self.clr_id = param1
		self.clr_name = param2
		self.clr_avatar = param3
		self.clr_level = param4
		self.clr_time = param5

	def read(self, param1):
		self.clr_id = param1.readUTF()
		self.clr_name = param1.readUTF()
		self.clr_avatar = param1.readUTF()
		self.clr_level = param1.readUnsignedInt()
		self.clr_time = param1.readDouble()

	def write(self, param1):
		param1.writeUTF(self.clr_id)
		param1.writeUTF(self.clr_name)
		param1.writeUTF(self.clr_avatar)
		param1.writeInt(self.clr_level)
		param1.writeDouble(self.clr_time)

class PClanTreasury(object):
	__slots__ = ['clt_gold', 'clt_logs']
	def __init__(self, param1 = 0, param2 = None):
		self.clt_gold = param1
		self.clt_logs = param2

	def read(self, param1):
		self.clt_gold = param1.readUnsignedInt()
		self.clt_logs = {}
		for i in xrange(param1.readUnsignedShort()):
			self.clt_logs[i] = PCtrLog()
			self.clt_logs[i].read(param1)

	def write(self, param1):
		param1.writeInt(self.clt_gold)
		if self.clt_logs == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.clt_logs))
			for i in xrange(len(self.clt_logs)):
				self.clt_logs[i].write(param1)

class PCtrLog(object):
	__slots__ = ['trl_date', 'trl_id', 'trl_gold']
	def __init__(self, param1 = 0, param2 = "", param3 = 0):
		self.trl_date = param1
		self.trl_id = param2
		self.trl_gold = param3

	def read(self, param1):
		self.trl_date = param1.readDouble()
		self.trl_id = param1.readUTF()
		self.trl_gold = param1.readInt()

	def write(self, param1):
		param1.writeDouble(self.trl_date)
		param1.writeUTF(self.trl_id)
		param1.writeInt(self.trl_gold)

class PClanChangeInfo(object):
	__slots__ = ['name', 'description', 'requests_limit', 'requests_min_level']
	def __init__(self, param1 = "", param2 = "", param3 = 0, param4 = 0):
		self.name = param1
		self.description = param2
		self.requests_limit = param3
		self.requests_min_level = param4

	def read(self, param1):
		self.name = param1.readUTF()
		self.description = param1.readUTF()
		self.requests_limit = param1.readUnsignedShort()
		if param1.readUnsignedByte() == 1:
			self.requests_min_level = param1.readUnsignedInt()
		else:
			self.requests_min_level = None

	def write(self, param1):
		param1.writeUTF(self.name)
		param1.writeUTF(self.description)
		param1.writeShort(self.requests_limit)
		if self.requests_min_level != None:
			param1.writeByte(1)
			param1.writeInt(self.requests_min_level)
		else:
			param1.writeByte(0)

class PClanEventBoardObjAction(object):
	CE_ACTION_STASHED = 1
	CE_ACTION_MOVED = 0

	__slots__ = ['ce_obj', 'ce_action_variance']
	def __init__(self, param1 = None, param2 = 0):
		self.ce_obj = param1
		self.ce_action_variance = param2

	def read(self, param1):
		self.ce_obj = PObj()
		self.ce_obj.read(param1)
		self.ce_action_variance = param1.readUnsignedByte()

	def write(self, param1):
		self.ce_obj.write(param1)
		param1.writeByte(self.ce_action_variance)

class PClanExceptions(object):
	CLAN_CANT_ADD_OBJ = 2
	CLAN_NOT_ENOUGH_RESERVE = 1
	CLAN_MAKE_OBJECT_INACTIVE = 0

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.CLAN_MAKE_OBJECT_INACTIVE:
			self.value = param1.readUnsignedInt()
		elif self.variance == self.CLAN_NOT_ENOUGH_RESERVE:
			self.value = a_PWarehouseReserve_a_time()
			self.value.read(param1)
		elif self.variance == self.CLAN_CANT_ADD_OBJ:
			pass
		else:
			raise Exception ("ERROR: incorrect PClanExceptions data")

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.CLAN_MAKE_OBJECT_INACTIVE:
			param1.writeInt(self.value)
		elif self.variance == self.CLAN_NOT_ENOUGH_RESERVE:
			self.value.write(param1)
		elif self.variance == self.CLAN_CANT_ADD_OBJ:
			pass
		else:
			raise Exception ("ERROR: incorrect PClanExceptions data")

class PClanLevelTargetsInfo(object):
	__slots__ = ['clt_level', 'clt_targets', 'clt_prize']
	def __init__(self, param1 = 0, param2 = None, param3 = None):
		self.clt_level = param1
		self.clt_targets = param2
		self.clt_prize = param3

	def read(self, param1):
		self.clt_level = param1.readUnsignedShort()
		self.clt_targets = {}
		for i in xrange(param1.readUnsignedShort()):
			self.clt_targets[i] = PClanTargetLevelGe_str()
			self.clt_targets[i].read(param1)
		self.clt_prize = PQuestPrizes()
		self.clt_prize.read(param1)

	def write(self, param1):
		param1.writeShort(self.clt_level)
		if self.clt_targets == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.clt_targets))
			for i in xrange(len(self.clt_targets)):
				self.clt_targets[i].write(param1)
		self.clt_prize.write(param1)


class PClanTarget(object):
	CT_GET_BOOTH_RECIPE = 16
	CT_OPEN_COFFER = 15
	CT_ATTACK_RAID = 14
	CT_TRANSFER_LEADER_ROLE = 13
	CT_DONATE_COSMOGOLD = 12
	CT_GET_DECOR_POINTS = 11
	CT_ATTACK_MONSTER = 10
	CT_GROW_STONE = 9
	CT_KICK_STONE = 8
	CT_GROW_WOOD = 7
	CT_KICK_WOOD = 6
	CT_PROCESS_BUILDING = 5
	CT_MAKE_RECIPE = 4
	CT_GROW_ANIMAL = 3
	CT_FEED_ANIMAL = 2
	CT_HARVEST_SEEDBED = 1
	CT_PLANT_SEEDBED = 0

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.CT_PLANT_SEEDBED:
			self.value = param1.readUTF()
		elif self.variance == self.CT_HARVEST_SEEDBED:
			self.value = param1.readUTF()
		elif self.variance == self.CT_FEED_ANIMAL:
			self.value = param1.readUTF()
		elif self.variance == self.CT_GROW_ANIMAL:
			self.value = param1.readUTF()
		elif self.variance == self.CT_MAKE_RECIPE:
			self.value = param1.readUTF()
		elif self.variance == self.CT_PROCESS_BUILDING:
			self.value = param1.readUTF()
		elif self.variance == self.CT_KICK_WOOD:
			pass
		elif self.variance == self.CT_GROW_WOOD:
			pass
		elif self.variance == self.CT_KICK_STONE:
			pass
		elif self.variance == self.CT_GROW_STONE:
			pass
		elif self.variance == self.CT_ATTACK_MONSTER:
			pass
		elif self.variance == self.CT_GET_DECOR_POINTS:
			pass
		elif self.variance == self.CT_DONATE_COSMOGOLD:
			pass
		elif self.variance == self.CT_TRANSFER_LEADER_ROLE:
			self.value = param1.readUTF()
		elif self.variance == self.CT_ATTACK_RAID:
			pass
		elif self.variance == self.CT_OPEN_COFFER:
			pass
		elif self.variance == self.CT_GET_BOOTH_RECIPE:
			pass
		else:
			raise Exception ("ERROR: incorrect PClanTarget data")

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.CT_PLANT_SEEDBED:
			param1.writeUTF(self.value)
		elif self.variance == self.CT_HARVEST_SEEDBED:
			param1.writeUTF(self.value)
		elif self.variance == self.CT_FEED_ANIMAL:
			param1.writeUTF(self.value)
		elif self.variance == self.CT_GROW_ANIMAL:
			param1.writeUTF(self.value)
		elif self.variance == self.CT_MAKE_RECIPE:
			param1.writeUTF(self.value)
		elif self.variance == self.CT_PROCESS_BUILDING:
			param1.writeUTF(self.value)
		elif self.variance == self.CT_KICK_WOOD:
			pass
		elif self.variance == self.CT_GROW_WOOD:
			pass
		elif self.variance == self.CT_KICK_STONE:
			pass
		elif self.variance == self.CT_GROW_STONE:
			pass
		elif self.variance == self.CT_ATTACK_MONSTER:
			pass
		elif self.variance == self.CT_GET_DECOR_POINTS:
			pass
		elif self.variance == self.CT_DONATE_COSMOGOLD:
			pass
		elif self.variance == self.CT_TRANSFER_LEADER_ROLE:
			param1.writeUTF(self.value)
		elif self.variance == self.CT_ATTACK_RAID:
			pass
		elif self.variance == self.CT_OPEN_COFFER:
			pass
		elif self.variance == self.CT_GET_BOOTH_RECIPE:
			pass
		else:
			raise Exception ("ERROR: incorrect PClanTarget data")

class PClanTargetLevelGe(object):
	CTGE_GET_BOOTH_RECIPE = 11
	CTGE_BUILDING_FINISH = 10
	CTGE_HAVE_RARES = 9
	CTGE_FINISH_RAID = 8
	CTGE_FAIR_BUILDING_FINISH_STEP = 7
	CTGE_FAIR_BUILDING_COLLECT_BONUS = 6
	CTGE_FAIR_BUILDING_FINISH = 5
	CTGE_FAIR_BUILDING_PUT = 4
	CTL_ADD_TARGETS = 3
	CTL_ADD_MEMBER = 2
	CTL_UPGRADE_BUILDING = 1
	CTL_BUILDING_FINISH = 0

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.CTL_BUILDING_FINISH:
			self.value = param1.readUTF()
		elif self.variance == self.CTL_UPGRADE_BUILDING:
			self.value = str_us()
			self.value.read(param1)
		elif self.variance == self.CTL_ADD_MEMBER:
			pass
		elif self.variance == self.CTL_ADD_TARGETS:
			pass
		elif self.variance == self.CTGE_FAIR_BUILDING_PUT:
			self.value = param1.readUTF()
		elif self.variance == self.CTGE_FAIR_BUILDING_FINISH:
			self.value = param1.readUTF()
		elif self.variance == self.CTGE_FAIR_BUILDING_COLLECT_BONUS:
			self.value = param1.readUTF()
		elif self.variance == self.CTGE_FAIR_BUILDING_FINISH_STEP:
			self.value = str_us()
			self.value.read(param1)
		elif self.variance == self.CTGE_FINISH_RAID:
			self.value = param1.readUTF()
		elif self.variance == self.CTGE_HAVE_RARES:
			self.value = PRare()
			self.value.read(param1)
		elif self.variance == self.CTGE_BUILDING_FINISH:
			self.value = param1.readUTF()
		elif self.variance == self.CTGE_GET_BOOTH_RECIPE:
			pass
		else:
			raise Exception ("ERROR: incorrect PClanTargetLevel data")

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.CTL_BUILDING_FINISH:
			param1.writeUTF(self.value)
		elif self.variance == self.CTL_UPGRADE_BUILDING:
			self.value.write(param1)
		elif self.variance == self.CTL_ADD_MEMBER:
			pass
		elif self.variance == self.CTL_ADD_TARGETS:
			pass
		elif self.variance == self.CTGE_FAIR_BUILDING_PUT:
			param1.writeUTF(self.value)
		elif self.variance == self.CTGE_FAIR_BUILDING_FINISH:
			param1.writeUTF(self.value)
		elif self.variance == self.CTGE_FAIR_BUILDING_COLLECT_BONUS:
			param1.writeUTF(self.value)
		elif self.variance == self.CTGE_FAIR_BUILDING_FINISH_STEP:
			self.value.write(param1)
		elif self.variance == self.CTGE_FINISH_RAID:
			param1.writeUTF(self.value)
		elif self.variance == self.CTGE_HAVE_RARES:
			self.value.write(param1)
		elif self.variance == self.CTGE_BUILDING_FINISH:
			param1.writeUTF(self.value)
		elif self.variance == self.CTGE_GET_BOOTH_RECIPE:
			pass
		else:
			raise Exception ("ERROR: incorrect PClanTargetLevel data")

class PClanTargetLevelGe_str(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = None, param2 = ""):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = PClanTargetLevelGe()
		self.field_0.read(param1)
		self.field_1 = param1.readUTF()

	def write(self, param1):
		self.field_0.write(param1)
		param1.writeUTF(self.field_1)

class PClanTargetChange(object):
	__slots__ = ['target', 'is_new']
	def __init__(self, param1 = None, param2 = False):
		self.target = param1
		self.is_new = param2

	def read(self, param1):
		self.target = PClanTarget()
		self.target.read(param1)
		self.is_new = param1.readBoolean()

	def write(self, param1):
		self.target.write(param1)
		param1.writeBoolean(self.is_new)

class PClanTargetCount(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = None, param2 = 0):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = PClanTarget()
		self.field_0.read(param1)
		self.field_1 = param1.readUnsignedInt()

	def write(self, param1):
		self.field_0.write(param1)
		param1.writeInt(self.field_1)

class str_PClmRole(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = "", param2 = None):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = param1.readUTF()
		self.field_1 = PClmRole()
		self.field_1.read(param1)

	def write(self, param1):
		param1.writeUTF(self.field_0)
		self.field_1.write(param1)

class PReserves(object):
	__slots__ = ['value']
	def __init__(self, param1 = None):
		self.value = param1

	def read(self, param1):
		self.value = {}
		for i in xrange(param1.readUnsignedShort()):
			self.value[i] = PWarehouseReserve()
			self.value[i].read(param1)

	def write(self, param1):
		if self.value == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.value))
			for i in xrange(len(self.value)):
				self.value[i].write(param1)


class str_PUserClanInfo_PSign(object):
	__slots__ = ['field_0', 'field_1', 'field_2']
	def __init__(self, param1 = "", param2 = None, param3 = None):
		self.field_0 = param1
		self.field_1 = param2
		self.field_2 = param3

	def read(self, param1):
		self.field_0 = param1.readUTF()
		self.field_1 = PUserClanInfo()
		self.field_1.read(param1)
		self.field_2 = PSign()
		self.field_2.read(param1)

	def write(self, param1):
		param1.writeUTF(self.field_0)
		self.field_1.write(param1)
		self.field_2.write(param1)

class uint_oPObj_a_PFarmAction_a_PQuestPrizes(object):
	__slots__ = ['field_0', 'field_1', 'field_2', 'field_3']
	def __init__(self, param1 = 0, param2 = None, param3 = None, param4 = None):
		self.field_0 = param1
		self.field_1 = param2
		self.field_2 = param3
		self.field_3 = param4

	def read(self, param1):
		self.field_0 = param1.readUnsignedInt()
		if param1.readUnsignedByte() == 1:
			self.field_1 = PObj()
			self.field_1.read(param1)
		else:
			self.field_1 = None
		self.field_2 = {}
		for i in xrange(param1.readUnsignedShort()):
			self.field_2[i] = PFarmAction()
			self.field_2[i].read(param1)
		self.field_3 = PQuestPrizes()
		self.field_3.read(param1)

	def write(self, param1):
		param1.writeInt(self.field_0)
		if self.field_1 != None:
			param1.writeByte(1)
			self.field_1.write(param1)
		else:
			param1.writeByte(0)
		if self.field_2 == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.field_2))
			for i in xrange(len(self.field_2)):
				self.field_2[i].write(param1)
		self.field_3.write(param1)

class PObj_a_PQuestPrize_a(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = None, param2 = None):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = PObj()
		self.field_0.read(param1)
		self.field_1 = {}
		for i in xrange(param1.readUnsignedShort()):
			self.field_1[i] = PQuestPrize()
			self.field_1[i].read(param1)

	def write(self, param1):
		self.field_0.write(param1)
		if self.field_1 == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.field_1))
			for i in xrange(len(self.field_1)):
				self.field_1[i].write(param1)

class PObj_PQuestPrizes(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = None, param2 = None):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = PObj()
		self.field_0.read(param1)
		self.field_1 = PQuestPrizes()
		self.field_1.read(param1)

	def write(self, param1):
		self.field_0.write(param1)
		self.field_1.write(param1)


class PObj_a_PFarmAction_a_a_PQuestPrize_a(object):
	__slots__ = ['field_0', 'field_1', 'field_2']
	def __init__(self, param1 = None, param2 = None, param3 = None):
		self.field_0 = param1
		self.field_1 = param2
		self.field_2 = param3

	def read(self, param1):
		self.field_0 = PObj()
		self.field_0.read(param1)
		self.field_1 = {}
		for i in xrange(param1.readUnsignedShort()):
			self.field_1[i] = PFarmAction()
			self.field_1[i].read(param1)
		self.field_2 = {}
		for i in xrange(param1.readUnsignedShort()):
			self.field_2[i] = PQuestPrize()
			self.field_2[i].read(param1)

	def write(self, param1):
		self.field_0.write(param1)
		if self.field_1 == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.field_1))
			for i in xrange(len(self.field_1)):
				self.field_1[i].write(param1)
		if self.field_2 == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.field_2))
			for i in xrange(len(self.field_2)):
				self.field_2[i].write(param1)

class PObj_a_PFarmAction_a_PQuestPrizes(object):
	__slots__ = ['field_0', 'field_1', 'field_2']
	def __init__(self, param1 = None, param2 = None, param3 = None):
		self.field_0 = param1
		self.field_1 = param2
		self.field_2 = param3

	def read(self, param1):
		self.field_0 = PObj()
		self.field_0.read(param1)
		self.field_1 = {}
		for i in xrange(param1.readUnsignedShort()):
			self.field_1[i] = PFarmAction()
			self.field_1[i].read(param1)
		self.field_2 = PQuestPrizes()
		self.field_2.read(param1)

	def write(self, param1):
		self.field_0.write(param1)
		if self.field_1 == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.field_1))
			for i in xrange(len(self.field_1)):
				self.field_1[i].write(param1)
		self.field_2.write(param1)

class PUintOfarmAction(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = 0, param2 = None):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = param1.readUnsignedInt()
		if param1.readUnsignedByte() == 1:
			self.field_1 = PFarmAction()
			self.field_1.read(param1)
		else:
			self.field_1 = None

	def write(self, param1):
		param1.writeInt(self.field_0)
		if self.field_1 != None:
			param1.writeByte(1)
			self.field_1.write(param1)
		else:
			param1.writeByte(0)

class PObj_oPFarmAction(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = None, param2 = None):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = PObj()
		self.field_0.read(param1)
		if param1.readUnsignedByte() == 1:
			self.field_1 = PFarmAction()
			self.field_1.read(param1)
		else:
			self.field_1 = None

	def write(self, param1):
		self.field_0.write(param1)
		if self.field_1 != None:
			param1.writeByte(1)
			self.field_1.write(param1)
		else:
			param1.writeByte(0)

class PObj_oPFarmAction_a_PQuestPrize_a(object):
	__slots__ = ['field_0', 'field_1', 'field_2']
	def __init__(self, param1 = None, param2 = None, param3 = None):
		self.field_0 = param1
		self.field_1 = param2
		self.field_2 = param3

	def read(self, param1):
		self.field_0 = PObj()
		self.field_0.read(param1)
		if param1.readUnsignedByte() == 1:
			self.field_1 = PFarmAction()
			self.field_1.read(param1)
		else:
			self.field_1 = None
		self.field_2 = {}
		for i in xrange(param1.readUnsignedShort()):
			self.field_2[i] = PQuestPrize()
			self.field_2[i].read(param1)

	def write(self, param1):
		self.field_0.write(param1)
		if self.field_1 != None:
			param1.writeByte(1)
			self.field_1.write(param1)
		else:
			param1.writeByte(0)
		if self.field_2 == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.field_2))
			for i in xrange(len(self.field_2)):
				self.field_2[i].write(param1)

class PReserves_time(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = None, param2 = 0):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = PReserves()
		self.field_0.read(param1)
		self.field_1 = param1.readDouble()

	def write(self, param1):
		self.field_0.write(param1)
		param1.writeDouble(self.field_1)

class PResMakeRecipe(object):
	__slots__ = ['building_id', 'results', 'recipe_amount']
	def __init__(self, param1 = 0, param2 = None, param3 = None):
		self.building_id = param1
		self.results = param2
		self.recipe_amount = param3

	def read(self, param1):
		self.building_id = param1.readUnsignedInt()
		self.results = {}
		for i in xrange(param1.readUnsignedShort()):
			self.results[i] = PWarehouseReserve()
			self.results[i].read(param1)
		self.recipe_amount = {}
		for i in xrange(param1.readUnsignedShort()):
			self.recipe_amount[i] = str_uint()
			self.recipe_amount[i].read(param1)

	def write(self, param1):
		param1.writeInt(self.building_id)
		if self.results == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.results))
			for i in xrange(len(self.results)):
				self.results[i].write(param1)
		if self.recipe_amount == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.recipe_amount))
			for i in xrange(len(self.recipe_amount)):
				self.recipe_amount[i].write(param1)

class PResDropGarbage_PQuestPrizes(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = None, param2 = None):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = PResDropGarbage()
		self.field_0.read(param1)
		self.field_1 = PQuestPrizes()
		self.field_1.read(param1)

	def write(self, param1):
		self.field_0.write(param1)
		self.field_1.write(param1)

class PResDropStone_PQuestPrizes(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = None, param2 = None):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = PResDropStone()
		self.field_0.read(param1)
		self.field_1 = PQuestPrizes()
		self.field_1.read(param1)

	def write(self, param1):
		self.field_0.write(param1)
		self.field_1.write(param1)

class PResDropWood_PQuestPrizes(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = None, param2 = None):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = PResDropWood()
		self.field_0.read(param1)
		self.field_1 = PQuestPrizes()
		self.field_1.read(param1)

	def write(self, param1):
		self.field_0.write(param1)
		self.field_1.write(param1)

class PResUseFertilizer_a_PQuestPrize_a(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = None, param2 = None):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = PResUseFertilizer()
		self.field_0.read(param1)
		self.field_1 = {}
		for i in xrange(param1.readUnsignedShort()):
			self.field_1[i] = PQuestPrize()
			self.field_1[i].read(param1)

	def write(self, param1):
		self.field_0.write(param1)
		if self.field_1 == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.field_1))
			for i in xrange(len(self.field_1)):
				self.field_1[i].write(param1)

class PGameCurrencyExch(object):
	__slots__ = ['from_cost', 'to_cost', 'icon', 'num']
	def __init__ (self, param1 = None, param2 = None, param3 = "", param4 = 0):
		self.from_cost = param1
		self.to_cost = param2
		self.icon = param3
		self.num = param4

	def read(self, param1):
		self.from_cost = PCost()
		self.from_cost.read(param1)
		self.to_cost = PCost()
		self.to_cost.read(param1)
		self.icon = param1.readUTF()
		self.num = param1.readUnsignedByte()

	def write(self, param1):
		self.from_cost.write(param1)
		self.to_cost.write(param1)
		param1.writeUTF(self.icon)
		param1.writeByte(self.num)

class PChatMember(object):
	__slots__ = ['cm_id', 'cm_name', 'cm_role']
	def __init__ (self, param1 = "", param2 = "", param3 = None):
		self.cm_id = param1
		self.cm_name = param2
		self.cm_role = param3

	def read(self, param1):
		self.cm_id = param1.readUTF()
		self.cm_name = param1.readUTF()
		self.cm_role = PClmRole()
		self.cm_role.read(param1)

	def write(self, param1):
		param1.writeUTF(self.cm_id)
		param1.writeUTF(self.cm_name)
		self.cm_role.write(param1)

class PChatMessage(object):
	__slots__ = ['m_from', 'm_to', 'm_time', 'm_data']
	def __init__ (self, param1 = None, param2 = None, param3 = 0, param4 = ""):
		self.m_from = param1
		self.m_to = param2
		self.m_time = param3
		self.m_data = param4

	def read(self, param1):
		self.m_from = PChatMember()
		self.m_from.read(param1)
		if param1.readUnsignedByte() == 1:
			self.m_to = PChatMember()
			self.m_to.read(param1)
		else:
			self.m_to = None
		self.m_time = param1.readDouble()
		self.m_data = param1.readUTF()

	def write(self, param1):
		self.m_from.write(param1)
		if self.m_to != None:
			param1.writeByte(1)
			self.m_to.write(param1)
		else:
			param1.writeByte(0)
		param1.writeDouble(self.m_time)
		param1.writeUTF(self.m_data)

class PChatSendMessage(object):
	__slots__ = ['sm_to', 'sm_data']
	def __init__(self, param1 = None, param2 = ""):
		self.sm_to = param1
		self.sm_data = param2

	def read(self, param1):
		if param1.readUnsignedByte() == 1:
			self.sm_to = PChatMember()
			self.sm_to.read(param1)
		else:
			self.sm_to = None
		self.sm_data = param1.readUTF()

	def write(self, param1):
		if self.sm_to != None:
			param1.writeByte(1)
			self.sm_to.write(param1)
		else:
			param1.writeByte(0)
		param1.writeUTF(self.sm_data)

class PClanLogAction(object):
	__slots__ = ['date', 'member_id', 'action', 'action_param', 'goodies', 'expenses', 'is_target', 'is_treasury']
	def __init__(self, param1 = 0, param2 = "", param3 = "", param4 = "", param5 = None, param6 = None, param7 = False, param8 = False):
		self.date = param1
		self.member_id = param2
		self.action = param3
		self.action_param = param4
		self.goodies = param5
		self.expenses = param6
		self.is_target = param7
		self.is_treasury = param8

	def read(self, param1):
		self.date = param1.readDouble()
		self.member_id = param1.readUTF()
		self.action = param1.readUTF()
		self.action_param = param1.readUTF()
		self.goodies = {}
		for i in xrange(param1.readUnsignedShort()):
			self.goodies[i] = PQuestPrize()
			self.goodies[i].read(param1)
		self.expenses = {}
		for i in xrange(param1.readUnsignedShort()):
			self.expenses[i] = PQuestPrize()
			self.expenses[i].read(param1)
		self.is_target = param1.readBoolean()
		self.is_treasury = param1.readBoolean()

	def write(self, param1):
		param1.writeDouble(self.date)
		param1.writeUTF(self.member_id)
		param1.writeUTF(self.action)
		param1.writeUTF(self.action_param)
		if self.goodies == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.goodies))
			for i in xrange(len(self.goodies)):
				self.goodies[i].write(param1)
		if self.expenses == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.expenses))
			for i in xrange(len(self.expenses)):
				self.expenses[i].write(param1)
		param1.writeBoolean(self.is_target)
		param1.writeBoolean(self.is_treasury)

class POffer(object):
	__slots__ = ['o_kind', 'o_offer_item', 'o_finish_time', 'o_is_reminder']
	def __init__(self, param1 = None, param2 = {}, param3 = 0, param4 = False):
		self.o_kind = param1
		self.o_offer_item = param2
		self.o_finish_time = param3
		self.o_is_reminder = param4

	def read(self, param1):
		self.o_kind = POfferKind()
		self.o_kind.read(param1)
		self.o_offer_item = {}
		for i in xrange(param1.readUnsignedShort()):
			self.o_offer_item[i] = POfferItem()
			self.o_offer_item[i].read(param1)
		self.o_finish_time = param1.readDouble()
		self.o_is_reminder = param1.readBoolean()

	def write(self, param1):
		self.o_kind.write(param1)
		if self.o_offer_item == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.o_offer_item))
			for i in xrange(len(self.o_offer_item)):
				self.o_offer_item[i].write(param1)
		param1.writeDouble(self.o_finish_time)
		param1.writeBoolean(self.o_is_reminder)

class POfferAction(object):
	FERTILIZE = 7
	BUILDING_LEVEL_UP = 6
	EXEC_STUDY = 5
	UNLOCK_EXPANSION = 4
	FEED_PET = 3
	FEED_ANIMALS = 2
	GATHER_PLANTS = 1
	FINISH_BUILDING = 0

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.FINISH_BUILDING:
			self.value = param1.readInt()
		elif self.variance == self.GATHER_PLANTS:
			if param1.readUnsignedByte() == 1:
				self.value = param1.readInt()
			else:
				self.value = None
		elif self.variance == self.FEED_ANIMALS:
			if param1.readUnsignedByte() == 1:
				self.value = param1.readInt()
			else:
				self.value = None
		elif self.variance == self.FEED_PET:
			self.value = param1.readInt()
		elif self.variance == self.UNLOCK_EXPANSION:
			self.value = i_i()
			self.value.read(param1)
		elif self.variance == self.EXEC_STUDY:
			self.value = PMyStudy()
			self.value.read(param1)
		elif self.variance == self.BUILDING_LEVEL_UP:
			self.value = i_i()
			self.value.read(param1)
		elif self.variance == self.FERTILIZE:
			self.value = str_i()
			self.value.read(param1)
		else:
			raise Exception ("ERROR: incorrect POfferAction data")

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.FINISH_BUILDING:
			param1.writeInt(self.value)
		elif self.variance == self.GATHER_PLANTS:
			if self.value != None:
				param1.writeByte(1)
				param1.writeInt(self.value)
			else:
				param1.writeByte(0)
		elif self.variance == self.FEED_ANIMALS:
			if self.value != None:
				param1.writeByte(1)
				param1.writeInt(self.value)
			else:
				param1.writeByte(0)
		elif self.variance == self.FEED_PET:
			param1.writeInt(self.value)
		elif self.variance == self.UNLOCK_EXPANSION:
			self.value.write(param1)
		elif self.variance == self.EXEC_STUDY:
			self.value.write(param1)
		elif self.variance == self.BUILDING_LEVEL_UP:
			self.value.write(param1)
		elif self.variance == self.FERTILIZE:
			self.value.write(param1)
		else:
			raise Exception ("ERROR: incorrect POfferAction data")

class POfferItemKind(object):
	ACTION = 2
	SHOP = 1
	RESOURCE = 0

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.RESOURCE:
			self.value = PCost_bool()
			self.value.read(param1)
		elif self.variance == self.SHOP:
			self.value = str_i()
			self.value.read(param1)
		elif self.variance == self.ACTION:
			self.value = POfferAction()
			self.value.read(param1)
		else:
			raise Exception ("ERROR: incorrect POfferAction data")

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.RESOURCE:
			self.value.write(param1)
		elif self.variance == self.SHOP:
			self.value.write(param1)
		elif self.variance == self.ACTION:
			self.value.write(param1)
		else:
			raise Exception ("ERROR: incorrect POfferAction data")

class PProlongOffer(object):
	__slots__ = ['po_id', 'po_period', 'po_price']
	def __init__(self, param1 = "", param2 = 0, param3 = None):
		self.po_id = param1
		self.po_period = param2
		self.po_price = param3

	def read(self, param1):
		self.po_id = param1.readUTF()
		self.po_period = param1.readDouble()
		self.po_price = PCost()
		self.po_price.read(param1)

	def write(self, param1):
		param1.writeUTF(self.po_id)
		param1.writeDouble(self.po_period)
		self.po_price.write(param1)

class PPayHistDetail(object):
	__slots__ = ['phd_action', 'phd_kind', 'phd_cost']
	def __init__(self, param1 = "", param2 = "", param3 = None):
		self.phd_action = param1
		self.phd_kind = param2
		self.phd_cost = param3

	def read(self, param1):
		self.phd_action = param1.readUTF()
		self.phd_kind = param1.readUTF()
		self.phd_cost = PCost()
		self.phd_cost.read(param1)

	def write(self, param1):
		param1.writeUTF(self.phd_action)
		param1.writeUTF(self.phd_kind)
		self.phd_cost.write(param1)

class PClanDict(object):
	__slots__ = ['shop', 'recipes_info', 'comfort_levels', 'levels_targets_info', 'sign_invite', 'clan_fair_buildings_info', 'game_event_targets']
	def __init__(self, param1 = None, param2 = None, param3 = None, param4 = None, param5 = None, param6 = None, param7 = None):
		self.shop = param1
		self.recipes_info = param2
		self.comfort_levels = param3
		self.levels_targets_info = param4
		self.sign_invite = param5
		self.clan_fair_buildings_info = param6
		self.game_event_targets = param7

	def read(self, param1):
		self.shop = {}
		for i in xrange(param1.readUnsignedShort()):
			self.shop[i] = PShopObj()
			self.shop[i].read(param1)
		self.recipes_info = {}
		for i in xrange(param1.readUnsignedShort()):
			self.recipes_info[i] = PRecipeInfo()
			self.recipes_info[i].read(param1)
		self.comfort_levels = {}
		for i in xrange(param1.readUnsignedShort()):
			self.comfort_levels[i] = PDcComfortLevel()
			self.comfort_levels[i].read(param1)
		self.levels_targets_info = {}
		for i in xrange(param1.readUnsignedShort()):
			self.levels_targets_info[i] = PClanLevelTargetsInfo()
			self.levels_targets_info[i].read(param1)
		self.sign_invite = PSign()
		self.sign_invite.read(param1)
		self.clan_fair_buildings_info = {}
		for i in xrange(param1.readUnsignedShort()):
			self.clan_fair_buildings_info[i] = PClanFairBuildingsInfo()
			self.clan_fair_buildings_info[i].read(param1)
		self.game_event_targets = {}
		for i in xrange(param1.readUnsignedShort()):
			self.game_event_targets[i] = i_a_t_PClanTargetLevelGe_str_t_a()
			self.game_event_targets[i].read(param1)

	def write(self, param1):
		if self.shop == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.shop))
			for i in xrange(len(self.shop)):
				self.shop[i].write(param1)
		if self.recipes_info == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.recipes_info))
			for i in xrange(len(self.recipes_info)):
				self.recipes_info[i].write(param1)
		if self.comfort_levels == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.comfort_levels))
			for i in xrange(len(self.comfort_levels)):
				self.comfort_levels[i].write(param1)
		if self.levels_targets_info == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.levels_targets_info))
			for i in xrange(len(self.levels_targets_info)):
				self.levels_targets_info[i].write(param1)
		self.sign_invite.write(param1)
		if self.clan_fair_buildings_info == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.clan_fair_buildings_info))
			for i in xrange(len(self.clan_fair_buildings_info)):
				self.clan_fair_buildings_info[i].write(param1)
		if self.game_event_targets == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.game_event_targets))
			for i in xrange(len(self.game_event_targets)):
				self.game_event_targets[i].write(param1)

class PClanApprovedEvent(object):
	__slots__ = ['level', 'targets']
	def __init__(self, param1 = 0, param2 = None):
		self.level = param1
		self.targets = param2

	def read(self, param1):
		self.level = param1.readInt()
		self.targets = {}
		for i in xrange(param1.readUnsignedShort()):
			self.targets[i] = PClanTargetLevelGe()
			self.targets[i].read(param1)

	def write(self, param1):
		param1.writeInt(self.level)
		if self.targets == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.targets))
			for i in xrange(len(self.targets)):
				self.targets[i].write(param1)

class PClanFairBuilding(object):
	__slots__ = ['progress', 'can_collect_bonus']
	def __init__(self, param1 = 0, param2 = False):
		self.progress = param1
		self.can_collect_bonus = param2

	def read(self, param1):
		self.progress = param1.readInt()
		self.can_collect_bonus = param1.readBoolean()

	def write(self, param1):
		param1.writeInt(self.progress)
		param1.writeBoolean(self.can_collect_bonus)

class PClanFairBuildingsInfo(object):
	__slots__ = ['cfbl_kind', 'cfbl_price_capacity', 'cfbl_level_capacity', 'cfbl_coefficient_capacity', 'cfbl_level_comfort', 'cfbl_bonus_period', 'cfbl_bonus', 'cfbl_game_event_id', 'cfbl_bonus_period_speedup', 'cfbl_price', 'cfbl_purchase_start', 'cfbl_purchase_finish']
	def __init__(self, param1 = "", param2 = None, param3 = None, param4 = 0, param5 = None, param6 = 0, param7 = None, param8 = 0, param9 = 0, param10 = None, param11 = 0, param12 = 0):
		self.cfbl_kind = param1
		self.cfbl_price_capacity = param2
		self.cfbl_level_capacity = param3
		self.cfbl_coefficient_capacity = param4
		self.cfbl_level_comfort = param5
		self.cfbl_bonus_period = param6
		self.cfbl_bonus = param7
		self.cfbl_game_event_id = param8
		self.cfbl_bonus_period_speedup = param9
		self.cfbl_price = param10
		self.cfbl_purchase_start = param11
		self.cfbl_purchase_finish = param12

	def read(self, param1):
		self.cfbl_kind = param1.readUTF()
		self.cfbl_price_capacity = PCost()
		self.cfbl_price_capacity.read(param1)
		self.cfbl_level_capacity = {}
		for i in xrange(param1.readUnsignedShort()):
			self.cfbl_level_capacity[i] = param1.readInt()
		self.cfbl_coefficient_capacity = param1.readDouble()
		self.cfbl_level_comfort = {}
		for i in xrange(param1.readUnsignedShort()):
			self.cfbl_level_comfort[i] = param1.readInt()
		self.cfbl_bonus_period = param1.readDouble()
		self.cfbl_bonus = PCost()
		self.cfbl_bonus.read(param1)
		self.cfbl_game_event_id = param1.readInt()
		self.cfbl_bonus_period_speedup = param1.readDouble()
		self.cfbl_price = PCost()
		self.cfbl_price.read(param1)
		self.cfbl_purchase_start = param1.readDouble()
		self.cfbl_purchase_finish = param1.readDouble()

	def write(self, param1):
		param1.writeUTF(self.cfbl_kind)
		self.cfbl_price_capacity.write(param1)
		if self.cfbl_level_capacity == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.cfbl_level_capacity))
			for i in xrange(len(self.cfbl_level_capacity)):
				param1.writeInt(self.cfbl_level_capacity[i])
		param1.writeDouble(self.cfbl_coefficient_capacity)
		if self.cfbl_level_comfort == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.cfbl_level_comfort))
			for i in xrange(len(self.cfbl_level_comfort)):
				param1.writeInt(self.cfbl_level_comfort[i])
		param1.writeDouble(self.cfbl_bonus_period)
		self.cfbl_bonus.write(param1)
		param1.writeInt(self.cfbl_game_event_id)
		param1.writeDouble(self.cfbl_bonus_period_speedup)
		self.cfbl_price.write(param1)
		param1.writeDouble(self.cfbl_purchase_start)
		param1.writeDouble(self.cfbl_purchase_finish)

class PClanTopEvent(object):
	__slots__ = ['cl_id', 'cl_name', 'cl_emblem', 'cl_level', 'cl_rating', 'cl_place']
	def __init__(self, param1 = "", param2 = "", param3 = "", param4 = 0, param5 = 0, param6 = 0):
		self.cl_id = param1
		self.cl_name = param2
		self.cl_emblem = param3
		self.cl_level = param4
		self.cl_rating = param5
		self.cl_place = param6

	def read(self, param1):
		self.cl_id = param1.readUTF()
		self.cl_name = param1.readUTF()
		self.cl_emblem = param1.readUTF()
		self.cl_level = param1.readUnsignedShort()
		self.cl_rating = param1.readUnsignedInt()
		self.cl_place = param1.readUnsignedInt()

	def write(self, param1):
		param1.writeUTF(self.cl_id)
		param1.writeUTF(self.cl_name)
		param1.writeUTF(self.cl_emblem)
		param1.writeShort(self.cl_level)
		param1.writeInt(self.cl_rating)
		param1.writeInt(self.cl_place)

class PNewsButton(object):
	CLAN = 3
	OK = 2
	CASE = 1
	DECOR = 0

	__slots__ = ['variance']
	def __init__(self, param1 = 0):
		self.variance = param1

	def read(self, param1):
		self.variance = param1.readUnsignedByte()

	def write(self, param1):
		param1.writeByte(self.variance)

class PProcessClanFairBuilding(object):
	__slots__ = ['id', 'cnt']
	def __init__(self, param1 = 0, param2 = 0):
		self.id = param1
		self.cnt = param2

	def read(self, param1):
		self.id = param1.readUnsignedInt()
		self.cnt = param1.readUnsignedInt()

	def write(self, param1):
		param1.writeInt(self.id)
		param1.writeInt(self.cnt)

class PWarehouseClanFair(object):
	__slots__ = ['kind', 'progress', 'count']
	def __init__(self, param1 = "", param2 = 0, param3 = 0):
		self.kind = param1
		self.progress = param2
		self.count = param3

	def read(self, param1):
		self.kind = param1.readUTF()
		self.progress = param1.readUnsignedInt()
		self.count = param1.readUnsignedInt()

	def write(self, param1):
		param1.writeUTF(self.kind)
		param1.writeInt(self.progress)
		param1.writeInt(self.count)

class str_PCost(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = "", param2 = None):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = param1.readUTF()
		self.field_1 = PCost()
		self.field_1.read(param1)

	def write(self, param1):
		param1.writeUTF(self.field_0)
		self.field_1.write(param1)

class PWarehouseClanFair_time(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = None, param2 = 0):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = PWarehouseClanFair()
		self.field_0.read(param1)
		self.field_1 = param1.readDouble()

	def write(self, param1):
		self.field_0.write(param1)
		param1.writeDouble(self.field_1)

class PAcceptGift(object):
	__slots__ = ['gift_id', 'result', 'request_id']
	def __init__(self, param1 = 0, param2 = None, param3 = ""):
		self.gift_id = param1
		self.result = param2
		self.request_id = param3

	def read(self, param1):
		self.gift_id = param1.readUnsignedInt()
		self.result = PAcceptGiftRes()
		self.result.read(param1)
		self.request_id = param1.readUTF()

	def write(self, param1):
		param1.writeInt(self.gift_id)
		self.result.write(param1)
		param1.writeUTF(self.request_id)

class PAcceptGiftRes(object):
	NOT_FOUND_GIFT_ID = 8
	EXP = 7
	BAKS = 6
	ENERGY = 5
	GOLD = 4
	FOOD = 3
	TIMBER = 2
	ELT_COLLECTION = 1
	RESERVE = 0

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.RESERVE:
			self.value = PWarehouseReserve()
			self.value.read(param1)
		elif self.variance == self.ELT_COLLECTION:
			self.value = PEltCollection()
			self.value.read(param1)
		elif self.variance == self.TIMBER:
			self.value = param1.readInt()
		elif self.variance == self.FOOD:
			self.value = param1.readInt()
		elif self.variance == self.GOLD:
			self.value = param1.readInt()
		elif self.variance == self.ENERGY:
			self.value = param1.readInt()
		elif self.variance == self.BAKS:
			self.value = param1.readInt()
		elif self.variance == self.EXP:
			self.value = param1.readInt()
		elif self.variance == self.NOT_FOUND_GIFT_ID:
			pass
		else:
			raise Exception ("ERROR: incorrect PAcceptGiftRes data")

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.RESERVE:
			self.value.write(param1)
		elif self.variance == self.ELT_COLLECTION:
			self.value.write(param1)
		elif self.variance == self.TIMBER:
			param1.writeInt(self.value)
		elif self.variance == self.FOOD:
			param1.writeInt(self.value)
		elif self.variance == self.GOLD:
			param1.writeInt(self.value)
		elif self.variance == self.ENERGY:
			param1.writeInt(self.value)
		elif self.variance == self.BAKS:
			param1.writeInt(self.value)
		elif self.variance == self.EXP:
			param1.writeInt(self.value)
		elif self.variance == self.NOT_FOUND_GIFT_ID:
			pass
		else:
			raise Exception ("ERROR: incorrect PAcceptGiftRes data")

class PSendGift(object):
	__slots__ = ['sndg_gift_kind', 'sndg_gift_cnt', 'sndg_friend_id', 'sndg_is_free', 'sndg_request_id']
	def __init__(self, param1 = "", param2 = 0, param3 = "", param4 = True, param5 = ""):
		self.sndg_gift_kind = param1
		self.sndg_gift_cnt = param2
		self.sndg_friend_id = param3
		self.sndg_is_free = param4
		self.sndg_request_id = param5

	def read(self, param1):
		self.sndg_gift_kind = param1.readUTF()
		self.sndg_gift_cnt = param1.readInt()
		self.sndg_friend_id = param1.readUTF()
		self.sndg_is_free = param1.readBoolean()
		self.sndg_request_id = param1.readUTF()

	def write(self, param1):
		param1.writeUTF(self.sndg_gift_kind)
		param1.writeInt(self.sndg_gift_cnt)
		param1.writeUTF(self.sndg_friend_id)
		param1.writeBoolean(self.sndg_is_free)
		param1.writeUTF(self.sndg_request_id)

class PSendGiftRes(object):
	GIFT_SEND_ERROR = 5
	ALREADY_SENDED = 4
	SIGN = 3
	GAME_EVENT_FINISHED = 2
	CAN_SEND_ONLY = 1
	OK = 0

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.OK:
			self.value = i_bool()
			self.value.read(param1)
		elif self.variance == self.CAN_SEND_ONLY:
			self.value = param1.readInt()
		elif self.variance == self.GAME_EVENT_FINISHED:
			pass
		elif self.variance == self.SIGN:
			self.value = PSign()
			self.value.read(param1)
		elif self.variance == self.ALREADY_SENDED:
			pass
		elif self.variance == self.GIFT_SEND_ERROR:
			self.value = param1.readUTF()
		else:
			raise Exception ("ERROR: incorrect PSendGiftRes data")

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.OK:
			self.value.write(param1)
		elif self.variance == self.CAN_SEND_ONLY:
			param1.writeInt(self.value)
		elif self.variance == self.GAME_EVENT_FINISHED:
			pass
		elif self.variance == self.SIGN:
			self.value.write(param1)
		elif self.variance == self.ALREADY_SENDED:
			pass
		elif self.variance == self.GIFT_SEND_ERROR:
			param1.writeUTF(self.value)
		else:
			raise Exception ("ERROR: incorrect PSendGiftRes data")

class PSendGiftUser(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = None, param2 = ""):
		self.field_0 = param1
		self.field_1 = param2 #user_id

	def read(self, param1):
		self.field_0 = PSendGiftRes()
		self.field_0.read(param1)
		self.field_1 = param1.readUTF()

	def write(self, param1):
		self.field_0.write(param1)
		param1.writeUTF(self.field_1)

class PClanBonusData(object):
	__slots__ = ['cb_level', 'cb_time', 'cb_prize']
	def __init__(self, param1 = 0, param2 = 0, param3 = None):
		self.cb_level = param1
		self.cb_time = param2
		self.cb_prize = param3

	def read(self, param1):
		self.cb_level = param1.readUnsignedByte()
		self.cb_time = param1.readDouble()
		self.cb_prize = {}
		for i in xrange(param1.readUnsignedShort()):
			self.cb_prize[i] = PQuestPrize()
			self.cb_prize[i].read(param1)

	def write(self, param1):
		param1.writeByte(self.cb_level)
		param1.writeDouble(self.cb_time)
		if self.cb_prize == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.cb_prize))
			for i in xrange(len(self.cb_prize)):
				self.cb_prize[i].write(param1)

class PClanBonusData_uint(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = None, param2 = 0):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = PClanBonusData()
		self.field_0.read(param1)
		self.field_1 = param1.readUnsignedInt()

	def write(self, param1):
		self.field_0.write(param1)
		param1.writeInt(self.field_1)

class PClanBonusInfo(object):
	__slots__ = ['cb_level', 'cb_expense', 'cb_time', 'cb_timeout', 'cb_prize']
	def __init__(self, param1 = 0, param2 = None, param3 = 0, param4 = 0, param5 = None):
		self.cb_level = param1
		self.cb_expense = param2
		self.cb_time = param3
		self.cb_timeout = param4
		self.cb_prize = param5

	def read(self, param1):
		self.cb_level = param1.readUnsignedByte()
		self.cb_expense = PCost()
		self.cb_expense.read(param1)
		self.cb_time = param1.readDouble()
		self.cb_timeout = param1.readDouble()
		self.cb_prize = {}
		for i in xrange(param1.readUnsignedShort()):
			self.cb_prize[i] = {}
			for j in xrange(param1.readUnsignedShort()):
				self.cb_prize[i][j] = PQuestPrize()
				self.cb_prize[i][j].read(param1)

	def write(self, param1):
		param1.writeByte(self.cb_level)
		self.cb_expense.write(param1)
		param1.writeDouble(self.cb_time)
		param1.writeDouble(self.cb_timeout)
		if self.cb_prize == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.cb_prize))
			for i in xrange(len(self.cb_prize)):
				if self.cb_prize[i] == {}:
					param1.writeShort(0)
				else:
					param1.writeShort(len(self.cb_prize[i]))
					for j in xrange(len(self.cb_prize[i])):
						self.cb_prize[i][j].write(param1)

class PClanBonusPrize(object):
	__slots__ = ['level', 'prize']
	def __init__(self, param1 = 0, param2 = None):
		self.level = param1
		self.prize = param2

	def read(self, param1):
		self.level = param1.readUnsignedByte()
		self.prize = {}
		for i in xrange(param1.readUnsignedShort()):
			self.prize[i] = PQuestPrize()
			self.prize[i].read(param1)

	def write(self, param1):
		param1.writeByte(self.level)
		if self.prize == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.prize))
			for i in xrange(len(self.prize)):
				self.prize[i].write(param1)

class PClanBonusState(object):
	START_WAIT = 1
	START = 0

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.START:
			self.value = PClanBonusData_uint()
			self.value.read(param1)
		elif self.variance == self.START_WAIT:
			self.value = PClanBonusData()
			self.value.read(param1)
		else:
			raise Exception ("ERROR: incorrect PClanBonusState data")

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.START:
			self.value.write(param1)
		elif self.variance == self.START_WAIT:
			self.value.write(param1)
		else:
			raise Exception ("ERROR: incorrect PClanBonusState data")


class PContract(object):
	__slots__ = ['contract_reserv', 'contract_difficulty', 'contract_cost', 'contract_status', 'contract_time_perf', 'contract_event_id', 'contract_prizes', 'contract_reserv', 'contract_reserv', 'contract_difficulty', 'contract_difficulty', 'contract_cost', 'contract_cost', 'contract_status', 'contract_status', 'contract_time_perf', 'contract_event_id', 'contract_prizes', 'contract_prizes', 'contract_reserv', 'contract_difficulty', 'contract_cost', 'contract_status', 'contract_time_perf', 'contract_event_id', 'contract_prizes']
	def __init__(self, param1 = None, param2 = None, param3 = None, param4 = None, param5 = 0, param6 = 0, param7 = None):
		self.contract_reserv = param1
		self.contract_difficulty = param2
		self.contract_cost = param3
		self.contract_status = param4
		self.contract_time_perf = param5
		self.contract_event_id = param6
		self.contract_prizes = param7

	def read(self, param1):
		self.contract_reserv = PWarehouseReserve()
		self.contract_reserv.read(param1)
		self.contract_difficulty = PContractDifficulty()
		self.contract_difficulty.read(param1)
		self.contract_cost = PCost()
		self.contract_cost.read(param1)
		self.contract_status = PContractStatus()
		self.contract_status.read(param1)
		self.contract_time_perf = param1.readDouble()
		self.contract_event_id = param1.readInt()
		self.contract_prizes = PQuestPrizes()
		self.contract_prizes.read(param1)

	def write(self, param1):
		self.contract_reserv.write(param1)
		self.contract_difficulty.write(param1)
		self.contract_cost.write(param1)
		self.contract_status.write(param1)
		param1.writeDouble(self.contract_time_perf)
		param1.writeInt(self.contract_event_id)
		self.contract_prizes.write(param1)

class PContractDifficulty(object):
	HARD = 2
	NORMAL = 1
	SIMPLE = 0

	__slots__ = ['variance', 'variance', 'variance']
	def __init__(self, param1 = 0):
		self.variance = param1

	def read(self, param1):
		self.variance = param1.readUnsignedByte()

	def write(self, param1):
		param1.writeByte(self.variance)

class PContractRatingPrizeInfo(object):
	__slots__ = ['crpi_min_place', 'crpi_max_place', 'crpi_reserves', 'crpi_min_place', 'crpi_max_place', 'crpi_reserves', 'crpi_reserves', 'crpi_reserves', 'crpi_min_place', 'crpi_max_place', 'crpi_reserves', 'crpi_reserves', 'crpi_reserves', 'crpi_reserves']
	def __init__(self, param1 = 0, param2 = 0, param3 = None):
		self.crpi_min_place = param1
		self.crpi_max_place = param2
		self.crpi_reserves = param3

	def read(self, param1):
		self.crpi_min_place = param1.readInt()
		self.crpi_max_place = param1.readInt()
		self.crpi_reserves = {}
		for i in xrange(param1.readUnsignedShort()):
			self.crpi_reserves[i] = PWarehouseReserve()
			self.crpi_reserves[i].read(param1)

	def write(self, param1):
		param1.writeInt(self.crpi_min_place)
		param1.writeInt(self.crpi_max_place)
		if self.crpi_reserves == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.crpi_reserves))
			for i in xrange(len(self.crpi_reserves)):
				self.crpi_reserves[i].write(param1)

class PContractRprize(object):
	__slots__ = ['field_0', 'field_1', 'field_0', 'field_1', 'field_1', 'field_1', 'field_0', 'field_1', 'field_1', 'field_1', 'field_1']
	def __init__(self, param1 = 0, param2 = None):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = param1.readInt()
		self.field_1 = {}
		for i in xrange(param1.readUnsignedShort()):
			self.field_1[i] = PWarehouseReserve()
			self.field_1[i].read(param1)

	def write(self, param1):
		param1.writeInt(self.field_0)
		if self.field_1 == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.field_1))
			for i in xrange(len(self.field_1)):
				self.field_1[i].write(param1)

class PContractStatus(object):
	COOLDOWN_FINISH = 2
	COOLDOWN_REFUSED = 1
	WAIT = 0

	__slots__ = ['variance', 'value', 'variance', 'variance', 'WAIT', 'value', 'variance', 'COOLDOWN_REFUSED', 'value', 'variance', 'COOLDOWN_FINISH', 'value', 'variance', 'variance', 'WAIT', 'value', 'variance', 'COOLDOWN_REFUSED', 'value', 'variance', 'COOLDOWN_FINISH', 'value']
	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.WAIT:
			self.value = param1.readDouble()
		elif self.variance == self.COOLDOWN_REFUSED:
			self.value = param1.readDouble()
		elif self.variance == self.COOLDOWN_FINISH:
			self.value = param1.readDouble()
		else:
			raise Exception ("ERROR: incorrect PContractStatus data")

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.WAIT:
			param1.writeDouble(self.value)
		elif self.variance == self.COOLDOWN_REFUSED:
			param1.writeDouble(self.value)
		elif self.variance == self.COOLDOWN_FINISH:
			param1.writeDouble(self.value)
		else:
			raise Exception ("ERROR: incorrect PContractStatus data")

class PRatingLine(object):
	__slots__ = ['rl_id', 'rl_name', 'rl_avatar', 'rl_score', 'rl_level', 'rl_id', 'rl_name', 'rl_avatar', 'rl_score', 'rl_level', 'rl_id', 'rl_name', 'rl_avatar', 'rl_score', 'rl_level']
	def __init__(self, param1 = "", param2 = "", param3 = "", param4 = 0, param5 = 0):
		self.rl_id = param1
		self.rl_name = param2
		self.rl_avatar = param3
		self.rl_score = param4
		self.rl_level = param5

	def read(self, param1):
		self.rl_id = param1.readUTF()
		self.rl_name = param1.readUTF()
		self.rl_avatar = param1.readUTF()
		self.rl_score = param1.readInt()
		self.rl_level = param1.readInt()

	def write(self, param1):
		param1.writeUTF(self.rl_id)
		param1.writeUTF(self.rl_name)
		param1.writeUTF(self.rl_avatar)
		param1.writeInt(self.rl_score)
		param1.writeInt(self.rl_level)


class PReqContract(object):
	RESET_COOLDOWN = 4
	GET_CONTRACT = 3
	FINISH_CONTRACT_FOR_MONEY = 2
	FINISH_CONTRACT = 1
	REFUSING_CONTRACT = 0

	__slots__ = ['variance', 'value', 'variance', 'variance', 'REFUSING_CONTRACT', 'value', 'value', 'variance', 'FINISH_CONTRACT', 'value', 'value', 'variance', 'FINISH_CONTRACT_FOR_MONEY', 'value', 'value', 'variance', 'GET_CONTRACT', 'value', 'value', 'variance', 'RESET_COOLDOWN', 'value', 'value', 'variance', 'variance', 'variance', 'REFUSING_CONTRACT', 'value', 'variance', 'FINISH_CONTRACT', 'value', 'variance', 'FINISH_CONTRACT_FOR_MONEY', 'value', 'variance', 'GET_CONTRACT', 'value', 'variance', 'RESET_COOLDOWN', 'value']
	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.REFUSING_CONTRACT:
			self.value = PContractDifficulty()
			self.value.read(param1)
		elif self.variance == self.FINISH_CONTRACT:
			self.value = PContractDifficulty()
			self.value.read(param1)
		elif self.variance == self.FINISH_CONTRACT_FOR_MONEY:
			self.value = PContractDifficulty()
			self.value.read(param1)
		elif self.variance == self.GET_CONTRACT:
			self.value = PContractDifficulty()
			self.value.read(param1)
		elif self.variance == self.RESET_COOLDOWN:
			self.value = PContractDifficulty()
			self.value.read(param1)
		else:
			raise Exception ("ERROR: incorrect PReqContract data")

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.REFUSING_CONTRACT:
			self.value.write(param1)
		elif self.variance == self.FINISH_CONTRACT:
			self.value.write(param1)
		elif self.variance == self.FINISH_CONTRACT_FOR_MONEY:
			self.value.write(param1)
		elif self.variance == self.GET_CONTRACT:
			self.value.write(param1)
		elif self.variance == self.RESET_COOLDOWN:
			self.value.write(param1)
		else:
			raise Exception ("ERROR: incorrect PReqContract data")

class PBarnActionResult(object):
	__slots__ = ['farm_action', 'objs', 'actions', 'remain_plants']
	def __init__(self, param1 = None, param2 = None, param3 = None, param4 = None):
		self.objs = param1
		self.actions = param2
		self.remain_plants = param3
		self.ids = param3

	def write(self, param1):
		if self.objs == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.objs))
			for i in xrange(len(self.objs)):
				self.objs[i].write(param1)
		if self.actions == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.actions))
			for i in xrange(len(self.actions)):
				if self.actions[i] == {}:
					param1.writeShort(0)
				else:
					param1.writeShort(len(self.actions[i]))
					for j in xrange(len(self.actions[i])):
						self.actions[i][j].write(param1)
		if self.remain_plants == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.remain_plants))
			for i in xrange(len(self.remain_plants)):
				self.remain_plants[i].write(param1)
		if self.ids == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.ids))
			for i in xrange(len(self.ids)):
				param1.writeInt(self.ids[i]);

	def read(self,param1):
		self.objs = {}
		for i in xrange(param1.readUnsignedShort()):
			self.objs[i] = PObj()
			self.objs[i].read(param1)
		self.actions = {}
		for i in xrange(param1.readUnsignedShort()):
			self.actions[i] = {}
			for j in xrange(param1.readUnsignedShort()):
				self.actions[i][j] = PFarmAction()
				self.actions[i][j].read(param1)
		self.remain_plants = {}
		for i in xrange(param1.readUnsignedShort()):
			self.remain_plants[i] = PPlant()
			self.remain_plants[i].read(param1)
		self.ids = {}
		for i in xrange(param1.readUnsignedShort()):
			self.ids[i] = param1.readUnsignedInt()

class PResBuySegway(object):
	__slots__ = ['kind', 'segways', 'seg_faction']
	def __init__(self, param1 = "", param2 = None, param3 = None):
		self.kind = param1
		self.segways = param2
		self.seg_faction = param3

	def read(self, param1):
		self.kind = param1.readUTF()
		self.segways = {}
		for i in xrange(param1.readUnsignedShort()):
			self.segways[i] = PSegwayBuy()
			self.segways[i].read(param1)
		if param1.readUnsignedByte() == 1:
			self.seg_faction = PFarmAction()
			self.seg_faction.read(param1)
		else:
			self.seg_faction = None

	def write(self, param1):
		param1.writeUTF(self.kind)
		if self.segways == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.segways))
			for i in xrange(len(self.segways)):
				self.segways[i].write(param1)
		if self.seg_faction != None:
			param1.writeByte(1)
			self.seg_faction.write(param1)
		else:
			param1.writeByte(0)

class PResBuyOffer(object):
	B_ORESOURCE = 9
	B_OFERTILIZE = 8
	B_OBUILDINGS_LEVEL_UP = 7
	B_OEXEC_STUDY = 6
	B_OEXPANSION = 5
	B_OFEED_PET = 4
	B_OBUY_SEGWAY = 3
	B_OBUILDING_BONUS_COLLECTEDAOF = 2
	B_OFINISH_BUILDING = 1
	B_OWAREHOUSE = 0

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.B_OWAREHOUSE:
			self.value = PWarehouseReserve()
			self.value.read(param1)
		elif self.variance == self.B_OFINISH_BUILDING:
			self.value = PResFinishBuilding()
			self.value.read(param1)
		elif self.variance == self.B_OBUILDING_BONUS_COLLECTEDAOF:
			self.value = PCollectBonusResult()
			self.value.read(param1)
		elif self.variance == self.B_OBUY_SEGWAY:
			self.value = PResBuySegway()
			self.value.read(param1)
		elif self.variance == self.B_OFEED_PET:
			self.value = PFarmAction()
			self.value.read(param1)
		elif self.variance == self.B_OEXPANSION:
			self.value = PResExpansion()
			self.value.read(param1)
		elif self.variance == self.B_OEXEC_STUDY:
			self.value = PMyStudy()
			self.value.read(param1)
		elif self.variance == self.B_OBUILDINGS_LEVEL_UP:
			self.value = str_i()
			self.value.read(param1)
		elif self.variance == self.B_OFERTILIZE:
			self.value = {}
			for i in xrange(param1.readUnsignedShort()):
				self.value[i] = PResUseFertilizer()
				self.value[i].read(param1)
		elif self.variance == self.B_ORESOURCE:
			self.value = PCost()
			self.value.read(param1)
		else:
			raise Exception ("ERROR: incorrect PResBuyOffer data")

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.B_OWAREHOUSE:
			self.value.write(param1)
		elif self.variance == self.B_OFINISH_BUILDING:
			self.value.write(param1)
		elif self.variance == self.B_OBUILDING_BONUS_COLLECTEDAOF:
			self.value.write(param1)
		elif self.variance == self.B_OBUY_SEGWAY:
			self.value.write(param1)
		elif self.variance == self.B_OFEED_PET:
			self.value.write(param1)
		elif self.variance == self.B_OEXPANSION:
			self.value.write(param1)
		elif self.variance == self.B_OEXEC_STUDY:
			self.value.write(param1)
		elif self.variance == self.B_OBUILDINGS_LEVEL_UP:
			self.value.write(param1)
		elif self.variance == self.B_OFERTILIZE:
			if self.value == {}:
				param1.writeShort(0)
			else:
				param1.writeShort(len(self.value))
				for i in xrange(len(self.value)):
					self.value[i].write(param1)
		elif self.variance == self.B_ORESOURCE:
			self.value.write(param1)
		else:
			raise Exception ("ERROR: incorrect PResBuyOffer data")

class POfferPack(object):
	__slots__ = ['op_kind', 'op_price', 'op_scurrency_price', 'op_sale', 'op_label']
	def __init__(self, param1 = None, param2 = None, param3 = 0, param4 = 0, param5 = 0):
		self.op_kind = param1
		self.op_price = param2
		self.op_scurrency_price = param3
		self.op_sale = param4
		self.op_label = param5

	def read(self, param1):
		self.op_kind = POfferItemKind()
		self.op_kind.read(param1)
		self.op_price = PCost()
		self.op_price.read(param1)
		if param1.readUnsignedByte() == 1:
			self.op_scurrency_price = param1.readDouble()
		else:
			self.op_scurrency_price = None
		if param1.readUnsignedByte() == 1:
			self.op_sale = param1.readInt()
		else:
			self.op_sale = None
		if param1.readUnsignedByte() == 1:
			self.op_label = param1.readUTF()
		else:
			self.op_label = None

	def write(self, param1):
		self.op_kind.write(param1)
		self.op_price.write(param1)
		if self.op_scurrency_price != None:
			param1.writeByte(1)
			param1.writeDouble(self.op_scurrency_price)
		else:
			param1.writeByte(0)
		if self.op_sale != None:
			param1.writeByte(1)
			param1.writeInt(self.op_sale)
		else:
			param1.writeByte(0)
		if self.op_label != None:
			param1.writeByte(1)
			param1.writeUTF(self.op_label)
		else:
			param1.writeByte(0)

class oPOffer_PCost_ot_uint_a_PResBuyOffer_a_a_PQuestPrize_a_t(object):
	__slots__ = ['field_0', 'field_1', 'field_2']
	def __init__(self, param1 = None, param2 = None, param3 = None):
		self.field_0 = param1
		self.field_1 = param2
		self.field_2 = param3

	def read(self, param1):
		if param1.readUnsignedByte() == 1:
			self.field_0 = POffer()
			self.field_0.read(param1)
		else:
			self.field_0 = None
		self.field_1 = PCost()
		self.field_1.read(param1)
		if param1.readUnsignedByte() == 1:
			self.field_2 = uint_a_PResBuyOffer_a_a_PQuestPrize_a()
			self.field_2.read(param1)
		else:
			self.field_2 = None

	def write(self, param1):
		if self.field_0 != None:
			param1.writeByte(1)
			self.field_0.write(param1)
		else:
			param1.writeByte(0)
		self.field_1.write(param1)
		if self.field_2 != None:
			param1.writeByte(1)
			self.field_2.write(param1)
		else:
			param1.writeByte(0)


class uint_a_PResBuyOffer_a_a_PQuestPrize_a(object):
	__slots__ = ['field_0', 'field_1', 'field_2']
	def __init__(self, param1 = None, param2 = None, param3 = None):
		self.field_0 = param1
		self.field_1 = param2
		self.field_2 = param3

	def read(self, param1):
		self.field_0 = param1.readUnsignedInt()
		self.field_1 = {}
		for i in xrange(param1.readUnsignedByte()):
			self.field_1[i] = PResBuyOffer()
			self.field_1[i].read(param1)
		self.field_2 = {}
		for i in xrange(param1.readUnsignedShort()):
			self.field_2[i] = PQuestPrize()
			self.field_2[i].read(param1)

	def write(self, param1):
		param1.writeInt(self.field_0)
		if self.field_1 == {}:
			param1.writeByte(0)
		else:
			param1.writeByte(len(self.field_1))
			for i in xrange(len(self.field_1)):
				self.field_1[i].write(param1)
		if self.field_2 == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.field_2))
			for i in xrange(len(self.field_2)):
				self.field_2[i].write(param1)

class PDictClanCoffer(object):
	__slots__ = ['clan_coffer_event_id', 'clan_coffer_kind', 'clan_coffer_target', 'clan_coffer_period', 'clan_coffer_prize']
	def __init__(self, param1 = 0, param2 = "", param3 = None, param4 = 0, param5 = None):
		self.clan_coffer_event_id = param1
		self.clan_coffer_kind = param2
		self.clan_coffer_target = param3
		self.clan_coffer_period = param4
		self.clan_coffer_prize = param5

	def read(self, param1):
		self.clan_coffer_event_id = param1.readInt()
		self.clan_coffer_kind = param1.readUTF()
		self.clan_coffer_target = PClanTargetLevelGe()
		self.clan_coffer_target.read(param1)
		self.clan_coffer_period = param1.readDouble()
		self.clan_coffer_prize = {}
		for i in xrange(param1.readUnsignedShort()):
			self.clan_coffer_prize[i] = PQuestPrize()
			self.clan_coffer_prize[i].read(param1)

	def write(self, param1):
		param1.writeInt(self.clan_coffer_event_id)
		param1.writeUTF(self.clan_coffer_kind)
		self.clan_coffer_target.write(param1)
		param1.writeDouble(self.clan_coffer_period)
		if self.clan_coffer_prize == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.clan_coffer_prize))
			for i in xrange(len(self.clan_coffer_prize)):
				self.clan_coffer_prize[i].write(param1)

class PMagicAction(object):
	__slots__ = ['ma_building', 'ma_kind']
	def __init__(self, param1 = "", param2 = None):
		self.ma_building = param1
		self.ma_kind = param2

	def read(self, param1):
		self.ma_building = param1.readUTF()
		self.ma_kind = PMagicActionKind()
		self.ma_kind.read(param1)

	def write(self, param1):
		param1.writeUTF(self.ma_building)
		self.ma_kind.write(param1)

class PMagicActionKind(object):
	COLLECT_BONUS = 4
	ANTIGROWTH = 3
	ANTI_MONSTER = 2
	CHOP_STONES = 1
	CHOP_TREES = 0

	__slots__ = ['variance']
	def __init__ (self, param1 = 0):
		self.variance = param1

	def read(self, param1):
		self.variance = param1.readUnsignedByte()

	def write(self, param1):
		param1.writeByte(self.variance)

class PMagicAction_time(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self,param1 = None, param2 = 0):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = PMagicAction()
		self.field_0.read(param1)
		self.field_1 = param1.readDouble()

	def write(self, param1):
		self.field_0.write(param1)
		param1.writeDouble(self.field_1)

class PMagicActionInfo(object):
	__slots__ = ['mai_building', 'mai_kind', 'mai_cost', 'mai_period']
	def __init__(self, param1 = "", param2 = None, param3 = {}, param4 = 0):
		self.mai_building = param1
		self.mai_kind = param2
		self.mai_cost = param3
		self.mai_period = param4

	def read(self, param1):
		self.mai_building = param1.readUTF()
		self.mai_kind = PMagicActionKind()
		self.mai_kind.read(param1)
		self.mai_cost = {}
		for i in xrange(param1.readUnsignedShort()):
			self.mai_cost[i] = PCost()
			self.mai_cost[i].read(param1)
		if param1.readUnsignedByte() == 1:
			self.mai_period = param1.readDouble()
		else:
			self.mai_period = None

	def write(self, param1):
		param1.writeUTF(self.mai_building)
		self.mai_kind.write(param1)
		if self.mai_cost == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.mai_cost))
			for i in xrange(len(self.mai_cost)):
				self.mai_cost[i].write(param1)
		if self.mai_period != None:
			param1.writeByte(1)
			param1.writeDouble(self.mai_period)
		else:
			param1.writeByte(0)

class str_PQuestPrizes_a_str_a(object):
	__slots__ = ['field_0', 'field_1', 'field_2']
	def __init__(self, param1 = "", param2 = None, param3 = None):
		self.field_0 = param1
		self.field_1 = param2
		self.field_2 = param3

	def read(self, param1):
		self.field_0 = param1.readUTF()
		self.field_1 = PQuestPrizes()
		self.field_1.read(param1)
		self.field_2 = {}
		for i in xrange(param1.readUnsignedShort()):
			self.field_2[i] = param1.readUTF()

	def write(self, param1):
		param1.writeUTF(self.field_0)
		self.field_1.write(param1)
		if self.field_2 == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.field_2))
			for i in xrange(len(self.field_2)):
				param1.writeUTF(self.field_2[i])

class PClanCoffer(object):
	__slots__ = ['cc_kind', 'cc_target', 'cc_prize', 'cc_date_end', 'cc_open_members', 'cc_is_target_perf', 'cc_status', 'cc_members']
	def __init__(self, param1 = "", param2 = None, param3 = None, param4 = 0, param5 = None, param6 = False, param7 = None, param8 = None):
		self.cc_kind = param1
		self.cc_target = param2
		self.cc_prize = param3
		self.cc_date_end = param4
		self.cc_open_members = param5
		self.cc_is_target_perf = param6
		self.cc_status = param7
		self.cc_members = param8

	def read(self, param1):
		self.cc_kind = param1.readUTF()
		self.cc_target = PClanTargetLevelGe()
		self.cc_target.read(param1)
		self.cc_prize = {}
		for i in xrange(param1.readUnsignedShort()):
			self.cc_prize[i] = PQuestPrize()
			self.cc_prize[i].read(param1)
		if param1.readUnsignedByte() == 1:
			self.cc_date_end = param1.readDouble()
		else:
			self.cc_date_end = None
		self.cc_open_members = {}
		for i in xrange(param1.readUnsignedShort()):
			self.cc_open_members[i] = param1.readUTF()
		self.cc_is_target_perf = param1.readBoolean()
		self.cc_status = PClanCofferStatus()
		self.cc_status.read(param1)
		self.cc_members = {}
		for i in xrange(param1.readUnsignedShort()):
			self.cc_members[i] = param1.readUTF()

	def write(self, param1):
		param1.writeUTF(self.cc_kind)
		self.cc_target.write(param1)
		if self.cc_prize == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.cc_prize))
			for i in xrange(len(self.cc_prize)):
				self.cc_prize[i].write(param1)
		if self.cc_date_end != None:
			param1.writeByte(1)
			param1.writeDouble(self.cc_date_end)
		else:
			param1.writeByte(0)
		if self.cc_open_members == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.cc_open_members))
			for i in xrange(len(self.cc_open_members)):
				param1.writeUTF(self.cc_open_members[i])
		param1.writeBoolean(self.cc_is_target_perf)
		self.cc_status.write(param1)
		if self.cc_members == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.cc_members))
			for i in xrange(len(self.cc_members)):
				param1.writeUTF(self.cc_members[i])

class PClanCofferState(object):
	__slots__ = ['ccs_event_id', 'ccs_coffers']
	def __init__(self, param1 = 0, param2 = None):
		self.ccs_event_id = param1
		self.ccs_coffers = param2

	def read(self, param1):
		self.ccs_event_id = param1.readUnsignedInt()
		self.ccs_coffers = {}
		for i in xrange(param1.readUnsignedShort()):
			self.ccs_coffers[i] = PClanCoffer()
			self.ccs_coffers[i].read(param1)

	def write(self, param1):
		param1.writeInt(self.ccs_event_id)
		if self.ccs_coffers == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.ccs_coffers))
			for i in xrange(len(self.ccs_coffers)):
				self.ccs_coffers[i].write(param1)

class PClanCofferStatus(object):
	BLOCKED = 4
	DROP = 3
	STEAL = 2
	FIND = 1
	NOT_FIND = 0

	__slots__ = ['variance']
	def __init__(self, param1 = 0):
		self.variance = param1

	def read(self, param1):
		self.variance = param1.readUnsignedByte()

	def write(self, param1):
		param1.writeByte(self.variance)

class PClonerAction(object):
	HARVEST_HYBRID = 5
	HARVEST = 4
	FEED_ANIMAL = 3
	DROP_GARBAGE = 2
	DROP_STONE = 1
	DROP_WOOD = 0

	__slots__ = ['variance']
	def __init__(self, param1 = 0):
		self.variance = param1

	def read(self, param1):
		self.variance = param1.readUnsignedByte()

	def write(self, param1):
		param1.writeByte(self.variance)

class PClonerFinish(object):
	FAIL = 1
	SUCCESS = 0

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.SUCCESS:
			if param1.readUnsignedByte() == 1:
				self.value = PFarmAction()
				self.value.read(param1)
			else:
				self.value = None
		elif self.variance == self.FAIL:
			self.value = param1.readUTF()
		else:
			raise Exception ("ERROR: incorrect PClonerFinish data")

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.SUCCESS:
			if self.value != None:
				param1.writeByte(1)
				self.value.write(param1)
			else:
				param1.writeByte(0)
		elif self.variance == self.FAIL:
			param1.writeUTF(self.value)
		else:
			raise Exception ("ERROR: incorrect PClonerFinish data")

class PReqApplyCloner(object):
	__slots__ = ['ral_building_id', 'ral_priority_actions', 'ral_neightbours_count']
	def __init__(self, param1 = 0, param2 = None, param3 = 0):
		self.ral_building_id = param1
		self.ral_priority_actions = param2
		self.ral_neightbours_count = param3

	def read(self, param1):
		self.ral_building_id = param1.readUnsignedInt()
		self.ral_priority_actions = {}
		for i in xrange(param1.readUnsignedShort()):
			self.ral_priority_actions[i] = PClonerAction()
			self.ral_priority_actions[i].read(param1)
		self.ral_neightbours_count = param1.readInt()

	def write(self, param1):
		param1.writeInt(self.ral_building_id)
		if self.ral_priority_actions == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.ral_priority_actions))
			for i in xrange(len(self.ral_priority_actions)):
				self.ral_priority_actions[i].write(param1)
		param1.writeInt(self.ral_neightbours_count)

class PDirectory(object):
	__slots__ = ['dir', 'files']
	def __init__(self,param1 = "", param2 = None):
		self.dir = param1
		self.files = param2

	def read(self, param1):
		self.dir = param1.readUTF()
		self.files = {}
		for i in xrange(param1.readUnsignedShort()):
			self.files[i] = param1.readUTF()

	def write(self, param1):
		param1.writeUTF(self.dir)
		if self.files == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.files))
			for i in xrange(len(self.files)):
				param1.writeUTF(self.files[i])

class PCost_bool(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = None, param2 = False):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = PCost()
		self.field_0.read(param1)
		self.field_1 = param1.readBoolean()

	def write(self, param1):
		self.field_0.write(param1)
		param1.writeBoolean(self.field_1)

class i_PSign(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = 0, param2 = None):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = param1.readInt()
		self.field_1 = PSign()
		self.field_1.read(param1)

	def write(self, param1):
		param1.writeInt(self.field_0)
		self.field_1.write(param1)

class PClanRaidBossHp(object):
	__slots__ = ['crbh_id', 'crbh_damage', 'crbh_hp', 'crbh_kind']
	def __init__(self, param1 = "", param2 = 0, param3 = 0, param4 = ""):
		self.crbh_id = param1
		self.crbh_damage = param2
		self.crbh_hp = param3
		self.crbh_kind = param4

	def read(self, param1):
		self.crbh_id = param1.readUTF()
		self.crbh_damage = param1.readInt()
		self.crbh_hp = param1.readInt()
		self.crbh_kind = param1.readUTF()

	def write(self, param1):
		param1.writeUTF(self.crbh_id)
		param1.writeInt(self.crbh_damage)
		param1.writeInt(self.crbh_hp)
		param1.writeUTF(self.crbh_kind)

class PCrrUseCase(object):
	__slots__ = ['raid_kind', 'kind', 'from_shop', 'from_treasury']
	def __init__(self, param1 = "", param2 = "", param3 = False, param4 = False):
		self.raid_kind = param1
		self.kind = param2
		self.from_shop = param3
		self.from_treasury = param4

	def read(self, param1):
		self.raid_kind = param1.readUTF()
		self.kind = param1.readUTF()
		self.from_shop = param1.readBoolean()
		self.from_treasury = param1.readBoolean()

	def write(self, param1):
		param1.writeUTF(self.raid_kind)
		param1.writeUTF(self.kind)
		param1.writeBoolean(self.from_shop)
		param1.writeBoolean(self.from_treasury)

class PRaidMessage(object):
	__slots__ = ['rm_message', 'rm_name', 'rm_avatar']
	def __init__(self, param1 = "", param2 = "", param3 = ""):
		self.rm_message = param1
		self.rm_name = param2
		self.rm_avatar = param3

	def read(self, param1):
		self.rm_message = param1.readUTF()
		self.rm_name = param1.readUTF()
		self.rm_avatar = param1.readUTF()

	def write(self, param1):
		param1.writeUTF(self.rm_message)
		param1.writeUTF(self.rm_name)
		param1.writeUTF(self.rm_avatar)

class uint_PResAttackMonster(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = 0, param2 = None):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = param1.readUnsignedInt()
		self.field_1 = PResAttackMonster()
		self.field_1.read(param1)

	def write(self, param1):
		param1.writeInt(self.field_0)
		self.field_1.write(param1)

class i_a_t_PClanTargetLevelGe_str_t_a(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = 0, param2 = None):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = param1.readInt()
		self.field_1 = {}
		for i in xrange(param1.readUnsignedShort()):
			self.field_1[i] = PClanTargetLevelGe_str()
			self.field_1[i].read(param1)

	def write(self, param1):
		param1.writeInt(self.field_0)
		if self.field_1 == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.field_1))
			for i in xrange(len(self.field_1)):
				self.field_1[i].write(param1)


class PResFinishRaid(object):
	__slots__ = ['leader', 'prize', 'sign', 'raid_kind', 'finish_time', 'raids_stats_on_start', 'raids_stats_on_finish']
	def __init__(self, param1 = False, param2 = None, param3 = None, param4 = "", param5 = 0, param6 = None, param7 = None):
		self.leader = param1
		self.prize = param2
		self.sign = param3
		self.raid_kind = param4
		self.finish_time = param5
		self.raids_stats_on_start = param6
		self.raids_stats_on_finish = param7

	def read(self, param1):
		self.leader = param1.readBoolean()
		self.prize = PRaidResult()
		self.prize.read(param1)
		if param1.readUnsignedByte() == 1:
			self.sign = PSign()
			self.sign.read(param1)
		else:
			self.sign = None
		self.raid_kind = param1.readUTF()
		self.finish_time = param1.readDouble()
		if param1.readUnsignedByte() == 1:
			self.raids_stats_on_start = PRaidsStats()
			self.raids_stats_on_start.read(param1)
		else:
			self.raids_stats_on_start = None
		if param1.readUnsignedByte() == 1:
			self.raids_stats_on_finish = PRaidsStats()
			self.raids_stats_on_finish.read(param1)
		else:
			self.raids_stats_on_finish = None

	def write(self, param1):
		param1.writeBoolean(self.leader)
		self.prize.write(param1)
		if self.sign != None:
			param1.writeByte(1)
			self.sign.write(param1)
		else:
			param1.writeByte(0)
		param1.writeUTF(self.raid_kind)
		param1.writeDouble(self.finish_time)
		if self.raids_stats_on_start != None:
			param1.writeByte(1)
			self.raids_stats_on_start.write(param1)
		else:
			param1.writeByte(0)
		if self.raids_stats_on_finish != None:
			param1.writeByte(1)
			self.raids_stats_on_finish.write(param1)
		else:
			param1.writeByte(0)

class PShopSuitcase(object):
	__slots__ = ['ss_kind', 'ss_discount', 'ss_count']
	def __init__(self, param1 = "", param2 = 0, param3 = 0):
		self.ss_kind = param1
		self.ss_discount = param2
		self.ss_count = param3

	def read(self, param1):
		self.ss_kind = param1.readUTF()
		if param1.readUnsignedByte() == 1:
			self.ss_discount = param1.readInt()
		else:
			self.ss_discount = None
		self.ss_count = param1.readInt()

	def write(self, param1):
		param1.writeUTF(self.ss_kind)
		if self.ss_discount != None:
			param1.writeByte(1)
			param1.writeInt(self.ss_discount)
		else:
			param1.writeByte(0)
		param1.writeInt(self.ss_count)

class PWarehouseReserve_bool(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = None, param2 = False):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = PWarehouseReserve()
		self.field_0.read(param1)
		self.field_1 = param1.readBoolean()

	def write(self, param1):
		self.field_0.write(param1)
		param1.writeBoolean(self.field_1)

class PVip(object):
	CREATED_ELIXIRE_ANC = 2
	CREATED_YOUTH_ELIXIR = 1
	NOTHING = 0

	__slots__ = ['variance']
	def __init__(self, param1 = 0):
		self.variance = param1

	def read(self, param1):
		self.variance = param1.readUnsignedByte()

	def write(self, param1):
		param1.writeByte(self.variance)

class PStateKind(object):
	CHILD_GROWTH = 8
	INC_FRIEND_ACTION = 7
	INC_ANIMAL_DROP = 6
	SPEEDUP_CRAFT = 5
	INC_ELIXIR = 4
	INC_LUCK = 3
	INC_GROWTH = 2
	INC_EXP = 1
	SPEEDING_UP = 0

	__slots__ = ['variance']
	def __init__(self, param1 = 0):
		self.variance = param1

	def read(self, param1):
		self.variance = param1.readUnsignedByte()

	def write(self, param1):
		param1.writeByte(self.variance)

class PStage_i(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = None, param2 = 0):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = PStage()
		self.field_0.read(param1)
		self.field_1 = param1.readInt()

	def write(self, param1):
		self.field_0.write(param1)
		param1.writeInt(self.field_1)

class i_PFairCompAwardInfo(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = 0, param2 = None):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = param1.readInt()
		self.field_1 = PFairCompAwardInfo()
		self.field_1.read(param1)

	def write(self, param1):
		param1.writeInt(self.field_0)
		self.field_1.write(param1)


class PStaticDict(object):
	__slots__ = ['stage_to_int', 'quests', 'reference', 'levels', 'studys', 'combo_info', 'raid', 'raid_rares', 'raid_energy', 'fair_building_info', 'gifts', 'pets', 'story_lines_info', 'decors_puzzle_info', 'gold_gift_case_content', 'magic_actions', 'prolong_offer', 'prolong_stimulate_pay', 'barn_grow_fertilizer', 'trades_info', 'trade_medium_prizes', 'collections_info', 'gold_price_info', 'baks_price_info', 'dc_comfort_levels', 'game_events', 'event_quests_info', 'gold_exch_baks_info', 'fair_comp_award_info', 'clan_gold_price_info', 'gold_exch_clan_gold_info', 'clan_roles_info', 'clan_bonus_info', 'clan_coffers', 'clan_energy_repair_time', 'ok_promo', 'child_levels_info', 'child_skill_levels_info', 'child_stages_info']
	def __init__(self, param1 = None, param2 = None, param3 = None, param4 = None, param5 = None, param6 = None, param7 = None, param8 = None, param9 = None, param10 = None, param11 = None, param12 = None, param13 = None, param14 = None, param15 = None, param16 = None, param17 = None, param18 = None, param19 = '', param20 = None, param21 = None, param22 = None, param23 = None, param24 = None, param25 = None, param26 = None, param27 = None, param28 = None, param29 = None, param30 = None, param31 = None, param32 = None, param33 = None, param34 = None, param35 = 0, param36 = None, param37 = None, param38 = None, param39 = None):
		self.stage_to_int = param1
		self.quests = param2
		self.reference = param3
		self.levels = param4
		self.studys = param5
		self.combo_info = param6
		self.raid = param7
		self.raid_rares = param8
		self.raid_energy = param9
		self.fair_building_info = param10
		self.gifts = param11
		self.pets = param12
		self.story_lines_info = param13
		self.decors_puzzle_info = param14
		self.gold_gift_case_content = param15
		self.magic_actions = param16
		self.prolong_offer = param17
		self.prolong_stimulate_pay = param18
		self.barn_grow_fertilizer = param19
		self.trades_info = param20
		self.trade_medium_prizes = param21
		self.collections_info = param22
		self.gold_price_info = param23
		self.baks_price_info = param24
		self.dc_comfort_levels = param25
		self.game_events = param26
		self.event_quests_info = param27
		self.gold_exch_baks_info = param28
		self.fair_comp_award_info = param29
		self.clan_gold_price_info = param30
		self.gold_exch_clan_gold_info = param31
		self.clan_roles_info = param32
		self.clan_bonus_info = param33
		self.clan_coffers = param34
		self.clan_energy_repair_time = param35
		self.ok_promo = param36
		self.child_levels_info = param37
		self.child_skill_levels_info = param38
		self.child_stages_info = param39

	def read(self, param1):
		self.stage_to_int = {}
		for i in xrange(param1.readUnsignedShort()):
			self.stage_to_int[i] = PStage_i()
			self.stage_to_int[i].read(param1)
		self.quests = {}
		for i in xrange(param1.readUnsignedShort()):
			questsStr = param1.readUTF()
			self.quests[questsStr] = PQuestInfo()
			self.quests[questsStr].read(param1)
		self.reference = PReference()
		self.reference.read(param1)
		self.levels = {}
		for i in xrange(param1.readUnsignedShort()):
			self.levels[i] = PLevelInfo()
			self.levels[i].read(param1)
		self.studys = {}
		for i in xrange(param1.readUnsignedShort()):
			self.studys[i] = PStudy()
			self.studys[i].read(param1)
		self.combo_info = {}
		for i in xrange(param1.readUnsignedShort()):
			self.combo_info[i] = PComboInfo()
			self.combo_info[i].read(param1)
		self.raid = {}
		for i in xrange(param1.readUnsignedShort()):
			self.raid[i] = PRaidInfo()
			self.raid[i].read(param1)
		self.raid_rares = {}
		for i in xrange(param1.readUnsignedShort()):
			self.raid_rares[i] = PRaidRareInfo()
			self.raid_rares[i].read(param1)
		self.raid_energy = {}
		for i in xrange(param1.readUnsignedShort()):
			self.raid_energy[i] = PRaidRareInfo()
			self.raid_energy[i].read(param1)
		self.fair_building_info = {}
		for i in xrange(param1.readUnsignedShort()):
			self.fair_building_info[i] = PFairBuildingInfo()
			self.fair_building_info[i].read(param1)
		self.gifts = {}
		for i in xrange(param1.readUnsignedShort()):
			self.gifts[i] = PSimpleGift()
			self.gifts[i].read(param1)
		self.pets = {}
		for i in xrange(param1.readUnsignedByte()):
			self.pets[i] = PPetInfo()
			self.pets[i].read(param1)
		self.story_lines_info = {}
		for i in xrange(param1.readUnsignedShort()):
			self.story_lines_info[i] = PStoryLineInfo()
			self.story_lines_info[i].read(param1)
		self.decors_puzzle_info = {}
		for i in xrange(param1.readUnsignedShort()):
			self.decors_puzzle_info[i] = PDecorPuzzleInfo()
			self.decors_puzzle_info[i].read(param1)
		self.gold_gift_case_content = {}
		for i in xrange(param1.readUnsignedShort()):
			self.gold_gift_case_content[i] = PWarehouseReserve()
			self.gold_gift_case_content[i].read(param1)
		self.magic_actions = {}
		for i in xrange(param1.readUnsignedShort()):
			self.magic_actions[i] = PMagicActionInfo()
			self.magic_actions[i].read(param1)
		self.prolong_offer = {}
		for i in xrange(param1.readUnsignedShort()):
			self.prolong_offer[i] = PProlongOffer()
			self.prolong_offer[i].read(param1)
		self.prolong_stimulate_pay = {}
		for i in xrange(param1.readUnsignedByte()):
			self.prolong_stimulate_pay[i] = ub_ub()
			self.prolong_stimulate_pay[i].read(param1)
		self.barn_grow_fertilizer = param1.readUTF()
		self.trades_info = {}
		for i in xrange(param1.readUnsignedShort()):
			self.trades_info[i] = PTradeInfo()
			self.trades_info[i].read(param1)
		self.trade_medium_prizes = {}
		for i in xrange(param1.readUnsignedShort()):
			self.trade_medium_prizes[i] = PTradeMediumPrize()
			self.trade_medium_prizes[i].read(param1)
		self.collections_info = {}
		for i in xrange(param1.readUnsignedShort()):
			self.collections_info[i] = PCollection()
			self.collections_info[i].read(param1)
		self.gold_price_info = {}
		for i in xrange(param1.readUnsignedShort()):
			self.gold_price_info[i] = PMoneyPriceInfo()
			self.gold_price_info[i].read(param1)
		self.baks_price_info = {}
		for i in xrange(param1.readUnsignedShort()):
			self.baks_price_info[i] = PMoneyPriceInfo()
			self.baks_price_info[i].read(param1)
		self.dc_comfort_levels = {}
		for i in xrange(param1.readUnsignedShort()):
			self.dc_comfort_levels[i] = PDcComfortLevel()
			self.dc_comfort_levels[i].read(param1)
		self.game_events = {}
		for i in xrange(param1.readUnsignedShort()):
			self.game_events[i] = PGameEvent()
			self.game_events[i].read(param1)
		self.event_quests_info = {}
		for i in xrange(param1.readUnsignedShort()):
			self.event_quests_info[i] = PEventQuestInfo()
			self.event_quests_info[i].read(param1)
		if param1.readUnsignedByte() == 1:
			self.gold_exch_baks_info = {}
			for i in xrange(param1.readUnsignedShort()):
				self.gold_exch_baks_info[i] = PGameCurrencyExch()
				self.gold_exch_baks_info[i].read(param1)
		else:
			self.gold_exch_baks_info = None
		self.fair_comp_award_info = {}
		for i in xrange(param1.readUnsignedShort()):
			self.fair_comp_award_info[i] = i_PFairCompAwardInfo()
			self.fair_comp_award_info[i].read(param1)
		self.clan_gold_price_info = {}
		for i in xrange(param1.readUnsignedShort()):
			self.clan_gold_price_info[i] = PMoneyPriceInfo()
			self.clan_gold_price_info[i].read(param1)
		if param1.readUnsignedByte() == 1:
			self.gold_exch_clan_gold_info = {}
			for i in xrange(param1.readUnsignedShort()):
				self.gold_exch_clan_gold_info[i] = PGameCurrencyExch()
				self.gold_exch_clan_gold_info[i].read(param1)
		else:
			self.gold_exch_clan_gold_info = None
		self.clan_roles_info = {}
		for i in xrange(param1.readUnsignedShort()):
			self.clan_roles_info[i] = PClanRoleInfo()
			self.clan_roles_info[i].read(param1)
		self.clan_bonus_info = {}
		for i in xrange(param1.readUnsignedShort()):
			self.clan_bonus_info[i] = PClanBonusInfo()
			self.clan_bonus_info[i].read(param1)
		self.clan_coffers = {}
		for i in xrange(param1.readUnsignedShort()):
			self.clan_coffers[i] = PDictClanCoffer()
			self.clan_coffers[i].read(param1)
		self.clan_energy_repair_time = param1.readUnsignedInt()
		self.ok_promo = {}
		for i in xrange(param1.readUnsignedShort()):
			self.ok_promo[i] = time_time()
			self.ok_promo[i].read(param1)
		self.child_levels_info = {}
		for i in xrange(param1.readUnsignedShort()):
			self.child_levels_info[i] = PChildLevelInfo()
			self.child_levels_info[i].read(param1)
		self.child_skill_levels_info = {}
		for i in xrange(param1.readUnsignedShort()):
			self.child_skill_levels_info[i] = PChildSkillLevelInfo()
			self.child_skill_levels_info[i].read(param1)
		self.child_stages_info = {}
		for i in xrange(param1.readUnsignedShort()):
			self.child_stages_info[i] = PChildStageInfo()
			self.child_stages_info[i].read(param1)

	def write(self, param1):
		if self.stage_to_int == None:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.stage_to_int))
			for i in xrange(len(self.stage_to_int)):
				self.stage_to_int[i].write(param1)
		raise Exception ("can't write quests, assoc writing not implemented. FIXME PLEASE!!!")

class i_PTradeSaleDescr_a_PCost_a(object):
	__slots__ = ['field_0', 'field_1', 'field_2']
	def __init__(self, param1 = 0, param2 = None, param3 = None):
		self.field_0 = param1
		self.field_1 = param2
		self.field_2 = param3

	def read(self, param1):
		self.field_0 = param1.readInt()
		self.field_1 = PTradeSaleDescr()
		self.field_1.read(param1)
		self.field_2 = {}
		for i in xrange(param1.readUnsignedShort()):
			self.field_2[i] = PCost()
			self.field_2[i].read(param1)

	def write(self, param1):
		param1.writeInt(self.field_0)
		self.field_1.write(param1)
		if self.field_2 == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.field_2))
			for i in xrange(len(self.field_2)):
				self.field_2[i].write(param1)

class PTrade(object):
	__slots__ = ['trade_event_id', 'trade_group', 'trade_cells_state', 'trade_current_quest_num', 'trade_is_finished']
	def __init__(self, param1 = 0, param2 = 0, param3 = None, param4 = 0, param5 = False):
		self.trade_event_id = param1
		self.trade_group = param2
		self.trade_cells_state = param3
		self.trade_current_quest_num = param4
		self.trade_is_finished = param5

	def read(self, param1):
		self.trade_event_id = param1.readInt()
		self.trade_group = param1.readInt()
		self.trade_cells_state = {}
		for i in xrange(param1.readUnsignedShort()):
			self.trade_cells_state[i] = PTradeCellState()
			self.trade_cells_state[i].read(param1)
		self.trade_current_quest_num = param1.readInt()
		self.trade_is_finished = param1.readBoolean()

	def write(self, param1):
		param1.writeInt(self.trade_event_id)
		param1.writeInt(self.trade_group)
		if self.trade_cells_state == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.trade_cells_state))
			for i in xrange(len(self.trade_cells_state)):
				self.trade_cells_state[i].write(param1)
		param1.writeInt(self.trade_current_quest_num)
		param1.writeBoolean(self.trade_is_finished)

class PTradeCellState(object):
	BOUGHT = 3
	TRADE_OFFER = 2
	EMPTY = 1
	UNVAILABLE = 0

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.UNVAILABLE:
			pass
		elif self.variance == self.EMPTY:
			pass
		elif self.variance == self.TRADE_OFFER:
			self.value = PRareItem()
			self.value.read(param1)
		elif self.variance == self.BOUGHT:
			self.value = PTradeSaleDescr()
			self.value.read(param1)
		else:
			raise Exception ("ERROR: incorrect PTradeCellState data")

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.UNVAILABLE:
			pass
		elif self.variance == self.EMPTY:
			pass
		elif self.variance == self.TRADE_OFFER:
			self.value.write(param1)
		elif self.variance == self.BOUGHT:
			self.value.write(param1)
		else:
			raise Exception ("ERROR: incorrect PTradeCellState data")

class PTradeGroupInfo(object):
	__slots__ = ['tgi_group', 'tgi_rare']
	def __init__(self, param1 = 0, param2 = ""):
		self.tgi_group = param1
		self.tgi_rare = param2

	def read(self, param1):
		self.tgi_group = param1.readInt()
		self.tgi_rare = param1.readUTF()

	def write(self, param1):
		param1.writeInt(self.tgi_group)
		param1.writeUTF(self.tgi_rare)


class PTradeInfo(object):
	__slots__ = ['ti_kind', 'ti_event_id', 'ti_price_rare', 'ti_price_exp', 'ti_exchange_tax', 'ti_groups', 'ti_quest_prefix', 'ti_quest_num', 'ti_dice_price']
	def __init__(self, param1 = "", param2 = 0, param3 = None, param4 = None, param5 = 0, param6 = None, param7 = "", param8 = 0, param9 = None):
		self.ti_kind = param1
		self.ti_event_id = param2
		self.ti_price_rare = param3
		self.ti_price_exp = param4
		self.ti_exchange_tax = param5
		self.ti_groups = param6
		self.ti_quest_prefix = param7
		self.ti_quest_num = param8
		self.ti_dice_price = param9

	def read(self, param1):
		self.ti_kind = param1.readUTF()
		self.ti_event_id = param1.readInt()
		self.ti_price_rare = PCost()
		self.ti_price_rare.read(param1)
		self.ti_price_exp = PCost()
		self.ti_price_exp.read(param1)
		self.ti_exchange_tax = param1.readInt()
		self.ti_groups = {}
		for i in xrange(param1.readUnsignedShort()):
			self.ti_groups[i] = PTradeGroupInfo()
			self.ti_groups[i].read(param1)
		self.ti_quest_prefix = param1.readUTF()
		self.ti_quest_num = param1.readInt()
		self.ti_dice_price = PCost()
		self.ti_dice_price.read(param1)

	def write(self, param1):
		param1.writeUTF(self.ti_kind)
		param1.writeInt(self.ti_event_id)
		self.ti_price_rare.write(param1)
		self.ti_price_exp.write(param1)
		param1.writeInt(self.ti_exchange_tax)
		if self.ti_groups == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.ti_groups))
			for i in xrange(len(self.ti_groups)):
				self.ti_groups[i].write(param1)
		param1.writeUTF(self.ti_quest_prefix)
		param1.writeInt(self.ti_quest_num)
		self.ti_dice_price.write(param1)

class PTradeSaleDescr(object):
	__slots__ = ['tbd_user_id', 'tbd_tm', 'tbd_offer']
	def __init__(self, param1 = "", param2 = 0, param3 = None):
		self.tbd_user_id = param1
		self.tbd_tm = param2
		self.tbd_offer = param3

	def read(self, param1):
		self.tbd_user_id = param1.readUTF()
		self.tbd_tm = param1.readDouble()
		self.tbd_offer = PRareItem()
		self.tbd_offer.read(param1)

	def write(self, param1):
		param1.writeUTF(self.tbd_user_id)
		param1.writeDouble(self.tbd_tm)
		self.tbd_offer.write(param1)

class PRaidsStats(object):
	__slots__ = ['field_0_0', 'field_0_1', 'field_1_0', 'field_1_1']
	def __init__(self, param1 = "", param2 = 0, param3 = "", param4 = 0):
		self.field_0_0 = param1
		self.field_0_1 = param2
		self.field_1_0 = param3
		self.field_1_1 = param4

	def read(self, param1):
		self.field_0_0 = param1.readUTF()
		self.field_0_1 = param1.readInt()
		self.field_1_0 = param1.readUTF()
		self.field_1_1 = param1.readInt()

	def write(self, param1):
		param1.writeUTF(self.field_0_0)
		param1.writeInt(self.field_0_1)
		param1.writeUTF(self.field_1_0)
		param1.writeInt(self.field_1_1)

class PClanBoothRecipe(object):
	__slots__ = ['cbr_ingredients', 'cbr_result', 'cbr_hash']
	def __init__(self, param1 = None, param2 = None, param3 = 0):
		self.cbr_ingredients = param1
		self.cbr_result = param2
		self.cbr_hash = param3

	def read(self, param1):
		self.cbr_ingredients = PQuestPrizes()
		self.cbr_ingredients.read(param1)
		self.cbr_result = PQuestPrizes()
		self.cbr_result.read(param1)
		self.cbr_hash = param1.readUnsignedInt()

	def write(self, param1):
		self.cbr_ingredients.write(param1)
		self.cbr_result.write(param1)
		param1.writeInt(self.cbr_hash)


class PClanBoothRecipeState(object):
	RECIPE = 2
	RECIPE_COOLDOWN = 1
	NO_RECIPE = 0

	__slots__ = ['variance', 'value']
	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.NO_RECIPE:
			pass
		elif self.variance == self.RECIPE_COOLDOWN:
			self.value = param1.readDouble()
		elif self.variance == self.RECIPE:
			self.value = PClanBoothRecipe()
			self.value.read(param1)
		else:
			raise Exception ("ERROR: incorrect PClanBoothRecipeState data")

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.NO_RECIPE:
			pass
		elif self.variance == self.RECIPE_COOLDOWN:
			param1.writeDouble(self.value)
		elif self.variance == self.RECIPE:
			self.value.write(param1)
		else:
			raise Exception ("ERROR: incorrect PClanBoothRecipeState data")

class PResGetBoothRecipe(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = None, param2 = None):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = PClanBoothRecipeState()
		self.field_0.read(param1)
		self.field_1 = PClanCoffer()
		self.field_1.read(param1)

	def write(self, param1):
		self.field_0.write(param1)
		self.field_1.write(param1)

class PTradeMediumPrize(object):
	def __init__(self, param1=0, param2=0, param3 = {}):
		self.tmp_event_id = param1
		self.tmp_quest_num = param2
		self.tmp_prizes = param3

	def read(self, param1):
		self.tmp_event_id = param1.readInt()
		self.tmp_quest_num = param1.readInt()
		self.tmp_prizes = {}
		for i in xrange(param1.readUnsignedShort()):
			self.tmp_prizes[i] = PQuestPrize()
			self.tmp_prizes[i].read(param1)

	def write(self, param1):
		param1.writeInt(self.tmp_event_id)
		param1.writeInt(self.tmp_quest_num)
		if self.tmp_prizes == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.tmp_prizes))
			for i in xrange(len(self.tmp_prizes)):
				self.tmp_prizes[i].write(param1)

class POfferKind(object):
	VILLAGE = 2
	CHILD = 1
	COMMON = 0
	__slots__ = ['variance']
	def __init__(self, param1 = 0):
		self.variance = param1

	def read(self, param1):
		self.variance = param1.readUnsignedByte()

	def write(self, param1):
		param1.writeByte(self.variance)

class POfferItem(object):
	__slots__ = ['oi_id','oi_pack','oi_price','oi_scurrency']
	def __init__(self, param1 = 0, param2 = None, param3 = None, param4 = 0):
		self.oi_id = param1
		self.oi_pack = param2
		self.oi_price = param3
		self.oi_scurrency = param4

	def read(self, param1):
		self.oi_id = param1.readInt()
		self.oi_pack = {}
		for i in xrange(param1.readUnsignedShort()):
			self.oi_pack[i] = POfferPack()
			self.oi_pack[i].read(param1)
		self.oi_price = PCost()
		self.oi_price.read(param1)
		if param1.readUnsignedByte() == 1:
			self.oi_scurrency = param1.readDouble()
		else:
			self.oi_scurrency = None

	def write(self, param1):
		param1.writeInt(self.oi_id)
		if self.oi_pack == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.oi_pack))
			for i in xrange(len(self.oi_pack)):
				self.oi_pack[i].write(param1)
		self.oi_price.write(param1)
		if self.oi_scurrency != None:
			param1.writeByte(1)
			param1.writeDouble(self.oi_scurrency)
		else:
			param1.writeByte(0)

class PAchievement(object):
	__slots__ = ['achv_kind','achv_level','achv_prev']	
	def __init__(self,param1 = "", param2 = 0, param3 = {}):
		self.achv_kind = param1
		self.achv_level = param2
		self.achv_prev = param3

	def read(self, param1):
		self.achv_kind = param1.readUTF()
		self.achv_level = param1.readUnsignedByte()
		self.achv_prev = {}
		for i in xrange(param1.readUnsignedShort()):
			self.achv_prev[i] = param1.readUTF()

	def write(self, param1):
		param1.writeUTF(self.achv_kind)
		param1.writeByte(self.achv_level)
		if self.achv_prev == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.achv_prev))
			for i in xrange(len(self.achv_prev)):
				param1.writeUTF(self.achv_prev[i])

class PChild(object):
	__slots__ = ['child_name','child_sex','child_stage','child_level','child_exp','child_need','child_skills','child_skill_cooldown','child_rename_price']		
	def __init__(self, param1 = "", param2 = "", param3 = 0, param4 = 0, param5 = 0, param6 = None, param7 = None, param8 = 0, param9 = None):
		self.child_name = param1
		self.child_sex = param2
		self.child_stage = param3
		self.child_level = param4
		self.child_exp = param5
		self.child_need = param6
		self.child_skills = param7
		self.child_skill_cooldown = param8
		self.child_rename_price = param9

	def read(self, param1):
		self.child_name = param1.readUTF()
		self.child_sex = param1.readUTF()
		self.child_stage = param1.readInt()
		self.child_level = param1.readInt()
		self.child_exp = param1.readInt()
		if param1.readUnsignedByte() == 1:
			self.child_need = PChildNeed()
			self.child_need.read(param1)
		else:
			self.child_need = None
		self.child_skills = {}
		for i in xrange(param1.readUnsignedShort()):
			self.child_skills[i] = PChildSkill()
			self.child_skills[i].read(param1)
		self.child_skill_cooldown = param1.readDouble()
		if param1.readUnsignedByte() == 1:
			self.child_rename_price = PCost()
			self.child_rename_price.read(param1)
		else:
			self.child_rename_price = None

	def write(self, param1):
		param1.writeUTF(self.child_name)
		param1.writeUTF(self.child_sex)
		param1.writeInt(self.child_stage)
		param1.writeInt(self.child_level)
		param1.writeInt(self.child_exp)
		if self.child_need != None:
			param1.writeByte(1)
			self.child_need.write(param1)
		else:
			param1.writeByte(0)
		if self.child_skills == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.child_skills))
			for i in xrange(len(self.child_skills)):
				self.child_skills[i].write(param1)
		param1.writeDouble(self.child_skill_cooldown)
		if self.child_rename_price != None:
			param1.writeByte(1)
			self.child_rename_price.write(param1)
		else:
			param1.writeByte(0)

class PChildLevelInfo(object):
	__slots__ = ['cli_level','cli_required_to_level']	
	def __init__(self, param1 = 0, param2 = 0):
		self.cli_level = param1
		self.cli_required_to_level = param2

	def read(self, param1):
		self.cli_level = param1.readInt()
		self.cli_required_to_level = param1.readInt()

	def write(self, param1):
		param1.writeInt(self.cli_level)
		param1.writeInt(self.cli_required_to_level)

class PChildStageInfo(object):
	__slots__ = ['csi_stage','csi_level','csi_paywall_price']	
	def __init__(self, param1 = 0, param2 = 0, param3 = None):
		self.csi_stage = param1
		self.csi_level = param2
		self.csi_paywall_price = param3

	def read(self, param1):
		self.csi_stage = param1.readInt()
		self.csi_level = param1.readInt()
		self.csi_paywall_price = {}
		for i in xrange(param1.readUnsignedShort()):
			self.csi_paywall_price[i] = PWarehouseReserve()
			self.csi_paywall_price[i].read(param1)

	def write(self, param1):
		param1.writeInt(self.csi_stage)
		param1.writeInt(self.csi_level)
		if self.csi_paywall_price == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.csi_paywall_price))
			for i in xrange(len(self.csi_paywall_price)):
				self.csi_paywall_price[i].write(param1)

class PChildNeed(object):
	__slots__ = ['cn_current','cn_next','cn_cooldown']
	def __init__(self, param1 = None, param2 = None, param3 = 0):
		self.cn_current = param1
		self.cn_next = param2
		self.cn_cooldown = param3

	def read(self, param1):
		self.cn_current = PChildNeedDesc()
		self.cn_current.read(param1)
		self.cn_next = PChildNeedDesc()
		self.cn_next.read(param1)
		if param1.readUnsignedByte() == 1:
			self.cn_cooldown = param1.readDouble()
		else:
			self.cn_cooldown = None

	def write(self, param1):
		self.cn_current.write(param1)
		self.cn_next.write(param1)
		if self.cn_cooldown != None:
			param1.writeByte(1)
			param1.writeDouble(self.cn_cooldown)
		else:
			param1.writeByte(0)

class PChildNeedDesc(object):
	__slots__ = ['cnd_kind','cnd_price','cnd_exp']
	def __init__(self, param1 = None, param2 = None, param3 = 0):
		self.cnd_kind = param1
		self.cnd_price = param2
		self.cnd_exp = param3

	def read(self, param1):
		self.cnd_kind = PChildNeedKind()
		self.cnd_kind.read(param1)
		self.cnd_price = PWarehouseReserve()
		self.cnd_price.read(param1)
		self.cnd_exp = param1.readInt()

	def write(self, param1):
		self.cnd_kind.write(param1)
		self.cnd_price.write(param1)
		param1.writeInt(self.cnd_exp)

class PChildNeedKind(object):
	WALK = 3
	CURE = 2
	WASH = 1
	EAT = 0
	__slots__ = ['variance']
	def __init__(self, param1 = 0):
		self.variance = param1

	def read(self, param1):
		self.variance = param1.readUnsignedByte()

	def write(self, param1):
		param1.writeByte(self.variance)

class PChildSkill(object):
	__slots__ = ['cs_kind','cs_level','cs_exp']
	def __init__(self, param1 = None, param2 = 0, param3 = 0):
		self.cs_kind = param1
		self.cs_level = param2
		self.cs_exp = param3

	def read(self, param1):
		self.cs_kind = PChildSkillKind()
		self.cs_kind.read(param1)
		self.cs_level = param1.readInt()
		self.cs_exp = param1.readInt()

	def write(self, param1):
		self.cs_kind.write(param1)
		param1.writeInt(self.cs_level)
		param1.writeInt(self.cs_exp)

class PChildSkillKind(object):
	KNOWLEDGE = 3
	LOGIC = 2
	MUSIC = 1
	SPORT = 0
	__slots__ = ['variance']
	def __init__(self, param1 = 0):
		self.variance = param1

	def read(self, param1):
		self.variance = param1.readUnsignedByte()

	def write(self, param1):
		param1.writeByte(self.variance)

class PChildSkillLevelInfo(object):
	__slots__ = ['csli_skill','csli_level','csli_required_to_level']
	def __init__(self, param1 = None, param2 = 0, param3 = 0):
		self.csli_skill = param1
		self.csli_level = param2
		self.csli_required_to_level = param3

	def read(self, param1):
		self.csli_skill = PChildSkillKind()
		self.csli_skill.read(param1)
		self.csli_level = param1.readInt()
		self.csli_required_to_level = param1.readInt()

	def write(self, param1):
		self.csli_skill.write(param1)
		param1.writeInt(self.csli_level)
		param1.writeInt(self.csli_required_to_level)

class PSrvEnergy(object):
	__slots__ = ['se_energy','se_energy_next_time','se_tm']
	def __init__(self, param1 = 0, param2 = 0, param3 = 0):
		self.se_energy = param1
		self.se_energy_next_time = param2
		self.se_tm = param3

	def read(self, param1):
		self.se_energy = param1.readUnsignedInt()
		if param1.readUnsignedByte() == 1:
			self.se_energy_next_time = param1.readDouble()
		else:
			self.se_energy_next_time = None
		self.se_tm = param1.readDouble()

	def write(self, param1):
		param1.writeInt(self.se_energy)
		if self.se_energy_next_time != None:
			param1.writeByte(1)
			param1.writeDouble(self.se_energy_next_time)
		else:
			param1.writeByte(0)
		param1.writeDouble(self.se_tm)

class uint_a_PObj_a(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = 0, param2 = None):
		self.field_0 = param1 #map_id?
		self.field_1 = param2
	
	def read(self, param1):
		self.field_0 = param1.readUnsignedInt()
		self.field_1 = {}
		for i in xrange(param1.readUnsignedShort()):
			self.field_1[i] = PObj()
			self.field_1[i].read(param1)
	
	def write(self, param1):
		param1.writeInt(self.field_0)
		if self.field_1 == None:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.field_1))
			for i in xrange(len(self.field_1)):
				self.field_1[i].write(param1)

class PStrTeamBuildInfo(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = "", param2 = None):
		self.field_0 = param1
		self.field_1 = param2
	
	def read(self, param1):
		self.field_0 = param1.readUTF()
		self.field_1 = PTeamBuildInfo()
		self.field_1.read(param1)
	
	def write(self, param1):
		param1.writeUTF(self.field_0)
		self.field_1.write(param1)

class oPQuestPrize_otime(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = None, param2 = None):
		self.field_0 = param1
		self.field_1 = param2
	
	def read(self, param1):
		if param1.readUnsignedByte() == 1:
			self.field_0 = PQuestPrize()
			self.field_0.read(param1)
		else:
			self.field_0 = None
		if param1.readUnsignedByte() == 1:
			self.field_1 = param1.readDouble()
		else:
			self.field_1 = None
	
	def write(self, param1):
		if self.field_0 != None:
			param1.writeByte(1)
			self.field_0.write(param1)
		else:
			param1.writeByte(0)
		if self.field_1 != None:
			param1.writeByte(1)
			param1.writeDouble(self.field_1)
		else:
			param1.writeByte(0)

class PBuyFairBuilding_i(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = None, param2 = 0):
		self.field_0 = param1
		self.field_1 = param2
	
	def read(self, param1):
		self.field_0 = PBuyFairBuilding()
		self.field_0.read(param1)
		self.field_1 = param1.readInt()
	
	def write(self, param1):
		self.field_0.write(param1)
		param1.writeInt(self.field_1)

class PTeamBuildReqMy(object):
	__slots__ = ['tbrm_id', 'tbrm_is_decline']
	def __init__(self, param1 = "", param2 = False):
		self.tbrm_id = param1
		self.tbrm_is_decline = param2
	
	def read(self, param1):
		self.tbrm_id = param1.readUTF()
		self.tbrm_is_decline = param1.readBoolean()
	
	def write(self, param1):
		param1.writeUTF(self.tbrm_id)
		param1.writeBoolean(self.tbrm_is_decline)

class PTeamBuildInfo(object):
	__slots__ = ['tbi_score_own', 'tbi_fr_cnt', 'tbi_step','tbi_buyed_slots']
	def __init__(self, param1 = 0, param2 = 0, param3 = 0, param4 = 0):
		self.tbi_score_own = param1
		self.tbi_fr_cnt = param2
		self.tbi_step = param3
		self.tbi_buyed_slots = param4
	
	def read(self, param1):
		self.tbi_score_own = param1.readInt()
		self.tbi_fr_cnt = param1.readInt()
		self.tbi_step = param1.readInt()
		self.tbi_buyed_slots = param1.readInt()
	
	def write(self, param1):
		param1.writeInt(self.tbi_score_own)
		param1.writeInt(self.tbi_fr_cnt)
		param1.writeInt(self.tbi_step)
		param1.writeInt(self.tbi_buyed_slots)

class PTeamBuildAssistants(object):
	__slots__ = ['tba_id', 'tba_score']
	def __init__(self, param1 = "", param2 = 0):
		self.tba_id = param1
		self.tba_score = param2
	
	def read(self, param1):
		self.tba_id = param1.readUTF()
		self.tba_score = param1.readInt()
	
	def write(self, param1):
		param1.writeUTF(self.tba_id)
		param1.writeInt(self.tba_score)

class PScurrencyWishList(object):
	FAIR_BUILDING = 1
	RESERVES = 0
	__slots__ = ['variance','value']
	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.RESERVES:
			self.value = str_i()
			self.value.read(param1)
		elif self.variance == self.FAIR_BUILDING:
			self.value = PBuyFairBuilding_i()
			self.value.read(param1)
		else:
			raise Exception ("ERROR: incorrect PScurrencyWishList data")

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.RESERVES:
			self.value.write(param1)
		elif self.variance == self.FAIR_BUILDING:
			self.value.write(param1)
		else:
			raise Exception ("ERROR: incorrect PScurrencyWishList data")

class PContractExpireQuest(object):
	__slots__ = ['ceq_quest_id', 'ceq_count']
	def __init__(self, param1 = None, param2 = None):
		self.ceq_quest_id = param1
		self.ceq_count = param2
	
	def read(self, param1):
		self.ceq_quest_id = param1.readUTF()
		self.ceq_count = param1.readUnsignedInt()
	
	def write(self, param1):
		param1.writeUTF(self.ceq_quest_id)
		param1.writeInt(self.ceq_count)


class i_PContractExpireQuest(object):
	__slots__ = ['field_0', 'field_1']
	def __init__(self, param1 = None, param2 = None):
		self.field_0 = param1
		self.field_1 = param2
	
	def read(self, param1):
		self.field_0 = param1.readInt()
		self.field_1 = PContractExpireQuest()
		self.field_1.read(param1)
	
	def write(self, param1):
		param1.writeInt(self.field_0)
		self.field_1.write(param1)

class PCollectBonusResult(object):
	__slots__ = ['cbr_action', 'cbr_barn_result']
	def __init__(self, param1, param2):
		self.cbr_action = param1
		self.cbr_barn_result = param2

	def read(self, param1):
		if param1.readUnsignedByte() == 1:
			self.cbr_action = PFarmAction()
			self.cbr_action.read(param1)
		else:
			self.cbr_action = None
		if param1.readUnsignedByte() == 1:
			self.cbr_barn_result = PBarnActionResult()
			self.cbr_barn_result.read(param1)
		else:
			self.cbr_barn_result = None

	def write(self, param1):
		if self.cbr_action != None:
			param1.writeByte(1)
			self.cbr_action.write(param1)
		else:
			param1.writeByte(0)
		if self.cbr_barn_result != None:
			param1.writeByte(1)
			self.cbr_barn_result.write(param1)
		else:
			param1.writeByte(0)

class PBuyFairBuilding_i_oPSign(object):
	__slots__ = ['field_0', 'field_1', 'field_2']
	def __init__(self, param1, param2, param3):
		self.field_0 = param1
		self.field_1 = param2
		self.field_2 = param3
		
	def read(self, param1):
		self.field_0 = PBuyFairBuilding()
		self.field_0.read(param1)
		self.field_1 = param1.readInt()
		if param1.readUnsignedByte() == 1:
			self.field_2 = PSign()
			self.field_2.read(param1)
		else:
			self.field_2 = None
		
	def write(self, param1):
		self.field_0.write(param1)
		param1.writeInt(self.field_1)
		if self.field_2 != None:
			param1.writeByte(1)
			self.field_2.write(param1)
		else:
			param1.writeByte(0)

