#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import proto
import random
import time
import re
import vk_get_sid
import sys
lang = proto.lang
info = proto.get_info_obj()

proto.sid = ''

#raise Exception("PCraft changed")

try:
	with open('settings.txt','r') as settingsfile:
		settings = eval(settingsfile.read())
	proto.sid = settings["sid"]
except IOError:
	pass

newsid = raw_input('Input sid ['+proto.sid+']:')

if newsid != "":
	proto.sid = newsid

if re.search(":",proto.sid):
	l,p = proto.sid.split(":")
	proto_ver_check = proto.PROTO_VERSION
	proto.sid,proto.PROTO_VERSION,proto.swf,proto.s1,proto.s2,proto.hash_type,proto.hash_init_const = vk_get_sid.get_sid(l,p)
	if proto_ver_check != proto.PROTO_VERSION:
		print "PROTOCOL VERSION CHANGED! Server: " + str(proto.PROTO_VERSION) + ", Bot: " + str(proto_ver_check)
	print "Hash type: " + proto.hash_type
	proto.headers['Referer']=proto.swf
else:
	raise Exception ("ERROR: sid is not supported anymore, use login:password")

if proto.sid == '':
	print "We need sid to connect. Aborting!"
	sys.exit()

myfarm = proto.GetMyFarm(True,0)

print "We are "+ myfarm.me.base.name


#this should do, but need to check if their time is same as ours..
def isGameEventActive(eid, event_only = False):
	if eid == None and event_only == False:
		return True
	else:
		for i in myfarm.dict.static_dict.game_events:
			if myfarm.dict.static_dict.game_events[i].game_event_id == eid:
				if myfarm.dict.static_dict.game_events[i].game_event_date_start <= time.time() <= myfarm.dict.static_dict.game_events[i].game_event_date_finish:
					return True
	return False

# Should also handle TYPE - basic resources, buildings
def whReserveItem_isenough(kind, cnt):
	for wh in myfarm.me.warehouse:
		if myfarm.me.warehouse[wh].reserve_kind == kind and myfarm.me.warehouse[wh].count >= cnt:
			return True
	return False

def labFormulaIngridients_isenough(lab):
	for j in lab.c_base.cr_ingrs:
		if whReserveItem_isenough(lab.c_base.cr_ingrs[j].ingr_kind, lab.c_base.cr_ingrs[j].ingr_cnt) == False:
			return False
	return True

def whReserveItem_use(kind, cnt):
	for wh in myfarm.me.warehouse:
		if myfarm.me.warehouse[wh].reserve_kind == kind and myfarm.me.warehouse[wh].count >= cnt:
			myfarm.me.warehouse[wh].count = myfarm.me.warehouse[wh].count - cnt
			return True
	return False

def labFormulaIngridients_use(lab):
	if labFormulaIngridients_isenough(lab):
		for j in lab.c_base.cr_ingrs:
			if whReserveItem_use(lab.c_base.cr_ingrs[j].ingr_kind, lab.c_base.cr_ingrs[j].ingr_cnt) == False:
				raise Exception ("wtf, we just checked and had enough!")
		return True
	return False

#Figure out how to check crafts in progress
def BuildingHasFinishedFormula(building_kind):
	for i in myfarm.farm.finished_formulas:
		if myfarm.farm.finished_formulas[i].field_0 == building_kind: #'bl_research_center':
			return True, myfarm.farm.finished_formulas[i].field_0, myfarm.farm.finished_formulas[i].field_1
	return False, None, None

# We can return timer until completed and sleep for it, but that is out of scope for event crafts, which are really quick.
# Also we never update log internally like we should (res < startLabCraft), so we shouldn't rely on this if bot started craft within a session.
def BuildingHasFormulasInProgress(building_kind):
	for i in myfarm.farm.actions:
		if myfarm.farm.actions[i].log.variance == proto.model.PFarmActionLog.EXEC_FORMULA:
			print str(myfarm.farm.actions[i])
			if myfarm.farm.actions[i].log.value.field_0 == building_kind:
				return True
	return False

rng = []
print 'id', '\t', 'kind', '\t', 'name', '\t', 'cnt', '\t', 'got_ingr'
for i in myfarm.dict.laboratory:
	# как вариант c_tabs:{0: u'bl_research_center'},
	if isGameEventActive(myfarm.dict.laboratory[i].c_base.cr_game_event_id, True): #and type(myfarm.dict.laboratory[1174].c_base.cr_result[0]) == proto.model.PQuestPrize
		print i, '\t', myfarm.dict.laboratory[i].c_base.cr_result[0].value.reserve_kind, '\t', lang[myfarm.dict.laboratory[i].c_base.cr_result[0].value.reserve_kind], '\t', myfarm.dict.laboratory[i].c_base.cr_result[0].value.count, '\t', labFormulaIngridients_isenough(myfarm.dict.laboratory[i])
		rng.append(i)

i = int(raw_input('Pick formula id to craft:'))
if i in rng:
	lab = myfarm.dict.laboratory[i]

	if isGameEventActive(lab.c_base.cr_game_event_id, True):
		# Hackery, find the right way !
		# But this actually works if you ask server nicely to craft stuff you can't see in game however only bot can pick it up,
		# so we detect finished formula (fixed it in progress) and give 10 secs to cancel new craft.
		if lab.c_tabs == {}:
			lab.c_tabs = {0: u'bl_warehouse'}
		if BuildingHasFormulasInProgress(lab.c_tabs[0]) == False:
			if BuildingHasFinishedFormula(lab.c_tabs[0])[0] == True:
				finished_bool,building,formula =  BuildingHasFinishedFormula(lab.c_tabs[0])
				print "There is completed craft waiting. Getting it first!"
				res = proto.getLabCraft(building,formula)
				print "You have 10 secs to cancel new craft"
				time.sleep(10)
				print "...10 passed."

			while labFormulaIngridients_use(lab) == True:
				print "Crafting " + lang[lab.c_base.cr_result[0].value.reserve_kind]
				res = proto.startLabCraft(lab.c_tabs[0],lab.c_base.cr_kind)
				time.sleep(lab.c_exec_time+1)
				res = proto.getLabCraft(lab.c_tabs[0],lab.c_base.cr_kind)

			print "\n Done!!"
		else:
			print "Building has formula in progress. Aborting!"
	else:
		print "Formula is from inactive game event or there is completed one. Aborting!"
else:
	print "Formula id is not in list. Aborting!"
