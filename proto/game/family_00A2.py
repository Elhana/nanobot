from proto.model import *

class Packet_00A2_0A (object):
	def __init__(self, param1 = False):
		self.need_dict = param1

	def write(self, param1):
		param1.family = 0xA2
		param1.subfamily = 0x0A
		param1.writeBoolean(self.need_dict)

class Packet_00A2_10(object):
	NOT_HAVE_CLAN = 1
	INFO = 0

	def __repr__(self):
		return "{variance:%s,value:%s}" % (self.variance,self.value)
		#return "{f0:%s,f1:%s}" % (self.field_0,self.field_1)

	def __init__(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.INFO:
			self.value = PClanInfo()
			self.value.read(param1)
		elif self.variance == self.NOT_HAVE_CLAN:
			pass
		else:
			raise Exception ("ERROR: incorrect Packet_00A2_10 data")

#A2_10
class PClanInfo(object):
	def __init__(self, param1 = None, param2 = None, param3 = None, param4 = None, param5 = None, param6 = None, param7 = 0, param8 = None, param9 = None, param10 = None, param11 = None, param12 = None, param13 = None, param14 = None):
		self.clan_dict = param1
		self.clan = param2
		self.farm = param3
		self.pos = param4
		self.warehouse = param5
		self.targets = param6
		self.targets_level_completed_0 = param7
		self.targets_level_completed_1 = param8
		self.warehouse_fair_buildings = param9
		self.approved_events = param10
		self.user_clan_info = param11
		self.raids_finished = param12
		self.unlocks = param13
		self.states = param14

	def read(self, param1):
		if param1.readUnsignedByte() == 1:
			self.clan_dict = PClanDict()
			self.clan_dict.read(param1)
		else:
			self.clan_dict = None
		self.clan = PClan()
		self.clan.read(param1)
		self.farm = PFarm()
		self.farm.read(param1)
		self.pos = Position()
		self.pos.read(param1)
		self.warehouse = {}
		for i in xrange(param1.readUnsignedShort()):
			self.warehouse[i] = PWarehouseReserve()
			self.warehouse[i].read(param1)
		self.targets = {}
		for i in xrange(param1.readUnsignedShort()):
			self.targets[i] = PClanTargetCount()
			self.targets[i].read(param1)
		self.targets_level_completed_0 = param1.readUnsignedShort()
		self.targets_level_completed_1 = {}
		for i in xrange(param1.readUnsignedShort()):
			self.targets_level_completed_1[i] = PClanTargetLevelGe()
			self.targets_level_completed_1[i].read(param1)
		self.warehouse_fair_buildings = {}
		for i in xrange(param1.readUnsignedShort()):
			self.warehouse_fair_buildings[i] = str_i()
			self.warehouse_fair_buildings[i].read(param1)
		self.approved_events = {}
		for i in xrange(param1.readUnsignedShort()):
			self.approved_events[i] = i_PClanApprovedEvent()
			self.approved_events[i].read(param1)
		if param1.readUnsignedByte() == 1:
			self.user_clan_info = PUserClanInfo()
			self.user_clan_info.read(param1)
		else:
			self.user_clan_info = None
		self.raids_finished = {}
		for i in xrange(param1.readUnsignedShort()):
			self.raids_finished[i] = param1.readUTF()
		self.unlocks = {}
		for i in xrange(param1.readUnsignedShort()):
			self.unlocks[i] = param1.readUTF()
		self.states = {}
		for i in xrange(param1.readUnsignedShort()):
			self.states[i] = PState()
			self.states[i].read(param1)

	def write(self, param1):
		if self.clan_dict != None:
			param1.writeByte(1)
			self.clan_dict.write(param1)
		else:
			param1.writeByte(0)
		self.clan.write(param1)
		self.farm.write(param1)
		self.pos.write(param1)
		if self.warehouse == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.warehouse))
			for i in xrange(len(self.warehouse)):
				self.warehouse[i].write(param1)
		if self.targets == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.targets))
			for i in xrange(len(self.targets)):
				self.targets[i].write(param1)
		param1.writeShort(self.targets_level_completed_0)
		if self.targets_level_completed_1 == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.targets_level_completed_1))
			for i in xrange(len(self.targets_level_completed_1)):
				self.targets_level_completed_1[i].write(param1)
		if self.warehouse_fair_buildings == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.warehouse_fair_buildings))
			for i in xrange(len(self.warehouse_fair_buildings)):
				self.warehouse_fair_buildings[i].write(param1)
		if self.approved_events == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.approved_events))
			for i in xrange(len(self.approved_events)):
				self.approved_events[i].write(param1)
		if self.user_clan_info != None:
			param1.writeByte(1)
			self.user_clan_info.write(param1)
		else:
			param1.writeByte(0)
		if self.raids_finished == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.raids_finished))
			for i in xrange(len(self.raids_finished)):
				param1.writeUTF(self.raids_finished[i])
		if self.unlocks == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.unlocks))
			for i in xrange(len(self.unlocks)):
				param1.writeUTF(self.unlocks[i])
		if self.states == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.states))
			for i in xrange(len(self.states)):
				self.states[i].write(param1)

class i_PClanApprovedEvent(object):
	def __init__(self, param1 = 0, param2 = None):
		self.field_0 = param1
		self.field_1 = param2

	def read(self, param1):
		self.field_0 = param1.readInt()
		self.field_1 = PClanApprovedEvent()
		self.field_1.read(param1)

	def write(self, param1):
		param1.writeInt(self.field_0)
		self.field_1.write(param1)

#pylint: disable=function-redefined
class PClanDict(object):
	def __init__(self, param1 = None, param2 = None, param3 = None, param4 = None, param5 = None):
		self.shop = param1
		self.recipes_info = param2
		self.comfort_levels = param3
		self.levels_targets_info = param4
		self.sign_invite = param5

	def __repr__(self):
		return "{shop,recipes_info,comfort_levels,levels_targets_info,sign_invite}"

	def read(self, param1):
		self.shop = {}
		for i in xrange(param1.readUnsignedShort()):
			self.shop[i] = PShopObj()
			self.shop[i].read(param1)
		self.recipes_info = {}
		for i in xrange(param1.readUnsignedShort()):
			self.recipes_info[i] = PRecipeInfo()
			self.recipes_info[i].read(param1)
		self.comfort_levels = {}
		for i in xrange(param1.readUnsignedShort()):
			self.comfort_levels[i] = PDcComfortLevel()
			self.comfort_levels[i].read(param1)
		self.levels_targets_info = {}
		for i in xrange(param1.readUnsignedShort()):
			self.levels_targets_info[i] = PClanLevelTargetsInfo()
			self.levels_targets_info[i].read(param1)
		self.sign_invite = PSign()
		self.sign_invite.read(param1)

	def write(self, param1):
		if self.shop == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.shop))
			for i in xrange(len(self.shop)):
				self.shop[i].write(param1)
		if self.recipes_info == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.recipes_info))
			for i in xrange(len(self.recipes_info)):
				self.recipes_info[i].write(param1)
		if self.comfort_levels == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.comfort_levels))
			for i in xrange(len(self.comfort_levels)):
				self.comfort_levels[i].write(param1)
		if self.levels_targets_info == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.levels_targets_info))
			for i in xrange(len(self.levels_targets_info)):
				self.levels_targets_info[i].write(param1)
		self.sign_invite.write(param1)

#Requests.runClanTaskAction(this,Packet_00A2_0F.ACTION_SEEDBED,PNewSeedbed.create(this.sb.bpos,this.seedKind,this.wr == null),Packet_00A2_11.RES_CLAN_SEEDBED);
class Packet_00A2_0F(object):
	ACTION_GET_BOOTH_RECIPE_RESULT = 11
	ACTION_CREATE_BOOTH_RECIPE = 10
	ACTION_OPEN_CLAN_COFFER = 9
	ACTION_ATTACK_MONSTER = 8
	ACTION_CLAN_MAKE_RECIPE = 7
	ACTION_CLAN_PROCESS_BUILDING = 6
	ACTION_DROP_GARBAGE = 5
	ACTION_DROP_STONE = 4
	ACTION_FEED_ANIMAL = 3
	ACTION_HARVEST = 2
	ACTION_SEEDBED = 1
	ACTION_DROP_WOOD = 0

	def __init__(self, param1 = 0, param2 = None, param3 = None):
		self.action_variance = param1
		self.action_value = param2
		self.pos = param3

	def write(self, param1):
		param1.family = 0xA2
		param1.subfamily = 0x0F
		param1.writeByte(self.action_variance)
		if self.action_variance == self.ACTION_DROP_WOOD:
			param1.writeInt(self.action_value)
		elif self.action_variance == self.ACTION_SEEDBED:
			self.action_value.write(param1)
		elif self.action_variance == self.ACTION_HARVEST:
			param1.writeInt(self.action_value)
		elif self.action_variance == self.ACTION_FEED_ANIMAL:
			param1.writeInt(self.action_value)
		elif self.action_variance == self.ACTION_DROP_STONE:
			param1.writeInt(self.action_value)
		elif self.action_variance == self.ACTION_DROP_GARBAGE:
			param1.writeInt(self.action_value)
		elif self.action_variance == self.ACTION_CLAN_PROCESS_BUILDING:
			param1.writeInt(self.action_value)
		elif self.action_variance == self.ACTION_CLAN_MAKE_RECIPE:
			self.action_value.write(param1)
		elif self.action_variance == self.ACTION_ATTACK_MONSTER:
			param1.writeInt(self.action_value)
		elif self.action_variance == self.ACTION_OPEN_CLAN_COFFER:
			param1.writeUTF(self.action_value)
		elif self.action_variance == self.ACTION_CREATE_BOOTH_RECIPE:
			pass
		elif self.action_variance == self.ACTION_GET_BOOTH_RECIPE_RESULT:
			pass
		else:
			raise Exception ("ERROR: incorrect Packet_00A2_0F data")
		self.pos.write(param1)


class Packet_00A2_11(object):
	RES_CLAN_GET_BOOTH_RECIPE_RESULT = 12
	RES_CLAN_CREATE_BOOTH_RECIPE = 11
	RES_CLAN_OPEN_COFFER = 10
	RES_CLAN_EEXCEPTIONS = 9
	RES_CLAN_ATTACK_MONSTER = 8
	RES_CLAN_FEED_ANIMAL = 7
	RES_CLAN_MAKE_RECIPE = 6
	RES_CLAN_PROCESS_BUILDING = 5
	RES_CLAN_DROP_GARBAGE = 4
	RES_CLAN_DROP_STONE = 3
	RES_CLAN_HARVEST = 2
	RES_CLAN_SEEDBED = 1
	RES_CLAN_DROP_WOOD = 0

	def __init__(self, param1):
		self.res_variance = param1.readUnsignedByte()
		if self.res_variance == self.RES_CLAN_DROP_WOOD:
			self.res_value = PResDropWood()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_CLAN_SEEDBED:
			self.res_value = PResNewSeedbed()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_CLAN_HARVEST:
			if param1.readUnsignedByte() == 1:
				self.res_value = PResNewSeedbed()
				self.res_value.read(param1)
			else:
				self.res_value = None
		elif self.res_variance == self.RES_CLAN_DROP_STONE:
			self.res_value = PResDropStone()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_CLAN_DROP_GARBAGE:
			self.res_value = PResDropGarbage()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_CLAN_PROCESS_BUILDING:
			self.res_value = PBuildingState()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_CLAN_MAKE_RECIPE:
			self.res_value = PResMakeRecipe()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_CLAN_FEED_ANIMAL:
			self.res_value = PResFeedAnimal()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_CLAN_ATTACK_MONSTER:
			self.res_value = PResAttackMonster()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_CLAN_EEXCEPTIONS:
			self.res_value = PClanExceptions()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_CLAN_OPEN_COFFER:
			self.res_value = PSign()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_CLAN_CREATE_BOOTH_RECIPE:
			self.res_value = PClanBoothRecipe()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_CLAN_GET_BOOTH_RECIPE_RESULT:
			self.res_value = PResGetBoothRecipe()
			self.res_value.read(param1)
		else:
			raise Exception ("ERROR: incorrect Packet_00A2_11 data")
		self.timestamp = param1.readDouble()
		self.user_goodies = PQuestPrizes()
		self.user_goodies.read(param1)
		self.clan_goodies = PQuestPrizes()
		self.clan_goodies.read(param1)
