from distutils.core import setup
import py2exe
setup(
	name="Nanobot",
	data_files = [('', ['nanobot.ico'])],
	options = {
			"py2exe":{
			"dll_excludes": ["MSVCP90.dll"],
			"optimize": 2,
			"bundle_files": 2,
		}
	},
	#console=['nanobot-gui.py']
	windows=['nanobot-gui.py']
)