from proto.model import *

class Packet_0095_01(object):
	def __init__(self, param1 = "", param2 = ""):
		self.raid_id = param1
		self.text = param2

	def write(self, param1):
		param1.family = 0x95
		param1.subfamily = 0x01
		param1.writeUTF(self.raid_id)
		param1.writeUTF(self.text)

class Packet_0095_03(object):
	def __init__(self, param1 = ""):
		self.value = param1

	def write(self, param1):
		param1.family = 0x95
		param1.subfamily = 0x03
		param1.writeUTF(self.value)

class Packet_0095_04(object):
	def __init__(self, param1):
		self.value = PSign()
		self.value.read(param1)


class Packet_0095_05(object):
	FRIEND_RAIDS = 1
	MY_RAID = 0

	def __init__(self, param1 = 0, param2 = ""):
		self.variance = param1
		self.value = param2

	def write(self, param1):
		param1.family = 0x95
		param1.subfamily = 0x05
		param1.writeByte(self.variance)
		if self.variance == self.MY_RAID:
			param1.writeUTF(self.value)
		elif self.variance == self.FRIEND_RAIDS:
			return
		else:
			raise Exception ("ERROR: incorrect Packet_0095_05 data")

class Packet_0095_07(object):
	START_FROM_EV_ID = 1
	START_FROM_TIME = 0

	def __init__(self, param1 = None, param2 = 0, param3 = None, param4 = False):
		self.object_ids = param1
		self.start_from_variance = param2
		self.start_from_value = param3
		self.long = param4

	def write(self, param1):
		param1.family = 0x95
		param1.subfamily = 0x07
		if self.object_ids == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.object_ids))
			for i in xrange(len(self.object_ids)):
				param1.writeUTF(self.object_ids[i])
		param1.writeByte(self.start_from_variance)
		if self.start_from_variance == self.START_FROM_TIME:
			param1.writeDouble(self.start_from_value)
		elif self.start_from_variance == self.START_FROM_EV_ID:
			param1.writeDouble(self.start_from_value)
		else:
			raise Exception ("ERROR: incorrect Packet_0095_07 data")
		param1.writeBoolean(self.long)

class Packet_0095_08(object):
	def __init__(self, param1):
		self.event_id = param1.readDouble()
		self.events = {}
		for i in xrange(param1.readUnsignedShort()):
			self.events[i] = PRaidFriendEvent()
			self.events[i].read(param1)

class Packet_0095_09(object):
	def __init__(self, param1 = "", param2 = ""):
		self.raid_id = param1
		self.kick_user_id = param2

	def write(self, param1):
		param1.family = 0x95
		param1.subfamily = 0x09
		param1.writeUTF(self.raid_id)
		param1.writeUTF(self.kick_user_id)

class Packet_0095_11(object):
	def __init__(self):
		pass

	def write(self, param1):
		param1.family = 0x95
		param1.subfamily = 0x11

class Packet_0095_12(object):
	def __init__(self, param1):
		self.value = PRaidsStats()
		self.value.read(param1)

class Packet_0095_13(object):
	def __init__(self, param1 = "", param2 = False):
		self.field_0 = param1
		self.field_1 = param2

	def write(self, param1):
		param1.family = 0x95
		param1.subfamily = 0x13
		param1.writeUTF(self.field_0)
		param1.writeBoolean(self.field_1)

class Packet_0095_14(object):
	NORMAL = 1
	SHORT = 0

	def __init__(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.SHORT:
			self.value = PShortCompetition()
			self.value.read(param1)
		if self.variance == self.NORMAL:
			self.value = PCompetition()
			self.value.read(param1)
		else:
			raise Exception ("ERROR: incorrect Packet_0095_14 data")

class Packet_0095_15(object):
	def __init__(self):
		pass

	def write(self, param1):
		param1.family = 0x95
		param1.subfamily = 0x15

class Packet_0095_16(object):
	def __init__(self, param1):
		self.value = PRating()
		self.value.read(param1)
