from proto.model import PUserBase
from proto.tuples import str_bool

class PNeighbour(object):
	def __init__(self, param1=None, param2=False, param3=False, param4=0, param5=0, param6=False):
		self.user_base = param1
		self.can_send_free = param2
		self.can_send_free_box = param3
		self.fr_actions_exists = param4
		self.can_apply_gift_cnt = param5
		self.is_app = param6

	def write(self, param1):
		self.user_base.write(param1)
		param1.writeBoolean(self.can_send_free)
		param1.writeBoolean(self.can_send_free_box)
		param1.writeInt(self.fr_actions_exists)
		param1.writeInt(self.can_apply_gift_cnt)
		param1.writeBoolean(self.is_app)

	def read(self, param1):
		self.user_base = PUserBase()
		self.user_base.read(param1)
		self.can_send_free = param1.readBoolean()
		self.can_send_free_box = param1.readBoolean()
		self.fr_actions_exists = param1.readInt()
		self.can_apply_gift_cnt = param1.readInt()
		self.is_app = param1.readBoolean()


class Packet_0030_02 (object):
	def __init__(self, param1):
		self.user = PUserBase()
		self.user.read(param1)

		self.neighbours = {}
		for i in xrange(param1.readUnsignedShort()):
			self.neighbours[i] = PNeighbour()
			self.neighbours[i].read(param1)

		# ?
		self.received_request_from = {}
		for i in xrange(param1.readUnsignedShort()):
			self.received_request_from[i] = PUserBase()
			self.received_request_from[i].read(param1)

		# Invites I sent
		self.sended_invite_to = {}
		for i in xrange(param1.readUnsignedShort()):
			self.sended_invite_to[i] = PUserBase()
			self.sended_invite_to[i].read(param1)

		# Invite Neighbours
		self.not_neighbours = {}
		for i in xrange(param1.readUnsignedShort()):
			self.not_neighbours[i] = PUserBase()
			self.not_neighbours[i].read(param1)

		# ??
		self.other_friends = {}
		for i in xrange(param1.readUnsignedShort()):
			self.other_friends[i] = PUserBase()
			self.other_friends[i].read(param1)

		# ??
		self.random_friends = {}
		for i in xrange(param1.readUnsignedShort()):
			self.random_friends[i] = PUserBase()
			self.random_friends[i].read(param1)

		# professor, something else?
		self.big_friend_id = {}
		for i in xrange(param1.readUnsignedShort()):
			self.big_friend_id[i] = str_bool()
			self.big_friend_id[i].read(param1)

