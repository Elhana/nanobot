#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import proto
import random
import time
import datetime
import re
import vk_get_sid
import sys
lang = proto.lang
info = proto.get_info_obj()

#proto.sid = 'acfdfa0d-f820-456e-97ae-1c516f30f06e'

try:
	with open('settings.txt','r') as settingsfile:
		settings = eval(settingsfile.read())
	proto.sid = settings["sid"]
except IOError:
	pass

newsid = raw_input('Input login:pass ['+proto.sid+']:')

if newsid != "":
	proto.sid = newsid

if re.search(":",proto.sid):
	l,p = proto.sid.split(":")
	proto_ver_check = proto.PROTO_VERSION
	proto.sid,proto.PROTO_VERSION,proto.swf,proto.s1,proto.s2,proto.hash_type,proto.hash_init_const = vk_get_sid.get_sid(l,p)
	if proto_ver_check != proto.PROTO_VERSION:
		print "PROTOCOL VERSION CHANGED! Server: " + str(proto.PROTO_VERSION) + ", Bot: " + str(proto_ver_check)
	print "Hash type: " + proto.hash_type
else:
	print "ERROR: sid is not supported anymore, use login:password"
	sys.exit()

if proto.sid == '':
	print "We need sid to connect. Aborting!"
	sys.exit()

while True:
	myfarm = proto.GetMyFarm(True,0)
	print "We are "+ myfarm.me.base.name
	spacefarm = proto.GetSpaceFarm(False)

	if spacefarm.variance == proto.game.family_00A2.Packet_00A2_10.NOT_HAVE_CLAN:
		print "ERROR: we are not in a clan(expedition)"
		sys.exit()

	for i in spacefarm.value.farm.objects:
		if spacefarm.value.farm.objects[i].obj_kind == "bl_clan_lab":
			lab_pos = spacefarm.value.farm.objects[i].obj_pos

	print "We have %s energy." % myfarm.me.clan_info.uci_energy
	if myfarm.me.clan_info.uci_energy > 0:
		recipe_action = proto.tuples.str_uint('rp_recipe1',myfarm.me.clan_info.uci_energy)
		print "Used %s energy" % myfarm.me.clan_info.uci_energy
		proto.MyFarmActionA2(proto.game.family_00A2.Packet_00A2_0F.ACTION_CLAN_MAKE_RECIPE, recipe_action, lab_pos.clone_d(1,5))
	print "Sleeping for one hour. Ctrl-C to stop."
	time.sleep(60*55+60*10*random.random()) #one hour

