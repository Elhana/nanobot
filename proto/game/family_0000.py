from proto.tuples import str_str

class Packet_0000_00(object):
	UNATHORIZED = 4
	OLD_CLIENT = 3
	SOCIAL_ERROR = 2
	CLIENT_ERROR = 1
	INTERNAL_SERVER_ERROR = 0

	def __init__(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.INTERNAL_SERVER_ERROR:
			# Sometimes we can skip, try harder
			print "ERROR: INTERNAL_SERVER_ERROR"
		elif self.variance == self.CLIENT_ERROR:
			self.value = str_str()
			self.value.read(param1)
			if 'process combo goodies - not_enought_goodies' in self.value.field_0:
				print "ERROR: CLIENT_ERROR: %s:%s" % (self.value.field_0, self.value.field_1)
				print "We are probably lagging badly :("
			elif 'process combo goodies - not_enought_goodies' in self.value.field_1:
				#print "ERROR: CLIENT_ERROR: %s:%s" % (self.value.field_0, self.value.field_1)
				print "ERROR: CLIENT_ERROR"
				print self.value.field_0
				print self.value.field_1
				print "We are probably lagging badly :("
			else:
				raise Exception ("ERROR: CLIENT_ERROR: %s:%s" % (self.value.field_0, self.value.field_1) )
		elif self.variance == self.SOCIAL_ERROR:
			self.value = param1.readUTF()
			print "ERROR: SOCIAL_ERROR: %s" % self.value

		elif self.variance == self.OLD_CLIENT:
			#This is critical, we want to stop
			print "OLD_CLIENT: Get a new version!"
			raise Exception ("ERROR: OLD_CLIENT")

		elif self.variance == self.UNATHORIZED:
			#This is critical, we want new SID
			print "Server says our SID is not valid"
			raise Exception ("ERROR: UNATHORIZED")

		else:
			raise Exception ("ERROR: incorrect Error data")
 