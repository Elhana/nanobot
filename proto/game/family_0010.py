from proto.model import *

class Packet_0010_01(object):
	ACT_DROP_VILLAGE_BOX = 9
	ACT_PROCESS_DECOR_PUZZLE = 8
	ACT_DROP_GARBAGE = 7
	ACT_DROP_STONE = 6
	ACT_FEED_ANIMAL = 5
	ACT_PROCESS_BUILDING = 4
	ACT_ATTACK_MONSTER = 3
	ACT_DROP_WOOD = 2
	ACT_HARVEST = 1
	ACT_SEEDBED = 0

	def __init__(self, param1 = 0, param2 = None, param3 = None, param4 = 0):
		self.act_variance = param1
		self.act_value = param2
		self.pos = param3
		self.map_id = param4

	def write(self, param1):
		param1.family = 0x10
		param1.subfamily = 0x01
		param1.writeByte(self.act_variance)
		if self.act_variance == self.ACT_SEEDBED:
			self.act_value.write(param1)
		elif self.act_variance == self.ACT_HARVEST:
			param1.writeInt(self.act_value)
		elif self.act_variance == self.ACT_DROP_WOOD:
			param1.writeInt(self.act_value)
		elif self.act_variance == self.ACT_ATTACK_MONSTER:
			self.act_value.write(param1)
		elif self.act_variance == self.ACT_PROCESS_BUILDING:
			param1.writeInt(self.act_value)
		elif self.act_variance == self.ACT_FEED_ANIMAL:
			param1.writeInt(self.act_value)
		elif self.act_variance == self.ACT_DROP_STONE:
			param1.writeInt(self.act_value)
		elif self.act_variance == self.ACT_DROP_GARBAGE:
			param1.writeInt(self.act_value)
		elif self.act_variance == self.ACT_PROCESS_DECOR_PUZZLE:
			param1.writeInt(self.act_value)
		elif self.act_variance == self.ACT_DROP_VILLAGE_BOX:
			param1.writeInt(self.act_value)
		else:
			raise Exception ("ERROR: incorrect Packet_0010_01 data")
		self.pos.write(param1)
		param1.writeInt(self.map_id)

class Packet_0010_02(object):
	def __init__(self, param1):
		self.res = PResData()
		self.res.read(param1)
		self.goodies = PQuestPrizes()
		self.goodies.read(param1)
		self.energy = PSrvEnergy()
		self.energy.read(param1)
		if param1.readUnsignedByte() == 1:
			self.village_box = i_i.read(param1)
		else:
			self.village_box = None

class Packet_0010_03(object):
	def __init__(self, param1 = 0):
		self.value = param1

	def write(self, param1):
		param1.family = 0x10
		param1.subfamily = 0x03
		param1.writeInt(self.value)

class PResData(object):
	DROP_VILLAGE_BOX = 10
	HARVEST = 9
	PROCESS_DECOR_PUZZLE = 8
	DROP_GARBAGE = 7
	DROP_STONE = 6
	MONSTER_ATTACK_USER = 5
	FEED_ANIMAL = 4
	PROCESS_BUILDING = 3
	ATTACK_MONSTER = 2
	DROP_WOOD = 1
	SEEDBED = 0

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.SEEDBED:
			self.value.write(param1)
		elif self.variance == self.DROP_WOOD:
			self.value.write(param1)
		elif self.variance == self.ATTACK_MONSTER:
			self.value.write(param1)
		elif self.variance == self.PROCESS_BUILDING:
			self.value.write(param1)
		elif self.variance == self.FEED_ANIMAL:
			self.value.write(param1)
		elif self.variance == self.MONSTER_ATTACK_USER:
			param1.writeInt(self.value)
		elif self.variance == self.DROP_STONE:
			self.value.write(param1)
		elif self.variance == self.DROP_GARBAGE:
			self.value.write(param1)
		elif self.variance == self.PROCESS_DECOR_PUZZLE:
			self.value.write(param1)
		elif self.variance == self.HARVEST:
			self.value.write(param1)
		elif self.variance == self.DROP_VILLAGE_BOX:
			pass
		else:
			raise Exception ("ERROR: incorrect PResData data")

	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.SEEDBED:
			self.value = PResNewSeedbed()
			self.value.read(param1)
		elif self.variance == self.DROP_WOOD:
			self.value = PResDropWood()
			self.value.read(param1)
		elif self.variance == self.ATTACK_MONSTER:
			self.value = PResAttackMonster()
			self.value.read(param1)
		elif self.variance == self.PROCESS_BUILDING:
			self.value = PBuildingState()
			self.value.read(param1)
		elif self.variance == self.FEED_ANIMAL:
			self.value = PResFeedAnimal()
			self.value.read(param1)
		elif self.variance == self.MONSTER_ATTACK_USER:
			self.value = param1.readUnsignedInt()
		elif self.variance == self.DROP_STONE:
			self.value = PResDropStone()
			self.value.read(param1)
		elif self.variance == self.DROP_GARBAGE:
			self.value = PResDropGarbage()
			self.value.read(param1)
		elif self.variance == self.PROCESS_DECOR_PUZZLE:
			self.value = PBuildingState()
			self.value.read(param1)
		elif self.variance == self.HARVEST:
			self.value = PHarvestSeedbedResult()
			self.value.read(param1)
		elif self.variance == self.DROP_VILLAGE_BOX:
			pass
		else:
			raise Exception ("ERROR: incorrect PResData data")


