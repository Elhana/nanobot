from proto.model import *

class Packet_0050_01(object):
	def __init__ (self, param1 = None, param2 = None):
		self.friend_id = param1
		self.map_id = param2

	def write(self, param1):
		param1.family = 0x50
		param1.subfamily = 0x01
		if self.friend_id != None:
			param1.writeByte(1)
			param1.writeUTF(self.friend_id)
		else:
			param1.writeByte(0)
		if self.map_id != None:
			param1.writeByte(1)
			param1.writeByte(self.map_id)
		else:
			param1.writeByte(0)

class Packet_0050_02(object):
	def __init__ (self, param1):
		self.map_id = param1.readUnsignedByte()
		self.friends_actions_cnt = param1.readUnsignedInt()
		self.friends_farm = PFarm()
		self.friends_farm.read(param1)
		if param1.readUnsignedByte() == 1:
			self.friend = PUserBase()
			self.friend.read(param1)
		else:
			self.friend = None
		self.friend_random_cnt = param1.readInt()

	def __str__(self):
		return "{map_id:%s,\r\nfriends_actions_cnt:%s,\r\nfriends_farm:%s}" % (str(self.map_id), str(self.friends_actions_cnt), str(self.friends_farm))

class Packet_0050_03(object):
	ACTION_MAGIC_BOX_CLICK = 5
	ACTION_DROP_GARBAGE = 4
	ACTION_DROP_STONE = 3
	ACTION_FEED_ANIMAL = 2
	ACTION_HARVEST = 1
	ACTION_DROP_WOOD = 0

	def __init__(self, param1 = "", param2 = 0, param3 = None, param4 = None):
		self.friend_id = param1
		self.map_id = param2
		self.action_variance = param3
		self.action_value = param4

	def write(self, param1):
		param1.family = 0x50
		param1.subfamily = 0x03
		param1.writeUTF(self.friend_id)
		param1.writeInt(self.map_id)
		param1.writeByte(self.action_variance)
		if self.action_variance == self.ACTION_DROP_WOOD:
			param1.writeInt(self.action_value)
		elif self.action_variance == self.ACTION_HARVEST:
			param1.writeInt(self.action_value)
		elif self.action_variance == self.ACTION_FEED_ANIMAL:
			param1.writeInt(self.action_value)
		elif self.action_variance == self.ACTION_DROP_STONE:
			param1.writeInt(self.action_value)
		elif self.action_variance == self.ACTION_DROP_GARBAGE:
			param1.writeInt(self.action_value)
		elif self.action_variance == self.ACTION_MAGIC_BOX_CLICK:
			return
		else:
			raise Exception ("ERROR: incorrect Packet_0050_03")

class Packet_0050_04(object):
	def __init__(self, param1):
		self.res = PFresData()
		self.res.read(param1)

		self.goodies = {}
		for i in xrange(param1.readUnsignedByte()):
			self.goodies[i] = PQuestPrize()
			self.goodies[i].read(param1)

		if param1.readUnsignedByte() == 1:
			self.sign = PSign()
			self.sign.read(param1)
		else:
			self.sign = None

		if param1.readUnsignedByte() == 1:
			self.magic_box = param1.readUnsignedByte()
		else:
			self.magic_box = None

	def __str__(self):
		return "{res:%s,\r\ngoodies:%s,\r\nsign:%s,\r\nmagic_box:%s}" % (str(self.res), str(self.goodies), str(self.sign), str(self.magic_box))

class Packet_0050_05(object):
	def __init__(self, param1="", param2=0, param3=False, param4 = None):
		self.friend_id = param1
		self.map_id = param2
		self.is_apply = param3
		self.actions = param4

	def write(self, param1):
		param1.family = 0x50
		param1.subfamily = 0x05
		param1.writeUTF(self.friend_id)
		param1.writeInt(self.map_id)
		param1.writeBoolean(self.is_apply)
		if self.actions == {}:
			param1.writeShort(0)
		else:
			param1.writeShort(len(self.actions))
			for i in xrange(len(self.actions)):
				self.actions[i].write(param1)

class Packet_0050_06(object):
	def __init__(self, param1):
		self.res_help = PResHelp()
		self.res_help.read(param1)
		if param1.readUnsignedByte() == 1:
			self.next_help = PFriendsHelp()
			self.next_help.read(param1)
		else:
			self.next_help = None

# PFresData
class PFresData(object):
	MAGIC_BOX_CLICK = 6
	DROP_GARBAGE = 5
	DROP_STONE = 4
	SEEDBED_REPAIR = 3
	FEED_ANIMAL = 2
	HARVESTED = 1
	DROP_WOOD = 0

	def __init__(self, param1 = None, param2 = None):
		self.variance = param1
		self.value = param2

	def __repr__(self):
		return "{variance:%s,value:%s}" % (self.variance, self.value)

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.DROP_WOOD:
			self.value.write(param1)
		elif self.variance == self.HARVESTED:
			return
		elif self.variance == self.FEED_ANIMAL:
			if self.value != None:
				param1.writeByte(1)
				self.value.write(param1)
			else:
				param1.writeByte(0)
		elif self.variance == self.SEEDBED_REPAIR:
			self.value.write(param1)
		elif self.variance == self.DROP_STONE:
			self.value.write(param1)
		elif self.variance == self.DROP_GARBAGE:
			self.value.write(param1)
		elif self.variance == self.MAGIC_BOX_CLICK:
			return
		else:
			raise Exception ("ERROR: incorrect PFresData data")

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.DROP_WOOD:
			self.value = PResDropWood()
			self.value.read(param1)
		elif self.variance == self.HARVESTED:
			return
		elif self.variance == self.FEED_ANIMAL:
			if param1.readUnsignedByte() == 1:
				self.value = PResFeedAnimal()
				self.value.read(param1)
			else:
				self.value = None
		elif self.variance == self.SEEDBED_REPAIR:
			self.value = PSeedbed()
			self.value.read(param1)
		elif self.variance == self.DROP_STONE:
			self.value = PResDropStone()
			self.value.read(param1)
		elif self.variance == self.DROP_GARBAGE:
			self.value = PResDropGarbage()
			self.value.read(param1)
		elif self.variance == self.MAGIC_BOX_CLICK:
			return
		else:
			raise Exception ("ERROR: incorrect PFresData data")

class PResHelp(object):
	CANCELLED = 1
	APPLIED = 0

	def write(self, param1):
		param1.writeByte(self.variance)
		if self.variance == self.APPLIED:
			if self.value == {}:
				param1.writeShort(0)
			else:
				param1.writeShort(len(self.value))
				for i in xrange(len(self.value)):
					self.value[i].write(param1)
		elif self.variance == self.CANCELLED:
			pass
		else:
			raise Exception ("ERROR: incorrect PResHelp data")

	def __init__(self, param1 = 0, param2 = None):
		self.variance = param1
		self.value = param2

	def read(self, param1):
		self.variance = param1.readUnsignedByte()
		if self.variance == self.APPLIED:
			self.value = {}
			for i in xrange(param1.readUnsignedShort()):
				self.value[i] = PResFriendsHelp()
				self.value[i].read(param1)
		elif self.variance == self.CANCELLED:
			pass
		else:
			raise Exception ("ERROR: incorrect PResHelp data")

class PResFriendsHelp(object):
	RES_DROP_GARBAGE = 5
	RES_DROP_STONE = 4
	RES_SEEDBED_REPAIR = 3
	RES_FEED_ANIMAL = 2
	RES_HARVEST = 1
	RES_DROP_WOOD = 0

	def write(self, param1):
		param1.writeByte(self.res_variance)
		if self.res_variance == self.RES_DROP_WOOD:
			self.res_value.write(param1)
		elif self.res_variance == self.RES_HARVEST:
			self.res_value.write(param1)
		elif self.res_variance == self.RES_FEED_ANIMAL:
			self.res_value.write(param1)
		elif self.res_variance == self.RES_SEEDBED_REPAIR:
			param1.writeInt(self.res_value)
		elif self.res_variance == self.RES_DROP_STONE:
			self.res_value.write(param1)
		elif self.res_variance == self.RES_DROP_GARBAGE:
			self.res_value.write(param1)
		else:
			raise Exception ("ERROR: incorrect PResFriendsHelp data")

		if self.goodies == {}:
			param1.writeByte(0)
		else:
			param1.writeByte(len(self.goodies))
			for i in xrange(len(self.goodies)):
				self.goodies[i].write(param1)

	def __init__(self, param1 = 0, param2 = None, param3 = None):
		self.res_variance = param1
		self.res_value = param2
		self.goodies = param3

	def read(self, param1):
		self.res_variance = param1.readUnsignedByte()
		if self.res_variance == self.RES_DROP_WOOD:
			self.res_value = PResDropWood()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_HARVEST:
			self.res_value = PResFriendHarvest()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_FEED_ANIMAL:
			self.res_value = PResFeedAnimal()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_SEEDBED_REPAIR:
			self.res_value = param1.readUnsignedInt()
		elif self.res_variance == self.RES_DROP_STONE:
			self.res_value = PResDropStone()
			self.res_value.read(param1)
		elif self.res_variance == self.RES_DROP_GARBAGE:
			self.res_value = PResDropGarbage()
			self.res_value.read(param1)
		else:
			raise Exception ("ERROR: incorrect PResFriendsHelp data")

		self.goodies = {}
		for i in xrange(param1.readUnsignedByte()):
			self.goodies[i] = PQuestPrize()
			self.goodies[i].read(param1)

class PResFriendHarvest(object):
	def __init__(self, param1 = 0, param2 = None):
		self.seedbed_id = param1
		self.reserves = param2

	def write(self, param1):
		param1.writeInt(self.seedbed_id)
		self.reserves.write(param1)

	def read(self, param1):
		self.seedbed_id = param1.readUnsignedInt()
		self.reserves = PHarvestSeedbedResult()
		self.reserves.read(param1)



class PAction_removed (object):
	HARVEST_HYBRID = 5
	HARVEST = 4
	FEED_ANIMAL = 3
	DROP_GARBAGE = 2
	DROP_STONE = 1
	DROP_WOOD = 0

	def write(self, param1):
		param1.writeByte(self.variance)

	def __init__(self,param1 = 0):
		self.variance = param1

	def read(self,param1):
		self.variance = param1.readUnsignedByte()
